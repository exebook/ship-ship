fun sy_num up node {
	ret lam m {
		if m == mode.TYPE {
			ret 'var'
		}
		but m == mode.CODE {
			re := c_con(node.num+'')
			ret re
		}
	}
}

fun sy_string up node {
	ret lam m {
		if m == mode.TYPE {
			ret 'var'
		}
		but m == mode.CODE {
			re := c_string(node.str)
			ret re
		}
	}
}

fun sy_array_literal up node {
	list := _sy_each(up node.list sy_expr)
	ret lam m {
		if m == mode.TYPE {
			ret 'var'
		}
		but m == mode.CODE {
			ret c_array_literal(list(m))
		}
	}
}

fun sy_object_literal up node {
	list := []
	cache := {} // for rec

	each node.list {
		expr := sy_expr(up item.expr)
		list += { id: item.id expr }
		cache[item.id] = [item expr]
	}

	//eff ScopePass {
		//each node.list {
			//ty := up(uplink.NAME_GET item.id)
			//if ty and ty[0:4] == 'rec-' {
				//if item.expr.type == types.object_literal {
					//item.expr.ty = ty
				//}
			//}
		//}
	//}

	up(uplink.AFTER lam stage {
		if stage == stag.TYPE {
			each node.list {
				ty := up(uplink.NAME_GET item.id)
				if ty and ty[0:4] == 'rec-' {
					// record within record/object
					if item.expr.type == types.object_literal {
						item.expr.ty = ty
					}
				}
			}
		}
	})

	ret lam m {
		if m == mode.TYPE {
			if node.ty ret node.ty
			else ret 'var'
		}
		but m == mode.CODE {
			if node.ty {
				src_list := []
				check := {}
				ty := (node.ty / '-')[1..]
				each ty check[item] = 1

				// find extra fields
				each node.list {
					init check[item.id] {
						throw stop(item.pos tem(E018_2 node.rec_name, (node.ty / '-')[1..]*' '))
					}
				}

				each ty { // ty has correct order
					field := item
					x := cache[field]
					if x src_list += x[1](m)
					else src_list += c_nil()
				}

				ret c_rec_literal(up(uplink.REC_TYPE node.ty) src_list)
			}
			else {
				kv := []
				each list {
					kv += c_string(item.id)
					kv += item.expr(m)
				}
				ret c_object_literal(kv)
			}
		}
	}
}

fun sy_if_ex up node {
	expr := sy_expr(up node.expr)
	x0 := sy_expr(up node.x0)
	x1 := sy_expr(up node.x1)

	ret lam m {
		if m == mode.TYPE {
			a := x0(m)
			b := x1(m)
			if a == b ret a
			ret 'var'
		}
		but m == mode.CODE {
			ret c_ifex(expr(m) x0(m) x1(m))
		}
	}
}

fun sy_len up node {
	expr := sy_expr(up node.expr)

	ret lam m {
		if m == mode.TYPE {
			ret 'var'
		}
		but m == mode.CODE {
			if node.expr.type == types.args ret c_len_args()
			if node.expr.type == types.id {
				e := up(uplink.ENUM_GET node.expr.id)
				if e {
					ret c_con(len e.items + '')
				}
			}
			ret c_len(expr(m))
		}
	}
}

fun sy_unary up node {
	if node.op == Token['len'] ret sy_len(up node)
	but node.op == Token['the'] ret sy_the(up node)

	expr := sy_expr(up node.expr)

	ret lam m {
		if m == mode.TYPE {
			if node.op == Token['-'] {
				ret expr(m)
			}
			but node.op == Token['not'] ret 'var' // 'num'
			but node.op == Token['chr'] ret 'var' // 'str'
			but node.op == Token['ord'] ret 'var' // 'num'
		}
		but m == mode.CODE {
			if node.op == Token['-'] ret c_negation(expr(m))
			but node.op == Token['not'] ret c_not(expr(m))
			but node.op == Token['chr'] ret c_chr(expr(m))
			but node.op == Token['ord'] ret c_ord(expr(m))
			throw stop(node.pos 'unary <' + Token[node.op] + '> is not implemented')
		}
	}
}

fun sy_tee up node {
	T := node.set.target
	if len T != 1 {
		throw stop(T[1].pos 'tee with more than one argument is not implemented yet')
	}
	T = T[0]
	t := sy_expr(up T)

	s := sy_statem(up node.set)
	ret lam m {
		if m == mode.TYPE {
			ret t(m)
		}
		but m == mode.CODE {
			ret c_tee(t(m) s(m))
		}
	}
}

fun sy_log_color up node {
	ret lam m {
		if m == mode.CODE {
			ret c_color_term(node.color nil)
		}
	}
}

fun sy_the up node {
	//e := sy_value_dispatch(up node.value)
	e := sy_operand(up node.target)
	up(uplink.THE node.target)
	ret e
}

fun sy_operand up node {
	if node.type == types.number ret sy_num(up node)
	but node.type == types.id ret sy_id(up node)

	//but node.type == types.arg ret sy_arg(up node)
	but node.type == types.indexing ret sy_index(up node)
	but node.type == types.member ret sy_member(up node)
	but node.type == types.range ret sy_range(up node)
	but node.type == types.call ret sy_call(up node)

	but node.type == types.string ret sy_string(up node)
	but node.type == types.array_literal ret sy_array_literal(up node)
	but node.type == types.expr ret sy_expr(up node)
	but node.type == types.if_ex ret sy_if_ex(up node)
	but node.type == types.object_literal ret sy_object_literal(up node)
	but node.type == types.unary ret sy_unary(up node)
	but node.type == types.item ret sy_item(up node)
	but node.type == types.each_index ret sy_each_index(up node)
	but node.type == types.fun ret sy_lam(up node)
	but node.type == types.args ret sy_args(up node)
	but node.type == types.charmap ret sy_charmap(up node)
	but node.type == types['this'] ret sy_this(up node)
	but node.type == types['it'] ret sy_it(up node)
	but node.type == types['that'] ret sy_that(up node)
	but node.type == types['error'] ret sy_error(up node)
	but node.type == types['langc'] ret sy_langc_expr(up node)
	but node.type == types.tee ret sy_tee(up node)
	but node.type == types.log_color ret sy_log_color(up node)
	but node.type == types.the ret sy_the(up node)
	throw stop(node.pos tem(E003_1 types[node.type]))
}

// indent+litaral -> index call member

fun sy_binary up node f a b {
	ret lam m {
		if m == mode.CODE ret line_info(node f(a(m) b(m)))
	}
}

fun sy_operator up x a b {
	f := nil
	if x.op == Token['+'] f = c_add
	but x.op == Token['-'] f = c_sub
	but x.op == Token['*'] f = c_mul
	but x.op == Token['/'] f = c_div
	but x.op == Token['div'] f = c_idiv
	but x.op == Token['=='] f = c_eq
	but x.op == Token['mod'] f = c_mod
	but x.op == Token['!='] f = c_ne
	but x.op == Token['<'] f = c_lt
	but x.op == Token['>'] f = c_gt
	but x.op == Token['<='] f = c_le
	but x.op == Token['>='] f = c_ge
	but x.op == Token['or'] f = c_or
	but x.op == Token['and'] f = c_and
	but x.op == Token['OR'] f = c_OR
	but x.op == Token['AND'] f = c_AND
	but x.op == Token['SHR'] f = c_SHR
	but x.op == Token['SHL'] f = c_SHL
	// TODO remove this from compiler, this must be done on parser level with a simple conversion to define
	but x.op == Token['+='] f = c_add_on
	but x.op == Token['-='] f = c_sub_on
	but x.op == Token['*='] f = c_mul_on
	but x.op == Token['/='] f = c_div_on
	else throw stop(x.pos 'unknown operator')
	ret sy_binary(up x f a b)
}

fun sy_expr up node {
	re := []
	if node.type == types.expr stack := node.stack else stack = [node]
	each stack {
		if item.type == types.operator {
			b := pop re
			a := re[-1]
			re[-1] = sy_operator(up item a b)
		}
		else {
			re += sy_operand(up item)
		}
	}

	ret lam m {
		if m == mode.TYPE ret re[0](m)
		but m == mode.CODE ret re[0](m)
		//if m == mode.CODE ret line_info(node re[0](m))
	}
}

fun sy_expr_statem up node {
	use node.expr.stack
		throw stop(node.pos E017_0)
	ret sy_expr(up node.expr)
}

