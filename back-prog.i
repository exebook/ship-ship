//each 5 log item

//each 65..90 cout chr item
//type p {
        //assign: strict
//}
//struct p { x y }
//p := { 5 6 }
//print_all([p])
//fun print_all a {
//}
//number y

//b := { a: { x: 1 y: 2 z: { k: 4 } } b: 5 }
//a := { y: 20 }


/:item
	item type:
	rec a { b }
	each a log item.b
item:/

/:fun
	fun g a { log type_name(a) a } g({ x: 5 })
	 type inference
	o := { f: lam b {} }
	o.f(a)

	each time function is declared it's signature
	becomes a type for all functions of same name
	but what about lambdas?
	fun f1 a b {}
	u := f1 // ?
	o := { f: f1 } // ?
fun:/

//fun f b { log b }
//rec a { x y }
//a := { x: 1 y: 2 }
//f(2+2)


