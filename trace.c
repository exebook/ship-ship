/*
	consider https://github.com/ianlancetaylor/libbacktrace as an option
	and builtin this addr2line/gdb info line fucntionality into a compiler:
	compiler --backtrace "/path/binary,0x0000123,0x0000345"

	how to handle crashes
	https://habr.com/en/post/332626/


	real code reflection in C!
	#include <stdio.h>
	extern void* lab1; // must be declared before the __asm
	void f() {
		__asm("lab1:");
		printf("\n");
	}
	int main() {
		f();
		printf("l1: %p\n", &lab1);
		printf("f:  %p\n", &f);
		return 0;
	}
*/
void *__main_end;
void *__main_begin;

void __stack_trace_init(void *begin, void* end) {
	__main_begin = begin;
	__main_end = end;
}

#ifdef __TINYC__
int backtrace(void **buffer, int size) {
	int main();
	uint64_t **p, *bp, *frame;
	asm ("mov %%rbp, %0;" : "=r" (bp));
	p = (uint64_t**) bp;
	int i = 0;
	while (i < size) {
		frame = p[0];
		buffer[i++] = p[1];
		if ((void*)p[1] > (void*)main) return i;
		p = (uint64_t**) frame;
	}
	return i;
}
#endif

uint64_t ASLR_base() {
	__enter();
	val binary = __str(__global.argv[0]);
	val a = __concat(__str("gdb -q "), binary, __str(" -batch -ex 'info address ASLR_base'"));
	char *cmd = __pchar(a);
	val s = __str(__call_system(cmd));
	//int j = __str_find_at(s, __str("not in executable format:"), 0);
	//printf("j=%i\n", j); // TODO: update __call_system to read stderr using fork/dup2
	s = __array_get(__split(s, __str("0x")), 1);
	s = __array_get(__split(s, __str(".")), 0);
	char *x = __pchar(s);
	uint64_t X = (uintptr_t)ASLR_base - strtoul(x, 0, 16);
	__return(X);
}

val __ask_gdb_for_stack_trace(void **buffer, int size) {
	__enter();
	uint64_t base = ASLR_base();
	val binary = __str(__global.argv[0]);
	val a = __concat(__str("gdb -q "), binary, __str(" -batch -nh -nx -n "));
	__hold(&a);
	for (int i = 0; i < size; i++) {
		__loop_mark();
		char hex[200];
		sprintf(hex, "%ld", (uint64_t) buffer[i] - base - CALL_CPU_INSTRUCTION_SIZE);
		a = __concat(a, __str(" -ex 'info line *"));
		a = __concat(a, __str(hex));
		a = __concat(a, __str("'"));
		if (buffer[i] >= __main_begin && buffer[i] <= __main_end) {
			__loop_release();
			break;
		}
		__loop_release();
	}
	char *cmd = __pchar(a);
	val stack = __str(__call_system(cmd));
	stack = __split(stack, __str("\n"));
	__return(stack);
}

val __addr_to_line(val stack) {
	__enter();
	val re = __array();

	__iterate_array(i1, stack) {
		val s = i1_items[i1_index];
		char *separator = " starts at address";
		int have_data = __get_i(__find(s, __str(separator), 0));

		if (have_data >= 0) {
			s = __split(s, __str(separator));
			if (__len(s) > 0) {
				s = __split(__array_get(s, 0), __str(" of "));
				val row;
				if (__len(s) == 2) {
					val line = __array_get(s, 0);
					line = __array_get(__split(line, __str(" ")), 1);
					val fname = __array_get(s, 1);
					if (__charcode_at(fname, 0) == '"' && __charcode_at(fname, -1) == '"') {
						fname = __range(fname, 1, -2, 0);
					}
					row = __array(fname, __parse_num(line, 10) - 1);
				}
				else {
					//char addr[20];
					//sprintf(addr, "%p", buffer[i1_index]);
					//row = __str(addr);
					row = s;
				}
				__array_push(re, row);
			}
		}
		else {
			char *separator = " for address ";
			int have_data = __find(s, __str(separator), 0);
			if (have_data >= 0) {
				val line = __array_get(__split(s, __str(separator)), 1);
				val addr = __array_get(__split(line, __str(" ")), 0);
				__array_push(re, __array(__nil, addr));
			}
			else {
				//__array_push(re, __array(__nil, __let_i(0)));
			}
		}
	}
	__return(re);
}

val __get_line_source(val file_line) {
	__enter();
	if (!__is_array(file_line) || __len(file_line) != 2) {
		printf("Runtime panic: __get_line_source received unparsable data:\n   ");
		__log(file_line);
		exit(1);
	}
	char *pcwd = get_current_dir_name();
	val cwd = __add(__str(pcwd), __str("/"));
	free(pcwd);
	val line_no = __array_get(file_line, 1);
	val path = __array_get(file_line, 0);
	if (__is_nil(path)) {
		__return(__array(__str(""), line_no));
	}
	if (__find(path, cwd, 0) == 0) {
		path = __array_get(__split(path, cwd), -1);
	}
	if (__find(path, __str("./"), 0) == 0) {
		path = __array_get(__split(path, __str("./")), 1);
	}
	int e = 0;
	val src = __silent_load(path, &e);
	if (__is_nil(src)) {
		__return(__array(__concat(path, __str(":"), line_no+1), __str("...")));
	}
	else {
		src = __split(src, __str("\n"));
		val line_text = __trim(__array_get(src, line_no));
		if (__is_nil(line_text)) {
			line_text = __str("<EOF>");
		}
		__return(__array(__concat(path, __str(":"), line_no+1), line_text));
	}
}

val __format_trace(val stack) {
	fflush(0);
	__enter();
	int max_width = 0;
	val item;
	__hold(&item);
	__iterate_array(i1, stack) {
		__loop_mark();
		item = i1_items[i1_index];
		if (__is_array(item)) {
			item = __get_line_source(item);
			val tmp = __array_get(item, 0);
			leaf *ltmp = __leaf(tmp);
			int len = __len(tmp);
			if (max_width < len) {
				max_width = len;
			}
		}
		else {
			item = __array(item, __str(""));
		}
		i1_items[i1_index] = item;
		__loop_release();
	}

	val re = __array();
	int len = __len(stack);
	__iterate_array(i2, stack) {
		__loop_mark();
		int x = len - i2_index - 1;
		val item = i2_items[x];
		val path_line = __array_get(item, 0);
		val line_text = __array_get(item, 1);
		__hold(&path_line);
		while (__len(path_line) < max_width) {
			__loop_mark();
			path_line = __concat(path_line, __str(" "));
			__loop_release();
		}
		val line = __array();
		__array_push(line, __str("|"));
		__array_push(line, path_line);
		__array_push(line, __str("  "));
		__array_push(line, line_text);
		__array_push(re, __array_join(line, __str("")));
		__loop_release();
	}
	__return(re);
}

val __filter_out_runtime(val lines) {
	__enter();
	//char * runtimes[] = { "last.c",
		//"hashmap.c", "cell.c", "leaf.c", "stacks.c", "try.c", "gc.c", "bignum.c", "array.c", "object.c", "closure.c", "utf.c", "string.c", "log.c", "range.c", "val.c", "ari.c", "cmp.c", "each.c", "trace.c", "system.c", "sort.c", "shame.c", "builtins.c" };
	char * runtimes[] = { ".c" };
	int count = sizeof(runtimes) / sizeof(char*);
	val re = __array();
	for(int i = 0; i < __len(lines); i++) {
		__loop_mark();
		val line = __array_get(lines, i);
		val fname = __array_get(line, 0);
		bool is_runtime_file = false;
		if (!__is_nil(fname)) for(int j = 0; j < count; j++) {
			__loop_mark();
			int F = __find(fname, __str(runtimes[j]), 0);
			if (F == __len(fname) - 2) {
				is_runtime_file = true;
				break;
			}
			//__log(__str("find("), fname, __str(", "), __str(runtimes[j]), __str(") = "), __let(F));
			__loop_release();
		}
		else {
			is_runtime_file = true;
		}
		if (!is_runtime_file ) {
			__array_push(re, line);
		}
		__loop_release();
	}

	__return(re);
}

val __trace_int(void* crash_IP) {
	__enter();
	static int __trace_level = 0;
	if (__trace_level > 0) {
		printf("internal error: __show_trace()\n");
		exit(1);
	}
	__trace_level++;
	void *buffer[20];
	//__log(__str(COMPILER_NAME));
	int size;
	if (crash_IP) {
		buffer[0] = crash_IP;
	}
	else {
		size = backtrace(buffer, 20);
	}
	val stack = __ask_gdb_for_stack_trace(buffer, size);
	val lines = __addr_to_line(stack);
	if (__global.trace_runtimes == false) {
		lines = __filter_out_runtime(lines);
	}
	val re = __format_trace(lines);
	__trace_level--;
	__return(re);
}

val __trace() {
	return __trace_int(0);
}

void __show_trace_crash(void* crash_IP) {
	val lines = __trace_int(crash_IP);
	__log(__array_join(lines, __str("\n")));
}

void __show_trace() {
	val lines = __trace();
	__log(__array_join(lines, __str("\n")));
}
