/*

fun co_enum_tail up node enu {
	part := node.tail[0]
	else throw stop(node.tail[0].pos tem('<^> is enum, what are you trying to do?' enu.name))

	value := enu.items[id]
	init value throw stop(node.tail[0].pos tem('enum <^> does not have <^>' enu.name id))

	if len node.tail > 1 {
		throw stop(node.tail[1].pos tem('enum <^.^> is not an object' enu.name id))
	}

	ret c_con(value+'')
}
*/


fun enum_add_element enu eid {
	count := -1
	each enu.items if count < item count = item
	inc count
	enu.items[eid] = count
	ret count
}

fun sy_index up node {
	x := sy_expr(up node.ix)
	typ := nil
	enu := nil
	obj_node := node.parent
	obj_sy := sy_value_dispatch(up obj_node)

	if node.parent.type == types.args {
		arg_num := sy_expr(up node.ix)
		ret lam m {
			if m == mode.CODE ret c_arg(arg_num(m))
		}
	}

	up(uplink.AFTER lam stage {
		if stage == stag.SCOPE {
			if obj_node.type == types.id {
				enu = up(uplink.ENUM_GET obj_node.id)
				if enu typ = 1
			}
		}
	})

	ret lam m {
		if m == mode.CODE {
			if enu {
				if node.ix.type == types.string {
					eid := node.ix.str
					value := enu.items[eid]
					use value {
						ret c_con(value+'')
					}
					else {
						if enu.is_free {
							value = enum_add_element(enu eid)
							ret c_con(value+'')
						}
						throw stop(node.ix.pos tem(E008_2 enu.name eid))
					}
				}
				else {
					enu.reflect = true
					ret c_enum_reflect(enu x(m))
				}
			}
			else ret c_index_get(obj_sy(m) x(m))
		}
	}
}

fun sy_member up node {
	enu := nil
	record := nil
	rec_type := nil
	parent := node.parent
	obj_sy := sy_value_dispatch(up parent)

	up(uplink.AFTER lam stage {
		if stage == stag.SCOPE {
			if parent.type == types.id or parent.type == types.member {
				tid := parent.id
				if parent.type == types.id enu = up(uplink.ENUM_GET tid)
				init enu {
					nam := up(uplink.NAME_GET tid)
					if nam and nam[0:3] == 'rec' {
						record = (nam / '-')[1..]
					}
				}
			}
		}
	})

	ret lam m {
		if m == mode.CODE {
			if enu {
				value := enu.items[node.id]
				use value {
					ret c_con(value+'')
				}
				else {
					if enu.is_free {
						value = enum_add_element(enu node.id)
						enu.items[node.id] = value
						ret c_con(value+'')
					}
					else throw stop(node.pos tem(E008_2 enu.name node.id))
				}
			}
			if record {
				ret c_rec_prop_get(obj_sy(m) find1(record node.id))
			}
			ret c_property_get(obj_sy(m) c_string(node.id))
		}
	}
}

fun sy_property_set up node expr {
	dest := sy_value_dispatch(up node.parent)
	src := sy_expr(up expr)
	parent := node.parent
	record := nil

	up(uplink.AFTER lam stage {
		if stage == stag.SCOPE {
			if parent.type == types.id or parent.type == types.member {
				parent.type == types.id enu := up(uplink.ENUM_GET parent.id)
				if enu {
					throw stop(node.pos parent.id + ' is enum')
				}
				nam := up(uplink.NAME_GET parent.id)
				if nam and nam[0:3] == 'rec' {
					record = (nam / '-')[1..]
				}
			}
		}
	})

	ret lam m {
		if m == mode.CODE {
			if record {
				ret line_info(
					node c_rec_prop_set(dest(m) find1(record node.id) src(m))
				)
			}
			ret line_info(node c_property_set(dest(m) c_string(node.id) src(m)))
		}
	}
}

fun sy_range up node {
	ix := nil
	range := nil
	obj := sy_value_dispatch(up node.parent)

	if node.ix ix = sy_expr(up node.ix)
	if node.range range = sy_expr(up node.range)

	ret lam m {
		if m == mode.CODE {
			b := 0
			if node.delim == Token[':'] b = 1

			o := obj(m)
			to := nil
			from := nil
			if range to = range(m) else to = c_len(o)
			if ix from = ix(m) else from = c_con("0")

			ret c_range(o from to b)
		}
	}
}

fun code_call_id up node id_sy id_node argums {
	id := id_node.id
	d := up(uplink.GET id)
	local := true
	init d {
		d = up(uplink.FIND id)
		init d {
			throw stop(id_node.pos 'undeclared identifier.2') //this is handled before, test and remove this line
		}
		local = false
	}
	if d.type == decl.FUN {
		if d.handler(uplink.ENVSIZE) == 0 {
			// callee is a pure simple function without environment
			if d.node { // todo think about builtins
				expects := len d.node.args
				if expects != len argums and d.node.free == 0 {
					signature := d.node.id + '(' + map(d.node.args lam x = x.id)*', ' + ')'
					throw stop(node.pos tem('argument count mismatch: ^' signature))
				}
			}
			ret line_info(node c_call(d.handler(uplink.C_ID) argums))
		}
		else {
			//callee is a closure function with environent
			env := _create_env(up node d)
			f := c_closure(d.handler(uplink.C_ID) env)
			ret line_info(node c_closure_call(f nil argums))
		}
	}
	else {
		// callee is a defined variable
		ret line_info(node c_closure_call(id_sy(mode.CODE) nil argums))
	}
}

fun sy_this_call up node a {
	_object := sy_expr(up node.parent.parent)

	ret lam m {
		if m == mode.CODE {
			each a item = item (m)
			ret line_info(node c_member_call(_object(m) c_string(node.parent.id) a))
		}
	}
}

fun sy_call up node {
	a := map(node.args lam x=x)
	each a item = sy_expr(up item)

	up(uplink.AFTER lam stage {
		if stage == stag.TYPE {
			if node.parent.type == types.id {
				sig := up(uplink.NAME_GET node.parent.id)
			}
			if sig {
				sig_args := ((sig/'>')[1] / ':')[0] / ','
				each node.args {
					if item.type == types.id {
						if item.id != sig_args[index] {
							// only check args that have explicit types
							arg_type := up(uplink.NAME_GET sig_args[index])
							if arg_type != nil {
								throw stop(item.pos tem('parameter <^> expected' sig_args[index]))
							}
						}
					}
					else {
						// maybe handle expressions here constants, literals, calls etc
						//rec a {}
						//fun f a {}
						//f(10)
						ty := a[index](mode.TYPE)
					}
				}
			}
		}
	})


	if node.parent.type == types.id {
		id_node := node.parent
		callee := sy_id(up id_node)
		ret lam m {
			if m == mode.CODE {
				each a item = item(m)
				ret code_call_id(up node callee id_node a)
			}
		}
	}
	else {
		if node.parent.type == types.member
			ret sy_this_call(up node a)

		callee = sy_value_dispatch(up node.parent)
		ret lam m {
			if m == mode.CODE {
				each a item = item(m)
				ret line_info(node c_closure_call(callee(m) nil a))
			}
		}
	}
}

fun sy_value_dispatch up node {
	f := nil
	if node.type == types.id f = sy_id
	but node.type == types.string f = sy_string
	but node.type == types.array_literal f = sy_array_literal
	but node.type == types.object_literal f = sy_object_literal
	but node.type == types.item f = sy_item
	but node.type == types.each_index f = sy_each_index
	but node.type == types.fun f = sy_lam //what is this???
	but node.type == types.args f = sy_args
	but node.type == types['this'] f = sy_this
	but node.type == types['it'] f = sy_it
	but node.type == types['that'] f = sy_that
	but node.type == types['error'] f = sy_error

	but node.type == types.indexing f = sy_index
	but node.type == types.call f = sy_call
	but node.type == types.member f = sy_member
	but node.type == types.range f = sy_range
	but node.type == types.expr f = sy_expr
	else {
		log nodestr(node)
		throw stop(node.pos 'internal error, sy_value_dispatch node is <'+types[node.type]+'>')
	}
	ret f(up node)
}

fun sy_index_set up node expr {
	dest := sy_value_dispatch(up node.parent)
	ix := sy_expr(up node.ix)
	src := sy_expr(up expr)

	ret lam m {
		if m == mode.CODE {
			ret line_info(node c_index_set(dest(m) ix(m) src(m)))
		}
	}
}

fun sy_BIT_set up node expr {
	dest := node.parent
	log 'ix:' past(node)
	log 'expr:' past(expr)
	i := sy_expr(up node.expr)
	src := sy_expr(up expr)
	ret lam m {
		if m == mode.CODE {
			log ~red 'obj:' dest(m).expr()
			ret line_info(node c_bit_set(dest(m) i(m) src(m)))
		}
	}
}

