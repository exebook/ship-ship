// TODO написать прогу которая всегда висит на экране и показывает случайное китайское слово
// иероглиф, произношение перевод, оно может висеть несколько минут, иногда повторяться
// можно на шипе)

[
  {
    cp:0,type::expr,stack:[
      {
        cp:0,num:10,type::int
      },{
        cp:2,num:1,type::int
      },{
        cp:1,type::operator,operator::"-"
      }
    ]
  }
]

int64_t t1 = 555; // store = tmp i64
int64_t t2 = 111; // store = tmp i64
int64_t t3 = t1 + t2; // store = tmp i64
expr_push(var(0)); // store = stack
expr_push(var(1)); // store = stack
op_add(); // store = stack
expr_push(cell_let(t3)); // store = stack
op_add(); // store = stack
op_to_int(); // stor = stack
int64_t t6 = expr_pop(); // store = tmp i64

alternate (@ means no code generated, only compiler state is changed):
@1 <555> // store = c_expr int (this line is not generated, result is string that can be inserted as-is
int64_t t1 = 111; // store = tmp int
@2 <t1 + @1 555> // store = c_expr int

int64_t t1 = 555; // store = tmp i64
int64_t t2 = 111; // store = tmp i64
int64_t t3 = t1 + t2; // store = tmp i64
var(0) = 101; // store = var [0]
var(1) = 102; // store = var [1]
var(2) = ex_add(var(0), var(1)); // store = var [2]
var(3) = ex_add(cell_let(t3), var(2));
	// box:
	store = store_type.var
	n = 3 // var num
	code = ['var(3) = ex_add(cell_let(t3), var(2));']
	use = 'var(3)'
int64_t t6 = ex_to_int(var(3)); // store = tmp i64

TYPES:
	c_var //c variable, c_type stores actual C type
	c_lit // c literal, '1', '1.0', '2+2' etc
	var // gc'ed ship cells
SUBTYPES:
	if VAR
		var, num, array, obj // if var is certain to hold a value of particular type at the moment cell_let/ex_to_num/ex_to_int...)
	if C_VAR
		i8-64, u8-64 f32-64 ptr
fun new_cu {
	ret {
		store, 
		
	}
}
enum cu_type {
}
------------------------------------------
	GEN_CU
	//instead of this string:
	'var(3) = ex_add(cell_let(t3), var(2));'
	//we generate this structure (aka CU, code unit)
	cu = {
		store: store_type.var
		n: 3// var(3)
		op: ex_add // ex_add(...)
		items: [
			cell_let(t3) <--- comes from previous calculation
			var(2) <--- comes from previous calculation
		]
	}
	use = 'var(3)'
	
------------------------------------------
	GEN_CU
	//instead of this string:
	'uint64_t t2 = ex_add_u64(100, t1);'
	//we generate this structure (aka CU, code unit)
	cu = {
		store: store_type.c_var
		c_type: c_types.u64
		n: 3// var(3)
		op: ex_add // ex_add(...)
		items: [
			cell_let(t3) <--- comes from previous calculation
			var(2) <--- comes from previous calculation
		]
	}
-----------------------------------------
name i32 i j.
name string s t.
type A {
}

name A a.
name B b.

label c_prime
tag c_prime i8 u8 i16 u32 i64 u16 u32 u64 f32 f64.

type i8 {
	= c_prime
	= var
}

type user_id {
	userId
}

name user_id { id }

id := cast 'user1'
name f64: x.
x := 123
id = x // error
a := x // a:var, x:f64 OK

assign i32 string
string i32

s := i // error





