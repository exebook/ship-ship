fun _setter up d x {
	if d.type == decl.LOCAL ret c_varset(d.stack_addr x)
	but d.type == decl.ARG ret c_argset(d.arg_addr x)
	but d.type == decl.FLYARG ret c_flyargset(d.arg_addr x)
	but d.type == decl.ENV ret c_envset(d.env_addr x)
	but d.type == decl.FLY ret c_flyset(d.stack_addr x)
	else throw stop(d.node.pos 'sy_setvar unhandled')
}

fun sy_set_vars up target expr {
	x := sy_expr(up expr)

	up(uplink.AFTER lam stage {
		if stage == stag.CALL {
			each target {
				d := up(uplink.GET item.id)
				init d {
					throw stop(item.pos 'undeclared ' + item.id + '')
				}
				up(uplink.VARSET d)
			}
		}
	})

	ret lam m {
		if m == mode.CODE {
			tmp_addr := up(uplink.TMP)
			list := [c_varset(tmp_addr, x(m))]

			each target {
				d := up(uplink.GET item.id)

				// destructuring assignment a,b := [1 2]
				list += _setter(up d c_multi(c_varget(tmp_addr) index))
			}
			ret block(list)
		}
	}
}

fun trace x { log x ret x }

fun sy_setvar up node {
//	if node.type == types.the ...
	id := node.target[0].id
	x := sy_expr(up node.expr)

	up(uplink.AFTER lam stage {
		if stage == stag.CALL {
			d := up(uplink.GET id)
			init d throw stop(node.pos 'undeclared? ' + id + '')
			up(uplink.VARSET d)

		}
		if stage == stag.TYPE {
			tid := node.target[0].id
			ty := up(uplink.NAME_GET tid)
			if ty and ty[0:4] == 'rec-' {
				if node.expr.type == types.object_literal {
					node.expr.ty = ty
					node.expr.rec_name = id
				}
			}
			expr_ty := x(mode.TYPE)
			//log ~1 ty '=' expr_ty
		}
	})

	ret lam m {
		if m == mode.CODE {
			x = x(m)
			d := up(uplink.GET id)
			ret line_info(node _setter(up d x))
		}
	}
}

//fun _lito_to_litr ty node {
	//node.type = types.rec_literal
	//node.ty = ty
	//ret node
//}

fun sy_define up node {
	each node.thes {
		up(uplink.THE item.target)
	}
	each node.target {
		prev := up(uplink.DEF item)
		if prev {
			throw stop((item.pos) 'already declared')
		}
	}
	if len node.target == 1 {
		ret sy_setvar(up node)
	}
	else
		ret sy_set_vars(up node.target node.expr)
}

fun sy_set_item up node expr {
	each_id := up(uplink.EACH_ID node.level)
	init each_id {
		if node.level == 0 throw stop(node.pos tem(E015_1 'item'))
		else throw stop(node.pos tem(E014_2 'item' node.level))
	}

	x := sy_expr(up expr)

	ret lam m {
		if m == mode.CODE {
			ret line_info(node c_item_set(each_id x(m)))
		}
	}
}

fun sy_assign up node {
	if not is_array(node.target)
		throw 'internal error in sy_assign, target is not an array'

	each node.thes {
		up(uplink.THE item.target)
	}
	if len node.target == 1 {
		t1 := node.target[0]

		if t1.type == types.it {
			t1 = up(uplink.IT)
			node.target[0] = t1
		}
		but t1.type == types.that {
			t1 = up(uplink.THAT)
			node.target[0] = t1
		}

		if t1.type == types.id {
			//are t1 'NOW ASSIGN'
			up(uplink.REF t1)
			ret sy_setvar(up node)
		}
		but t1.type == types.item { // move this to value base
			ret sy_set_item(up t1 node.expr)
		}
		but t1.type == types.member {
			ret sy_property_set(up t1 node.expr)
		}
		but t1.type == types.indexing {
			ret sy_index_set(up t1 node.expr)
		}
		else throw stop(node.pos 'assign node error')
	}
	else {
		each node.target {
			if item.type != types.id throw stop(node.pos E010_0)
			// todo support this, the only problem is a creation of a temp variable
			// also maybe explicitly demand usage of comma for this
			up(uplink.REF item)
		}
		ret sy_set_vars(up node.target node.expr)
	}
}

fun sy_op_assign up node {
	op := node.op

	if len node.target == 1 {
		tar := node.target[0]
		if tar.type == types.id {
			tid := tar.id
			ty := up(uplink.NAME_GET tid)
			if ty and ty[0:3] == 'rec' {
				throw stop(node.pos 'operation '+Token[op]+' is not applicable to records')
			}
		}
		node.type = types.assign
		node.expr = {
			pos: node.pos
			type: types.expr,
			stack: [
				node.target[0]
				node.expr
				{ type: types.operator op, pos: node.pos }
			]}
		ret sy_assign(up node)

	}
	else {
		each node.target up(uplink.REF item)
		throw stop(node.pos 'multiple target operational assign is not implemented')
	}
}

fun sy_inc up node op {
	a := {
		type: types.assign
		target: [ node.value ]
		thes: []
		expr: {
			pos: node.pos
			type: types.expr,
			stack: [
				node.value
				{ type: types.number num: 1 pos: node.pos }
				{ type: types.operator op, pos: node.pos }
			]
		}
		pos: node.pos
	}
	ret sy_assign(up a)
}
