Color := __color

fun show_error_point str i {
	x1 := i - 10
	x2 := i + 10
	if x1 < 0 x1 = 0
	x3 := i - x1
	s := ''
	while len s < x3 s += ' '
	s += '^'
	cout ~13 '"' str[x1:x2] '"' chr 10
	cout ' ' ~78 s ~~ chr 10
}

fun parse_hex s {
	throw 'parse_hex from tool.ship is deprecated, use parse_int builtin.'
	/:deprecated
	n := 0
	i := 0
	while i < len s {
		c := s[i]
		if c >= ord '0' and c <= ord '9' { c -= ord '0' }
		but c >= ord 'A' and c <= ord 'F' { c -= ord 'A' - 10 }
		but c >= ord 'a' and c <= ord 'f' { c -= ord 'a' - 10 }
		else {
			throw 'parse hex: invalid hex number'
		}
		n = (n SHL 4) + c
		inc i
	}
	ret n
	deprecated:/
}

fun to_hex x {
	s := ''
	while x > 0 {
		d := x AND 15
		if d >= 10 d += 55
		else d += 48
		s = chr d + s
		x = x SHR 4
	}
	ret s
}

fun trim s {
	if len s == 0 ret s
	if s[0] <= 32 ret trim(s[1..])
	if s[-1] <= 32 ret trim(s[..-2])
	ret s
}

fun get_env n {
	re := ''
	begin
		char *p = alloc_utf_from_val(& $ n);
		char *s = getenv(p);
		free(p);
		$ re = new_string_from_utf(s);
	end
	ret re
}

//fun stack_trace addr_array {
	//re := ''
	//if addr_array {
		//log 'stack_trace with an argument is not implemented'
		//kill
	//}
	//begin
		//void *buffer[20];
		//int size = backtrace(buffer, 20);
		//char cmd[1024];
		//char buf[20]; // should be enough to hold pointer value
		//char *prog_name = "nil";
		//if (global_argv) prog_name = global_argv[0];
		//#ifdef __TINYC__
			//sprintf(cmd, "read-stabs -m -e %s", prog_name);
		//#else
			//sprintf(cmd, "addr2line -C -s -a -i -p -f -e %s", prog_name);
		//#endif
		//for (int i = 1; i < size; i++) {
			//if (buffer[i] > (void*) show_trace_buf + 0x99900000) break;
			//sprintf(buf, " %p", buffer[i]);
			//strcat(cmd, buf);
		//}
		//$ re = new_string_from_ascii(get_system(cmd));
	//end
	//ret re// / '\n'
//}

fun __color a txt {
	if txt {
		ret __color(a, nil) + txt + __color(-1, nil)
	}
	esc := chr 0x1b
	if a == -1 ret esc +'(B'+ esc +'[m'
	ret esc +'[38;5;'+ a +'m'
}

fun column_tabify txt delim draw_border {
	/*
log column_tabify('1|abc|hello
200|x
1|2|3|4|5
|||o\\|k', '  ', true)
	*/
	if delim == nil delim = ' '
	if is_array(txt) {
		throw 'column_tabify accepts string, but got an array'
	}
	max_size := 0
	txt = txt / '\\|' * 'ESCAPED_PIPE' / '\n'
	columns := [] // store column widths
	each txt {
		item /= '|'
		each item {
			init the columns[index] it = 0
			if it < the len item that = it
		}
	}
	each txt {
		each item {
			while len item < columns[index] item += ' '
		}
		item *= delim
	}
	txt = txt * '\n' / 'ESCAPED_PIPE' * '|'
	if draw_border {
		line := ''
		w := 0
		each columns w += item
		w += len delim * (len columns - 1)
		are w columns
		while len line < w line += '-'
		txt = line + '\n' + txt + '\n' + line
	}
	ret txt
}

fun turn_out_object obj { // { a: 'b' } -> { b: ["a"] }
	K := keys(obj)
	V := vals(obj)
	J := {}
	each K {
		if obj[item] == nil cont

		J[obj[item]] = []
	}
	each K {
		if obj[item] == nil cont
		J[obj[item]] += item
	}
	ret J
}

fun invert_object obj { // { a: 'b' } -> { b: 'a' }
	K := keys(obj)
	V := vals(obj)
	J := {}
	each K {
		J[obj[item]] = item
	}
	ret J
}

fun utf_decode data { // [208, 185] -> 'й'
	ln := len data
	begin
		char *c = malloc(cell_get($ ln));
		int n = 0;
	end
	each data {
		ch := item
		begin
			c[n++] = cell_get($ ch);
		end
	}

	re := nil
	begin
		c[n++] = 0;
		$ re = ship_str(c);
		free(c);
	end
	ret re
}

fun lowercase s {
	re := ''
	each s {
		if item >= ord 'А' and item <= ord 'Я' {
			re += chr (item + 32)
		}
		but item >= ord 'A' and item <= ord 'Z' {
			re += chr (item + 32)
		}
		else re += chr item
	}
	ret re
}

fun uppercase s {
	re := ''
	each s {
		if item >= ord 'а' and item <= ord 'я' {
			re += chr (item - 32)
		}
		but item >= ord 'a' and item <= ord 'z' {
			re += chr (item - 32)
		}
		else re += chr item
	}
	ret re
}

begin
#include <regex.h>
end

fun regexp_match str pattern {
	re := nil
	begin
		regex_t rx;
		char *str = ship_pchar($ str);
		char *pattern = ship_pchar($ pattern);

		if (!regcomp(&rx, pattern, REG_EXTENDED)) {
			int status = regexec(&rx, str, 0, NULL, 0);
			$ re = cell_bool(status == 0);
		}
		free(str);
		free(pattern);
		regfree(&rx);
	end
	ret re
}

mask_match_arr := nil

fun mask_match str m {
	if mask_match_arr == nil {
		mask_match_arr := '\\#|()[]{}^$+.' / ''
	}
	each mask_match_arr {
		m = m / item * ('\\' + item)
	}
	m = m / '*' * '.*'
	m = m / '?' * '.'
	m = '^' + m + '$'
	ret regexp_match(str, m)
}

fun sorensen_dice A B { // string distance, removing spaces and lower/upper case is up to you
	if A == B ret 1
	if len A <= 1 or len B <= 1 ret 0

	M := {}
	each A {
		b := A[index:2]
		if len b == 1 cont
		count := 1
		use the M[b] count = it + 1
		it = count
	}

	intersectionSize := 0
	each B {
		b = B[index:2]
		if len b == 1 cont

		count = 0
		use the M[b] count = it

		if count > 0 {
			M[b] -= 1
			inc intersectionSize
		}
	}
	ret (2.0 * intersectionSize) / (len A + len B - 2)
}

fun map_each arr value {
	re := {}
	each arr re[item] = value
	ret re
}

fun round_to signs value {
	j := ('' + value) / '.'
	j[1] = j[1][0:signs]
	ret j * '.'
}

