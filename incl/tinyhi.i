incl color
hilite_theme := [171 239 244 1 33 196 100 100 95]
hilite_keywords := 'fun lam ret if else but loop each while break cont log cout are kill lang index item ekey len chr ord inc dec not NOT BIT and or AND OR SHL SHR XOR mod div mul pow init' //space separated

//log hilite('fun a { log 2+2 }')

incl tinylex

fun hilite text {
	brace_depth := 0

	fun color_x x {
		n := 0
		while x > 0 {
			inc n
			x = x SHR 1
		}
		ret n - 1
	}
//
	//fun get_id_color id {
		//init color_map {
			//color_map = create_color_map()
		//}
		//ret color_map[id]
	//}

	toks := tinylex('txt' text)
	keywords := {}
	hilite_keywords /= ' '
	each hilite_keywords keywords[item] = 6

	re := ''
	preproc_color := 193
	preproc := nil
	preproc_y := -1
	space := 0
	each toks {
		T := item
		C := 0
		key := T[2]
		value := T[3]
		t := value
		//if key == Token.Key value = Keys[key]
		while space < T[1] {
			re += chr text[space]
			inc space
		}
		space += len (''+value)
		if preproc != nil {
			if item.y != preproc_y {
				re += preproc
				preproc = nil
			}
			else {
				if key < len hilite_theme C = hilite_theme[key]
				if key == Token.Str1 or key == Token.Str2 t = '"'+t+'"'
				preproc += color(C bg(preproc_color t))
				cont
			}
		}
		if t == '#' {
			re += bg(preproc_color t)
			preproc = ''
			preproc_y = item.y
			cont
		}
		if key < len hilite_theme C = hilite_theme[key]
		if key == Token.Str1 or key == Token.Str2 t = '"'+t+'"'
		but key == Token.Id {
			q := hilite_theme[0]
			use q C = q
		}
		if t == '{' {
			C = hilite_theme[brace_depth]
			inc brace_depth
		}
		but t == '}' {
			dec brace_depth
			C = hilite_theme[brace_depth]
		}
		but the keywords[t] != nil {
			C = hilite_theme[4]
		}
		P := text[T[1] - 1]

		re += color(C) + t
	}
	re += color()
	ret re
}
