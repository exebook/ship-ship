incl color
//hilite_theme := [171 239 244 1 33 196 100 100 95]
hilite_theme := [5 0 7 1 2 3 6 14 0 8]
hilite_keywords := '' //space separated
//lex_type { raw space comment str int float sym keyword id }


//log hilite('fun a { log 2 + 2 }')

fun hilite text {
	brace_depth := 0

	//fun create_color_map {
		//color_map := {}
		//fun set color src {
			//each src / ' ' {
				//color_map[item] = color
			//}
		//}
		//var e
		//// This must be in sync with deodar/lexer.js and ship/lex.ship
		//// types
		//set(46, 'i8 u8 i16 u16 i32 u32 i64 u64 f32 f64 int var number array string enum object')
		//// infrastructure/logging/debugging
		//set(214, 'fun incl log are cout time_begin time_end kill re the it')
		//// operations
		//set(40, 'not plus minus inc dec chr ord put pull push pop is')
		//// control (and, or are operators, but used to separate parts of the expression visually)
		//set(39, 'lam if but ret lam var loop each else while break @ cont halt init '
		     //+ 'use try catch throw charmap found miss and or begin end')
		//// values
		//set(27, 'nil true false index lap len item OR AND SHR SHL NOT XOR match')
		//// concept
		//set(200, 'name rec union')
		//ret color_map
	//}
	//
	//var color_map

	fun color_x x {//what was the purpose of this?
		n := 0
		while x > 0 {
			inc n
			x = x SHR 1
		}
		ret n - 1
	}
//
	//fun get_id_color id {
		//init color_map {
			//color_map = create_color_map()
		//}
		//ret color_map[id]
	//}

	src := load_src_str(text, '<text>')
	toks := new_lexer({ space:lex_option_space.token, comments: true })(src)
	keywords := {}
	hilite_keywords /= ' '
	each hilite_keywords keywords[item] = lex_type.keyword
	re := ''
	preproc := nil
	preproc_y := -1
	each toks.tokens {
		t := item.s
		if preproc != nil {
			if t == '\\' {
				preproc += t
				preproc_y = item.y+1
				preproc_color = 0
				cont
			}
			if item.y != preproc_y {
				re += preproc
				preproc = nil
			}
			else {
				C = hilite_theme[item.type]
				if item.type == lex_type.str t = '"'+t+'"'
				preproc += color(C color(preproc_color t))
				cont
			}
		}
		if t == '#' {
			preproc_color := 7
			re += color(preproc_color t)
			preproc = ''
			preproc_y = item.y
			cont
		}
		t = t / '\t' * '   '
		C := hilite_theme[item.type]
		if item.type == lex_type.str t = '"'+t+'"'
		//but item.type == lex_type.id {
			//q := nil//color_x(find_flags(item.s))
			//use q C = q
			//C = hilite_theme[2]
		//}
		if t == '{' {
			C = hilite_theme[brace_depth]
			inc brace_depth
		}
		but t == '}' {
			dec brace_depth
			C = hilite_theme[brace_depth]
		}
		but the keywords[t] != nil {
			C = hilite_theme[it]
		}
		re += color(C) + t
	}
	re += color()
	ret re
}
