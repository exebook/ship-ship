begin
	#include <sys/file.h>
	#include <errno.h>
end

fun single_instance _name {
	re := true
	path := '/var/lock/' + _name
	begin
		char *path = ship_pchar($ path);
		int fd = open(path, O_CREAT | O_RDWR, 0666);
		int rc = flock(fd, LOCK_EX | LOCK_NB);
		if (rc) {
			if (EWOULDBLOCK == errno);
			else printf("what is this situation?\n");
		}
		else {
			$ re = cell_false();
		}
		free(path);
	end
	ret re
}

/*
// Example usage
incl 'single-instance'

other := single_instance('udev-phone')
if other {
	log 'another instance is running, pid:' other.pid
	ret 1
}

fun delay sec {
	usleep(sec * 1_000_000)
}

loop {
	cout '.'
	delay(0.5)
}

*/

