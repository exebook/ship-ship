begin
#include <sys/ioctl.h>
end

ioctl := {
get_stdout_columns: lam {
	var x y
	begin
		struct winsize ws = { 0, 0 };
		if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) == -1 || ws.ws_col == 0) {

		}
		else {
			$ x = cell_let(ws.ws_col);
			$ y = cell_let(ws.ws_row);
		}
	end
	ret { x, y }
}

}
