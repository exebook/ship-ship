fun foreach a f {
	each a f(item)
}

fun some a f {
	each a if f(item) ret true
	ret false
}

fun every a f {
	each a if not f(item) ret false
	ret true
}

fun filter a f {
	re := []
	each a
		if f(item) re += item
	ret re
}

fun map a f {
	if is_array(a) {
		n := len a
		if n == 0 ret []
		re := []
		re[n - 1] = nil // resize

		each a re[index] = f(item)
		ret re
	}
	if is_object(a) {
		re = {}
		k := keys(a)
		each k re[item] = f(a[item] item)
		ret re
	}
}

fun every_tail a f {
	if len a <= 1 ret true
	last := len a - 1
	each a {
		if index == last ret true
		else if not f(item) ret false
	}
}

fun map_tail a f ft {
	// map_tail([1 2] lam x = x + 100 lam x = x + 200) ----> [101 202]
	if len a == 0 ret []
	if len a == 1 ret [ ft(a[0]) ]
	last := len a - 1
	re := []
	re[last] = nil // resize

	each a {
		if index == last re[index] = ft(item) else re[index] = f(item)
	}
	ret re
}

fun reduce list f {
	sum := nil
	each list {
		if index == 0 {
			sum = item
		}
		else {
			sum = f(sum item)
		}
	}
	ret sum
}

fun range_insert str sub at {
	if at == 0 ret sub + str
	ret str[..at-1] + sub + str[at..]
}

fun range_delete str from to {
	if from > 0 {
		a := str[..from-1]
	}
	else {
		a = str[1..0] // empty
	}
	b := str[to..]
	ret a + b
}

fun flatten x {
	re := []
	each x {
		if is_array(item) re += flatten(item)
		else re += item
	}
	ret re
}

fun curry f a { ret lam x = f(a x) }
