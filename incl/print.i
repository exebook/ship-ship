fun print_node node {
	fun quot s c {
		if c == nil ret '"'+s+'"'
		ret color(c) + '"'+s+'"' + color()
	}
	fun filter k v {
		try {
			if k == 'id' v = color(9) + v + color()
		}
		catch {
			log 'print_node: error 1'
		}

//		if k == 'str' {
		if is_string(v) {
			v = '"'+ v / '\n' * '\\n' +'"'
		}

		if k == 'cp' and is_object(v) {
			v = v.i
		}

		try {
			if k == 'type' {
				v = ':'+color(4) + parse_type[v] + color()
			}
		}
		catch { log 'print_node: error 2' }

		try {
			if k == 'operator' and is_number(v) v = ':'+quot(lex_infix_enum[v], 9)
		}
		catch {
			are 'print_node: error 3' v lex_infix_enum
		}

		try {
			if k == 'sym' { log 'HEREE', k, is_number(k) }
			if k == 'sym' and is_number(v) v = quot(lex_sym[v], 9)
		}
		catch {
			are 'print_node: error sym' v lex_sym
		}
		try {
			if is_number(v) {
				a := color(46) + v + color()
				ret a
			}
		}
		catch {
			log 'print_node: error 4'
			log error
			kill
		}

		if k == 'i' ret nil
		ret v
	}

	s := shame(node, { filter, indent: 2, filter_val_bare: true })
	log s
}

