incl unix
begin
	#include <signal.h>
	int signal_value = -1;
end

timers := []
unix_signal := nil

fun hard_signal_handler id {
	// to write your own signal handler, just write an usual C style signal handler, assign it using signal() or a similar function. Then set a C variable `signal_value` inside
	// your signal handler to anything other than 0 (normally to a signal number)
	// then async will detect it and call this handler, in ship
	// use `unix_signal` functional variable as a callback, which can be stacked
	begin
		switch ((int)cell_get($ id)) {
			case SIGWINCH: {
				$ id = ship_str("SIGWINCH");
				// TODO: add more cases here
				break;
			}
		}
	end
	if the unix_signal it(id)
}

fun end_main_loop {
	async_state.want_quit = true
}

fun async_add_fd fd f {
	//install_after_main()
	fid := -1
	each the async_state.list {
		if item == nil {
			fid = index
			@
		}
	}
	if fid < 0 fid = len it

	it[fid] = { fd, f }
	ret it[fid]
}

//fun async_remove_fd fd {
	//log 'removing' fd async_state.list
	//each the async_state.list {
		//if item and item.fd == fd {
			//item = nil
			//log 'removed;'
			//@
		//}
	//}
//}

fun async_file_read fd f {
	fun do_read a {
		s := unix.full_read_fd(fd)
		f(s)
	}
	async_add_fd(fd, do_read)
}

async_state := {
	timers: []
	list: []
	want_quit: false
}

begin
	fd_set fd_list;
end

fun async_list_len {
	re := 0
	each the async_state.list if item re += 1
	ret re
}

fun async_select {
	begin
		struct timeval tv, *timeout = 0;
	end
	the next_timer := -1
	if len timers > 0 {
		it = timers[-1].time
	}
	if async_list_len() == 0 {
		if next_timer == -1 {
			ret 0 // amount of events handled
		}
		wait_us := next_timer - time1000()
		if the wait_us > 0 {
			usleep(wait_us * 1000)
		}
		R := pop timers
		R.f()
		ret 1
	}

	if next_timer >= 0 {
		begin
			tv.tv_usec = (cell_get($ next_timer) - time1000()) * 1000;
			if (tv.tv_usec < 0) {
				tv.tv_usec = 0;
			}
			tv.tv_sec = 0;
			timeout = &tv;
		end
	}

	begin
		FD_ZERO(&fd_list);
	end

	max_fd := -1
	each async_state.list {
		if item == nil cont
		fd := item.fd
		begin
			FD_SET((int)cell_get($ fd), &fd_list);
		end
		if max_fd < fd {
			max_fd = fd
		}
	}
	inc max_fd

	selected := nil
	signaled := 0
	begin
		signal_value = 0;
		$ selected = cell_let(select(cell_get($ max_fd), &fd_list, NULL, NULL, timeout));
		if (signal_value >= 0) {
			$ signaled = cell_let(signal_value);
		}
	end

	if signaled {
		hard_signal_handler(signaled)
		ret 1 // 1 event happened (signal)
	}

	if selected > 0 {
		ret handle_ready()
	}
	but selected == 0 {
		timer := pop timers
		timer.f()
		ret 1 // 1 event happened (timer)
	}
	else {
		errno := nil
		begin
			$ errno = cell_let(errno);
		end
		if the async_state.on_error it(errno)
		ret -1
	}
}

fun handle_ready {
	ready := []
	each async_state.list {
		if item == nil cont
		fd := item.fd
		begin
			int fd = (int)cell_get($ fd);
			if (FD_ISSET(fd, &fd_list)) {
				end
					ready += { f: item.f fd: item.fd n: index }
					item = nil
				begin
			}
		end
	}
	each ready {
		item.f(item)
	}

	ret len ready
}

timer_next_id := 0
fun set_timeout f ms {
	install_after_main()
	active := true
	ms += time1000()
	timers += {
		id: timer_next_id
		f: lam {
			if active f()
		}, time: ms,
	}
	sort(timers, lam a b { ret a.time < b.time })
	inc timer_next_id
	ret lam { active = false }
}

fun set_interval f ms {
	// to clear interval just call the returned lambda
	active := true

	fun next {
		set_timeout(lam {
			if active f()
			if active next()
		}, ms)
	}

	next()
	ret lam { active = false }
}

async_run_called := false
async_run_auto_installed := false

fun async_run {
	async_run_called = true
	the async_state.want_quit = false
	while not it {
		re := async_select()

		if re <= 0 {
			@
		}
	}
}

fun async_run_auto {
	if async_run_called ret
	async_run()
}

fun install_after_main {
	if the async_run_auto_installed ret
	it = true
	f := async_run_auto
	begin
		root_push($ f);
		after_main = $ f;
	end
}
