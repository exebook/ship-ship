fun reset {
	ret '\1b[0m'
}

color := lam a b {
	init a ret '\1b(B\1b[m'
	use b ret color(a) + b + color()
	ret '\1b[38;5;'+ a +'m'
}

bg := lam a b {
	init a ret '\1b[49m'
	use b ret bg(a) + b + bg()
	ret '\1b[48;5;'+ a +'m'
}

fun cursor_hide {
	ret '\1b[?25l'
}

fun cursor_show {
	ret '\1b[?25h'
}

fun uncolor s {
	re := ''
	s = s / '\1b[0m' * '' // full reset
	s = s / '\1b(B\1b[m' * '' // reset fore
	s = s / '\1b[49m' * '' // reset back
	s = s / '\1b[38;5;'
	each s item = item[find_at(item 'm' 0)+1..]
	s *= ''
	s = s / '\1b[48;5;'
	each s item = item[find_at(item 'm' 0)+1..]
	s *= ''
	ret s
}

fun color_parse s {
	// use s * '' to join what this function returns
	re := []
	x := 0
	fun simple esq {
		l := len esq
		if s[x : l] == esq {
			if len re == 0 re = ['']
			re[-1] += s[x : l]
			x += l ret 1
		}
	}
	fun embrace A B {
		la := len A
		if s[x:la] == A {
			if len re == 0 re = ['']
			re[-1] += s[x : la]
			x += la
			mx := find_at(s B x)
			re[-1] += s[x..mx + len B - 1]
			x = mx + len B
			ret 1
		}
	}
	while x < len s {
		if simple('\1b[0m') cont
		if simple('\1b(B\1b[m') cont
		if simple('\1b[49m') cont
		if embrace('\1b[38;5;' 'm') cont
		if embrace('\1b[48;5;' 'm') cont
		re += chr s[x]
		x += 1
	}
	ret re
}

ansi := {
	parse: color_parse
	uncolor,
	color,
	bg,
	cursor_hide,
	cursor_show,
	reset,
	forward: lam x = '\1b['+ x +'C' ,
	back: lam x = '\1b['+ x +'D' ,
	erase_line: lam x = '\1b['+ x +'K',
}
//
//s := '{' + color(1 'zello') +'-'+ bg(2) + '==' + color(3, '*') + reset() + bg(4 '###')+ reset() + ' end'
//log len s, s
//t := uncolor(s)
//log len t, t
//p := color_parse(s)
//log len p p * ''
