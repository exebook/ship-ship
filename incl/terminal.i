incl ioctl

begin
#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>

struct termios orig_termios;
int terminal_fd;

void die(const char *s) {
	perror(s);
	exit(1);
}

void __disableRawMode(int i, void *p) {
	//write(STDOUT_FILENO, "\x1b[2J", 4);
	if (tcsetattr(terminal_fd, TCSAFLUSH, &orig_termios) == -1)
	die("tcsetattr");
}

void _sig_handler(int sig) {
	__disableRawMode(0, 0);
	exit(1);
}

void _sigwinch_handler(int sig) {
	signal_value = SIGWINCH;
}


void __enableRawMode() {
	if (tcgetattr(terminal_fd, &orig_termios) == -1) {
		die("tcgetattr");
	}
	//atexit(disableRawMode);
	on_exit(__disableRawMode, 0); // atexit() does not work with tcc
	signal(SIGINT, _sig_handler);
	signal(SIGTERM, _sig_handler);
	signal(SIGWINCH, _sigwinch_handler);
	struct termios raw = orig_termios;
	/*
	When BRKINT is turned on, a break condition will cause a SIGINT signal to be sent to the program, like pressing Ctrl-C.
	INPCK enables parity checking, which doesn’t seem to apply to modern terminal emulators.
	ISTRIP causes the 8th bit of each input byte to be stripped, meaning it will set it to 0. This is probably already turned off.
	CS8 is not a flag, it is a bit mask with multiple bits, which we set using the bitwise-OR (|) operator unlike all the flags we are turning off. It sets the character size (CS) to 8 bits per byte. On my system, it’s already set that way.
	disables ctrl-s/ctrl-q, ICRNL disables ctrl-m,ctrl-j
	// isig disables ctrl-z and ctrl-c, iexten disables ctrl-v
	*/
	raw.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
	raw.c_oflag &= ~(OPOST);
	raw.c_cflag |= (CS8);
	raw.c_lflag &= ~(ECHO | ICANON | ISIG | IEXTEN);

	if (0) {
		//enable timeout in tenths of a second
		raw.c_cc[VMIN] = 0;
		raw.c_cc[VTIME] = 1;
	}

	if (tcsetattr(terminal_fd, TCSAFLUSH, &raw) == -1) {
		die("tcsetattr");
	}
}


end

out := lam {
	s := args * ' '
	//q := s / '\1b' * '\\1b'
	//log 'Tout:' q
	begin
		char* p = ship_pchar($ s);
		printf("%s", p);
		free(p);
		fflush(0);
	end
}

fun read_csi data {
	// read an entire CSI sequence according to specs found in wikipedia
	x := 27 find data
	if x < 0 ret

	if data[x] != 27 ret
	inc x
	if data[x] != ord '[' ret
	inc x
	param := []
	inter := []
	final_byte := nil

	loop {
		if the data[x] >= 0x30 and it <= 0x3F {
			param += it
			inc x
		}
		else @
	}

	loop {
		if it >= 0x20 and it <= 0x2F {
			inter += it
			inc x
		}
		else @
	}

	if it >= 0x40 and it <= 0x7E {
		final_byte = it
		inc x
	}

	ret {
		param, inter, final_byte, x
	}
}

terminal := {
	change_screen_mode: true

	stdin: -1

	enter: lam mode {
		init the mode it = true
		this.change_screen_mode = mode
		if mode {
			out('\1b[?1049h')
		}
		stdin := nil
		begin
			terminal_fd = fileno(fopen("/dev/tty", "rw"));
			$ stdin = cell_let(terminal_fd);
			__enableRawMode();
		end
		this.stdin = stdin
	}

	exit: lam {
		if this.change_screen_mode {
			this.cursor_show() // back to primary terminal buffer
			out('\1b[?1049l')
		}
		begin
			__disableRawMode(0, 0);
		end
	}

	clear: lam {
		begin
			write(terminal_fd, "\x1b[2J", 4);
		end
	}

	cursor_hide: lam {
//cout 'terminal hiding cursor: '
		out('\1b[?25l')
	}

	cursor_show: lam {
//cout 'terminal showing cursor: '
		out('\1b[?25h')
	}

	set_xy: lam x y {
//cout 'terminal: set xy' x y ': '
		//out('\1b[' x 'C\1b[' y 'B')
		if typeof(x) != 'number' or typeof(y) != 'number' {
			throw 'terminal.set_xy() argument not a number'
		}
		out('\1b['+ y +';'+ x +'H')
	}

	get_wh: lam {
		xy := ioctl.get_stdout_columns()

		//if xy.x ret xy
		this.save_xy()
		out('\1b[999G\1b[999B')
		xy = this.get_xy()
		this.restore_xy()
		ret xy
	}

	wait_csi: lam num {
		// read reply to the get_width command
		buf := this.read_bytes(true)
		timeout := 0
		loop {
			if timeout > 100 {
				die(lam {
					log 'terminal.wait_csi (' num ') timeout'
				})
			}

			csi := read_csi(buf)
			if csi {
				if csi.final_byte == 82 @
				buf = buf[csi.x..]
				cont
			}
			usleep(1000*3)
			inc timeout
			buf += this.read_bytes(true)
		}
		ret csi
	}


// FOCUS CSI ? 1004 h 			Enable reporting focus. Reports whenever terminal emulator enters or exits focus as ESC [I and ESC [O, respectively.

	get_xy: lam {
		out('\1b[6n')
		csi := this.wait_csi(82)
		xy := (chr csi.param) / ';'
		each xy item = parse_int(item)
		ret { x: xy[1] y: xy[0] }
	}

	save_xy: lam {
	//cout 'Save xy: '
		out('\1b[s')
	}

	restore_xy: lam {
	//cout 'Restore xy: '
		out('\1b[u')
	}

	read_bytes: lam block { //returns array of bytes
		R := []
		C := 0
		fd := this.stdin
		begin
			int fd = cell_get($ fd);
			struct timeval tv = { 0, 0 }, *timeout = 0;
			fd_set set;
			FD_ZERO(&set);
			FD_SET(fd, &set);
			if (cell_get($ block)) {
				timeout = 0;
			}
			else {
				timeout = & tv;
			}
			int selected = select(fd + 1, &set, NULL, NULL, timeout);
			if (selected > 0) {
				int SIZE = 1024;
				char p[SIZE];
				int n = read(fd, p, SIZE);
				if (n > 0) {
					for (int x = 0; x < n; x++) {
						$ C = cell_let((byte)p[x]);
						end
							R += C
						begin
					}
				}
			}
		end
		ret R
	}

	async_read: lam fd f {
		self := this
		fun do_read a {
			s := self.read_bytes(false)
			f(s)
		}
		async_add_fd(fd, do_read)
	}


	on_read: lam f {
		self := this
		fun next {
			self.async_read(self.stdin, lam data {
				f(data)
				next()
			})
		}
		next()
	}
}

die := lam {
	// die(10 20 'abc')
	// die(lam { are x y })
	// do die { are x y } // impossible of course now
	// die() { are x y }
	terminal.exit()
	a := []
	each args {
		if typeof(item) == 'function' item()
		else a += item
	}
	if len a > 0 log a * ' '
	kill
}

//fun readchar {
	//c := nil
	//begin
		//char c;
		//if (read(STDIN_FILENO, &c, 1) == -1 && errno != EAGAIN) {
			//die("read");
		//}
		//$ c = cell_let(c);
	//end
	//ret c
//}



