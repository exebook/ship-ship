//incl 'incl/map'
//
//enum parse_type {
//	int float str bool operator expr id
//	prefix_def // a node representing a new prefix definition
//	prefix_sym
//	prefix_id
//	value lvalue call
//}
//L := load_parse_tests()
//log L

fun load_parse_tests {
	s := load('data/parse.ship')

	L := s / '\n'
	N := []
	each L {
		if item[0:2] == '//' cont
		N += item
	}

	N = N * '\n'
	L = N / '\n\n'
	L = filter(L, lam x { if x == "" ret nil else ret x })
	N = []
	i := 0
	while i < len L {
		if L[i] == '\n' @  // EOF
		s = L[i + 1]
		init s {
			log '"parse" file is not even'
			kill
		}
		
	// convert type names to numbers
		n := 0
		n_size := parse_type
		while n < n_size {
			s1 := 'type::' + parse_type[n]
			s = (s / s1) * ('type:'+n)
			inc n
		}

	// convert operator names to umbers
		n = 0
		n_size = lex_sym
		while n < n_size {
			s1 = 'operator::"' + lex_sym[n] + '"'
			s = (s / s1) * ('operator:'+n)
			inc n
		}
		N += { src: L[i], result: honor(s) }
		i += 2
	}
	ret N
}

