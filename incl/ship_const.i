incl 'incl/map'
print_type_map()

fun print_type_map {

	// Copy last version of c_types from expr.ship before running this code
	enum c_types {
		i8 u8 i16 u16 i32 u32 i64 u64 f32 f64 ptr cell 
	}

	L := 'i8 u8 i16 u16 i32 u32 i64 u64 f32 f64 ptr cell' / ' '
	N := [ 7 8  15  16  31  32 63  64  23 52  48 52 ]
	S := [ 1 0  1   0    1   0   1  0    1  1   0  1 ]
	F := [ 0 0  0   0    0   0   0  0    1  1   0  1 ]
	R := []
	x := 0
	while x < len L {
		R[x] = []
		y := 0
		while y < len L {
			t := chk(x, y)
			init t R[x][y] = nil
			else R[x][y] = t
			inc y
		}
		inc x
	}

	log 'c_type_pairs :=' R
	
		fun chk x y {
			if x == y ret x//L[x]
			signed := S[x] or S[y]
			float := F[x] or F[y]
			maxN := N[x]; if N[y] > maxN { maxN = N[y] }
			R := []
			each L {
				if N[index] >= maxN and S[index] == signed and F[index] == float {
					R += [{ id: item, n: index }]
				}
			}
			sort_with(R, lam a b { ret N[a.n] > N[b.n] })
			if len R > 1 R = filter(R, lam x{ ret x.id != 'ptr' })
			if len R == 0 ret nil
			if L[x] == 'cell' or L[y] == 'cell' ret c_types.cell
			if L[x] == 'ptr' or L[y] == 'ptr' {
				if float ret nil
				ret c_types.ptr
			}
			ret R[0].n
		}
}
