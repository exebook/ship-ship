/*
	ARM (ARray+Map) hybrid data structure
	allows to query the same data by a numeric id and by key (name)

	.add(key, value) -- method, adds a new element, returns numric id
	.data -- property, array of values (never insert to it, use add())
	.get(key) -- returns a value by key

	Suggested common variable names: r, arm, ar

	r := new_arm()
	a_id := r.add('a', { x: 123, y: 456 })
	b_id := r.add('b', { t: 'aaa', s: 'bbb' })

	log r.data[a_id] // -> { x: 123, y: 456 }
	log r.data[b_id] // -> { s: "bbb", t: "aaa" }
	log r.get('a') // -> { x: 123, y: 456 }

*/
fun new_arm {
	data := []
	m := {}
	ret {
		data, // contains actual values
		m, // contains indexes
		add: lam key elem {
			data += elem
			x := len data - 1
			m[key] = x
			ret x
		}
		get: lam key {
			ret data[m[key]]
		}
	}
}

copyprop := lam { // dest src prop1 prop2...
	// returns a new object that has all properties from dest and selected properties from src
	a := args
	re := {}

	dest := pull a
	each keys(dest) re[item] = dest[item]

	src := pull a
	each a {
		re[item] = src[item]
	}
	ret re
}

ocopy := lam { // src prop1 prop2...
	// returns a new object that has selected properties from src
	a := args
	re := {}

	src := pull a
	each a {
		re[item] = src[item]
	}
	ret re
}

omerge := lam a b {
	// returns a new object that has all properties from both a and b
	re := {}
	each keys(a) re[item] = a[item]
	each keys(b) re[item] = b[item]
	ret re
}


