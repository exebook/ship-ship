/*
	OPTION keys:

	list of keys, like -f or --count, that has a single argument after them.

	for example:
		a := load_argv(['f' 'c count'])

	Synonyms are space separated, like 'c' and 'count' in above example.
	note that single character arguments are commonly prependent with '-' while longer ones with '--'

	prog -f 1 -ff 2 3 -a -a --long 4 --big --long 5 -- b c

	{ raw: ["b","c"], keys: { a: 1, f: ["1","2","3"], big: 1, long: 1 }, list: [], binary: "prog" }


show_arg_help aligns strings by "|" character, indents them and prints
show_arg_help('   ',
	'-h --help | This help'
	'   --preact | enable preact'
)

*/

incl tool
incl map

fun synonyms keys {
	S := {}
	init keys ret S
	each keys {
		syn := item / ' '
		while len syn > 1 {
			x := pop syn
			S[x] = syn[0]
			keys += x
		}
		S[syn[0]] = syn[0]
		item = syn[0]
	}
	ret S
}


load_argv := lam keys basic_keys {
	a := {
		binary: argv[0]
		keys: {}   // things like '-f <arg>'
		list: []   // other arguments, like a list of files
		raw: []    // things after '--'
		unknown: {}
	}
	raw_skip := false
	S := synonyms(keys)
	B := synonyms(basic_keys)
	ix := 0
	while ix < len argv {
		A := argv[ix]
		if ix == 0 { inc ix cont }
		if A == '--' {

			ix += 1
			while ix < len argv {
				a.raw += argv[ix]
				ix += 1
			}
			@
		}

		if A[0] == ord '-' {
			follow_args := []
			if A[1] == ord '-' {
				k = A[2..]
				eq := k / '='
				k = eq[0]
				if S[k] {
					if len eq > 1 {
						init a.keys[S[k]] {
							a.keys[S[k]] = []
						}
						a.keys[S[k]] += eq[1]
					}
					else {
						follow_args += k
					}
				}
				else {
					if B[k] k = B[k]
					a.keys[k] = true
				}
			}
			else {
				y := 1
				while y < len A {
					k := chr A[y]
					if S[k] {
						follow_args += k
					}
					else {
						if S[k] == nil and B[k] == nil {
							a.unknown[k] = true
						}
						if B[k] k = B[k]
						if the a.keys[k] inc it
						else {
							a.keys[k] = 1
						}
					}
					inc y
				}
			}
			each follow_args {
				ix += 1
				A = argv[ix]
				if ix >= len argv or  argv[index][0] == '-' {
					throw { source: 'argv.ship' msg: item +' requires an argument' }
					//('Arguments parsing error: '+ item +' requires an argument')
				}
				syn := S[item]
				if the a.keys[syn] {
					it += A
				}
				else {
					it = [A]
				}
			}
		}
		else {
			a.list += A
		}
		ix += 1
	}
	ret a
}

show_arg_help := lam {
	indent := args[0]
	list := args[1..]
	maxsize := 1
	each list {
		if typeof(item) != 'string' {
			log 'correct signature: show_arg_help(indent string string string)'
			kill
		}
		a := map(
			item / '|',
			lam x { ret trim(x) }
		)
		if the len a[0] > maxsize maxsize = it
		item = a
	}
	each list {
		while (len item[0] < maxsize) item[0] += ' '
		log ~green indent + item[0] +' ' ~~ item[1..] * '|'
	}
}

