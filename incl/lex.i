incl 'tool'

enum lex_option_space { token property }
enum lex_option_comments { remove keep }

enum lex_type { raw space comment str int float sym keyword id }
enum lex_flag { ctype cell_type prefix infix postfix assign }

enum lex_keyword_ctype {
	'i8' 'u8' 'i16' 'u16' 'i32' 'u32' 'i64' 'u64' 'f32' 'f64' 'cell'
}
enum lex_keyword_cell_type {
	'nil' 'fun' 'string' 'number' 'array' 'object'
}
enum lex_keyword_infix {
	'or' 'OR' 'and' 'xor' 'AND' 'XOR' 'SHR' 'SHL' 'mul' 'sqr'
	'put' 'push' 'plus' 'find' 'minus' 'mod'
}
enum lex_keyword_prefix {
	// only constructs that are followed by <primary> and do not require a parselet
	'pop' 'NOT' 'not' 'dec' 'inc' 'len' 'chr' 'ord' 'pull'
}
enum lex_keyword_postfix {
	// only constructs that are preceeded by <primary> and do not require a parselet
	'dec' 'inc'
}
enum lex_keyword_other {
	'if' 'but' 'ret' 'log' 'are' 'lap' 'lam' 'rec' 'use' 'try' 'cout'
	'loop' 'each' 'else' 'true' 'item' 'cont' 'init'
	'kill' 'incl' 'enum' 'catch' 'false' 'while' 'index' 'break'
	'union' 'throw' 'error' 'match' 'charmap' 'tag'
}
enum lex_keyword
	= lex_keyword_ctype
	+ lex_keyword_cell_type
	+ lex_keyword_prefix
	+ lex_keyword_infix
	+ lex_keyword_postfix
	+ lex_keyword_other

enum lex_sym_assign { ':=' '=' '*=' '+=' '-=' '/=' }
enum lex_sym_prefix { '++' '--' '-' '*' '&' }
enum lex_sym_infix { '+' '<=' '>=' '>' '<' '==' '**' '!=' '/' '*' '-' }
enum lex_sym_postfix { '++' '--' '->' }
enum lex_sym_other { '?' '//' '/*' ',' ':' '[' ']' '{' '}' '.' '..' ';' '(' ')' '~' '@' }
enum lex_sym
	= lex_sym_assign
	+ lex_sym_prefix
	+ lex_sym_infix
	+ lex_sym_postfix
	+ lex_sym_other

enum lex_assign_enum = lex_sym_assign
enum lex_prefix_enum = lex_keyword_prefix + lex_sym_prefix
enum lex_infix_enum = lex_keyword_infix + lex_sym_infix + lex_sym_assign
enum lex_postfix_enum = lex_sym_postfix

lex_assign_map := enumap lex_assign_enum
lex_prefix_map := enumap lex_prefix_enum
lex_infix_map := enumap lex_infix_enum
lex_postfix_map := enumap lex_postfix_enum

lexmap := [ // order must be in sync with lex_flag
	enumap lex_keyword_ctype
	enumap lex_keyword_cell_type
	lex_prefix_map
	lex_infix_map
	lex_postfix_map
	lex_assign_map
]

fun check_flag flags flag {
	ret ((1 SHL flag) AND flags) > 0
}

fun load_src_str str fname {
	ret {
		text: str, fname: fname
	}
}

fun find_flags token {
	f := 0
	each lexmap
		use item[token] f += 1 SHL index
	ret f
}

fun make_token x y i type s n flag {
	ret {
		i, // character position relative to the start of document
		x, y, // character column and line positions
		type, // enum lex_type
		s, // original string value, must be always present
		n, // numerical value if any
		flag // enum lex_flag: store extra data
	}
}

lex_key := enumap lex_keyword
//init the lex_key it = enumap lex_keyword

//   =================== LEXER ======================
fun new_lexer options
//   ===============================================
{

//$  %% 13

lex_ret := nil
space := nil
start_map := []
tokens := []
id_array := []
id_map := {}
line_no := nil
line_start := nil

//test_flags()

fun test_flags {
	test := [
		['i32' make_flag(lex_flag.ctype)],
		['object' make_flag(lex_flag.cell_type)],
		['not' make_flag(lex_flag.prefix)],
		['->' make_flag(lex_flag.postfix)],
		['++' make_flag(lex_flag.prefix) + make_flag(lex_flag.postfix)],
		['<=' make_flag(lex_flag.infix)],
		['--' make_flag(lex_flag.prefix) + make_flag(lex_flag.postfix)],
		[':=' make_flag(lex_flag.infix) + make_flag(lex_flag.assign)],
		['s' 0]
	]

	each test {
		if find_flags(item[0]) == item[1]
			log item[0] ~green 'ok '
		else
			log item[0] ~red 'error '
	}
}

fun make_flag flag {
	ret 1 SHL flag
}

fun new_token i type s n flag {
	t := make_token(i - line_start, line_no, i, type, s, n, flag)
	ret t
}

//fun sym_group sym {
	//if sym < lex_sym['?'] ret lex_group.infix
////	if sym < lex_sym['?'] ret lex_group.prefix
//}

fun lex_line_comment str i {
	i0 := i
	loop {
		if is_nil(str[i]) or str[i] == 10 {
			inc line_no
			if options.comments {
				comment := str[i0..i]
				tokens += new_token(i0, lex_type.comment, comment, 0, 0)
			}
			inc i
			line_start = i
			ret i
		}
		inc i
	}
}

fun lex_long_comment s i {
	loop {
		if s[i] == 10 inc line_no
		line_start = i
		if s[i] == nil or (s[i] == ord '*' and s[i + 1] == ord '/') {
			ret i + 2
		}
		inc i
	}
}

fun lex_number s a {
	num := chr s[a]
	b := a + 1
	dot_count := 0
	value := 0
	e_count := 0
	if s[b] == ord 'x' {
		num = ''
		inc b
		loop {
			c1 := s[b]
			if c1 == ord '_'
			or (c1 >= ord '0' and c1 <= ord '9')
			or (c1 >= ord 'a' and c1 <= ord 'f')
			or (c1 >= ord 'A' and c1 <= ord 'F') {
				if c1 != ord '_' num += chr c1
				inc b
			}
			else break
		}
		value = parse_int(num 16)
		num = '0x' + num
	}
	else {
		loop {
			c1 = s[b]
			if c1 == ord '_'
			or c1 >= ord '0' and c1 <= ord '9'
			or c1 == ord '.' and s[b+1] != ord '.' and dot_count == 0
			or c1 == ord 'e' and e_count == 0 {
				if c1 == ord '.' inc dot_count
				if c1 == ord 'e' inc e_count
				if c1 != ord '_' {
					num += chr c1
				}
				b += 1
			}
			else break
		}
		if dot_count > 0 or e_count > 0 {
			value = parse_float(num)
		}
		else {
			value = parse_int(num, 0)
		}
	}
	type := lex_type.int
	if dot_count > 0 or e_count > 0 type = lex_type.float
	lex_ret = new_token(a, type, num, value, 0)
	ret b
}

fun lex_string s a {
	b := a
	q := chr s[a]
	loop {
		inc b
		b = find_at(s, q, b)
		if b < 0 or q == '`' break
		x := b - 1
		count_back_slashes := 0
		loop {
			if s[x] == ord '\\' {
				inc count_back_slashes
			}
			else break
			dec x
		}
		if count_back_slashes mod 2 == 0 break
	}
	if b < 0 {
		throw stoplex(ERR.lex, lex_place(s, a), 'unterminated string')
		//b = len s
	}
	else {
		inc b
	}
	s = s[a+1 .. b-2]
	lines := 0
	each s if item == 10 {
		inc lines
		line_start = a - index
	}

	t := ''
	ix := 0
	while ix < len s {
		C := s[ix]
		if C != ord '\\' {
			t += chr C
		}
		else {
			ix += 1
			if ix == len s {
				throw stoplex(ERR.lex, lex_place(s, ix), 'invalid string token')
			}
			if C == ord '\\' t += '\\'
			but C == ord q t += q
			but C == ord 'b' t += '\b'
			but C == ord 'r' t += '\r'
			but C == ord 't' t += '\t'
			but C == ord 'n' t += '\n'
			but C == ord '0' t += '\0'// TODO: Copy newer function from ship-js
			but C == ord '1' t += '\1'
		}
		inc ix
	}
	lex_ret = new_token(a, lex_type.str, t, 0, 0)
	line_no += lines
	ret b
}

fun lex_space str pos {
	i := pos
	while str[pos] <= 32 {
		if str[pos] == ord '\n' {
			inc line_no
			inc pos
			line_start = pos
		}
		else
			inc pos
	}
	if options.space == lex_option_space.token {
		lex_ret = new_token(i, lex_type.space, str[i:pos - i], 0, 0)
		//{ type: lex_type.space }
	}
	but options.space == lex_option_space.property {
		space = str[i:pos - i]
		lex_ret = nil
	}
	else lex_ret = nil
	ret pos
}

fun create_id i id flags {
	last := len id_array
	ix := id_map[id: last] // property set-or-get
	if ix == last {
		id_array += { id }
	}
	ret new_token(i, lex_type.id, id, ix, flags)
}

fun lex_id str pos {
	id := chr str[pos]
	i := pos + 1
	loop {
		if i >= len str @
		char := str[i]
		if char >= ord 'a' and char <= ord 'z'
		or char >= ord 'A' and char <= ord 'Z'
		or char >= ord '0' and char <= ord '9'
		or char == ord '_' {
			id += chr char
			inc i
			cont
		}
		@
	}
	use the lex_key[id] {
		lex_ret = new_token(pos, lex_type.keyword, id, it, find_flags(id))
	}
	else {
		lex_ret = create_id(pos, id, find_flags(id))
	}
	ret i
}

fun init_lexer {
	tokens = []
	start_map[9] = lex_space
	start_map[10] = lex_space
	start_map[32] = lex_space
	start_map[ord '"'] = lex_string
	start_map[ord "'"] = lex_string
	start_map[ord '`'] = lex_string
	f := lex_id
	i := ord 'a'
	while i < ord 'z' {
		start_map[i] = f
		inc i
	}
	i = ord 'A'
	while i < ord 'Z' {
		start_map[i] = f
		inc i
	}
	start_map[ord '_'] = f
	i = ord '0'
	f = lex_number
	while i <= ord '9' {
		start_map[i] = f
		inc i
	}
}

fun lex src {
	if not is_object(src) {
		throw stoplex(ERR.lex, lex_place('<unrecognized data>', 0), 'lex() must be a source object')
	}
	line_no = 0
	line_start = 0
	str := src.text
	init_lexer()
	i := 0
	sy := lex_type.sym
	loop {
		if i >= len str @
		tok := nil

		charmap str, i {
			enum lex_sym
			'//' {
				i = lex_line_comment(str, i)
				cont
			}
			'/*' {
				i = lex_long_comment(str, i)
				cont
			}
			found {
				s := lex_sym[match]
				tokens += new_token(i, lex_type.sym, s, match, find_flags(s))
				i += len s
				cont
			}
		}
		char := str[i]
		use start_map[char] {
			i = start_map[char](str, i)
			use lex_ret {
				tokens += lex_ret
			}
		}
		else {
			// i type group s n flag
			tokens += new_token(i, lex_type.raw, chr char, char, 0)
			inc i
		}
	}

	ret {
		src,
//		lines: src / '\n',
		tokens,
		id_map,
		id_array,
	}
}

ret lex }
//$  %% 13
fun lex_test {
	lexer := new_lexer({space: lex_option_space.property})
	src := load('self/lex.ship')
	log len src
	b := 0
	log 'initilaized'

	try {
		time_begin()
		t := lexer(src)
		cout 'lexer: '
		time_end()
	}
	catch {
		e := error
		if not is_object(e) {
			log 'lexer error:'
			log error
		}
		else {
			cout ~red e.source ' error' ~~ ': ' ~orange e.msg ~~
			log
		}
		kill
	}
	//log t
	log 'tokens parsed:' len t
	t = 'tokens = ' + json(t)
	log save('data.js', t)
	kill
	each t {
		log item
	//	log ~orange lex_type[item.type] ~5 item.s ~~ item.id ~2 item.group
		if index == 10 {
			log '...' len t - 10 'more'
			@
		}
	}
}
