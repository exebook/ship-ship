enum ERR {
	lex parse compile cu cc
}

fun stoplex type cp msg {
	ret {
		magic: 1234,
		cp, // cp can be nill
		type,
		msg
	}
}

fun stop_failed {
	ret '(calculation of details failed)'
}

fun is_proper_error e {
	if is_object(e) and e.debug != nil ret true
	ret false
}

fun show_error e {
	if is_object(e) {
		if e.magic == 1234 {
			use e.cp {
				token := e.cp.lexed.tokens[e.cp.i]
				use token {
					y := token.y
					x := token.x
					src := e.cp.lexed.src.text
					src = (src / '\n')[y]
					t := ''
					while len t < x t += ' '
					t += '^'
					type := ''
					use e.type {
						if e.type == ERR.lex type = 'lexer error'
						if e.type == ERR.parse type = 'parser error'
						if e.type == ERR.compile type = 'compiler error'
						if e.type == ERR.cu type = 'backend error'
						if e.type == ERR.cc type = 'C code generator error'
					}
					log e.cp.lexed.src.fname +':'+ (y+1)
					log src
					log ~red t ~~ type
				}
				else {
					log '* error message has nil token'
				}
				log ~red ~pink e.msg
			}
			else log e.msg
		}
		but e.msg {
			log e.line
			log e.msg
			show_trace(e.stack)
		}
		else log e
	}
	else log e
}

fun lex_place s i {
	ret {
		s, i
	}
}
