/:
incl ioctl
fun tab n s {
	while len s < n s += ' '
	ret s
}

fun columnize o gap {
	W := ioctl.get_stdout_columns()
	a := Columnize(o W.x gap lam x w {
		// in this callback you must render x in exactly w characters (usually by appending spaces)
		ret tab(w x)
	})
	ret a
}
:/

fun Columnize o width gap f {
	fun max_w list {
		re := 0
		each list if len item > re re = len item
		ret re
	}

	fun try_n n { //try to fit into n columns
		C := [[]] // columns
		view_height := ceil(len o / n)
		each o {
			if len C[-1] < view_height C[-1] += item
			else C += [[item]]
		}
		a := []
		each C a += max_w(item)
		real_w := (len a - 1) * gap + reduce(a lam a b = a + b)
		if width < real_w ret ;
		re := [] while len re < view_height re += ''
		gap_s := '' while len gap_s < gap gap_s += ' '

		idx := 0
		each C each item {
			if index 1 > 0 re[index] += gap_s
			re[index] += f(a[index 1] item, idx)
			inc idx
		}

		ret re * '\n'
	}

	x := 10
	while x >= 1 {
		re := try_n(x)
		use re ret re
		dec x
	}
}

