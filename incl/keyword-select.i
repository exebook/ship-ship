fun choose_lexer path first_line_text {
	init first_line_text = ''
	C1 := 'return static break switch case if else while for repeat struct union enum continue const typedef default cell Alloc Free Realloc goto do '

	C2 := 'val leaf unsigned void volatile inline int long double float char free malloc printf ifdef ifndef elif endif define undef sizeof printf short byte int64_t int32_t int8_t int128_t float32_t float64_t uint64_t uint32_t uint8_t uint128_t int16_t uint16_t true false i8 u8 i16 u16 i32 u32 i64 u64 f32 f64 '

	JS1 := 'abstract arguments async boolean break byte case catch char class const continue debugger default delete do double else enum eval export extends false final finally float for function goto if implements import in instanceof int interface let const long native new null package private protected public return short static super switch synchronized this throw throws transient true try typeof var void volatile while with yield constructor of type interface '

	JS2 := 'a b c $ _ console log warn NaN dnaof create kindof me can hand it join ret front back length callee object self await undefined then number string boolean any bigint symbol '

	file_name := pop (path / '/')
	dot := '.' find file_name
	if dot < 0 {
		if ('shi' find first_line_text) > 0 ext = 'ship'
		but ('tf' find first_line_text) > 0 ext = 'ship'
		but ('node' find first_line_text) > 0 ext = 'js'
		but ('cc' find first_line_text) > 0 ext = 'c'
		else ext = 'sh'
	}
	else {
		ext := file_name[dot+1..]
	}

	if ext == 'ship' or ext == 'i' {
		ret ['lam if but ret lam var loop each else while break @ cont halt init '
		+ 'use try catch throw charmap found miss and or mod '
		+ 'error cast tag type '
		+ 'OR AND SHR SHL NOT XOR BIT match enumap '
		+ 'fun name rec union with of c_begin c_end begin end bool val double float char void struct are atom langc do tee orbit uplink trail leave '
		+ C1,

		'$ nil true false item index cle len not plus minus inc dec chr ord put pull push pop find is '
		+ 'var number array string enum object this '
		+ 'incl log cout time_begin time_end kill re the it that cell '
		+ C2]
	}
	but ext == 'c' or ext == 'h' {
		ret [C1, C2]
	}
	but ext == 'js' or ext == 'html' or ext == 'ts' or ext == 'jsx' or ext == 'tsx' {
		ret [JS1, JS2]
	}
	but ext == 'fj' {
		ret [JS1, JS2 + ' front back both include text meta ']
	}
	but ext == 'sh' {
		ret ['case env exit export function getopts hash if import info let local logname read select seq set shift source trap tr true type ulimit umask uname unexpand uniq units unset unshar until which while xargs yes # else done for in do then fi','']
	}
	but ext == 'shame' {
		ret ['incl var ', 'title path scan ext items show ']
	}
	but ext == 'ml' {
		ret [
			'and as assert asr begin class constraint do done downto else end exception external false for fun function functor if in include inherit initializer land lazy let lor lsl lsr lxor match method mod module mutable new object of open or private sig struct then to true try type val virtual when while with'
			'rec nonrec '
		]
	}
	but ext == 'kk' {
		ret [
		'repeat for infix infixr infixl type cotype rectype alias struct con fun function val var external if then else elif return match private public private module import as include inline rec try yield enum interface instance'

		'resource dynamic some linear abstract shallow self noinline exists  extend is rcontrol forall co handle iff effect ambient file initially when in pub resume rcontext c open raw named cs except header finally cont fn control ref handler contro extern reference inject override context postfix mask scoped value behind local js with prefix'
		]
	}
	else ret [JS1 JS2]
}

