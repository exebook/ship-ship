begin
#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>//mkfifo
#include <limits.h> // PATH_MAX
#include <fcntl.h> //O_RDONLY (open)
#include <signal.h>
end

unix := {
	sleep: lam ms {
		begin
			usleep(cell_get($ms) * 1000);
		end
	}

	readdir: lam path {
		re := []
		begin
			DIR *dir;
			struct dirent *entry;
			char *path = ship_pchar($ path);
			dir = opendir(path);
			free(path);
			if (!dir) {
				closedir(dir);
				end
					throw 'diropen error'
				begin
			}
			int skip = 0;
			while ((entry = readdir(dir)) != NULL) {
				if (skip++ < 2) continue; // skip '.' and '..'
				val x = new_string_from_utf(entry->d_name);
				arr_push(&$re, &x);
			};
			closedir(dir);
		end
		ret re
	}

	expand_path: lam path {
		ret path / '~' * unix.getenv('HOME')
	}

	mkdir: lam path {
		R := nil
		begin
			char *p = ship_pchar($ path);
			$ R = cell_let(mkdir(p, 0755));
			free(p);
		end
		ret R
	}

	read_stdin: lam {
		re := []
		begin
			const MAX_SIZE = 1024*1024;
			int n;
			char buf[MAX_SIZE];
			while ((n = read(0, buf, MAX_SIZE)) > 0) {
				push_ref(&$ re);
				expr_push(new_string_from_utf_size(buf, n));
				op_add_to();
			}
			val b = cell_nil();
			$ re = array_join(&$ re, &b);
		end
		ret re
	}

	get_cwd: lam {
		re := nil
		begin
			char *s = get_current_dir_name();
			$ re = new_string_from_utf(s);
			free(s);
		end
		ret re
	}

	open: lam fn m {
		var fd a mr mw mrw
		begin
			$ a = cell_let(O_APPEND|O_WRONLY);
			$ mr = cell_let(O_RDONLY);
			$ mw = cell_let(O_WRONLY);
			$ mrw = cell_let(O_RDWR);
		end

		m = { 'a': a 'r':mr 'w':mw 'rw':mrw }[m]
		if m == nil m = mr

		begin
			char *p = ship_pchar($ fn);
			$ fd = cell_let(open(p, O_NONBLOCK | (int)cell_get($ m)));
			free(p);
		end

		ret fd
	}

	close: lam fd {
		begin
			close(cell_get($ fd));
		end
	}

	write: lam fd str {
		size := len str
		begin
			char *p = ship_pchar($ str);
			write((int)cell_get($ fd), p, (int)cell_get($ size));
			free(p);
		end
	}

	rm: lam path {
		begin
			char *p = ship_pchar($ path);
			unlink(p);
			free(p);
		end
	}

	quick_read_fd: lam fd { //TODO: handle file end/close
		R := nil
		begin
			int fd = cell_get($ fd);
			struct timeval tv = { 0, 0 };
			//tv.tv_sec = 1;
			fd_set set;
			FD_ZERO(&set);
			FD_SET(fd, &set);

			int selected = select(fd + 1, &set, NULL, NULL, & tv);
			if (selected > 0) {
				int SIZE = 0x10000;
				char p[SIZE];
				int n = read(fd, p, SIZE);
				if (n > 0) {
					$ R = new_string_from_utf_size(p, n);
				}
			}
			else {
				$ R = cell_nil();
			}
		end
		ret R
	}

	full_read_fd: lam fd { //TODO: handle file end/close
		R := nil
		loop {
			s := unix.quick_read_fd(fd)
			init s @
			if R == nil R = s
			else R += s
		}
		ret R
	}

	getpid: lam {
		R := -1
		begin $ R = cell_let(getpid()); end
		ret R
	}

	parent_pid_of: lam pid {
		path := '/proc/'+ pid +'/task/'+ pid + '/status'
		try {
			status := load(path)
			status /= '\n'
			each status {
				if item[0:4] == 'PPid' {
					num := pop ((item / ':')[1] / ' ')
					ret parse_int(num)
				}
			}
		}
		catch {
			ret -1
		}
	}

	get_all_parents_of: lam pid {
		R := []
		loop {
			pid = unix.parent_pid_of(pid)
			if pid <= 0 @
			R += pid
		}
		ret R
	}

	create_fifo: lam fifo {
		init fifo throw 'create_fifo needs a string'
		begin
			char *p = ship_pchar($ fifo);
			mkfifo(p, 0666);
			free(p);
		end
	}

	close: lam fd {
		begin
			close((int) cell_get($ fd));
		end;
	}

	exists: lam path {
		init path ret false
		R := nil
		begin
			char *path = ship_pchar($ path);
			struct stat statbuf;
			$ R = cell_let(stat(path, &statbuf));
			free(path);
		end
		ret R >= 0
	}

	getenv: lam variable {
		init variable ret false
		R := nil
		begin
			char *p = ship_pchar($ variable);
			char *value = getenv(p);
			if (value) {
				$ R = new_string_from_utf(value);
			}
			free(p);
		end
		ret R
	}

	setenv: lam id value {
		init id ret
		begin
			char *id = ship_pchar($ id);
			char *value = ship_pchar($ value);
			setenv(id, value, 1);
			free(id);
			free(value);
		end
	}

	stat: lam fn {
		var st_mode st_size st_atim st_mtim st_ctim at mt ct is_dir is_file
		begin
			struct stat s;
			char *p = ship_pchar($ fn);
			int ok = stat(p, &s);
			free(p);
			if (ok != 0) {
				end
					ret
				begin
			}
			$ st_mode = cell_let(s.st_mode);
			$ st_size = cell_let(s.st_size);
			$ at = cell_let(*(u64*)&s.st_atim);
			$ mt = cell_let(*(u64*)&s.st_mtim);
			$ ct = cell_let(*(u64*)&s.st_ctim);
			$ st_atim = new_string_from_utf(ctime(&s.st_atim));
			$ st_mtim = new_string_from_utf(ctime(&s.st_mtim));
			$ st_ctim = new_string_from_utf(ctime(&s.st_ctim));
                      			$ is_dir = cell_let(S_IFDIR & s.st_mode ? 1 : 0);
			$ is_file = cell_let(S_IFREG & s.st_mode ? 1 : 0);
		end
		ret {
			mode: st_mode
			size: st_size
			atime: st_atim[..-2]
			mtime: st_mtim[..-2]
			ctime: st_ctim[..-2]
			at, mt, ct,
			is_dir, is_file,
		}
	}

	epoch: lam {
		R := nil
		begin
			$ R = cell_let(time(0));
		end
		ret R
	}

	realpath: lam p {
		re := nil
		begin
			char *r = malloc(PATH_MAX);
			realpath(ship_pchar($ p), r);
			if (r) {
				$ re = ship_str(r);
			}
			free(r);
		end
		ret re
	}

	fork: lam {
		r := nil
		begin
			int r = fork();
			$ r = cell_let(r);
		end
		ret r
	}

	kill: lam pid sig {
		r := nil
		if sig == 'TERM' { begin $sig = cell_let(SIGTERM); end }
		if sig == 'KILL' { begin $sig = cell_let(SIGKILL); end }
		if sig == 'STOP' { begin $sig = cell_let(SIGSTOP); end }
		if sig == 'CONT' { begin $sig = cell_let(SIGCONT); end }
		if sig == 'USR1' { begin $sig = cell_let(SIGUSR1); end }
		if sig == 'USR2' { begin $sig = cell_let(SIGUSR2); end }
		begin
			int pid = cell_get($ pid), sig = cell_get($ sig);
			int r = kill(pid, sig);
			$ r = cell_let(r);
		end
		ret r
	}

	wait: lam {
		begin
			wait(0);
		end
	}

	testpid: lam pid { // 0:keep waiting -1: terminated PID: terminated

		re := nil
		begin
			int pid = cell_get($ pid);
			int re = waitpid(pid, -1, WNOHANG);
			$ re = cell_let(re);
		end
		ret re
	}

	isTTY: lam fd {
		init fd = 0
		re := 0
		begin
			$ re = cell_let(isatty(cell_get($ fd)));
		end
		ret re
	}

	daemon: lam {
		log 'wtf'
		begin
			printf("go\n");
			int e = daemon(1, 0);
			printf("e: %i\n", e);
		end
	}
}

//try {
//	a := unix.readdir('/v/js/ship/speed1')
//}
//catch {
//	log 'error' (error).param
//}
//b := unix.readdir('/v/js/ship/speed')
//log b

