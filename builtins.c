val __ship_runtime_round(__arg_decl) {
	if (__arg_count != 1) __throw(__str("round expects argument"));
	val X = __arg(0);
	if (!__is_num(X)) __throw(__str("round expects a number"));
	//double x = (double)__get(X);
	//return __let_i((int)round(x));
	return __let_i((int)(__get(X) + 0.5));
}

val __ship_runtime_floor(__arg_decl) {
	if (__arg_count != 1) __throw(__str("floor expects argument"));
	val X = __arg(0);
	if (!__is_num(X)) __throw(__str("floor expects a number"));
	double x = (double)__get(X);
	//return __let_i((int)floor(x));
	return __let_i((int)x);
}

val __ship_runtime_ceil(__arg_decl) {
	if (__arg_count != 1) __throw(__str("ceil expects argument"));
	val X = __arg(0);
	if (!__is_num(X)) __throw(__str("ceil expects a number"));
	double x = (double)__get(X);
	return __let_i((double)(int)x == x ? (int)x : (int)x + 1);
}

val __ship_runtime_abs(__arg_decl) {
	if (__arg_count != 1) __throw(__str("abs expects argument"));
	val X = __arg(0);
	if (!__is_num(X)) __throw(__str("abs expects a number"));
	double x = (double)__get(X);
	return __let(x >= 0 ? x : -x);
}

val __ship_runtime_time_ms(__arg_decl) {
	//TODO 64bit
	return __let((double)__time_ms());
}

val __ship_runtime_push(__arg_decl) {
	return __array_push(__arg(0), __arg(1));
}

val __ship_runtime_put(__arg_decl) {
	return __array_put(__arg(0), __arg(1));
}

val __ship_runtime_pop(__arg_decl) {
	return __array_pop(__arg(0));
}

val __ship_runtime_pull(__arg_decl) {
	return __array_pull(__arg(0));
}

val __ship_runtime_parse_num(__arg_decl) {
	int base = 10;
	val v = __arg(1);
	if (__is_num(v)) base = __get(v);
	return __parse_num(__arg(0), base);
}

val __ship_runtime_trim(__arg_decl) {
	return __trim(__arg(0));
}

val __ship_runtime_find(__arg_decl) {
	__size_t at = __is_num(__arg(2)) ? __get(__arg(2)) : 0;
	val re = __find(__arg(0), __arg(1), at);
	return re;
}

val __ship_runtime_type_name(__arg_decl) {
	return __type_of(__arg(0));
}

val __ship_runtime_is_number(__arg_decl) {
	return __is_num(__arg(0));
}

val __ship_runtime_is_string(__arg_decl) {
	return __is_string(__arg(0));
}

val __ship_runtime_is_array(__arg_decl) {
	return __is_array(__arg(0));
}

val __ship_runtime_is_object(__arg_decl) {
	return __is_object(__arg(0));
}

val __ship_runtime_is_fun(__arg_decl) {
	return __is_fun(__arg(0));
}

val __ship_runtime_is_nil(__arg_decl) {
	return __is_nil(__arg(0));
}

val __ship_runtime_shame(__arg_decl) {
	return __shame(__arg(0), __arg(1));
}

val __ship_runtime_honor(__arg_decl) {
	return __honor(__arg(0));
}

val __ship_runtime_sort(__arg_decl) {
	__sort(__arg(0), __arg(1));
	return __arg(0);
}

val __ship_runtime_charcode_at(__arg_decl) {
	__size_t x = 0;
	if (!__is_nil(__arg(1))) x = __get_i(__arg(1));
	return __let_i(__charcode_at(__arg(0), x));
}

val __ship_runtime_force_gc(__arg_decl) {
	printf("force_gc()\n");
	__gc_mark_release();
	return __nil;
}

val __ship_runtime_error_trace(__arg_decl) {
	return __last_stack_trace;
}

val __ship_runtime_stack_trace(__arg_decl) {
	return __trace();
}

val __ship_runtime_enable_trace(__arg_decl) {
	__global.trace_enabled = __get_i(__arg(0));
	return __nil;
}

val __ship_runtime_show_runtimes(__arg_decl) {
	__global.trace_runtimes = __get_i(__arg(0));
	return __nil;
}

val __ship_runtime_fcolor(__arg_decl) {
	return __fcolor(__get_i(__arg(0)), __arg(1));
}

val __ship_runtime_bcolor(__arg_decl) {
	return __bcolor(__get_i(__arg(0)), __arg(1));
}

val __ship_runtime_system(__arg_decl) {
	return __str(__call_system(__pchar(__arg(0))));
}

val __ship_runtime_load(__arg_decl) {
	return __load(__arg(0));
}

val __ship_runtime_save(__arg_decl) {
	__save(__arg(0), __arg(1));
	return __nil;
}

val __ship_runtime_delete(__arg_decl) {
	return __property_delete(__arg(0), __arg(1));
}

val __ship_runtime___try_stack(__arg_decl) {
	return __stack_count(try);
}

val __ship_runtime_keys(__arg_decl) {
	return __object_keys(__arg(0));
}

val __ship_runtime_values(__arg_decl) {
	return __object_values(__arg(0));
}

val __ship_runtime_bind(__arg_decl) {
	//printf("__arg_count: %i\n", (int)__arg_count);
	__enter();
	val a = __args;
	val *p = &__leaf(a)->p[2];
	val re = __bind(__arg(0), __arg(1), __arg_count - 2, p);
	__leave();
	return re;
}

char **__pchar_list(val list) {
	int L = __len(list);
	char **ss = malloc(sizeof(int*) * L + 1);
	__iterate_array(my, list) {
		ss[my_index] = __pchar(my_items[my_index]);
	}
	ss[L] = 0;
	return ss;
}

val __ship_runtime_execa(__arg_decl) {
	char **ss = __pchar_list(__arg(0));
	execvp(ss[0], ss);
	return __nil;
}

val __ship_runtime_argv(__arg_decl) {
	return __argv();
}

//fun load_bytes fn { TODO add this
	//re := []
	//size := nil
	//langc {
		//int fd = open(__pchar($ fn), O_NONBLOCK | O_RDONLY);
		//struct stat s;
		//stat(__pchar($ fn), &s);
		//int size =  s.st_size;
		//unsigned char *p = malloc(size);
		//read(fd, p, size);
		//$ size = __let(size);
	//}
	//re[size-1] = 1
	//each re item = langc { p[__get_i($ index)] }
	//ret re
//}

//fun save_bytes data fn {
	//size := len data
	//langc { unsigned char *p = malloc(__get_i($ size)); }
	//each data langc { p[__get_i($ index)] = __get_i($ item); }
//
	//langc {
		//FILE *f = fopen(__pchar($ fn), "wb");
		//if (f) {
			//rewind(f);
			//fwrite(p, 1, __get_i($ size), f);
			//fclose(f);
		//}
	//}
//}


// make map() builtin