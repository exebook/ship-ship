#define _GNU_SOURCE // for string.h (works on Mac as well)
#include <string.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>
#include <math.h>
#include <setjmp.h>
#include <malloc.h> // for malloc_usable_size
#ifndef __USE_GNU
    #define __MUSL__
#endif
#ifndef __TINYC__
#ifndef __MUSL__
#	include <execinfo.h>
#endif
#endif

#	if defined(__x86_64__) || defined(_M_X64)
		int CALL_CPU_INSTRUCTION_SIZE = 5;
#	else
		int CALL_CPU_INSTRUCTION_SIZE = 3;
#	endif
#	ifdef __clang__
#		define COMPILER_NAME "Clang"
#		if __has_builtin(__builtin_add_overflow)
#			define BUILTIN_OVERFLOW 1
#			define __add_overflow __builtin_add_overflow
#			define __sub_overflow __builtin_sub_overflow
#			define __mul_overflow __builtin_mul_overflow
#			define __ctz __builtin_ctz
#		endif
#		define _Ignore_array_bounds \
			_Pragma("clang diagnostic push") \
			_Pragma("clang diagnostic ignored \"-Warray-bounds\"")
#		define _Unignore_array_bounds \
			_Pragma("clang diagnostic pop")

#	elif defined(__GNUC__)
#		define COMPILER_NAME "GCC"
#		define GCC_VERSION (__GNUC__*10000+__GNUC_MINOR__*100+__GNUC_PATCHLEVEL__)
#		if GCC_VERSION >= 50100
#			define __add_overflow __builtin_add_overflow
#			define __sub_overflow __builtin_sub_overflow
#			define __mul_overflow __builtin_mul_overflow
#			define __ctz __builtin_ctz
#		endif
#		define _Ignore_array_bounds
#		define _Unignore_array_bounds
#	elif defined(__TINYC__)
#		define COMPILER_NAME "TinyC"
#		define __builtin_prefetch(x)
#		define _Pragma(x)
#		define _Ignore_array_bounds
#		define _Unignore_array_bounds
#	else
#		define COMPILER_NAME "Unknown"
#	endif
#	ifndef __add_overflow
#		define __add_overflow(a, b, c) ({ \
			int64_t d = (int64_t)a + (int64_t)b; \
			*c = d; \
			d < INT32_MIN || d > INT32_MAX; \
		})
#		define __sub_overflow(a, b, c) ({ \
			int64_t d = (int64_t)a - (int64_t)b; \
			*c = d; \
			d < INT32_MIN || d > INT32_MAX; \
		})
#		define __mul_overflow(a, b, c) ({ \
			int64_t d = (int64_t)a * (int64_t)b; \
			*c = d; \
			d < INT32_MIN || d > INT32_MAX; \
		})
#	endif
#	ifndef __ctz

	char __ctz(int32_t x) {
		char c = 0;
		while (x > 0) {
			if ((x & 1) == 0) c++;
			else
				break;
			x >>= 1;
		}
		return c;
	}
#endif

#define __is_type(a, t) __builtin_types_compatible_p(typeof(a), t)
#define __int_ternary(n, a, b) __builtin_choose_expr(__is_type(n, int), (a), (b))
#define __int32_ternary(n, a, b) __builtin_choose_expr(__is_type(n, int32_t), (a), (b))
#define false 0
#define true 1
#define bool int

