val __flyvar(val initial_value) {
	leaf *l = leaf_new(leaf_fly);
	l->fly = initial_value;
	return __mk_val(l);
}

val __closure(__lam_t f, __size_t size, val *env) {
	leaf *l = leaf_new(leaf_fun);
	l->size = size;
	l->f = f;
	int struct_size = sizeof(__env_t) + sizeof(val) * size;
	l->env = Alloc(struct_size);
	l->env->bind = 0;
	for (int i = 0; i < size; i++) {
		l->env->p[i] = env[i];
	}
	return __mk_val(l);
}

//val __bind(val f_src, val this, __size_t args_size, val args[]) {
val __bind(val f_src, val this, __size_t args_size, val *args) {
	leaf *S = __leaf(f_src);
	leaf *D = leaf_new(leaf_fun);
	__size_t env_size = S->size;
	val *env = S->env->p;
	D->f = S->f;
	D->size = env_size;
	int struct_size = sizeof(__env_t) + sizeof(val*) * (env_size + 1 + args_size);
	D->env = Alloc(struct_size);
	D->env->bind = 1 + args_size;
	int d = 0;
	val *p = D->env->p;
	while (d < env_size) {
		p[d] = env[d];
		d++;
	}
	p[d++] = this;
	for (int i = 0; i < args_size; i++) {
		p[d++] = args[i];
	}
	return __mk_val(D);
}

//val __closure_call_n(val closure, val this, __size_t size, val args[]) {
val __closure_call_n(val closure, val this, __size_t size, val *args) {
	if (!__is_fun(closure)) {
		__throw(__tem(E025_1, __type_of(closure)));
	}
	leaf *l = __leaf(closure);
	if (l->env->bind) {
		this = l->env->p[l->size];
		int new_size = l->env->bind - 1 + size;
		int struct_size = sizeof(val*) * new_size;
		val *tmp = alloca(struct_size);
		int d = 0;
		while (d < size) {
			tmp[d] = l->env->p[l->size + 1 + d];
			d++;
		}
		for (int i = 0; i < size; i++) {
			tmp[d] = args[i];
			d++;
		}
		return __call_v(l->f, l->env, this, new_size, tmp);
		// maybe store bind/this separately allowing only one of them to be present?
	}
	return __call_v(l->f, l->env, this, size, args);
}

//val __member_call_n(val object, val index, __size_t size, val args[]) {
val __member_call_n(val object, val index, __size_t size, val *args) {
	return __closure_call_n(__property_get(object, index), object, size, args);
}

//#define __this this
#define __args ({ val a = __array(); for (__size_t i = 0; i < __arg_count; i++) __array_push(a, __argn(i)); a; })
#define __call(f, a...) __call_n(f, 0, 0, sizeof((val[]){ a })/sizeof(val), a)
#define __call_ex(f, this, e, a...) __call_n(f, e, this, sizeof((val[]){ a })/sizeof(val), a)
#define __closure_call(valf, a...) __closure_call_n(valf, __nil, sizeof((val[]){ a })/sizeof(val), (val[]){ a })

/*
	a := x                   __flyvar(x)
	a = x                    __ref(a) = x
	__closure                (f [a b c])    new { f [a b c] }
	__closure_call(C args)   C.f(C.env args)
	__env(n)                __ref(env[n])
	__ref(a)                 (*__leaf(a)->ref)

	L   a = b
	E   __env(n) = __env(m)
	A   __arg(n) = __arg(m)
	F   __ref(n) = __ref(m)
*/


