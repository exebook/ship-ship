val __type_of(val a) {
	return __str(__p_typeof(a));
}

__size_t __len(val a) {
	if (__is_string(a) || __is_array(a)) return __leaf(a)->size;
	else if (__is_object(a)) return __object_size(a);

	__throw(__tem(E011_1, __type_of(a)));
	return 0;
}

val __multi(val src, int index) { //multi destination assignment
	return __is_array(src) ? __array_get(src, index) : src;
}

val __bit_get(val x, val index) {
	return __is_int(x) ? (x & (1 << index)) != 0 : __nil;
}

void __bit_set(val *x, val index, val item) {
	if (__is_int(*x) && __is_int(index) && __is_int(item)) {
		*x ^= (-(!!item) ^ *x) & (1UL << index);
	}
}

val __index_get(val object, val index) {
	if (__is_heap(object)) {
		leaf *l = __leaf(object);
		switch (l->type) {
			case leaf_arr: {
				return __array_get(object, __get(index));
			}
			case leaf_str: {
				return __char_at(object, __get(index));
				//return __let_i(__charcode_at(object, __get(index))); // old ship behavior
			}
			case leaf_obj: {
				return __property_get(object, index);
			}
			default:
				__throw(__tem(E005_1, __type_of(object)));
		}
	}
	else {
		__throw(__str(E006_0));
	}
	return 0;
}

void __index_set(val object, val index, val item) {
	if (__is_heap(object)) {
		leaf *l = __leaf(object);
		switch (l->type) {
			case leaf_arr:
				if (!__is_num(index)) __throw(__str(E004_0));
				*__array_at(object, __get(index)) = item;
				return ;
			case leaf_obj:
				__property_set(object, index, item);
				return ;
			default:
			__throw(__tem(E008_1, __type_of(object)));
		}
	}
	else {
		__throw(__tem(E008_1, __type_of(object)));
	}
}

val __val_add(val a, val b) {
	if (!__is_heap(a)) {
		if (__is_string(b)) {
			return __concat(__stringize_num(a), b);
		}
		else if (__is_fly(b))
			__throw(__str("internal error: __val_add->__is_fly#1"));
			//__val_add(a, __leaf(b)->fly);
		__throw(__tem(E009_2, __type_of(b), __type_of(a)));
	}

	if (__is_fly(b)) __throw(__str("internal error: __val_add->__is_fly#2"));
		//return __val_add(a, __leaf(b)->fly);

	leaf *A = __leaf(a);
	switch (A->type) {
		case leaf_arr: {
			val c = __array_allocopy(A->size, A->size ? __array_at(a, 0) : 0);
			return (__is_heap(b) && __leaf(b)->type == leaf_arr)
				? __array_concat(c, b)
				: __array_concat(c, __array(b));
		}
		case leaf_obj: {
			if (__is_object(b)) return __object_merge(a, b);
			__throw(__tem(E010_1, __type_of(a)));
		}
		case leaf_str: {
			if (__is_num(b)) b = __stringize_num(b);
			return __rooted(__string_concat(a, b));
		}
		case leaf_fly: {
			__throw(__str("internal error: __val_add->__is_heapvar#3"));
			//return __val_add(__leaf(a)->ref, b);
		}
		default: {
			__throw(__tem(E010_1, __type_of(a)));
		}
	}
	return a;
}

val __val_sub(val a, val b) {
	__throw(__tem(E009_2, __type_of(b), __type_of(a)));
	return __nil;
}

val __val_div(val a, val b) {
	if (!__is_heap(a)) {
		__throw(__str("operator div(/): unimplemented combination of operands"));
	}
	leaf *A = __leaf(a);
	switch (A->type) {
		case leaf_arr: {}
		case leaf_str: {
			if (__is_num(b)) b = __stringize_num(b);
			return __split(a, b);
		}
		default:
			__throw(__str("operator div(/): unknown left operand type"));
	}
	return a;
}

val __val_mul(val a, val b) {
	if (!__is_heap(a)) {
		__throw(__tem(E018_2, __type_of(b), __type_of(a)));
	}
	leaf *A = __leaf(a);
	switch (A->type) {
		case leaf_arr: {
			return __array_join(a, b);
		}
		case leaf_str: {}
		default: {
			//__log(a, __type_of(a));
			__throw(__str("operator mul(*): unknown left operand type"));
		}
	}
	return a;
}


// ---------------- _on, mutating math operations ----------------------------

val __val_add_on(val a, val b) { // a += b
	if (__is_heap(a)) {
		leaf *A = __leaf(a);
		switch (A->type) {
			case leaf_arr: {
				if (__is_heap(b) && __leaf(b)->type == leaf_arr) {
					__array_append(a, b);
				}
				else {
					__array_push(a, b);
				}
				return a;
			}
			case leaf_str: {
				return __concat(a, b);
			}
		}
	}
	else {
		if (__is_int(a) && __is_string(b)) {
			return __concat(__stringize_num(a), b);
		}
	}
	__throw(__tem(E009_2, __type_of(b), __type_of(a)));
	return __nil;
}

val __val_sub_on(val a, val b) {
	__throw(__tem(E009_2, __type_of(b), __type_of(a)));
	return __nil;
}

val __val_mul_on(val a, val b) {
	if (__is_heap(a)) {
		leaf *A = __leaf(a);
		switch (A->type) {
			case leaf_arr: {
				leaf *c = __leaf(__array_join(a, b));
				__mutate(A, c);
				return a;
			}
		}
	}
	__throw(__tem(E009_2, __type_of(b), __type_of(a)));
	return __nil;
}

val __val_div_on(val a, val b) {
	if (__is_heap(a)) {
		leaf *A = __leaf(a);
		switch (A->type) {
			case leaf_str: {
				if (__is_num(b)) b = __stringize_num(b);
				return __split(a, b);
			}
		}
		return a;
	}
	__throw(__tem(E009_2, __type_of(b), __type_of(a)));
	return __nil;
}

val __enum_reflect(__enum *e, val ix) {
	if (__is_num(ix)) {
		unsigned int index = __get_i(ix);
		for (unsigned int i = 0; i < e->size; i++) {
			if (index == e->kv[i].key) {
				return __str(e->kv[i].value);
			}
		}
		__throw(__tem(E023_1, ix));
	}
	else
		__throw(__tem(E024_1, __type_of(ix)));
	return __nil;
}
