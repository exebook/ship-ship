//x := 0
//y := x
//rec b { q w }
//rec a { x y }
//a := { x: 1 y: 2 }
//b := a
//log b
//
//
//rec move { x y }
//rec turn { degree }
//union cmd {
	//move
	//turn
//}
//
//cmds := [] // ty += array
//move := { x: 1 y: 2 }
//cmd := move
//cmds += move // ty += [cmd]


enum free E { a b }

//log E[E.c] E[E.a] E[E.b]
log E.c
//c a b
//2 0 1


//code free-enum2 enum
//enum free E { a b }
//
//log E.c E.a E.b
//log E['d']
//log E.a E.b E.c E.d
//output
//2 0 1
//3
//0 1 2 3