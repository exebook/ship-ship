#define STACKS_CHECK

#define __stack_size(a) a ## _stack_size
#define __stack_base(a) a ## _stack_base
#define __stack(a) a ## _stack

#define __declare_stack(_type_, name, size) \
	int __max_ ## name = 0 ;\
	const int __stack_size(name) = size; \
	_type_ __stack_base(name)[size]; \
	_type_ *__stack(name) = &__stack_base(name)[size];

#define __stack_count(a) ((__stack_base(a) + __stack_size(a) - __stack(a))) // correct, do not divide by sizeof(ptr)

#ifdef STACKS_CHECK
void __stacks_debug() {
	printf("qqq\n");
	printf("limit\n"); __show_trace(); exit(0); \
}

#define __stack_push(to, what) \
	if(__stack_base(to) < __stack(to)) { \
		*(--__stack(to)) = what; \
		if (__stack_count(to) == 9999999) { __stacks_debug(); }; \
		if (__max_ ## to < __stack_count(to))__max_ ## to = __stack_count(to); \
	} \
	else { \
		printf("fatal error: "#to"_stack[%i] overflow\n", __stack_size(to)); \
		__stack_clear(to);\
		__show_trace();\
		exit(1);\
	};
#else
	#define __stack_push(to, what) *(--__stack(to)) = what;
#endif

#define __stack_pop(from) (*(__stack(from)++))
#define __stack_clear(a) __stack(a) = &__stack_base(a)[__stack_size(a)]

__declare_stack(val, this, 1000)
__declare_stack(val, expr, 100000)
__declare_stack(__Try, try, 1000)
#undef __declare_stack










