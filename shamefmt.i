#!/usr/bin/env shi
//attempt to create a proper json formatter/wrapper
fun shamefmt j {
	prnfmt(90 tt(j) '')
	fun tt a {
		if is_string(a) ret '"'+a+'"'
		if is_number(a) ret ''+a
		if is_array(a) {
			s := ['[']
			e := []
			each a e += tt(item)
			s += [e]
			s += ']'
			ret s
		}
		if is_object(a) {
			s = ['{']
			e = []
			each a {
				if cle == 'pos' cont
				e += cle + ':'
				if cle == 'type' e += types[item]
				else e += tt(item)
			}
			s += [e]
			s += '}'
			ret s
		}
		throw 'n/a'
	}

	fun superflat x {
		if is_string(x) ret x
		re := []
		each x re += superflat(item)
		ret re * ' '
	}
	fun prnfmt  w j indent {

		if is_string(j) {
			log indent + j
			ret
		}


		s := superflat(j)
		if len indent + len s < w {
			log indent + s
			ret
		}

		q := []
		each j {
			if is_string(item) {
				if len indent + len (q * ' ') + len item < w {
					q += item
					cont
				}
				else {
					log indent + q * ' '
					q = []
				}
				log indent + item
			}
			else {
					log indent + q * ' '
					q = []
				prnfmt(w-2, item, indent + '  ')
			}
		}
		log indent + q * ' '
	}
}

//j := tt(honor(load('json')))
//j = tt([1 2 {a:3 b:4 }])
//j := tt([1 2 3])
//log ~green j
//log ww(90 j '')


//[{ block: [ { target: [ { id: x pos: 0 type: 3 } ] expr: { num: 6 pos: 2 type: 1 } pos: 1 type: 7 } { block: [ { args: [ { str: one pos: 8 type: 2 } ] pos: 7 type: 8 } ] expr: { pos: 4 type: 17 stack: [ { id: x pos: 4 type: 3 } { num: 1 pos: 6 type: 1 } { pos: 5 type: 18 op: 8 } ] } else: { block: [ { block: [ { args: [ { str: two pos: 15 type: 2 } ] pos: 14 type: 8 } ] expr: { pos: 11 type: 17 stack: [ { id: x pos: 11 type: 3 } { num: 2 pos: 13 type: 1 } { pos: 12 type: 18 op: 8 } ] } else: { block: [ { args: [ { str: wtf pos: 18 type: 2 } ] pos: 17 type: 8 } ] pos: 17 type: 15 } pos: 11 type: 13 } ] pos: 10 type: 15 } pos: 4 type: 13 } ] pos: 0 type: 6 }]
