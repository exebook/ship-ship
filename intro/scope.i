incl cc map flatten comp_er

ast := { type: 'fun' id: 'main' block: [
	{ type: 'def' id: 'x' val: 5 }
	{ type: 'def' id: 'y' val: 7 }
	{ type: 'fun' id: 'D' block: [
		{ type: 'def' id: 'z' val: 9 }
		{ type: 'log' id: 'x' }
		{ type: 'call' id: 'E' }
		{ type: 'fun' id: 'E' block: [ { type: 'log' id: 'z' } { type: 'log' id: 'y' } ] }
	] }

	{ type: 'fun' id: 'C' block: [
		{ type: 'def' id: 'x' val: 3 }
		{ type: 'fun' id: 'E' block: [ { type: 'log' id: 'x' } ] }
		{ type: 'fun' id: 'A' block: [ { type: 'log' id: 'x' } ]}
		{ type: 'fun' id: 'B' block: [
			{ type: 'def' id: 'x' val: 10 }
			{ type: 'call' id: 'A' }
			{ type: 'call' id: 'D' }
		]}
		{ type: 'call' id: 'B' }
	]}
	{ type: 'call' id: 'C' }
]}

//ast = {
//type: 'fun' id: 'main' block: [
	//{ type: 'def' id: 'a' val: 5 }
	//{ type: 'def' id: 'b' val: 7 }
	//{ type: 'fun' id: 'A' block: [ { type: 'log' id: 'a' } { type: 'call' id: 'B' } ]}
	//{ type: 'fun' id: 'B' block: [ { type: 'log' id: 'b' } { type: 'call' id: 'A' }]}
//]}

//ast = { type: 'fun' id: 'main' block: [
	//{ type: 'def' id: 'x' val: 5 }
	//{ type: 'fun' id: 'D' block: [
		//{ type: 'def' id: 'y' val: 7 }
		//{ type: 'call' id: 'E' }
		//{ type: 'fun' id: 'E' block: [
			//{ type: 'def' id: 'z' val: 9 }
			//{ type: 'call' id: 'F' }
			//{ type: 'fun' id: 'F' block: [
				//{ type: 'log' id: 'x' }
				//{ type: 'log' id: 'y' }
				//{ type: 'log' id: 'z' }
			//]}
		//]}
	//]}
	//{ type: 'call' id: 'D' }
//]}

/:
main: x^
D: y^ main.x
E: z^ main.x D.y
F: main.x D.y E.z

x := 5
fun D {
	y := 7
	E()

	fun E {
		z := 9
		F()
		fun F {
			log x y z
		}
	}
}
D()
:/

fun sy_block up block {
	each block {
		if item.type == 'fun' item = sy_fun(up item)
		but item.type == 'def' item = sy_def(up item)
		but item.type == 'log' item = sy_log(up item)
		but item.type == 'call' item = sy_call(up item)
	}
	ret lam m {
		each block
	}
}


fun show_fun h locals env calls {
	id := h('fuid')
	a := []
	each locals {
		addr := ''
		if item.type == 'local' or item.type == 'fly' addr = ':'+item.stack_addr+''
		a += cle + ' ' + color(1 item.type) + addr
	}
	b := []
	each env {
		b += cle + ':'+index+''
	}
	log ~blue 'fun' id '{' ~~ a*'  ' ~blue '}' b*' ' ~green keys(calls)*' '
}

fuids := {}
fun new_fuid id {
	if the fuids[id] == nil { it = 0 ret id }
	it += 1
	ret id + it
}

fun sy_fun up node {
	refs := {}
	calls := {}
	locals := {}
	env := {}
	stack_size := 0
	env_size := 0
	recursing := 0
	fuid := new_fuid(node.id)

	up('fun' handler)
	block := sy_block(handler node.block)
	up('after' after)

	each locals {
		d := item
		if d.type == 'local' {
			d.stack_addr = stack_size
			stack_size += 1
		}
	}

	fun make_path {
		re := up('make_path')
		re[fuid] = 1
		ret re
	}

	fun need id {
		if locals[id] {
			if locals[id].type != 'fly' locals[id].type = 'fly'
			ret { id, uid: fuid + '.' + id, fuid }
		}
		else {
			e := up('need' id)
			env[e.uid] = e
			ret e
		}
	}

	fun need_uid uid {
		each locals {
			id := cle
			cur_uid := fuid + '.' + id
			if cur_uid == uid {
				locals[id].type = 'fly'
				ret { id, uid, fuid }
			}
		}
		e := up('need_uid' uid)
		env[e.uid] = e
		ret e
	}

	fun scan_env path {
		ee := {}
		each env use path[item.fuid] ee[cle] = 1

		each calls {
			a := handler('find' cle)
			if a.type == 'fun' {
				e := a.handler('scan_env' path)
				each e ee[cle] = 1
			}
			else throw 'scan_env not fun'
		}
		ret ee
	}

	fun after stage {
		if stage == 0 scope_analysis()
		but stage == 1 call_analysis()
	}

	fun scope_analysis {
		each refs {
			init locals[cle] {
				e := need(cle)
				e.env_addr = env_size
				env_size += 1
				env[e.uid] = e
			}
		}
	}

	fun call_analysis {
		ee := {}
		each env ee[cle] = 1
		each calls {
			recursing = 1
			a := handler('find' cle)
			if a.type == 'fun' {
				mp := up('make_path')
				e := a.handler('scan_env' mp)
				each e ee[cle] = 1
			}
			else throw 'call is not to a fun'
			recursing = 0
		}
		each ee {
			e = need_uid(cle)
		}
		show_fun(handler locals env calls)
	}

	fun handler u a {
		if u == 'id' ret node.id
		but u == 'fuid' ret fuid
		but u == 'ref' refs[a] = 1
		but u == 'call' calls[a] = 1
		but u == 'def' locals[a] = { type: 'local' id: a }
		but u == 'fun' locals[a('id')] = { type: 'fun' id: a('id') handler: a }
		but u == 'need' ret need(a)
		but u == 'need_uid' ret need_uid(a)
		but u == 'after' ret up('after' a)
		but u == 'find' {
			if locals[a] ret locals[a]
			ret up('find' a)
		}
		but u == 'scan_env' {
			if not recursing ret scan_env(a)
			else log 'recursion ignore in' node.id a
			ret {}
		}
		but u == 'make_path' ret make_path()
	}

}

fun sy_def up node {
	up('def' node.id)
	ret lam m {
	}
}

fun sy_log up node {
	up('ref' node.id)
	ret lam m {
		ret c_log(c_con("111"))
	}
}

fun sy_call up node {
	up('call' node.id)
	ret lam m {
		ret c_rem("call " + node.id)
	}
}

afters := []
fun handler u a {
	if u == 'after' afters += a
	but u == 'level' ret 0
	but u == 'make_path' ret {}
}
sy_fun(handler ast)
each afters item(0)
each afters item(1)
each afters item(2)

//log shame(ast { indent: 3 json_mode: true })

/: x := 5 y := 7
fun D { z := 9 log x E() fun E { log z y }}
fun C {
	x := 3
	fun E { }
	fun A { log x }
	fun B { x := 10 A() D() }
	B()
}
C()
  -  -  -
fun main: x^ y^
fun D: z^ main.x main.y
fun E: D.z main.y
fun A: C.x
fun E1:
fun B: x C.x main.x main.y
fun C: x^ main.x main.y

main {
	D { E }
	C { E A B }
}

main {
	D { E }
	C {
		B{ A D }
		B
	}
	C
}

x := 1 y := 2
fun A { log x B() }
fun B { log y A() }

x := 5
fun D {
	y := 7
	E()

	fun E {
		z := 9
		F()
		fun F {
			log x y z
		}
	}
}
D()

:/


