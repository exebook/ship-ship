/:
	this version was created before the much simpler idea came to my mind
	ship.js used to pass values and compute expressions on expr_stack
	this was super easy to implement originally for ship.js
	but then I thought that some expressions can be calculated immediately
	as normal C code, so instead of:
	push(2)
	push(2)
	add()
	print()
	you can actually do print(add(2, 2)) in SOME cases
	so I decided to try to make this code generator prototype that can
	use immediate (immy) expressions intermixed with stack expressions.
	also each codegen operation can be sidy, i.e. produce side expressions.
	so overal each op can return an expression immediately or on a stack or
	produce side effects.
	after working on this I found a way to avoid stack based calculation and passing
	at all, so in a next version of codegen each opcode only have two funtions:
	1) return value (expr)
	2) do side effects (statement)
	the expr_stack remains, but only to root intermediate values for the GC

:/

incl map lex 'error' hilite
hilite_theme = [171 248 0 1 3 196 21 100 95]
					//# ? ? str num ? op ? ?
hilite_keywords = 'int char main void'
templ1 := '
#include <stdio.h>
int mem[20], *expr = &mem[20], *vars = &mem[0];
#define cell_nil() 0
#define expr_push(a) *(--expr) = a
#define expr_pop() (*(expr++))
#define op_add() expr[1] += expr[0]; expr_pop()

'

templ2 := ''

fun beautify_c code {
	if not is_string(code) throw 'not string'
	code = code / '\n' * ' '
	indent := 0

	fun tab indent line {
		t := ''
		while len t < indent*3 t += ' '
		ret t + line
	}

	line := ''
	re := ''
	i := 0
	while i < len code {
		ch := code[i]
		if ch == ord '{' { line += '{\n'; re += tab(indent line); line = '';indent += 1 }
		but ch == ord ';' { re += tab(indent line+';\n'); line = '' }
		but ch == ord '}' { indent -= 1; line += '}\n'; re += tab(indent line); line = '' }
		but ch == ord '#' {
			inc i
			ch = code[i]
			re += '#'
			while ch != ord ';' {
				re += chr ch
				inc i
				ch = code[i]
			}
			re += '\n'
		}
		else line += chr ch
		inc i
	}
	re += tab(indent line)
	ret re
}

cc := lam = args * ''
ids := { a: 0 b: 1 }

fun opcode _id sidy immy imm stack side {
	ret { _id, sidy, immy, imm, stack, side, }
}

fun _nil {
	ret opcode('_nil'
		lam = false
		lam = true
		lam = 'cell_nil()'
		lam = cc('expr_push(cell_nil());')
		lam = ''
	)
}

fun con value {
	ret opcode('con'
		lam = false
		lam = true
		lam = value
		lam = cc('expr_push(' value ');')
		lam = ''
	)
}

fun get id {
	ret opcode('get'
		lam = false
		lam = true
		lam = cc('vars[' ids[id]']')
		lam = cc('expr_push(vars[' ids[id] ']);')
		lam = ''
	)
}

fun sum a b {
	ret opcode('sum'
		lam = false
		lam = a.immy() and b.immy()
		lam = cc('(' a.imm() '+' b.imm() + ')')
		lam = cc(a.stack() b.stack() 'op_add();')
		lam = ''
	)
}

fun set id value {
	fun dest { ret cc('vars[' ids[id] '] = ') }
	ret opcode('set'
		lam = true
		lam = value.immy()
		lam {
			if value.immy() ret cc(dest() value.imm())
			else throw 'set: cannot imm'
		}
		lam {
			if value.immy() ret cc('expr_push(' dest() value.imm() ');')
			ret cc(value.stack() dest() 'expr[0];')
		}
		lam {
			if value.immy() ret cc(dest() value.imm() ';')
			ret cc('#line "test.c" 5;' value.stack() dest() 'expr_pop();')
		}
	)
}

fun block list {
	if len list == 0 ret opcode('b/nil'
		lam = false
		lam = true
		lam = '0'
		lam = 'expr_push(cell_nil());'
		lam = ''
	)
	but len list == 1 ret list[0]
	else ret opcode('block'
		lam = some(list lam x = x.sidy())
		lam = every(list lam x = x.immy()) and every(list[..-2] lam x = not x.sidy())
		lam = list[-1].imm()
 		lam = map_tail(list lam x = x.side() lam x = x.stack()) * ''
 		lam = map(list lam x = x.side()) * ''
	)
}

c_if := fun a b c {
	have_else := true
	if c == nil { c = _nil() have_else = false }
	fun _else f { if have_else ret cc('else {' c[f]() '}') else ret '' }

	ret opcode('if'
		lam = a.sidy() or b.sidy() or c.sidy()
		lam = a.immy() and b.immy() and c.immy()
		lam = cc('(' a.imm() ' ? ' b.imm() ' : ' c.imm() ')')
		lam {
			if a.immy() ret cc('if (' a.imm() ') {' b.stack() '}' _else('stack'))
			else ret a.stack() + cc('if (expr_pop()) {' b.stack() '}' _else('stack'))
		}
		lam {
			if a.immy() ret cc('if (' a.imm() ') {' b.side() '}' _else('side'))
			else ret a.stack() + cc('if (expr_pop()) {' b.side() '}' _else('side'))
		}
	)
}

fun prn1 x {
	fun code {
		if x.immy()
			ret cc('printf("%i ", ' x.imm() ');')
		else
			ret cc(x.stack() 'printf("%i ", expr[0]);')
	}
	ret opcode('prn1'
		lam = true
		lam = false
		lam { throw 'prn1 cannot imm' }
		lam = code()
		lam = cc(code() 'expr_pop();')
	)
}

prn := fun {
	a := args
	ret opcode('prn'
		lam = true
		lam = false
		lam { throw 'prn cannot imm' }
		lam = cc(stacks(map(a prn1)) * '' 'putc(10, stdout);cell_nil();')
		lam = cc(stacks(map(a prn1)) * '' 'putc(10, stdout);')
	)
}

rem := fun x {
	ret opcode('rem'
		lam = true
		lam = false
		lam { throw 'rem imm' }
		lam { throw 'rem in expr' }
		lam = cc('// ' x '\n')
	)
}

func := fun id b {
	ret opcode('func_' + id
		lam = true
		lam = false
		lam { throw 'func imm' }
		lam { throw 'func expr not yet impl' }
		lam = cc('void 'id'() {' block(b).stack() '}')
	)
}

call := fun id b {
	ret opcode('call_' + id
		lam = true
		lam = false
		lam { throw 'call imm' }
		lam = cc(id '();')
		lam = cc(id '();expr_pop();')
	)
}

fun best_ret a { if a.immy() ret a.imm() else ret a.stack() }
fun best_side a { if a.sidy() ret a.side() else ret '' }
fun stacks a { ret map(a lam x = x.stack()) }
fun sides a { ret map(a lam x = x.side()) }

b := map([
	func('f1' [sum(con(50) con(505))])
	//set('a' con(50))
	//set('b' con(5))
	//set('a' sum(con(500) sum(get('a') get('b'))))
	//set('b' sum(get('a') con(333)))
	//prn(get('a') get('b'))
	//prn(con(444) con(777))
	//rem('if')
	//prn(c_if(con(5) block([con(5) get('a') con(7) con(11)])))
	//c_if(con(1) prn(con(2)) prn(con(3)))
	//prn(c_if(con(5) con(7)))
	func('main' [
		set('a' call('f1'))
		prn(get('a'))
		call('f1')
	])
] best_side) * ''

main := cc(templ1 b templ2)
save('/tmp/peep.c' main)
log '-----------------------------------------------------------'
cout hilite(beautify_c(b))
log '-----------------------------------------------------------'

