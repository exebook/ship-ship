enum free flatter {}

hilite_theme = [171 248 0 1 3 196 21 100 95]
					//# ? ? str num ? op ? ?
//hilite_keywords = 'int char main void continue val'

//fun beautify_c code {
	//if not is_string(code) throw 'not string'
	//code = code / '\n' * ' '
	//indent := 0
//
	//fun tab indent line {
		//t := ''
		//while len t < indent*3 t += ' '
		//ret t + line
	//}
//
	//line := ''
	//re := ''
	//i := 0
	//while i < len code {
		//ch := code[i]
		//if ch == ord '{' { line += '{\n'; re += tab(indent line); line = '';indent += 1 }
		//but ch == ord ';' { re += tab(indent line+';\n'); line = '' }
		//but ch == ord '}' { indent -= 1; line += '}\n'; re += tab(indent line); line = '' }
		//but ch == ord '#' {
			//inc i
			//ch = code[i]
			//re += '#'
			//while ch != ord ';' {
				//re += chr ch
				//inc i
				//ch = code[i]
			//}
			//re += '\n'
		//}
		//else line += chr ch
		//inc i
	//}
	//re += tab(indent line)
	//ret re
//}

fun code_flatten x {

	fun stringify x {
		fun tab line {
			t := ''
			while len t < indent*3 t += ' '
			ret t + line
		}

		lines := []
		line := ''
		indent := 0
		each x {
			if item == flatter.eol {
				if len line lines += tab(line)
				line = ''
			}
			but item == ';' {
				lines += tab(line + ';')
				line = ''
			}
			but item == '{' {
				lines += tab(line + ' {')
				line = ''
				inc indent
			}
			but item == '}' {
				if len line lines += tab(line)
				dec indent
				lines += tab('}')
				line = ''
			}
			else {
				line += item
			}
		}
		if len line lines += tab(line)
		ret lines * '\n'
	}

	fun flatten x {
		re := []
		each x {
			if is_array(item) {
				if flatter.line == item[0] {
					re += flatter.eol
					re += '#line ' + item[2] + ' "' + item[1] + '"'
					re += flatter.eol
				}
				else re += flatten(item)
			}
			else re += item
		}
		ret re
	}

	x = flatten(x)
	x = stringify(x)
	ret x
}
