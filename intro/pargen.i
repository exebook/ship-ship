enum types {
	number
}

fun lx_init s { ret [0 s] }
fun lx_peek a { if a == nil ret nil ret a[1][a[0]] }
fun lx_next a { ret [a[0]+1, a[1]] }
fun lx a { ret a[0] }
fun ast a { ret a[1] }

fun p_lit a {
	t := lx_peek(a)
	if is_number(t) ret [lx_next(a) { type: types.number num: t }]
}

fun cp_embrace f {
	ret lam a {
		t := lx_peek(a)
		if t == '{' {
			lit := f(lx_next(a))
			if lit {
				b := lx(lit)
				if lx_peek(b) == '}' ret [lx_next(b) ast(lit)]
			}
		}
	}
}

fun cp_list f {
	ret lam a {
		list := []
		i := a
		loop {
			b := f(i)
			if b == nil break
			list += ast(b)
			i = lx(b)
		}
		if len list > 0 ret [i list]
	}
}


fun p_ship a {
	ret cp_embrace(cp_list(p_lit))(a)
}

fun go {
	log ast(p_ship(lx_init(['{' 7 9 '}'])))
}

go()