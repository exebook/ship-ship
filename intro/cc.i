CARG_MAX := 3
CVAR_PREFIX := '_v'
CARG_PREFIX := '_a'
CARG_PTR := '_aptr'
CARG_COUNT := '_acount'

templ1 := '
#include <ru.c>
'
templ2 := ''

fun opcode _id expr statem {
	ret { _id, expr, statem }
}

//  op math

fun to_string x = '' + x

fun split arr x {
	re := []
	each arr if index == 0 re += item else re += [x item]
	ret re
}
itself := fun x {
	ret opcode('itself'
		lam = x
		lam = x
	)
}
fun exprs a = map(a lam x { if is_object(x) ret x.expr(); ret x })
fun statems a = map(a lam x { if is_object(x) ret x.statem(); ret x })

//  op tool

fun var_decl n {
	re := []
	re[n-1] = 0
	each re item = CVAR_PREFIX + index
	ret ['val ' re*', ' ';']
}

fun arg_decl {
	re := []
	re[CARG_MAX - 1] = 0
	each re item = 'val ' + CARG_PREFIX + index
	re += 'val *' + CARG_PTR
	ret 'short ' + CARG_COUNT + ', ' + re * ', '
}

//  op nodes

fun _nil {
	ret opcode('_nil'
		lam = '0'
		lam { throw 'nil has no statem' }
	)
}

fun con value {
	if not is_string(value) throw 'con() value ' +value+ ' must be stringized'
	ret opcode('con'
		lam = ['__let(' value ')']
		lam { throw 'con has no statem' }
	)
}

fun varget id {
	if not is_number(id) throw 'varget: id must be a number'
	ret opcode('varget'
		lam = CVAR_PREFIX + id
		lam { throw 'varget: has no statem' }
	)
}

fun argget id {
	ret opcode('argget'
		lam {
			if id < CARG_MAX ret CARG_PREFIX + id
			ret [id '<' CARG_COUNT + '?' CARG_PTR '[' (id-CARG_MAX) '] : 0']
		}
		lam { throw 'get has no statem' }
	)
}

fun add a b {
	ret opcode('add'
		lam {
			re := ['__add(' a.expr() ',' b.expr() ')']
			are re
			ret re
		}
		lam { throw 'add has no statem' }
	)
}

fun cmp a b {
	ret opcode('cmp'
		lam = ['__eq(' a.expr() ',' b.expr() ')']
		lam { throw 'cmp: cannot be a statem' }
	)
}

fun varset id value {
	if not is_number(id) throw 'varset: id must be a number'
	ret opcode('set'
		lam { throw 'set has no expr' }
		lam = [
		//[flatter.line "test.c" 5]
		 CVAR_PREFIX+id ' = ' value.expr() ';']
	)
}

fun block list {
	ret opcode('block'
		lam {
			if len list == 0 throw 'empty block has no expr'
			ret [ '({' statems(list) '})' ]
		}
		lam {
			if len list == 0 ret '{}'
			ret [ '{' statems(list) '}' ]
		}
	)
}

fun c_ifex a b c {
	if not is_array(b) throw 'argument b of ifex must be array'
	if not is_array(c) throw 'argument c of ifex must be array'
	ret opcode('if'
		lam = ['(' a.expr() '?' b.expr() ':' c.expr() ')']
		lam { throw 'statem of ifex' }
	)
}

fun c_if a b {
	if not is_array(b) throw 'argument b of c_if must be array'
	ret opcode('if'
		lam { throw 'expr of if' }
		lam = ['if (' a.expr() ')' block(b).statem() ]
	)
}

fun rem x {
	ret opcode('rem'
		lam { throw 'rem have no expr' }
		lam = ['// ' x flatter.eol]
	)
}

fun __opti_return b {
	if len b > 0 and b[-1]._id == 'c_ret' {
		ret ''
	}
	else
		ret statems([leave() 'return 0' ';'])
}

fun func id var_count b {
	if not is_array(b) throw 'func body must be array'
	ret opcode('func_' + id
		lam { throw 'func: have no expr' }
		lam = [
			'val ' id '(' arg_decl() ')' '{'
				var_decl(var_count)
				statems([
					enter() statems(b) __opti_return(b)
				])
			'}'
		]
	)
}

fun c_main var_count pre b post {
	if not is_array(b) throw 'main body must be array'
	ret opcode('main'
		lam { throw 'main: have no expr' }
		lam = [
			'int main()' '{'
				statems(pre)
				enter().statem()
				var_decl(var_count)
				statems(b)
				leave().statem()
				statems(post)
				'return 0' ';'
			'}'
		]
	)
}

fun call id arg_list {
	if not is_array(arg_list) throw 'call: arg_list must be array'
	fun a_list {
		re := []
		p := []
		each arg_list {
			if index < CARG_MAX {
				re += [item.expr()]
			}
			else {
				n := index - CARG_MAX
				p += item.expr()
			}
		}
		while len re < CARG_MAX re += '0'
		if len p > 0
			re += '((val[]){' + p*', ' + '})'
		else
			re += '0'
		ret [len arg_list + ', ' split(re, ',')]
	}

	ret opcode('call_' + id
		lam = [id '(' a_list() ')']
		lam = [id '(' a_list() ')' ';']
	)
}

fun arr_literal values {
	if not is_array(values) throw 'arr_literal: values must be array'
	ret opcode('arr_literal'
		lam = [ 'array_literal(' + len values, ', (val[]){ ' exprs(split(values itself(','))) '} )' ]
		lam { throw 'arr_literal: cannot be a statement' }
	)
}

fun index_get obj ix {
	ret opcode('index_get'
		lam = [ '__index_get(' obj.expr() ', ' ix.expr() ')' ]
		lam { throw 'index_get: cannot be a statement' }
	)
}

fun index_set obj ix value {
	ret opcode('index_set'
		lam { throw 'index_set: cannot be an expr' }
		lam = [ '__index_set(' obj.expr() ', ' ix.expr() ', ' value.expr() ')' ';' ]
	)
}

fun add_to dest value {
	ret opcode('+='
		lam { throw '+=: cannot be an expr' }
		lam = [ '__object_add_to(' dest.expr() ', ' value.expr() ')' ';']
	)
}

fun enter {
	ret opcode('enter'
		lam { throw 'enter: cannot be an expr' }
		lam = [ '__enter()' ';' ]
	)
}

fun leave {
	ret opcode('leave'
		lam { throw 'leave: cannot be an expr' }
		lam = [ '__leave()' ';']
	)
}

fun c_ret value {
	ret opcode('c_ret'
		lam { throw 'c_ret: cannot be an expr' }
		lam = [ leave().statem() 'return rooted(' value.expr() ')' ';']
	)
}

fun c_loop body {
	if not is_array(body) throw 'c_loop: body must be an array'
	ret opcode('c_loop'
		lam { throw 'c_loop: cannot be an expr' }
		lam = [
			'__loop_mark()' ';'
			'int safe=10' ';'
			'while (safe-->=0) '
			'{'
				'__loop_release()' ';'
				statems(body)
			'}'
			]
	)
}

fun c_break {
	ret opcode('c_break'
		lam { throw 'c_break: cannot be an expr' }
		lam = [ 'break' ';']
	)
}

fun c_cont {
	ret opcode('c_cont'
		lam { throw 'c_cont: cannot be an expr' }
		lam = [ 'continue' ';']
	)
}


/:a
val __str_lit(int id) {
	val re = __str_lits[id];
	return re ? re : __init_str_lit(id);
}

int main() {
	val _v0 = __add(__str_lit(1), __str_lit(1));
	log(1, _v0);
}
a:/

//fun c_str_literals_init list {
	//if not is_array(list) throw 'c_str_literals_init: list must be an array'
	//ret opcode('c_str_literals_init'
		//lam { throw 'c_str_literals_init: cannot be an expr' }
		//lam = [
			//'void __init_str_lit(int id) {'
				//'static val *table = 0' ';'
				//'if (table == 0)' '{'
					//'table = malloc(' +len list+ ' * sizeof(val))' ';'
					//'for (int i = 0; i < '+ len list +'; i++) table[i] = 0' ';'
				//'}'
				//map(list lam x ix = 'table['+ ix +'] = __str_literal('+x+')')
			//'}'
		//]
	//)
//}

//fun c_str_literal id {
	//if not is_number(b) throw 'c_str_literal: string literal id must be a number'
	//ret opcode('str_literal'
		//lam = [ '__str_lit(' id ')' ]
		//lam { throw 'c_str_literal: is not a statement' }
	//)
//}

fun c_escape s {
	ret s
}

fun c_string s {
	ret opcode('c_string'
		lam {
			re := [ '__string_utf("'+ c_escape(s) +'")' ]
			are re
			ret re
		}
		lam { throw 'c_string: is not a statement' }
	)
}

//  runtime

fun c_gc_run {
	ret opcode('c_gc_run'
		lam { throw 'c_gc_run: not expr' }
		lam = [ '__gc_test_run()' ';' ]
	)
}

fun c_putc c {
	ret ['putc(' + c + ', stdout)' ';']
}

fun c_log a {
	if not is_array(a)
		throw 'c_log accepts array, but received ' + typeof(a)
	ret opcode('log'
		lam { throw 'log: have no expr' }
		lam = ['__ship_log(', to_string(len a) ',' exprs(split(a itself(','))) ')' ';']
	)
}

fun c_runtime_end {
	ret opcode('runtime_end'
		lam { throw 'runtime_end: cannot be used in expressions' }
		lam = [ '__runtime_end()' ';' ]
	)
}
