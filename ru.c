#include "platform.c"
#include "config.c"
#include "ru_error.c"

typedef uint64_t val;

struct {
	int argc;
	char** argv;
	bool trace_enabled;
	bool trace_runtimes;
	val rec_type_arr;//, rec_type_map;
} __global;

typedef struct {
	jmp_buf j;
	val e;
} __Try;

typedef struct {
	int key;
	char *value;
} __enum_key_value;

typedef struct {
	char *name;
	int size;
	__enum_key_value kv[];
} __enum;

void __show_trace();

void pp(char *c, int i) {
	printf("%s: %i\n", c, i);
}

#define __str(a) __string_utf(a)
#define __strn(s, n) __string_utf_size(s, n)
#define __log(a...) ({ __log_n(sizeof((val[]){ a })/sizeof(val), a); })
#define __cout(a...) __cout_n(sizeof((val[]){ a })/sizeof(val), a);
#define __array(a...) __array_allocopy(sizeof((val[]){ a })/sizeof(val), (val[]){ a })
#define __concat(a...) __array_join(__array(a), __str(""))
#define __ref(a) __leaf(a)->fly
#define __env(n) __ref(CARG_ENV->p[n])
#define __compact(a) if (__is_string(a)) __string_compact(a); else if (__is_array(a)) __array_compact(a);
#define __tem(str, arr...) __template(__str(str), __array(arr))

val __string_utf(char *s);
void ship_printf(char *c, ...);
void __cout_n(int count, ...);
void __log_n(int count, ...);
void __print(val, int);
val __shame(val, val);
void p_array(char *msg, val a);
char *__call_system(char *);
val __load(val);
val __silent_load(val path, int *e);
val __type_of(val a);
val __array_join(val a, val t);
void __string_compact(val a);
val __string_uchar(uchar* p, int size);
val __template(val str, val arr);
val __index_get(val object, val index);
val __val_add(val a, val b);
val __val_add_to(val a, val b);
__size_t __len(val a);
val __cmp(val a, val b, char op);
char __cmp_str(val a, val b);
int __str_find_at(val where, val what, int at);
val __stringize_num(val a);
void __gc_mark_release();
val __trace();
__size_t __object_size(val object);
val __array_allocopy(__size_t size, val *p);
val __array_get(val a, int64_t index);
val __char_at(val s, int index);
val __property_get(val object, val key);
val *__array_at(val a, int64_t index);
void __property_set(val object, val key, val value);
val __array_concat(val a, val b);
val __string_concat(val a, val b);
val __split(val a, val b);
void __array_append(val array_a, val array_b);
val __array_push(val arr, val item);
val __object_merge(val a, val b);
char *__pchar(val a);

#include "hashmap.c"
#include "cell.c"
#include "leaf.c"
#include "stacks.c"
#include "try.c"
#include "gc.c"
#include "bignum.c"
#include "val.c"
#include "ari.c"
#include "array.c"
#include "each.c"
#include "object.c"
#include "rec.c"
#include "closure.c"
#include "utf.c"
#include "string.c"
#include "log.c"
#include "range.c"
#include "cmp.c"
#include "trace.c"
#include "system.c"
#include "sort.c"
#include "shame.c"
#include "builtins.c"

val __after_main = __nil;

void __call_after_main() {
	if (__is_fun(__after_main)) {
		//__log(__str("does not work, probably GC issue, to fix"));
		//__log(__str("am:"), __after_main);
		//__call(__leaf(__after_main)->f);
	}
}

void __runtime_init(int argc, char**argv) {
	__enter();
	setbuf(stdout, NULL); // todo, make this optional
	setbuf(stderr, NULL);
	__global.argv = argv;
	__global.argc = argc;
	__global.trace_enabled = 1;
	__global.trace_runtimes = 0;
	__global.rec_type_arr = __array();
	__global_roots_add(__global.rec_type_arr);
	//__global_roots_add(__global.rec_type_map);
	__global_roots_add(__last_stack_trace);
	__leave();
}

void __runtime_end() {
	//__stack_clear(expr);
	__global_roots_clear();
	__vals_free(recursed);
	__gc_mark_release();
	//__gc_show();
	for (int i = 0; i < __stack_count(expr); i++) {
		//__log(expr_stack[i]);
		printf("%lX %i->\n", (uint64_t)expr_stack[i], i);
	}
	if (__stack_count(expr)) pp("items left on expr stack", __stack_count(expr));
	//else printf("mem OK\n");
}


