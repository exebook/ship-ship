E000_0 := 'unexpected identifier'
E001_0 := 'unexpected array literal'
E002_0 := 'unexpected array literal, for indexing please remove the space before ['
E003_1 := 'unexpected expression operand of type <^>'
E004_1 := 'cannot reference builtin function <^>'
E005_1 := '<^> is already defined'
E006_1 := '<^> is enum'
E007_3 := 'enums <^> and <^> both have member named <^>'
E008_2 := 'enum <^> does not have <^>'
E009_0 := 'cannot mix enums and strings in charmap'
E010_0 := 'multi-assign does not support compound values yet'
E011_1 := 'id: undeclared identifier <^>'
E012_0 := '<error> can only be used in <catch>'
E013_1 := 'compound value starting with <^>'
E014_2 := '^_for <^> is too big'
E015_1 := '^ outside of each'
E016_1 := 'cannot declare <^>'
E017_0 := 'meaningless expression statement'
E018_2 := 'not in <rec ^ {^}>'
E019_3 := 'fun <^> returns <^> but must return <^>'
E020_1 := 'fun <^> first defined here'


fun E00X_setup_for_tests {
	E000_0 = 'E000_0'
	E001_0 = 'E001_0'
	E002_0 = 'E002_0'
	E003_1 = 'E003_1 ^'
	E004_1 = 'E004_1 ^'
	E005_1 = 'E005_1 ^'
	E006_1 = 'E006_1 ^'
	E007_3 = 'E007_3 ^ ^ ^'
	E008_2 = 'E008_2 ^ ^'
	E009_0 = 'E009_0'
	E010_0 = 'E010_0'
	E011_1 = 'E011_1 ^'
	E012_0 = 'E012_0'
	E013_1 = 'E013_1 ^'
	E014_2 = 'E014_2 ^ ^'
	E015_1 = 'E015_1 ^'
	E016_1 = 'E016_1 ^'
	E017_0 = 'E017_0'
	E018_2 = 'E018_2 ^ ^'
	E019_3 = 'E019_3 ^ ^ ^'
	E020_1 = 'E020_1 ^'
}

tem := fun {
	a := args
	s := a[0] / '^'
	re := ''
	each a[1..] re += s[index] + item
	ret re + s[-1]
}
//log tem(m_operand1 '555') <- that's why

templ := fun {
	a := args
	ret a * ''
}
