typedef struct {
	int64_t from, to, size;
	int ok;
} range_args;

range_args __range_prepare_args(range_args A, int to_is_count, __size_t size) {
	range_args B;
	B.ok = 1;
	if (A.from < 0) {
		A.from += size;
		if (A.from < 0) A.from = 0;//B.ok = 0;
	}
	B.from = A.from;

	if ((A.to) < 0) {
		if (to_is_count) {
			B.from += A.to + 1;
			A.to = -A.to;
		}
		else {
			A.to = size + A.to;
		}
		if (A.to < 0) B.ok = 0;
	}
	B.to = A.to;

	if (B.ok) {
		B.to += to_is_count ? B.from : 1;
		if (B.from > size) {
			B.from = size;
			B.to = B.from;
		}
		if (B.to > size) B.to = size;
		B.size = B.to - B.from;
		if (B.size < 0) B.size = 0; // or throw?
	}
	else {
		B.to = B.from = B.size = 0;
	}
	return B;
}

val __range(val a, int64_t from, int64_t to, int to_is_count) {
	if (__is_array(a) || __is_string(a)) {
		leaf *l = __leaf(a);
		range_args R = { .from = from, .to = to };
		R = __range_prepare_args(R, to_is_count, l->size);
		if (!R.ok) {
			__throw(__str("__range: invalid range"));
		}
		if (__is_array(a)) {
			__array_compact(a);
			return __array_allocopy(R.size, & l->p[R.from]);
		}
		else if (__is_string(a)) {
			__string_compact(a);
			return __string_uchar(& l->s[R.from], R.size);
		}
	}
	if (__is_object(a)) {
		__throw(__str("__range: object get-or-set is not implemented"));
	}
	else {
		__throw(__str("__range: argument must be an array, string or object"));
	}
	return __nil;
}


