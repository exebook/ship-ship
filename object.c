#define __object() __object_new(0, ((val[]){}), 0)

val __object_new(__size_t count, val *kv, int hidden_type) {
	leaf *l = leaf_new(leaf_obj);
	dictionary *d = __dic_new(count + 1);
	l->d = d;
	l->hidden_type = hidden_type;
	__size_t i = 0;
	while (i < count) {
		val key = kv[i++];
		val value = kv[i++];
		__string_compact(key);
		__dic_add(d, __leaf(key)->s, __leaf(key)->size * sizeof(uchar));
		*d->value = value;
	}
	return __mk_val(l);
}

val __property_get(val object, val key) {
	if (!__is_object(object)) {
		__throw(__concat(__str("reading property of "), __type_of(object)));
	}
	if (!__is_string(key)) {
		if (__is_num(key))
			key = __stringize_num(key);
		else
			__throw(__tem(E001_0, __type_of(key)));
	}
	dictionary *d = __leaf(object)->d;
	__compact(key);

	leaf *key_l = __leaf(key);
	int ok = __dic_find(d, key_l->s, key_l->size * sizeof(uchar));
	return ok == 1 ? *d->value : __nil;
}

val __property_delete(val object, val key) {
	if (!__is_object(object)) {
		__throw(__concat(__str("deleting property of "), __type_of(object)));
	}
	if (!__is_string(key)) {
		if (__is_num(key))
			key = __stringize_num(key);
		else
			__throw(__tem(E001_0, __type_of(key)));
	}
	dictionary *d = __leaf(object)->d;
	__compact(key);

	leaf *key_l = __leaf(key);
	keynode *k = __dic_remove(d, key_l->s, key_l->size * sizeof(uchar));
	if (k) {
		//free_my_value(k->value);
		__keynode_free(k);
	}
	return __nil;
}


void __property_set(val object, val key, val value) {
	if (!__is_object(object)) {
		__throw(__concat(__str("writing property of "), __type_of(object)));
	}
	if (!__is_string(key)) {
		if (__is_num(key))
			key = __stringize_num(key);
		else
			__throw(__tem(E001_0, __type_of(key)));
	}
	__compact(key);
	leaf *l = __leaf(object);
	leaf *key_l = __leaf(key);
	dictionary *d = l->d;

	int ok = __dic_find(d, key_l->s, key_l->size * sizeof(uchar));
	if (ok == 1) {
		*d->value = value;
	}
	else {
		l->hidden_type = 0;
		__dic_add(d, __leaf(key)->s, __leaf(key)->size * sizeof(uchar));
		*d->value = value;
	}
}

int __object_serialize_func(void *key, int count, val *value, void *array) {
	//__enter();
	val k = __string_uchar(key, count / 2);
	__array_push((val)array, __array(k, *value));
	//__leave();
	return 1;
}

val __object_serialize(val object) { // returns array where keys/values are interleaved
	if (!__is_object(object)) {
		__throw(__concat(__str("serializing "), __type_of(object)));
	}
	//__enter();
	leaf *l = __leaf(object);
	val re = __array();
	__dic_each(l->d, __object_serialize_func, (void*)re);
	//__leave();
	return re;
}

int __object_keys_func(void *key, int count, val *value, void *array) {
	val k = __string_uchar(key, count / 2);
	__array_push((val)array, k);
	return 1;
}

val __object_keys(val object) {
	if (!__is_object(object)) {
		__throw(__concat(__str("cannot get keys of "), __type_of(object)));
	}
	leaf *l = __leaf(object);
	val re = __array();
	__dic_each(l->d, __object_keys_func, (void*)re);
	return re;
}

int __object_values_func(void *key, int count, val *value, void *array) {
	__array_push((val)array, *value);
	return 1;
}

val __object_values(val object) {
	if (!__is_object(object)) {
		__throw(__concat(__str("cannot get values of "), __type_of(object)));
	}
	leaf *l = __leaf(object);
	val re = __array();
	__dic_each(l->d, __object_values_func, (void*)re);
	return re;
}

__size_t __object_size(val object) {
	if (!__is_object(object)) {
		__throw(__concat(__str("object_size: argument is not an object "), __type_of(object)));
	}
	leaf *l = __leaf(object);
	return l->d->count;
}

val __object_merge(val a, val b) {
	if (!__is_object(a)) {
		__throw(__concat(__str("merge expects object but got "), __type_of(a)));
	}
	if (__is_object(b)) {
		val c = __object_new(0, ((val[]){}), 0);
		__each_iter i1, i2;
		__each_begin(& i1, __object_keys(a));
		__each_begin(& i2, __object_keys(b));

		while (__each_next(&i1)) {
			__index_set(c, __each_item(& i1), __index_get(a, __each_item(& i1)));
		}

		while (__each_next(&i2)) {
			__index_set(c, __each_item(& i2), __index_get(b, __each_item(& i2)));
		}
		return c;
	}
	else {
		__throw(__concat(__str("cannot merge "), __type_of(b)));
	}
	return __nil;
}
