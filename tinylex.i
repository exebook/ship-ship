enum Symbols {
	'+' '-' '*' '/'
	'<' '>' '>=' '<=' '==' '!='

	':=' '='
	'{' '}' '(' ')' '[' ']' ' ['
	',' '.' ';' ':' '..'
	'+=' '-=' '*=' '/=' 'mod='

	'^' '@' '%' '$' '#' '&' '!' '~' '~~'
	'//' '/:' '/*' '...' '++' '--' '->' '|'
}

enum Keywords {
	'.KeywordsBegin'
	fun lam ret do
	if else but
	loop each while break cont
	log cout are kill lang len chr ord
	index item index_for item_for //ekey ekey_for
	inc dec not NOT BIT
	and or AND OR SHL SHR XOR
	mod div mul pow
	init use the it that
	args this try catch throw
	enum free charmap
	incl langc
	error tee
	rec
	//'.KeywordsEnd'
}

enum Keys = Symbols + Keywords
enum TokTypes { Key Int Double Big Id Str1 Str2 Preproc }
enum TokenOther { __other }
enum Token = Keys + TokTypes
enum tokrec { fn pos id value newline line_no str }

fun tinylex fname str {
	keys := enumap Keys
	line_no := 1

	fun new_token pos id value {
		if next_on_new_line {
			next_on_new_line = false
			ret [[fname pos id value 1 line_no str]]
		}
		ret [[fname pos id value 0 line_no str]]
	}

	fun lex_line_comment pos {
		loop {
			inc pos
			if pos >= len str ret pos
			if str[pos] == 10 break
		}
		ret pos // cursor remains at eol
	}

	fun lex_colon_comment pos {
		pos += 2
		q := scan_id(pos)
		pos = find_at(str q+':/' pos + len q)
		if pos == -1 ret len str
		pos +=  len q + 2
		ret pos // cursor remains at eol
	}

	fun lex_star_comment pos {
		pos += 2
		pos = find_at(str '*/' pos)
		if pos == -1 ret len str
		pos += 2
		ret pos // cursor remains at eol
	}

	fun lex_str pos Q {
		s := ''  a := pos + 1
		loop {
			if a >= len str break
			ch := str[a]
			if ch == ord '\\' {
				inc a
				if a >= len str throw stop_lex(pos 'unterminated string')
				ch = str[a]
				if ch == ord 'n' ch = 10
				but ch == ord 'r' ch = 13
				but ch == ord 't' ch = 9
				but ch == ord 'b' ch = 8
				but str[a:2] == '0x' or ch == ord 'x' {
					n = ''
					if str[a] == ord '0' a += 1
					loop {
						inc a
						if a >= len str throw stop_lex(pos 'unterminated string')
						ch = str[a]
						if ch >= ord '0' and ch <= ord '9'
						or ch >= ord 'a' and ch <= ord 'f'
						or ch >= ord 'A' and ch <= ord 'F' {
							n += chr ch
						}
						else break
					}
					ch = parse_int(n 16)
					if str[a] != ord ';' dec a
				}
				but ch >= ord '0' and ch <= ord '9' {
					n := ''
					loop {
						if a >= len str throw stop_lex(pos 'unterminated string')
						ch = str[a]
						if ch >= ord '0' and ch <= ord '9' n += chr ch
						else break
						inc a
					}
					ch = parse_int(n 10)
					if str[a] != ord ';' dec a
				}
				s += chr ch
			}
			but ch == Q {
				_type := Token.Str1
				if Q == ord '"' _type = Token.Str2
				tokens += new_token(pos _type s)
				ret a + 1
			}
			else s += chr ch
			inc a
		}
		ret a
	}

	fun lex_num pos {
		s := ''  a := pos
		dot := nil
		loop {
			if a >= len str break
			ch := str[a]
			if ch == ord '_' { inc a cont }
			if ch == ord '.' {
				if str[a+1] != ord '.' {
					dot = a s += '.' inc a cont
				}
			}
			if ch < ord '0' or ch > ord '9' break
			if ch == ord '0' {
				if str[a+1] == ord 'x' {
					a += 1
					n := ''
					loop {
						inc a
						if a >= len str throw stop_lex(pos 'unterminated string')
						ch = str[a]
						if ch >= ord '0' and ch <= ord '9'
						or ch >= ord 'a' and ch <= ord 'f'
						or ch >= ord 'A' and ch <= ord 'F' {
							n += chr ch
						}
						else break
					}
					tokens += new_token(pos Token.Int parse_int(n 16))
					ret a
				}
			}
			s += chr ch
			inc a
		}
		if dot
			tokens += new_token(pos Token.Double parse_float(s))
		else
			tokens += new_token(pos Token.Int parse_int(s))
		ret a
	}

	fun scan_id pos {
		s := ''  a := pos
		loop {
			if a >= len str break
			ch := str[a]
			if ch >= ord 'a' and ch <= ord 'z'
			or ch >= ord 'A' and ch <= ord 'Z'
			or ch >= ord '0' and ch <= ord '9'
			or ch == ord '_' {
				s += chr ch
				inc a
			}
			else break
		}
		ret s
	}

	fun lex_id pos {
		s := scan_id(pos)
		K := Token.Id
		charmap s 0 {
			enum Keywords found {
				if len Keywords[match] == len s {
					K = match + Token['.KeywordsBegin']
				}
			}
		}
		tokens += new_token(pos K s)
		ret pos + len s
	}

	pos := 0
	tokens := []
	next_on_new_line := true
	loop {
		if pos >= len str break
		ch := str[pos]
		if ch == 32 or ch == 9 or ch == 13 { inc pos }
		but ch == 10 {
			inc pos
			inc line_no
			next_on_new_line = true
		}
		but ch == ord "'" pos = lex_str(pos ord "'")
		but ch == ord '"' pos = lex_str(pos ord '"')
		but ch >= ord '0' and ch <= ord '9' pos = lex_num(pos)
		but ch == ord 'm' and str[pos:4] == 'mod=' {
			//TODO: move charmap in front of the loop
			tokens += new_token(pos keys['mod='] 'mod=')
			pos += 4
		}
		but ch >= ord 'a' and ch <= ord 'z' or ch >= ord 'A' and ch <= ord 'Z'
			or ch >= ord '0' and ch <= ord '9' or ch == ord '_' pos = lex_id(pos)
		else {
			// operators
			str1 := str // bug in charmap compiler(flyvar?)
			charmap str1 pos {
				enum Keys found {
					s := Keys[match]
					s2 := s
					k := keys[s]
					if Keys['['] == match and str[pos - 1] == 32 {
						s2 = ' ['
						k = Keys[' [']
					}
					but match == Keys['//'] {
						pos = lex_line_comment(pos)
						cont
					}
					but match == Keys['/:'] {
						pos = lex_colon_comment(pos)
						cont
					}
					but match == Keys['/*'] {
						pos = lex_star_comment(pos)
						cont
					}

					tokens += new_token(pos k s2)
					pos += len s
					cont
				}
				miss {
					tokens += new_token(pos TokenOther.__other chr str[pos])
					pos += 1
					//throw stop_lex(pos 'creative input <'+ chr ch +'>')
				}
			}
			inc pos
		}
	}
	ret tokens

	fun stop_lex pos msg = { kind: 'tinylex' pos, str, lex_msg: msg }
}

fun token_str t {
	// crash here will cause stack overflow
	init t ret '<empty token>'
	fun tab x s { while len s < x s += ' ' ret s }
	s := ''
	key := t[2]
	value := t[3]
	if key == Token.Key value = Keys[key]
	if t[4] == 1 s += '\\n'
	//ret [t[0] Token[t[1]] '"'+value+'"' s] * ' '
	ret [color(60''+t[0]) color(20 ''+tab(3 Token[key])) color(1 '"'+value+'"') color(2 ''+s)] * ' '
}

fun test {
	//t := 'fun if else while break index '
	//s := ''
	//while len s < 10_000 s += t
	time_begin()
	tokens := tinylex('test.tf' "'2+2'2+2\na b fun")
	time_end()

	each tokens {
		log token_str(item)
		if index > 20 { log 'kill, tokens:' len tokens; kill }
	}
}

//test()