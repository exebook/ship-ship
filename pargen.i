incl map tinylex pa_error
incl tool

enum types {
	symbol number string id array_literal object_literal
	program decl log cout are log_color assign op_assign
	if_st if_ex else expr_statem
	expr operator call member range indexing BIT//value
	fun ret loop break cont while
	each item each_index each_range
	unary
	inc dec
	kill init use args arg 'this' 'the' 'it' 'that'
	try throw error enum free charmap incl langc
	tee rec rec_literal langc_shadow
	//negation not
}

enum parse_link { C_SHADOW }

// parser data types mapping:
// pos = [1 [tok tok tok]]
// ret = [pos ast]
// ast = { type, pos, ... }
// fun pa pos { return ret }

fun pos_pos a = a[0]
fun pos_tokens a = a[1]

fun pos_tok a { use a ret a[1][a[0]] }
fun pos_next a = [a[0]+1, a[1]]
fun pos_prev a { p := a[0] if p <= 0 throw 'pos_prev negative' ret [p-1, a[1]] }
fun pos_prev_tok a { use a ret a[1][a[0] - 1] }
//fun pos_where_text a { t := pos_tok(a) ret t[0] }

fun ret_pos a = a[0]
fun ret_ast a = a[1]

fun tok_eol a = a[4]

fun Key a = a[2]
fun Value a = a[3]

fun symbol key {
	ret lam A {
		if A == 1 ret '@'+Token[key]
		t := pos_tok(A)
		if t {
			if Key(t) == key ret [pos_next(A) { type: types.symbol sym: key pos: A }]
		}
	}
}

fun anyof list {
	ret lam A {
		if A == 1 ret 'anyof['+len list+']'
		each list {
			re := item(A)
			if re != nil break
		}
		ret re
	}
}

fun embrace open f close emsg {
	ret lam A {
		if A == 1 ret 'embrace' +open+f(1) +close
		R := symbol(open)(A)
		if R {
			R = f(ret_pos(R))
			if R {
				re := ret_ast(R)
				B := symbol(close)(ret_pos(R))
				init B {
					throw stop(ret_pos(R) emsg)
				}
				if B ret [ret_pos(B) re]
			}
		}
	}
}



fun _pa_loop sep eolish f {
	ret lam A {
		if A == 1 ret '_pa_loop.'+f(1)
		re := []
		B := A
		loop {
			if len re {
				S := symbol(sep)(B)
				if S { B = ret_pos(S) }
			}
			R := f(B)
			init R break
			a := ret_ast(R)
			if is_array(a) a = [a]
			re += a
			B = ret_pos(R)
			if eolish and at_new_line(B) { break }
		}
		ret [B re]
	}
}

fun auto_list sep f = _pa_loop(sep false f)
fun eol_list f = _pa_loop(Token[','] true f)

fun seq list {
	if not is_array(list) {
		throw dev_error('seq: expects array')
	}
	ret lam A {
		if A == 1 ret 'seq['+len list+']'
		re := []
		B := A
		each list {
			R := item(B)
			init R ret
			a := ret_ast(R)
			if is_array(a) a = [a]
			re += a
			B = ret_pos(R)
		}
		ret [B re]
	}
}

fun at_new_line A {
	T := pos_tok(A)
	init T ret false
	ret tok_eol(T)
}

fun pa_make doc ret_f a {
	ret lam A {
		if A == 1 ret doc
		R := a(A)
		if R {
			re := ret_f(A ret_ast(R))
			if re ret [ ret_pos(R) re ]
		}
	}
}

fun pa_node doc ret_f signature tail {
	ret lam A {
		if A == 1 ret doc
		R := signature(A)
		if R {
			B := ret_pos(R)
			C := tail(B)
			if C {
				ret [ ret_pos(C) ret_f(A ret_ast(R) ret_ast(C)) ]
			}
			else {
				throw stop(B doc + ': ' +tail(1)+ ' expected!')
			}
		}
	}
}

fun maybe f {
	ret lam A {
		if A == 1 ret 'maybe'
		R := f(A)
		if R ret R
		ret [A nil]
	}
}

fun same_line f {
	ret lam A {
		if A == 1 ret 'same_line'
		if at_new_line(A) ret
		R := f(A)
		ret R
	}
}

fun language up _A0 loader {
	shadow_c_names := []

	__block := nil
	fun pa_block {
		init __block = anyof([
			embrace(Token['{'] _pa_loop(Token[';'] false statem()) Token['}'] 'statement expected')
			pa_make('block_wrap' lam A x = [x] statem())
		])
		ret __block
	}

	incl pa_expr
	incl pa_lang

	//chain = prima[index|member|call] // lam  assignable = chain[-1] != call
	//if parse_keyword_statem() ret
	//a = expr
	//a := expr
	//[a b] = expr()
	//()
	//ret pa_index()(src)
	//ret auto_list(Token[','] literal())(src)
	//ret array_literal()(src)
	_R := program()(_A0)
	//ret decl()(src)
	//ret symbol(Token['{'])(src)
	//ret embrace('body' list('numbers' literal()))(src)
	_A := ret_pos(_R)
	if pos_pos(_A) < len pos_tokens(_A) {
		throw stop(_A 'parsing error')
	}
	up(parse_link.C_SHADOW shadow_c_names)
	ret _R
} // language

control_pa := lam a b { throw 'unhandled ' + a + b }

fun control_pa_add f {
	re := the control_pa
	it = f
	ret re
	control_pa f = [f control_pa]
}

fun handler_the {
	_the := nil
	_that := nil
	ret fun u a {
		if u == uplink.THE {
			_that = _the
			_the = a
		}
		but u == uplink.IT ret _the
		but u == uplink.THAT ret _that
		else ret control_pa(u a)
	}
}

fun parse_source up fname _src_ loader {
	if not is_string(_src_) {
		throw 'parser: invalid input'
	}
	//fname := 'test.i'
	tokens := tinylex(fname _src_)
	each tokens { // for debugging only
		if item[3] == 'end' {
			tokens = tokens[0..index-1]
			break
		}
	}
	if tokens == nil or len tokens == 0 {
		throw { msg: 'parser: nothing to parse' }
	}
	//up1 :=
	control_pa_add(handler_the())
	ast := language(up, [0 tokens] loader)
	//log nodestr(ast)
	ret ast
}

fun parse_test a {
	ast := ret_ast(parse_source(lam {}, 'test.i' a lam {}))
	use ast {
		log ~blue 'PARSING RESULT:'
		//log past(ret_ast(ast))
		prn_ast(ast 0)
	}
	else log ~red 'parser returned nil'
	log a
}

//parse_test("are 2/2")
//kill

