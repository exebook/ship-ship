
//  configurables
#undef __size_t //TODO use non conflicting name
typedef uint32_t __size_t; // int64_t when you have large data
typedef uint16_t uchar; // set to 32 to support wider character range
const float array_grow_factor = 1.5;
//#define DEBUG_GC_LEAKS 1
