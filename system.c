char* __call_system(char *cm) {
	static char c[1024*1024];
	char *err = "err-call_system";
	FILE *f = popen(cm, "r");
	if (f == NULL) {
		printf("__call_system() popen error\n");
		printf("%i %s\n", errno, strerror(errno));
		exit(1);
	}
	int n = fread(c, 1, sizeof(c), f);
	c[n] = 0;
	pclose(f);
	return c;
}

uint64_t __time_ms(){
	#ifdef TARGET_OS_MAC
		struct timeval time;
		gettimeofday(&time, NULL);
		return (time.tv_sec * 1000) + (time.tv_usec / 1000);
	#endif
	#ifdef __linux__
		struct timespec tp;
		clock_gettime(CLOCK_MONOTONIC, &tp);
		return tp.tv_sec * 1e3 + tp.tv_nsec / 1e6;
	#endif

	#ifdef _WIN64
		return GetTickCount();
	#endif
}

int __size_of_open_file(FILE *f) {
	// TODO: rewrite with (f)stat
	int prev = ftell(f);
	fseek(f, 0, 2);
	int t = ftell(f);
	fseek(f, prev, 0);
	return t;
}

val __silent_load(val path, int *e) {
	if (!__is_string(path)) {
		*e = 0;
		return __nil;
	}

	FILE *f = fopen(__pchar(path), "r");
	val re;
	if (f) {
		int size = __size_of_open_file(f);
		if (size < 0) {
			*e = 0;
			return __nil;
		}
		else if (size > 0) {
			char *buf = malloc(size);
			int n = fread(buf, 1, size, f);
			fclose(f);
			re = __string_utf_size(buf, n);
			free(buf);
			return re;
		}
		else {
			// size is unknown, need to read byte-by-byte until EOF
			re = __str("");
			char c[1024];
			while (1) {
				int n = fread(c, 1, sizeof(c), f);
				if (n == 0) break;
				re = __concat(re, __string_utf_size(c, n));
			}
			fclose(f);
			return re;
		}
	}
	else {
		*e = errno;
		return __nil;
	}
}

val __load(val path) {
	if (!__is_string(path)) {
		__throw(__str("__load: path must be a string"));
	}
	int e = 0;
	val re = __silent_load(path, &e);
	if (__is_nil(re)) {
		if (e) {
			static char emsg[1024];
			snprintf(emsg, 1024, "load(\"%s\")\n%s", __pchar(path), strerror(e));
			__throw(__str(emsg)); // TODO: get error code
		}
		else __throw(__concat(__str("unable to load: "), path));
		return __nil; // silence the compiler
	}
	else return re;
}

void __save(val fn, val data) {
	// todo improve error handling
	char *path = __pchar(fn);
	val u = __utf(data);
	char *buf = __leaf(u)->utf;
	__size_t size = __leaf(u)->size;

	FILE *f = fopen(path, "wb");
	if (f) {
		rewind(f);
		//__size_t written =
		fwrite(buf, 1, size, f);
		fclose(f);
	}
	else {
		const char *e = strerrorname_np(errno);
		__throw(__str((char*)e));
	}
}

#define ucontext asm_ucontext
#include <asm/ucontext.h>
#undef asm_ucontext

//void signal_handler_SIGSEGV(int in) {
void signal_handler_SIGSEGV(int sig_num, siginfo_t * info, struct asm_ucontext *uc) {
	// -fno-inline  -fno-omit-frame-pointer are required for this to work if -O2/-O3 used
	void *rip = (void *) uc->uc_mcontext.rip;

	void **p, *bp, *frame;
	asm ("mov %%rbp, %0;" : "=r" (bp));
	p = (void*) bp;
	int i = 0;
	void *b[20];
	int main();

	p[1] = rip;
	while (i < 20) {
		b[i] = p[1];
		if (b[i] > (void*)main && b[i] < (void*)main + 10000) // or i > 1
			{ break; }
		p = (void*) p[0];
		i++;
	}
	i++;
	val stack = __ask_gdb_for_stack_trace(b, i);
	val lines = __addr_to_line(stack);
	//lines = __filter_out_runtime(lines);
	lines = __format_trace(lines);
	__log(__array_join(lines, __str("\n")));
	printf(" ^ fatal low level crash\n");
	exit(1);
}

void install_signal_handlers() {
	//signal(SIGSEGV, (void*)signal_handler_SIGSEGV);
	struct sigaction S = {0};
	S.sa_sigaction = (void*)signal_handler_SIGSEGV;
	S.sa_flags = SA_RESTART | SA_SIGINFO;
	if (sigaction(SIGSEGV, &S, (struct sigaction *)NULL) != 0) {
		fprintf(stderr, "error setting signal handler for %d (%s)\n",
		SIGSEGV, strsignal(SIGSEGV));
		exit(EXIT_FAILURE);
	}
}

void __exit(val code) {
	exit(__is_num(code) ? __round(code) : 0);
}

val __argv() {
	char **C = __global.argv;
	val re = __array();
	for (int i = 0; i < __global.argc; i++) {
		__array_push(re, __str(C[i]));
	}
	return re;
}

