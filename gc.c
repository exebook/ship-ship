//TODO: only one global variable should be left for GC, and one for debugging variables
// in other words incorporate all gc related variables into a structure

// rule of using __enter and __hold:
// if you have loop that allocates on gc, you must use __loop_mark unlless you can guarantee the loop is very short
// if you declare a `val` variable before the loop and assign it inside of the loop, you must use __hold
// if you have __hold, you must have __enter
// otherwise using __enter does not look like a necessity unless you have very deep call stack

leaf *root_leaf = NULL;

int __gc_release_report = 0;
int __gc_limit_report = 0; // print message each time limit is increased

uint64_t gc_trigger = 100000; // how manyu allocations before each GC cycle
uint64_t gc_allocs = 0;

val *c_roots = 0;
int c_roots_size = 0;

#define __rooted(a)({ val _b = a; if (__is_heap(_b)) {__stack_push(expr, _b);} _b; })
#define __enter() val *__enter_mark = __stack(expr)
#define __leave() __stack(expr) = __enter_mark
#define __return(a) { val _b = a; __leave(); if (__is_heap(_b)) { __stack_push(expr, _b) }; return _b; }
#define __loop_mark() val *__loop_mark_ = __stack(expr)
#define __loop_release() __stack(expr) = __loop_mark_

void __gc_check();

#define DEBUG_MALLOC

#	ifdef DEBUG_MALLOC
#		define Alloc __ship_malloc
#		define Realloc __ship_realloc
#		define Calloc __ship_сalloc
#		define Free __ship_free
#	else
#		define Alloc malloc
#		define Realloc(a, b, c) realloc(a, b)
#		define Calloc calloc
#		define Free free
#	endif

//int gcount = 0;
//void* guard[100000] = { 0 };
//void* guardX[100000] = { 0 };

void *__ship_malloc(uint64_t size) {
	if (size == 0) return 0;
	gc_allocs++;
	return malloc(size);
}

void __ship_free(void *p0) {
	free(p0);
}

void *__ship_realloc(void *p0, uint64_t size, uint64_t old_size) {
	return realloc(p0, size);
}

void *__ship_сalloc(uint64_t n, uint64_t size) {
	return calloc(n, size);
}

//char G = 0;
//char *turn = "|/-\\";

void __gc_check() {
	void __gc_mark_release();
	if (gc_allocs > gc_trigger) {
		gc_allocs = 0;
		__gc_mark_release();
	}
}

int __leaf_count = 0;

leaf *leaf_new(char type) {
	__leaf_count++;
	//printf("leaf_new: %i\n", type);
	__gc_check();
	leaf *re = Alloc(sizeof(leaf));
	//re->is_static = 0;
	//re->mark = 0;
	re->type = type;
	re->next = root_leaf;
	root_leaf = re;
	if (type == leaf_str) {
		re->pair = 0;
	}
	__stack_push(expr, __mk_val(re));
	//printf("__new_leaf(%s) %p no. %i @ %p\n", __type_of_leaf(re), re, __leaf_count, __stack(expr));
	return re;
}

leaf *leaf_new_rec(int size, int rectype) {
	__leaf_count++;
	__gc_check();
	leaf *re = Alloc(sizeof(leaf) + sizeof(val) * size);
	re->type = leaf_rec;
	re->next = root_leaf;
	re->size = size;
	re->rectype = rectype;
	root_leaf = re;
	__stack_push(expr, __mk_val(re));
	return re;
}

void __hold(val *variable) {
	leaf *L = leaf_new(leaf_hold);
	L->hold = variable;
	__stack_push(expr, __mk_val(L));
}

void dump_pair(leaf *a, int d) {
	if (a->type != leaf_str) {
		printf("dump_pair fail: %p, %s\n", a, __type_of_leaf(a));
		exit(0);
	}
	if (a->pair) {
		dump_pair(a->left, d + 1);
		dump_pair(a->right, d + 1);
	}
	else {
		printf("|");
		for (int i = 0; i < d; i++) printf(" ");
		printf("'");
		for (int i = 0; i < a->size; i++) printf("%c", a->s[i]);
		printf("'");
		printf("\n");
	}
}

char *__dump_str(val s) {
	leaf *l = __leaf(s);
	static char p[100] = {0};
	int n = l->size;
	if (n > 99) n = 99;
	for (int i = 0; i < n; i++) p[i] = l->s[i];
	return p;
}

void __free_leaf(leaf *l) {
	__leaf_count--;
	//printf("__free_leaf: %p %s%s (leaf count: %i)", l, __type_of_leaf(l), __is_string(__mk_val(l))?__dump_str(__mk_val(l)):"", __leaf_count);
	//printf("\n");

	if (l->type == leaf_arr) {
		if (l->p) Free(l->p);
	}
	else if (l->type == leaf_fun) { Free(l->env); }
	else if (l->type == leaf_big) {}
	else if (l->type == leaf_str) {
		if (!l->pair) {
			if (l->s) {
				//for (int i = 0; i < l->size; i++) printf("%c", l->s[i]); printf("\n");
				Free(l->s);
			}
		}
		else {
			//printf("release pair\n");
			//dump_pair(l, 0);
			//printf("left: %s pair: %i\n", __type_of_leaf(l->left), l->left->pair);
			//printf("right: %s pair: %i\n", __type_of_leaf(l->right), l->right->pair);
				//for (int i = 0; i < l->size; i++) printf("%c", l->s[i]); printf("\n");
			//exit(0);
		}
	}
	else if (l->type == leaf_pchar) {
		Free(l->utf);
	}

	else if (l->type == leaf_fly) {}
	else if (l->type == leaf_hold) {}
	else if (l->type == leaf_obj) { __dic_delete(l->d); }
	else if (l->type == leaf_rec) {}
	else {
		printf("warning, __free_leaf unhandled case\n");
	}
	Free(l);
//	printf("DROPED %p\n", l);
	//printf("free_leaf)\n");fflush(0);
}

void __mark(leaf *l);

void __mark_env(leaf* l) {
	for (int i = 0; i < l->size; i++) {
		__mark(__leaf(l->env->p[i]));
	}
}

int __object_mark_func(void *key, int count, val *value, void *array) {
	if (__is_heap(*value)) __mark(__leaf(*value));
	return 1;
}

void __mark_str_stackless(leaf *p) {
	while (p->pair) {
		p->mark = 1;
		if (p->left->pair && !p->right->pair) {
			// optimization for most frequent case when + is used to build a long string
			__mark(p->right);
			p = p->left;
		}
		else if (!p->left->pair && p->right->pair) {
			// how can we possibly get here?
			__mark(p->left);
			p = p->right;
		}
		else {
			// fallback to stackful mark
			__mark(p->left);
			__mark(p->right);
			return;
		}
	}
	p->mark = 1;
}

void __mark(leaf *l) { // TODO replace /val v/ with /leaf *l/
	//if (!__is_heap(v)) return;
	//leaf *l = __leaf(v);
	//printf("mark(%lu): %s %i, leaf_count: %lu\n", depth, __type_of_leaf(l), l->mark, __leaf_count);
	if (l->mark == 1) return;

	if (l->type == leaf_str) {
		__mark_str_stackless(l);
	}
	else if (l->type == leaf_pchar || l->type == leaf_big) {
		l->mark = 1;
	}
	else if (l->type == leaf_arr) {
		l->mark = 1;
		__size_t size = l->size;
		val *p = l->p;
		for (int i = 0; i < size; i++) {
			if (__is_heap(p[i])) __mark(__leaf(p[i]));
		}
	}
	else if (l->type == leaf_fly) {
		l->mark = 1;
		if (__is_heap(l->fly)) __mark(__leaf(l->fly));
	}
	else if (l->type == leaf_hold) {
		l->mark = 1;
		if (__is_heap(*l->hold)) __mark(__leaf(*l->hold));
	}
	else if (l->type == leaf_fun) {
		l->mark = 1;
		__mark_env(l);
	}
	else if (l->type == leaf_obj) {
		l->mark = 1;
		__dic_each(l->d, __object_mark_func, 0);
	}
	else if (l->type == leaf_rec) {
		l->mark = 1;
		for (int i = 0; i < l->size; i++) {
			//printf("free rec item %i\n");
			if (__is_heap(l->items[i])) __mark(__leaf(l->items[i]));
		}
	}
	else {
		__log(__str("fatal: gc mark unknown"), __str(__type_of_leaf(l)));
		exit(0);
	}
	//depth--;
}

void __gc_show() {
	ship_printf("root_leaf -> ");
	leaf *f = root_leaf;
	while (f) {
		ship_printf("%p%s -> ", f, f->mark?"*":"");
		f = f->next;
	}
	ship_printf("nil\n");
}

void __gc_clear() {
	leaf *l = root_leaf;
//int AA = 0 ;
	while (l) {
		l->mark = 0;
		l = l->next;
		//AA++;
	}
	//printf("cleared: %i\n", AA);
}

void ppp1(leaf *z) {
	printf("%s", __type_of_leaf(z));
	if (z->type == leaf_hold) {
		printf(" ?? ");
	}
	if (z->type == leaf_fun) {
		printf("HOLD FUN %i %p", z->size, z->env);
//		printf(" %i + %i", z->size, z->env->bind);
	}
}

void ppp(leaf *z) {
	printf("%s", __type_of_leaf(z));
	if (z->type == leaf_hold) {
		if (__is_heap(*z->hold)) {
			printf(" -> ");
			ppp(__leaf(*z->hold));
		}
		else {
			printf("<%i>", __get_i(*z->hold));
		}
	}
	if (z->type == leaf_fun) {
		printf(" %i + %i", z->size, z->env->bind);
	}
	if (z->type == leaf_str) {
		printf("[%i] \"", z->size);
		for (int i = 0; i < z->size; i++) printf("%c", z->s[i]);
		printf("\"");
	}
	if (z->type == leaf_big) {
		printf(" %f", z->big);
	}
}

void __gc_release() {
	uint64_t
		//before_bytes = mem_size,
		release_count = 0;

	leaf *l = root_leaf, *prev = 0;
	int leaf_old = 0, leaf_new = 0;

	while (l) {
		leaf_old++;
		leaf *next = l->next;
		if (l->mark == 0) {
			__free_leaf(l);
			release_count++;
			if (prev == 0) {
				root_leaf = next;
			}
			else {
				prev->next = next;
			}
		}
		else {
			prev = l;
			leaf_new++;
		}
		l = next;
	}
	static int count = 0;
	if (__gc_release_report) {
		count++;
		ship_printf("gc #%lu released %lu of %lu objects, gc_trigger: %lu\n", count, release_count, leaf_old, gc_trigger);
	}

l = root_leaf; int ggg = 0;
while (l) {
	//printf("%i) ", ggg++);
	//ppp(l);
	//printf("\n");
	l = l->next;
}
//printf("N:%i\n", sizeof(leaf));


	//if (mem_size > limit / 2) {
		//if (__gc_limit_report) printf("limit increased from %lu ", limit);
		//limit = mem_size * 2;
		//if (__gc_limit_report) printf("to %lu\n", limit);
	//}
	//if (count > 5) exit(0);
//exit(0);
}

void __gc_mark_roots() {
	__gc_clear();
	int n;

	for (int i = 0; i < c_roots_size; i++) {
		if (__is_heap(c_roots[i])) __mark(__leaf(c_roots[i]));
	}

	n = __stack_count(expr);
	for (int i = 0; i < n; i++) {
		if (__is_heap(__stack(expr)[i])) __mark(__leaf(__stack(expr)[i]));
	}

	n = __stack_count(try);
	for (int i = 0; i < n; i++) {
		if (__is_heap(__stack(try)[i].e)) __mark(__leaf(__stack(try)[i].e));
	}

	//n = throw_stack_count();
	//for (int i = 0; i < n; i++) {
		//__mark(throw_stack[i]);
	//}
	//n = env_stack_count();
	//for (int i = 0; i < n; i++) {
		//envi *e = env_stack[i];
		//if (e) {
			//mark_env(e);
		//}
	//}
	//for (int i = 0; i < globals_count; i++) {
		//__mark(globals[i]);
	//}

}

void __gc_mark_release() {
	uint64_t __time_ms();
	uint64_t T = __time_ms();
	__gc_mark_roots();
	__gc_release();
	if (__gc_release_report) {
		T = __time_ms() - T;
		printf("GC time: %i ms\n", (int) T);
	}
}

val __ship_runtime_gc_test_run(__arg_decl) {
	int _tmp = __gc_release_report;
	if (__arg(0) == 1) {
		__gc_release_report = 1;
	}
	__gc_mark_roots();
	__gc_release();
	__gc_release_report = _tmp;
	return __nil;
}


//  manually handled global roots

int __global_roots_add(val a) {
	if (!__is_heap(a)) return -1;
	for (int i = 0; i < c_roots_size; i++) {
		if (__is_nil(c_roots[i])) {
			c_roots[i] = a;
			return i;
		}
	}
	c_roots = realloc(c_roots, (c_roots_size + 1) * sizeof(val));
	c_roots[c_roots_size] = a;
	c_roots_size++;
	return c_roots_size-1;
}

void __global_roots_remove(val a) {
	if (!__is_heap(a)) return;
	for (int i = 0; i < c_roots_size; i++) {
		if (c_roots[i] == a) {
			c_roots[i] = __nil;
			return;
		}
	}
	//todo maybe realloc if many empty slots are there
}

void __global_roots_clear() {
	c_roots_size = 0;
}
