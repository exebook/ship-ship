//Example:
//__iterate_array(my, $ a) {
//	vbind[my_index] = my_items[my_index];
//}

#define __iterate_array(uniq, x) \
	__array_compact(x); \
	val *uniq##_items = __leaf(x)->p; \
	__size_t * uniq##_sz = & __leaf(x)->size; \
	for (int uniq##_index = 0; uniq##_index < * uniq##_sz; uniq##_index++)


val __array_allocopy(__size_t size, val *p) {
	leaf *l = leaf_new(leaf_arr);
	l->size = size;
	l->a_tail = 0;
	l->p = Alloc(size * sizeof(val));
	val *A = l->p, *B = p;
	for (__size_t i = 0; i < size; i++) *A++ = *B++;
	return __mk_val(l); // already rooted
}

void __array_arrange(leaf *l, __size_t grow) {
	// data:  [1 2 3] null<-[4]<-[5]<-[6]<-leaf
	// sizes: [5] null<-[3]<-[1]<-[1]<-leaf
	__size_t
		old_size = l->size,
		new_size = old_size + grow,
		i = old_size;
	val *p = Realloc(l->p, new_size * sizeof(val));

	for (__size_t z = old_size; z < new_size; z++) {
		p[z] = 0;
	}

	array_tail *tail = l->a_tail;
	int tail_no = 0;
	uint64_t sum_tails = 0;
	while (tail) {
		__size_t tail_part_size = tail->size;
		if (tail->prev == 0) {
			tail_part_size = l->size - sum_tails - tail->size;
		}
		i -= tail_part_size;
		if (i < new_size) {
			for (__size_t x = 0; x < tail_part_size; x++) {
				p[i + x] = tail->p[x];
			}
		}
		sum_tails += tail->size;
		array_tail *t = tail;
		tail = tail->prev;
		Free(t);
		tail_no++;
	}
	l->size = new_size;
	l->p = p;
	l->a_tail = 0;
}

__size_t __array_size(val a) {
	return __leaf(a)->size;
}

void __array_compact(val a) {
	leaf *l = __leaf(a);
	if (l->a_tail) {
		__array_arrange(l, 0);
	}
}

void __array_resize(val a, __size_t size) {
	leaf *l = __leaf(a);
	__array_arrange(l, size - l->size);
}

val __array_get(val a, int64_t index) {
	if (!__is_array(a)) __throw(__str(E002_0));
	leaf *l = __leaf(a);
	if (index < 0) index = l->size + index;
	if (index >= l->size || index < 0) return __nil;
	__array_compact(a);
	return l->p[index];
}

val *__array_at(val a, int64_t index) {
	if (!__is_array(a)) __throw(__str(E002_0));
	leaf *l = __leaf(a);
	if (index < 0) index = l->size + index;
	if (index < 0) __throw(__str(E003_0));
	if (index >= l->size) {
		__array_resize(a, index + 1);
	}
	else {
		__array_compact(a);
	}
	return &l->p[index];
}

void __array_tail_new(leaf *l, __size_t size, val* p) {
	array_tail *tail = Alloc(sizeof(array_tail) + sizeof(val) * size);
	for (__size_t i = 0; i < size; i++) {
		tail->p[i] = p[i];
	}
	tail->size = l->a_tail ? size : l->size;
	tail->prev = l->a_tail ? l->a_tail : 0;
	l->a_tail = tail;
	l->size += size;
}

void __array_append(val array_a, val array_b) {
	leaf *l = __leaf(array_a);
	leaf *B = __leaf(array_b);
	__array_compact(array_b);
	__array_tail_new(l, B->size, B->p);
}

void __array_push(val a, val item) {
	leaf *l = __leaf(a);
	__array_tail_new(l, 1, &item);
}

val __array_pop(val a) {
	leaf *l = __leaf(a);
	if (l->size == 0) {
		return __nil;
	}
	if (l->a_tail == 0 || l->a_tail->size != 1) {
		printf("SLOW %p %i\n", l->a_tail, (int)l->a_tail->size);
		__array_compact(a);
		val re = l->p[l->size - 1];
		__array_resize(a, l->size - 1);
		return re;
	}
	else {
		printf("FAST\n");
		val re = l->a_tail->p[0];
		array_tail *tail = l->a_tail;
		l->a_tail = tail->prev;
		Free(tail);
		return re;
	}
}
