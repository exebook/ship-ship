/:ir
	IR (Intermediate representation) IRs form a tree-like expression structure.
	Each IR hold source code position, operation, operands, C-type, DOES hold Ship type.
	Why it holds ship type? Because type checking and IR generation are rolled into one pass.
ir:/

//incl 'incl/obj'

// auto-generated from incl/ship_const.ship
ctype_pairs := [[0,2,2,4,4,6,6,nil,8,9,10,11],[2,1,2,3,4,5,6,7,8,9,10,11],[2,2,2,4,4,6,6,nil,8,9,10,11],[4,3,4,3,4,5,6,7,8,9,10,11],[4,4,4,4,4,6,6,nil,9,9,10,11],[6,5,6,5,6,5,6,7,9,9,10,11],[6,6,6,6,6,6,6,nil,nil,nil,10,nil],[nil,7,nil,7,nil,7,nil,7,nil,nil,10,nil],[8,8,8,8,9,9,nil,nil,8,9,nil,11],[9,9,9,9,9,9,nil,nil,9,9,nil,11],[10,10,10,10,10,10,10,10,nil,nil,10,11],[11,11,11,11,11,11,nil,nil,11,11,11,11]]

type_map := {}

fun new_type id ctype {
	t := { id, ctype, set: {} }
	type_map[id] = t
	ret t
	//ret type_arm.add(id, { id, ctype, set: {} })
}

type_i8 := new_type('i8', ctypes.i8)
type_u8 := new_type('u8', ctypes.u8)
type_i16 := new_type('i16', ctypes.i16)
type_u16 := new_type('u16', ctypes.u16)
type_i32 := new_type('i32', ctypes.i32)
type_u32 := new_type('u32', ctypes.u32)
type_i64 := new_type('i64', ctypes.i64)
type_u64 := new_type('u64', ctypes.u64)
type_f32 := new_type('f32', ctypes.f32)
type_f64 := new_type('f64', ctypes.f64)
type_ptr := new_type('ptr', ctypes.ptr)
type_cell := new_type('cell', ctypes.cell)

promote_lookup := [
	type_i8 type_u8 type_i16 type_u16 type_i32 type_u32
	type_i64 type_u64 type_f32 type_f64 type_ptr type_cell
]

setup_basic_types()

math_ops := []
math_ops[lex_infix_enum['+']] = 1
math_ops[lex_infix_enum['-']] = 1
math_ops[lex_infix_enum['*']] = 1
math_ops[lex_infix_enum['/']] = 1
math_ops[lex_infix_enum['mod']] = 1
math_ops[lex_infix_enum['==']] = 1
math_ops[lex_infix_enum['!=']] = 1
math_ops[lex_infix_enum['>']] = 1
math_ops[lex_infix_enum['<']] = 1
math_ops[lex_infix_enum['>=']] = 1
math_ops[lex_infix_enum['<=']] = 1

fun new_ir f cp type op arg {
	ret {
		f, // function in which context this is happening
		cp, // code point (from parser node)
		type, // ship type id (from types_array)
		op, // operation code (enum ops)
		arg // arguments to op (variant)
	}
}

fun setup_basic_types {
	count := ctypes
	i := 0
	while i < count {
		j := 0
		i_id := ctypes[i]
		set := type_map[i_id].set
		while j < count {
			a := type_assignable.assign
			j_id := ctypes[j]
			if i_id == 'ptr' or j_id == 'ptr' {
				if i_id != j_id {
					a = type_assignable.error
				}
			}
			set[j_id] = a
			inc j
		}
		inc i
	}
	//log ~cyan ':' type_map
}

fun can_cast to_type from_type {
	ret type_assignable.assign
}

//────────────────────────────────────────────────────────────────────

fun compile_primary f node {
	t := node.type
	if t == parse_type.id {
		type := nil
		if node.id == 'B' type = type_u8
		but node.id == 'a' type = type_cell
		but node.id == 'b' type = type_cell
		but node.id == 'c' type = type_cell
		but node.id == 'i' type = type_i32
		but node.id == 'j' type = type_i32
		but node.id == 'k' type = type_i32
		but node.id == 'count' type = type_cell
		but node.id == 'step' type = type_cell
		but node.id == 'x' type = type_f64
		but node.id == 'q' { type = type_cell }
		else {
			throw stop_compile(node.cp, 'unknown id <'+ node.id +'>')
		}
		ir := new_ir(f, node.cp, type, ops.id, node.id)
	}
	but t == parse_type.int {
		num_type := nil
		if len node.num > 15 {
			ir = new_ir(f, node.cp, type_u64, ops.int, node.num)
		}
		n := parse_int(node.num, 10)
		if n >= 0 {
			if n > 4294967295 num_type = type_u64
			but n > 65535 num_type = type_u32
			but n > 255 num_type = type_u16
			else num_type = type_u8
		}
		else {
			throw stop_compile(node.cp, 'negative numbers are not possible')
		}
		ir = new_ir(f, node.cp, num_type, ops.int, node.num)
	}
	but t == parse_type.float {
		ir = new_ir(f, node.cp, type_f64, ops.float, node.num)
	}
	but t == parse_type.group {
		ir = compile_expr(f, node.expr, true)
	}
	but t == parse_type.if {
		ir = compile_if(f, node)
	}
	but t == parse_type.while {
		ir = compile_while(f, node)
	}
	but t == parse_type.builtin {
		ir = compile_builtin(f, node)
	}
	else {
		l := parse_type
		if t < l t = parse_type[t]
		throw stop_compile(node.cp, 'unknown primary <'+ t +'>')
	}
	ret ir
}

fun promote_args cp a b { // does it return ctype or ship_type? they seem to equal
	A := a.ctype
	B := b.ctype
	re := ctype_pairs[A][B]
	if re != nil {
		re = type_map[ctypes[re]]
	}
	else {
		msg := 'promotion of pair is impossibe: '
		try {
			msg += '<'+a.id+'> and <'+b.id+'>'
		}
		catch {
			msg += stop_failed()
		}
		throw stop_compile(cp, msg)
	}
	ret re
}

fun to_type f ir dest { // ship types -> ship type
	if dest == ir.type ret ir
	src_t := ir.type
	if src_t.ctype == dest.ctype {
		ir.type = dest
		ret ir
	}
	if can_cast(ir.type, dest) == type_assignable.assign {
		ret new_ir(f, ir.cp, dest, ops.cast, [ir src_t.ctype])
	}
	else {
		throw stop_compile(ir.cp, 'to_type: cannot cast')
	}
}

fun compile_expr f node need_result {
	if len node.stack == 1 {
		ret compile_primary(f, node.stack[0])
	}
	if len node.stack == 3 {
		a := compile_primary(f, node.stack[0])
		b := compile_primary(f, node.stack[1])

		if (node.stack[2].operator == lex_infix_enum['=']) {
			promo = a.type
			if a.type != b.type {
				b = to_type(f, b, promo)
			}
		}
		but (node.stack[2].operator == lex_infix_enum['>']) {
			// TODO: experimenting with ex_greater
			promo = promote_args(node.cp, a.type, b.type)
			if a.type != promo a = to_type(f, a, promo)
			if b.type != promo b = to_type(f, b, promo)
			promo = type_u8
		}
		else {
			promo := promote_args(node.cp, a.type, b.type)
			if a.type != promo a = to_type(f, a, promo)
			if b.type != promo b = to_type(f, b, promo)
		}

		arg := [a, b]

		op_node := node.stack[2]
		oper := op_node.operator
		ir_op := nil

		if math_ops[oper] {
			ir_op = ops.math
			arg += oper
		}

		init ir_op {
			if oper == lex_infix_enum['='] ir_op = ops.assign
			else {
				msg := 'compile_infix_operator, operator is not supported: '
				try {
					msg += lex_infix_enum[node.operator]
				}
				catch {
					msg += stop_failed()
				}
				throw stop_compile(node.cp, msg)
			}
		}

		ret new_ir(f, op_node.cp, promo, ir_op, arg)
	}
	else {
		throw stop_compile(node.cp, 'real expressions are not supported')
	}
}

