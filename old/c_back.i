/:cu
	CU (Code Units) hold compiled C or machine code.
	CUs are generated from IRs one to one recursively descending into each IR.
cu:/

enum cu_store { // storage types for boxes
	var // result is stored in a variable either 'soft' (ie val) or 'hard' (ie int/float etc)
	literal // result is a C literal, a string, (like '123.45', '1', '1.0', '2+2' etc)
	//stack // result resides on the top of the stack
}

fun new_cu store ctype re code {
	ret {
		store, // storage type literal/variable/stack
		ctype,
		re, // storage info like var(n) or literal
		code, // generated C or machine code
	}
}

fun print_cu cu {
	fun filter k v {
		use v charmap k {
			'store' v = cu_store[v]
			'ctype' v = ctypes[v]
			found v = color(4, v)
		}
		ret v
	}
	log '{CU}' shame(cu, { filter, filter_val_bare: true })
}

fun new_cell f msg {
	t := f.new_cell()
	use msg t = '_var('+ t + ', "'+ msg +'")'
	else t = 'var('+ t + ')'
	ret t
}

fun new_auto f ctype { // allocate either a cell or a temp, based on ctype type
	if ctype == ctypes.cell {
		ret new_cell(f, nil)
	}
	else {
		ret f.new_temp()
	}
}

ctype_str_map := nil

fun ctype_str ctype {
	init ctype_str_map {
		ctype_str_map = 'i8 u8 i16 u16 i32 u32 i64 u64 f32 f64 void* val'/' '
	}
	ret ctype_str_map[ctype]
}

fun cp_get_line cp {
	ret {
		y: cp.lexed.tokens[cp.i].y + 1
		f: cp.lexed.src.fname
	}
}

fun line_directives A {
	R := []
	cur_y := nil
	cur_f := nil
	last_f := nil
	each A {
		if is_object(item) {
			cur_f = item.f
			cur_y = item.y
		}
		else {
			if cur_y > 0 and cur_f != nil and item[0:2] != '//' {
				if cur_f == last_f {
					R += '#line ' + cur_y
				}
				else {
					R += '#line ' + cur_y + ' "'+ cur_f +'"'
					last_f = cur_f
				}
			}
			R += item
		}
	}
	ret R
/:sample
	s := ['a0''a1'{y:1 f:'A'}'a2''a3'{y:2 f:'A'}'a4''a5''a6'{y:1 f:'B'}'a7'{y:5 f:'B'}'a8''a9''a10']
sample:/
}

fun boiler_plate lines {
	lines = line_directives(lines)
	ret [
		'#define SHIP_DSYM'
		'#include "rtl.c"'
		'#include "ex.c"'
		'int main() {'
		'	u8 B = 55; i32 i = 777,j,k; f64 x = 3.33; int q_n=7, count_n=8, step_n=9, a_n=10, b_n=11, c_n=12;'
		'	init_stacks();'
		'	enter_static(20, 0);'
		'	var(q_n) = cell_let(888.88);'
			map(lines, lam x { ret '   ' + x }) * '\n'
		'	leave_static();'
		'	return 0;'
		'}'
		''
	] * '\n'
}

//fun escape_str s {
	//begin
		//$ s = escape_string_normal($ s, 0);
	//end
	//ret s
//}

// *** most basic CUs *******************************************

fun cu_cast ir {
	to := ir.type.ctype
	from := ir.arg[1] // ship type

	if to == from
		throw stop(ERR.cu, ir.cp, 'cu_cast: to and from can never be the same here')

	cu := dispatch_cu(ir.arg[0])
	if to == ctypes.cell {
		__t := ir.f.new_temp(nil)
		cu.code += ctype_str(ctypes.cell)+' '+ __t +' = cell_let('+ cu.re +'); // cast'
		// ^this can be literal (no GC involved) (but can thisbe slower, calculating cell_let twice?)
		// or better c_var
		cu.re = __t
		cu.ctype = to
		cu.store = cu_store.var
	}
	else {
		if from == ctypes.cell {
			throw stop(ERR.cu, ir.cp, 'casting from cell to number is not implemented')
		}
		cu.re = '(' +ctypes[to]+ ')' + cu.re
		cu.ctype = to
	}
	ret cu
}

fun cu_int ir {
	ret new_cu(cu_store.literal, ir.type.ctype, '('+ctypes[ir.type.ctype]+')' + ir.arg, [])
}

fun cu_float ir {
	ret new_cu(cu_store.literal, ir.type.ctype, '('+ctypes[ir.type.ctype]+')' + ir.arg, [])
}

fun cu_id ir {
	ctype := ir.type.ctype
	if ctype == ctypes.cell {
		lit := 'var('+ir.arg+'_n)'
	}
	else {
		lit = ir.arg
	}
	lit = '/*lit*/ '+ lit
	cu := new_cu(cu_store.literal, ctype, lit, [])
	ret cu
}

fun cu_discard ir {
	cu := dispatch_cu(ir.arg)
	__t := ir.f.new_temp() + '_discard'
	cu.code += ctype_str(cu.ctype) +' '+ __t +' = '+ cu.re + ';'
	ret cu
}

fun define_auto __a ctype {
	if ctype == ctypes.cell
		ret __a +' = cell_nil();'
	else
		ret ctype_str(ctype) +' '+ __a +' = 0;'
}

fun assign_auto __a val {
	ret  __a +' = '+ val + ';'
}

// ======================================

fun cu_math ir {
	// guaranteed to be the same type aleady
	op_x := ir.arg[2]
	op := lex_infix_enum[op_x]
	if op_x == lex_infix_enum['mod'] op = '%'

	a := dispatch_cu(ir.arg[0])
	b := dispatch_cu(ir.arg[1])
	ctype := ir.type.ctype
	if ctype == ctypes.cell {
		if op == '>' {
			__t = new_auto(ir.f, ctypes.u8)
			cu = new_cu(cu_store.var, ctypes.u8, __t, [])
			cu.code += a.code
			cu.code += b.code
			cu.code += 'u8 '+__t +' = (int)cell_get(ex_greater('+ a.re +', '+ b.re +'));'
		}
		else {
			__t := new_auto(ir.f, ctype)
			cu := new_cu(cu_store.var, ctype, __t, [])
			cu.code += cp_get_line(ir.cp)
			cu.code += a.code
			cu.code += b.code
			cu.code += 'expr_push('+ a.re +');'
			cu.code += 'expr_push('+ b.re +');'
			if op == '+' cu.code += 'op_add();'
			but op == '-' cu.code += 'op_sub();'
			but op == '*' cu.code += 'op_mul();'
			but op == '/' cu.code += 'op_div();'
			but op == '%' cu.code += 'op_mod();'

			but op == '==' cu.code += 'op_cmp(0);'
			but op == '<' cu.code += 'op_cmp(1);'
			//but op == '>' cu.code += 'op_cmp(2);'
			but op == '<=' cu.code += 'op_cmp(3);'
			but op == '>=' cu.code += 'op_cmp(4);'
			but op == '!=' cu.code += 'op_cmp(5);'
			cu.code += cu.re +' = expr_pop();'
		}
	}
	else {
		__t = new_auto(ir.f, ctype)
		cu = new_cu(cu_store.var, ctype, __t, [
			cp_get_line(ir.cp)
			a.code
			b.code
			ctype_str(ctype) +' '+ __t +' = '+ a.re +' '+ op +' '+ b.re +';'
		])
	}
	ret cu
}

fun cu_assign ir {
	if ir.arg[0].op != ops.id {
		throw stop(ERR.cu, ir.cp, 'cannot assign to this')
	}
	b := dispatch_cu(ir.arg[1])
	a := dispatch_cu(ir.arg[0]) // destination is always calculated last
	c_t := nil
	if the ir.type c_t = it.ctype
	cu := new_cu(cu_store.var, c_t, a.re, [])
	cu.code += cp_get_line(ir.cp)
	//cu.code += a.code
	cu.code += b.code
	cu.code += a.re +' = '+ b.re +';'
	ret cu
}

fun cu_call_ship ir {
	cu := new_cu(cu_store.var, ctypes.cell, nil, [])
	//add_line_info(cu, ir)
	//cu.code += '// call_ship'
	fname := ir.arg[0]
	T := []
	i := 1 while i < len ir.arg {
		a := dispatch_cu(ir.arg[i])
		cu.code += a.code
		// type is already 'cell', conversion happens in IR phase and in dispatch(item)
		__arg := new_cell(ir.f, 'arg')
		__t := ir.f.new_temp()
		T += __t
		cu.code += __arg +' = '+ a.re +'; // save arg'
		cu.code += ctype_str(ctypes.cell)+' '+__t+' = '+__arg+';'
		inc i
	}
	each T {
		cu.code += 'var_push('+item+'); // argument'
		//cu.code += ctype_str(ctypes.cell)+' '+__t+'='+item+'; var_push('+__t+'); // argument'
	}
	cu.code += 'arg_alloc('+ (len ir.arg - 1) +');'
	if ir.type == nil {
		cu.code += fname +'(); // call_ship'
	}
	else {
		cu.re = new_cell(ir.f, nil)
		cu.code += cu.re +' = '+ fname +'(); // ship call()'
	}
	//cu.code += '// } ship_call end'
	ret cu
}

fun cu_if ir {
	cu_expr := dispatch_cu(ir.arg[0])
	cu_block := dispatch_cu(ir.arg[1])

	ctype := cu_block.ctype
	__a := new_auto(ir.f, ctype)
	cu := new_cu(cu_store.var, ctype, __a, [])

	code := []
	code += cu_expr.code
	code += define_auto(__a, ctype)
	code += 'if ('+ cu_expr.re +')'
	code += '{'
	code += 	cu_block.code
	if cu_block.re code += assign_auto(__a, cu_block.re)
	code += '}'

	cu.code = code
	ret cu
}

fun cu_while ir {
	cu_expr := dispatch_cu(ir.arg[0])
	cu_block := dispatch_cu(ir.arg[1])

	ctype := cu_block.ctype
	__a := new_auto(ir.f, ctype)
	cu := new_cu(cu_store.var, ctype, __a, [])
	code := []
	use ir.type code += define_auto(__a, ctype)
	code += 'while (1)'
	code += '{'
	code += 	cu_expr.code
	code += 	'if (!('+ cu_expr.re +')) break;'

	code += 	cu_block.code
	use ir.type
		use cu_block.re
			code += assign_auto(__a, cu_block.re)
	code += '}'

	cu.code = code
	ret cu
}

fun dispatch_cu ir {
	op := ir.op
	init op {
		throw stop(ERR.cu, ir.cp, 'dispatch_cu: operation is nil') // TODO: why stackoverflow here?
	}
	if op == ops.int			{ ret cu_int(ir) }
	but op == ops.float		{ ret cu_float(ir) }
	but op == ops.cast		{ ret cu_cast(ir) }

	but op == ops.math		{ ret cu_math(ir) }

	but op == ops.call_ship	{ ret cu_call_ship(ir) }
	but op == ops.id			{ ret cu_id(ir) }
	but op == ops.discard	{ ret cu_discard(ir) }
	but op == ops.assign	{ ret cu_assign(ir) }
	but op == ops.if			{ ret cu_if(ir) }
	but op == ops.while	{ ret cu_while(ir) }
	but op == ops.block		{ ret cu_block(ir) }
	else {
		o := ir.op
		l := ops
		if o < l o = ops[o]
		msg := 'dispatch_cu, unrecognized op: '
		try {
			msg += '<'+ o +'>'
		}
		catch {
			msg += stop_failed()
		}
		throw stop(ERR.cu, ir.cp, msg)
	}
}

fun cu_block ir {
	code := []
	cu := nil
	each ir.arg {
		cu = dispatch_cu(item)
		are cu
		code += cp_get_line(item.cp)
		code += cu.code
	}
	use cu {
		cu.code = code
	}
	ret cu
}

fun generate ir {
	ret boiler_plate(cu_block(ir).code)
}

