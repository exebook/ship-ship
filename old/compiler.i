enum ctypes { // underlying C storage type
	i8 u8 i16 u16 i32 u32 i64 u64 f32 f64 ptr cell
}

enum ops {
	nop
	block
	cast
	call_c call_ship
	id ref
	int float string // constant literals
	math
	assign equal
	discard // expression statements with discarded results
	if while
}

enum type_assignable: assign cast error.

fun print_ir ir {
	fun filter k v {
		init v ret
		charmap k {
			'cp' v = '*'+v.i
			'type' v = v.id
			'op' v = ops[v]
			'f' v = v.id()
			found v = color(4, v)
		}
		ret v
	}
	log '{IR}' shame(ir, { indent:3 filter, filter_val_bare: true })
}

fun compiler_new compiler_options {

	incl '/v/ship/expr'
	incl '/v/ship/fun'

	fun stop_compile cp msg {
		ret stop(ERR.compile, cp, msg)
	}

	if not is_object(compiler_options) {
		log 'new_compiler: compiler_options must be an object'
		kill
	}

	ret compiler

	fun compile_log f node {
		arg := ['ex_log']
		each node.list {
			arg += to_type(f, compile_expr(f, item, true), type_cell)
		}
		ret new_ir(f, node.cp, nil, ops.call_ship, arg)
	}

	fun compile_cout f node {
		arg := ['ex_cout']
		each node.list {
			arg += to_type(f, compile_expr(f, item, true), type_cell)
		}
		ret new_ir(f, node.cp, nil, ops.call_ship, arg)
	}

	fun compile_builtin f node {
		arg := ['ex_'+node.name]
		each node.arg.args {
			arg += to_type(f, compile_expr(f, item, true), type_cell)
		}
		ret new_ir(f, node.cp, nil, ops.call_ship, arg)
	}

	fun compile_stm f node {
		var ir
		if node.type == parse_type.log {
			ir = compile_log(f, node)
			ir.type = nil
		}
		but node.type == parse_type.cout {
			ir = compile_cout(f, node)
			ir.type = nil
		}
		but node.type == parse_type.expr {
			ir = compile_expr(f, node, false)
			//ir.type = nil
		}
		else {
			msg := 'unknown statement: '
			try {
				msg += '<'+ parse_type[node.type] +'>'
			}
			catch {
				msg += stop_failed()
			}
			throw stop_compile(node.cp, msg)
		}
		ret ir
	}

	fun compile_if f node {
		// TODO: IR must return same type as block, promote all blocks (buts&else)to the same type
		expr := compile_expr(f, node.expr, true)
		block := compile_block(f, node.block)
		ir := new_ir(f, node.cp, block.type, ops.if, [expr, block])
		ret ir
	}

	fun compile_while f node {
		expr := compile_expr(f, node.expr, true)
		block := compile_block(f, node.block)
		ir := new_ir(f, node.cp, block.type, ops.while, [expr, block])
		ret ir
	}

	fun compile_block f block {
		ir := new_ir(f, nil, nil, ops.block, [])
		//last := len block - 1 // for discarding
		each block {
			x := compile_stm(f, item)
			//TODO add discard to all but the last one (but rething discard first)
			//if index < last {
				//x = new_ir(f, x.cp, nil, ops.discard, x)
			//}
			ir.arg += x
		}
		if len block > 0 {
			ir.cp = ir.arg[0].cp
			ir.type = ir.arg[-1].type
		}
		ret ir
	}

	fun compiler parsed {
		init parsed throw stop_compile(nil, 'compiler: input object is nil')
		init parsed.ast throw stop_compile(nil, 'compiler: input object has no ast')
		if len parsed.ast == 0 throw stop_compile(nil, 'compiler: input AST has zero nodes')
		code := []
		main := create_fun({ cp: parsed.ast[0].cp, head: { id: 'main' } })
		ir := compile_block(main, parsed.ast)
		ret ir
	}
}
