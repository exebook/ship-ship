fun parse_conditional_code i {
	a := tokens[i]
	if a and a.type == lex_type.sym and a.n == lex_sym['{'] {
		block := nil
		n := parse_block(i + 1)
		use n block = re else n = i
		a = tokens[n]
		if a and a.type == lex_type.sym and a.n == lex_sym['}'] inc n
		else {
			use block
				throw stop_parse(i, '"}" after conditional code expected')
			else
				throw stop_parse(i, 'conditional code or "}" expected')
		}
	}
	else {
		n = parse_statement(i)
		init n throw stop_parse(i, '"{" or a statement expected')
		block = [re]
	}
	re = block
	ret n
}

fun parse_else i { // reused in init/use
	a := tokens[i]
	if a and a.type == lex_type.keyword and a.n == lex_keyword.else {
		block := nil
		n := parse_conditional_code(i + 1)
		use n block = re else n = i
		re = { cp: place(i), type: parse_type.else, block }
		ret n
	}
}

fun parse_if_cont i {
	i = _parse_or(i, [parse_else, parse_but])
	ret i
}

fun parse_but i {
	a := tokens[i]
	if a and a.type == lex_type.keyword and a.n == lex_keyword.but {
		n := parse_expr(i + 1)
		init n throw stop_parse(i + 1, 'conditional expression expected after <but>')

		expr := re
		block := nil
		more := nil

		n = parse_conditional_code(n)
		block = re

		n1 := parse_if_cont(n)
		if n1 { n = n1; more = re }

		re = { type: parse_type.but, expr, block, more, cp: place(i) }
		ret n
	}
}

fun parse_if i {
	a := tokens[i]
	if a and a.type == lex_type.keyword and a.n == lex_keyword.if {
		n := parse_expr(i + 1)
		init n throw stop_parse(i + 1, 'conditional expression expected after <if>')
		expr := re

		n = parse_conditional_code(n)
		block := re

		more := nil
		n1 := parse_if_cont(i)
		use n1 { n = n1 more = re }
		re = { type: parse_type.if, expr, block, more, cp: place(i) }
		ret n
	}
}

fun parse_while i {
	a := tokens[i]
	if a and a.type == lex_type.keyword and a.n == lex_keyword.while {
		n := parse_expr(i + 1)
		init n throw stop_parse(i + 1, 'conditional expression expected after <while>')
		expr := re

		n = parse_conditional_code(n)
		block := re
		re = { type: parse_type.while, expr, block, cp: place(i) }
		ret n
	}
}

