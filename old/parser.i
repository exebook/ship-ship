/:todo
	make len part of parse_prefix and make 'the' use parse_prefix instead of parse_value
todo:/

enum parse_type {
	int float str bool operator expr id
	prefix_def // a node representing a new prefix definition
	prefix_sym
	prefix_id
	value call
	define
	each_index
	assign
	expr_stat
	member
	nil
	color
	log are cout
	semicolon
	tag
	group
	if but else while
	builtin
}

fun new_parser {
incl 'parse-if'
	fun stop_parse i msg {
		ret stop(ERR.parse, place(i), msg)
	}

var lexed tokens re id_array id_map
ret parser

fun parser _lexed {
	lexed = _lexed
	// copy vars from object for the faster access
	tokens = lexed.tokens
	id_array = lexed.id_array
	id_map = lexed.id_map

	ast := []
	i := parse_block(0)
	if i == len tokens {
		parsed := { lexed, ast: re }
		ret parsed
	}
	else {
		init i i = 0
		throw stop_parse(i, 'parse error: tokens remaining')
	}
}

//fun parse_stm i {
//	var n
//	n = parse_word(i)
//	if n ret n
//	n = parse_prefix_def(i)
//	if n ret n
//	n = parse_expr(i)
//	if n ret n
//}

fun place i {
	ret {
		i, lexed
	}
}

//$

fun parse_ship_list i item_parser break_on_newline {
	// TODO: think if this should return nil instead of []
	i0 := i
	x := i
	prev := nil
	if break_on_newline {
		y := tokens[x].y
		while x < len tokens {
			t := tokens[x]
			LN_break := y != t.y
			if prev and prev.type == lex_type.sym and prev.n == lex_sym[','] {
				y = t.y
				LN_break = false
			}
			if LN_break {
				bk_token := tokens[x]
				tokens[x] = make_token(
					bk_token.x,
					bk_token.y,
					bk_token.i,
					lex_type.sym,
					';', lex_sym[';'], 0)
				@
			}
			prev = t
			inc x
		}
	}
	R := []
	loop {
		nothing := true
		a := tokens[i]
		if a and a.type == lex_type.sym and a.n == lex_sym[','] {
			R += i // store numbers (token position) for comma
			inc i
			nothing = false
		}
		n := item_parser(i)
		if n {
			i = n
			nothing = false
			R += re
		}
		but nothing @
	}
	// logic:
	// [] = []
	// [1] = [1]
	// [, 1] = [nil, 1]
	// [1, ] = [1, nil]
	// [1,,2] = [1, nil, 2]
	// [1,2,3] = [1, 2, 3]
	// [1 2 3] = [1, 2, 3]
	// [1,,] = [1, nil, nil]
	// [,,1] = [nil, nil, 1]
	// [,] = [nil, nil]

	if break_on_newline and bk_token {
		tokens[x] = bk_token
	}

	re = []

	if len R == 0 {
		ret i
	}

	fun mk_nil i { ret { type: parse_type.nil, cp: place(i) } }

	if is_object(R[0]) { re += R[0] } else { re += mk_nil(R[0]) }

	x = 1
	while x < len R {
		if is_object(R[x]) {
			re += R[x]
		}
		else {
			if is_number(R[x - 1]) {
				re += mk_nil(R[x - 1])
			}
		}
		inc x
	}
	if is_number(R[-1]) re += mk_nil(R[-1])
	ret i
}

//fun parse_prefix_def i {
	//i0 := i
	//a := tokens[i]
	//if a and a.type == lex_type.id and a.s == 'prefix' {
		//inc i
		//a = tokens[i]
		//if a and a.type == lex_type.id {
			//id_array[a.n].type = lex_id_type.prefix
			//re = {
				//cp: place(i0)
				//type: parse_type.prefix_def
				//id: a.n
			//}
			//ret i + 1
		//}
		//else {
			//throw stop_parse(i, 'prefix keyword: id expected')
		//}
	//}
//}

fun parse_id i {
	token := tokens[i]
	if token and token.type == lex_type.id {
		re = {
			type: parse_type.id
			id: token.s
			cp: place(i)
		}
		ret i + 1
	}
}
//$

fun parse_tag i {
	a := tokens[i]
	if a and a.type == lex_type.id and a.n == lex_keyword.tag {
		n := parse_ship_list(i, parse_id, true)
		if n {
			re = { cp: place(i), type: parse_type.tag, list: re }
			ret n
		}
		else
			throw stop_parse(i, 'parse_tag: <tag> must be followed by a list of <id>')
	}
}

fun parse_literal i {
	a := tokens[i]
	init a ret
	if a.type == lex_type.int {
		re = {
			type: parse_type.int
			num: a.s
			cp: place(i)
		}
		ret i + 1
	}
	if a.type == lex_type.float {
		re = {
			type: parse_type.float
			num: a.s
			cp: place(i)
		}
		ret i + 1
	}
	if a.type == lex_type.str {
		re = {
			type: parse_type.str
			str: a.s
			cp: place(i)
		}
		ret i + 1
	}
	if a.type == lex_type.id {
		if a.n == lex_keyword.true {
			re = {
				type: parse_type.bool
				num: '1'
				cp: place(i)
			}
			ret i + 1
		}
		if a.n == lex_keyword.false {
			re = {
				type: parse_type.bool
				num: '0'
				cp: place(i)
			}
			ret i + 1
		}
	}
}

fun parse_prefix i {
	a := tokens[i]
	if a.type == lex_type.sym or a.type == lex_type.id {
		if check_flag(a.flag, lex_flag.prefix) {
			n := parse_primary(i + 1)
			if n {
				re = {
					type: parse_type.prefix_sym
					sym: a.n
					primary: re
					cp: place(i)
				}
				ret n
			}
			else throw stop_parse(i, 'parse prefix: <primary> operand expected')
		}
	}
	//but a.type == lex_type.id {
		//if id_array[a.n].type == lex_id_type.prefix {
			//n = parse_primary(i + 1)
			//re = {
				//type: parse_type.prefix_id
				//keyword: a.n
				//primary: re
				//cp: place(i)
			//}
			//ret n
		//}
	//}
}

fun parse_operator i {
	a := tokens[i]
	init a ret
	if check_flag(a.flag, lex_flag.infix) {
		re = {
			type: parse_type.operator,
			operator: lex_infix_map[a.s], // now .operator can compare to lex_infix_enum['+']
			cp: place(i)
		}
		ret i + 1
	}
}

fun _parse_or i ff {
	each ff {
		use item {
			n := item(i)
			if n {
				ret n
			}
		}
	}
}

fun _parse_list i f {
	// TODO: always return [], filter out semicolon somewhere else
	n := f(i)
	if n {
		R := []
		if re R += re // parse_semicolon() can return empty re, but non zero position
		i = n
		loop {
			n = f(i)
			if n {
				if re R += re
				i = n
				cont
			}
			else {
				re = R
				if len R == 0 re = nil
				ret i
			}
		}
	}
}

fun parse_call i {
	i0 := i
	a := tokens[i]
	if a and a.type == lex_type.sym and a.n == lex_sym['('] {
		inc i
		n := parse_expr(i)
		arg := []
		if n {
			arg += re
			loop {
				a = tokens[n]
				if a and a.type == lex_type.sym and a.n == lex_sym[','] {
					x := parse_expr(n + 1)
					init x {
						throw stop_parse(n + 1, 'parse call: expected argument expression')
					}
					arg += re
					n = x
				}
				else {
					break
				}
			}
		}
		else {
			n = i
		}
		b := tokens[n]
		if b and a.type == lex_type.sym and b.n == lex_sym[')'] {
			re = { type: parse_type.call, args: arg, cp:place(i0) }
			ret n + 1
		}
		else {
			init b throw stop_parse(n - 1, 'parsing call: expected ")", got EOF')
			else throw stop_parse(n, 'parsing call: expected ")"')
		}
	}
}

fun parse_member i {
	a := tokens[i]
	if a and a.type == lex_type.sym and a.n == lex_sym['.'] {
		a = tokens[i + 1]
		if a and a.type == lex_type.id {
			re = {
				type: parse_type.member
				id: a.s
				cp: place(i)
			}
			ret i + 2
		}
		else {
			throw stop_parse(i + 1, 'parse member: id expected, got <'+ lex_type[a.type] +'> <.'+a.s+'>')
		}
	}
}

fun parse_value i {
	//a := tokens[i]
	n := _parse_or(i,
		[
			// parse_each_item,
			// parse_each_index,
			parse_id,
			parse_group,
			// parse_array_literal,
			// parse_object_literal
		])
	if n {
		head := re
		n1 := n
		n = _parse_list(n, lam i {
			n := _parse_or(i, [
				parse_member,
				// parse_index,
				parse_call
			])
			ret n
		})
		if n {
			// if ret.length here is == 0, just find where, do not fix here!
			last := re[-1]
			if last.type == parse_type.call {
				re = { type: parse_type.value, head, tail: re, cp: place(i), lvalue: false }
			}
			else {
				re = { type: parse_type.value, head, tail: re, cp: place(i), lvalue: true }
			}
			ret n
		}
		else {
			re = head
			ret n1
		}
	}
}

fun parse_group i {
	a := tokens[i] // checked
	if a.type == lex_type.sym and a.n == lex_sym['('] {
		n := parse_expr(i + 1)
		if n {
			a = tokens[n]
			if a.type == lex_type.sym and a.n == lex_sym[')'] {
				re = {
					cp: place(i)
					type: parse_type.group
					expr: re
				}
				ret n + 1
			}
		}
		else {
			throw stop_parse(i + 1, 'expression expected after <(>')
		}
	}
}

fun parse_primary i {
	a := tokens[i] init a ret // TODO: remove this check from all subs
	var n
	n = parse_if(i) use n ret n
	n = parse_while(i) use n ret n
	n = parse_prefix(i) use n ret n
	n = parse_literal(i) use n ret n
	n = parse_builtin(i) use n ret n
   n = parse_value(i) use n ret n
}

fun parse_expr i {
	i0 := i
	i = parse_primary(i)
	use i {
		n := parse_expr_cont(i, [re])
		if n {
			re.cp = place(i0)
			ret n
		}
		re = {
			cp: place(i0)
			type: parse_type.expr
			stack: [re]
		}
		ret i
	}
}

fun parse_expr_cont i code {
	fun infix_precedence i op {
		/*
			TODO: implement table jump:
			t_jump op {
				lex_keyword.put { ret 16 }
				lex_keyword.pow { ret 15 }
			}
		*/
		// reordered for performance (frequent first)
		if op == lex_infix_enum['='] ret 1
		if op == lex_infix_enum['=='] or op == lex_infix_enum['!='] ret 7
		if op == lex_infix_enum['*'] or op == lex_infix_enum['/'] ret 11 or op == lex_infix_enum['mod'] ret 11
		if op == lex_infix_enum['+'] or op == lex_infix_enum['-'] ret 10
		//if op == lex_infix_enum['>='] or op == lex_infix_enum['<='] or op == lex_infix_enum['<'] or op == lex_infix_enum['>'] ret 8
		//if op == lex_keyword.and ret 3
		//if op == lex_keyword.or ret 2
		//if op == lex_infix_enum['+='] or op == lex_infix_enum['-='] or op == lex_infix_enum['*='] or op == lex_infix_enum['/='] ret 1
		//if op == lex_keyword.SHL or op == lex_keyword.SHR ret 9
		//if op == lex_keyword.AND ret 6
		//if op == lex_keyword.XOR ret 5
		//if op == lex_keyword.OR ret 4
		//if op == lex_keyword.put ret 13
////		if op == lex_keyword.pow ret 12
		throw stop_parse(i, 'parser: unknown operator ' + op)
	}
	i0 := i
	stack := []
	// stack-based "shunting yard" expression parsing
	loop {
		n := i
		i = parse_operator(i)
		init i @
		op := re
		i = parse_primary(i)
		init i @

		next := re
		precedence := infix_precedence(op.cp.i, op.operator)
		loop {
			if len stack == 0 @
			if infix_precedence(stack[-1].cp.i, stack[-1].operator) < precedence @
			code += pop stack
		}
		stack += op
		code += next
	}
	while len stack > 0 {
		code += pop stack
	}
	re = {
		type: parse_type.expr
		stack: code
		cp: place(i0)
	}
	ret n
}

var console_colors

fun parse_color i {
	// color := '~' [('red'|'green'|'blue'|'yellow'|'white'|'purple'|'reset'|num)] | '~~'
	init console_colors {
		console_colors = {
			black: 0,
			yellow: 178,
			green: 28,
			teal: 10,
			orange: 208,
			blue: 20,
			pink: 206,
			purple: 93,
			cloud: 33,
			brown: 124,
			red: 196,
			white: 15,
			gray: 8,
			back: 8,
			cyan: 45,
			reset: -1,
			//171 looks cool
		}
	}

	a := tokens[i]
	if a and a.type == lex_type.sym and a.n == lex_sym['~'] {
		a = tokens[i + 1]
		use a {
			if a.type == lex_type.id {
				s := a.n
				if console_colors[s] {
					re = { type: parse_type.color, num: console_colors[s], cp: place(i) }
					ret i + 2
				}
			}
			but a.type == lex_type.int {
				re = { type: parse_type.color, num: a.s, cp: place(i) }
				ret i + 2
			}
			but a.type == lex_type.sym and a.n == lex_sym['~'] {
				re = { type: parse_type.color, num: -1, cp: place(i) }
				ret i + 2
			}
		}
	}
}

builtins := nil

fun parse_builtin i {
	init the builtins it = {
		time_begin: 1
		time_end: 1
	}

	a := tokens[i]
	if a and a.type == lex_type.id and builtins[a.s] {
		n := parse_call(i + 1)
		if n {
			re = { cp: place(i), type: parse_type.builtin, name: a.s, arg: re }
			ret n
		}
	}
}

fun parse_log_are i keyword ptype {

		fun parse_expr_reflect i {
			n := parse_expr(i)
			if n {
				src := lexed.src.text
				a := tokens[i]
				b := tokens[n-1]
				re.reflect = src[a.i .. b.i+len b.s]
				ret n
			}
		}

		fun parse_log_expr i {
			ret _parse_or(i, [parse_color, parse_expr_reflect])
		}

	a := tokens[i]

	if a and a.type == lex_type.keyword and a.n == keyword {
		i0 := i
		inc i
		n := parse_ship_list(i, parse_log_expr, true)
		if n {
			re = { cp: place(i0), type: ptype, list: re }
			ret n
		}
	}
}

fun parse_log i {
	ret parse_log_are(i, lex_keyword.log, parse_type.log)
}

fun parse_are i {
	ret parse_log_are(i, lex_keyword.are, parse_type.are)
}

fun parse_cout i {
	ret parse_log_are(i, lex_keyword.cout, parse_type.cout)
}

fun parse_semicolon i {
	a := tokens[i]
	if a and a.type == lex_type.sym and a.n == lex_sym[';'] {
		re = { cp: place(i), type: parse_type.semicolon }
		ret i + 1
	}
}

fun parse_keyword_stt i {
//	ret nil

	ret _parse_or(i, [
		parse_semicolon,
//		parse_each,
//		parse_loop,
//		parse_function,
//		parse_while,
//		parse_return,
		parse_log,
		parse_are,
		parse_cout,
//		parse_halt,
		parse_tag,
//		parse_var,
//		parse_break,
//		parse_cont,
//		parse_inc,
//		parse_dec,
//		parse_assign_each_item,
//		parse_init,
//		parse_use,
//		parse_kill,
//		parse_try,
//		parse_throw,
//		parse_incl,
//		parse_enum,
//		parse_charmap,
	])
}


fun parse_block i {
	ret _parse_list(i, parse_statement)
}

fun parse_statement i {
	init tokens[i] ret
	n := parse_keyword_stt(i)
	use n ret n
	ret parse_expr(i)
}

} // end parser

/*
key { a b c }
key a b c.
key a b c
key a b,
	c


word int { i }

var x y: f32 b c: i8 a: val

i:i32 := 12
f:f32 := 1.25
d:f64 := 22
b:bool := true
log a b a + b
*/

/*
check_shortcut
get_precedence
parse
parse_array_literal
parse_assign_each_item
parse_break
parse_but
parse_charmap
parse_chr
parse_closing_brace
parse_color
parse_comma_expr_list
parse_cont
parse_cout
parse_dec
parse_each
parse_each_index
parse_each_item
parse_else
parse_enum
parse_enum
parse_expr_list
parse_expr_reflect
parse_found_miss
parse_func_body
parse_func_head
parse_function
parse_halt
parse_if
parse_if_cont
parse_inc
parse_incl
parse_index
parse_init
parse_is
parse_keys
parse_kill
parse_lambda
parse_lap
parse_len
parse_literal
parse_loop
parse_match
parse_nil
parse_object_literal
parse_ord
parse_param
parse_pop
parse_property
parse_property_list
parse_property_name
parse_pull
parse_return
parse_semicolon
parse_symbol
parse_throw
parse_try
parse_type_name
parse_use
parse_var
parser
*/