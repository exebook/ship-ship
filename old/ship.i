#!/usr/bin/env ship
// note this is not used anymore!

TEMP_C_FILE := '.last.c'

incl 'incl/load-ast-tests'
incl 'incl/argv'
incl 'incl/print'
incl 'incl/hilite'

incl 'error'
incl 'lex'
incl 'parser'
incl 'compiler'
incl 'c_back'

/***
-- boxing DONE
-- parsing of functions with all access to ids counted
-- code gen for expressions
-- mixing boxed and primary types
***/
cli_arg := nil
ship_main()

fun show_help {
	log `Ship compiler 2.x (written in ship)1`
	log
	log `ship [options] <source file> [options]`
	log `Debug options:`

	show_arg_help('   ',
		`--lex | show color lexer output (tokens)`
		`--lex-raw | show raw lexer output`
		`--ast | print AST`
		`--ast-shame | in shame form`
		`--ast-raw | in raw form`
		`--src | print syntax highlighted source file`
		`--src-mono | same but in monochrome`
		`--C-show | show C output`
		`--ir-show | show Intermediate Representation`
		`--ir-show-raw | in raw form`
		`-- | treat everything after '--' as code`
		`-x --exec <code> | process given code`

		`--lex-only | do not parse, compile or execute`
		`--parse-only | do not compile or execute`
		`-c --compile-only --no-run | compile only, do not run`
	)
	kill
}
log 'ASDASD'
fun c_compile c_code run {
	log 'HERE C COMPLILE'
	save(TEMP_C_FILE, c_code)
	out = system('if [ -e .last ]; then rm .last ; fi')
	out := system('tcc -g -I /v/ship/lib -I /v/js/ship/lib '+TEMP_C_FILE+' -o .last -lm')
	cout out

	if run {
		out = system('if [ -e .last ]; then ./.last ; else echo C compilation failed ; fi')
	}
	cout out
}

fun show_tokens tokens {
	log ~orange 'type' ~5 's' ~~ 'id' ~2 'group'
	log ~orange '─────────────────────────────────────────'
	each tokens
		log ~orange lex_type[item.type] ~5 '"'+item.s+'"' ~~ item.n ~2 item.group
}

fun show_raw_tokens tokens {
	log ~orange '─────────────────────────────────────────'
	each tokens
		log ~orange item
}

fun tokenize src {
	opt := { space: lex_option_space.property }
	lexer := new_lexer(opt)

	try {
		lexed := lexer(src)
	}
	catch {
		show_error(error)
		ret
	}

	use cli_arg.keys['lex'] {
		show_tokens(lexed.tokens)
	}
	use cli_arg.keys['lex-raw'] {
		show_raw_tokens(lexed.tokens)
	}
	ret lexed
}

fun ship_main {
	try {
		var src, tokens, node
		cli_arg = load_argv(['x exec'])
		ship_init_args()
		//are cli_arg
		lexed := tokenize(src)
		use cli_arg.keys['src'] {
			log hilite(src.text)
		}
		use cli_arg.keys['src-mono'] {
			log src.text
		}
		use cli_arg.keys['lex-only'] {
			ret
		}

		parsed := parse(lexed)
		init parsed ret

		use cli_arg.keys['parse-only'] {
			ret
		}

		compiler := compiler_new({})
		ir := compiler(parsed)

		use cli_arg.keys['ir-show-raw'] {
			log shame(ir, { indent: 3 })
			ret
		}

		use cli_arg.keys['ir-show'] {
			print_ir(ir)
			ret
		}

		try {
			c_code := generate(ir)
		}
		catch {
			show_error(error)
			ret
		}

		use cli_arg.keys['C-show'] {
			log hilite(c_code)
			ret
		}

		c_compile(c_code, cli_arg.keys['c'] == nil)
	}
	catch {
		show_error(error)
	}

fun parse lexed {
	parser := new_parser()
	try {
		parsed := parser(lexed)
	}
	catch {
		show_error(error)
		ret
	}

	init parsed {
		log 'parsing returned nil'
		ret
	}

	use cli_arg.keys['ast'] {
		try {
			print_node(parsed.ast)
			ret
		}
		catch {
			// non-ship error from print_node
			use parsed.ast {
				log 'the ast that had trouble printing:\n' parsed.ast
			}
			log ~gray error
			log ~red 'exception during printing of the parsing result'
			ret
		}
	}

	use cli_arg.keys['ast-shame'] {
		fun filter k v {
			if k == 'cp' {
				v = '<'+ v.i +'>'
			}
			ret v
		}
		log ~4 shame(parsed.ast, {filter})
		ret
	}
	use cli_arg.keys['ast-raw'] {
		log ~3 parsed.ast
		ret
	}
	ret parsed
}

//fun load_src_str str fname {
	//ret {
		//text: str, fname: fname
	//}
//}

fun remove_shebang t {
	if t {
		if t[0] == ord '#' {
			each t {
				if item == 10 { // Unix-like LF
					ret t[index+1..]
				}
				if item == 13 { // Mac CR
					index += 1
					if t[index] == 10 index += 1 // Windows CRLF
					ret t[index..]
				}
			}
		}
		ret t
	}
}

fun load_src_file fname {
	ret load_src_str(remove_shebang(load(fname)), fname)
}

fun ship_init_args {
	fun esc_cli_cmd s {
		ret s / '\\n' * '\n'
	}
	//are cli_arg
	x := nil
	if the cli_arg.keys.x x = it
	if the cli_arg.keys.exec x = it
	use x {
		x *= ' '
		src = load_src_str(esc_cli_cmd(x), '<x-args>')
		ret
	}
	if len cli_arg.raw > 0 {
		src = load_src_str(esc_cli_cmd(cli_arg.raw * ' '), '<raw-args>')
		ret
	}
	if len cli_arg.list > 0 {
		if len cli_arg.list > 1 {
			log 'only one file argument is supported'
			log 'arguments you provided:' cli_arg.list
			kill
		}
		src = load_src_file(cli_arg.list[0])
		ret
	}
	//if len src.text == 0
	show_help()
}

// No expressions below this line please, this is a functions area

}
