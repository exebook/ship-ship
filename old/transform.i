incl map lex 'error' hilite
hilite_theme = [171 248 0 1 3 196 21 100 95]
					//# ? ? str num ? op ? ?
hilite_keywords = 'int char main void'

fun flatten x {
	if typeof(x) == 'array' {
		re := []
		each x re += flatten(item)
		ret re
	}
	else ret x
}

ast := {
	type: 'prog'
	block: [
		{ type: 'define' id: 'a' expr: { type: 'num' num: 10 }}
		{ type: 'call' id: 'f2' args: [] }
		{ type: 'fun' id: 'f2' block: [
			{ type: 'fun' id: 'f1' block: [
				{ type: 'define' id: 'b' expr: { type: 'num' num: 20 }}
				{ type: 'log' list: [
					{ type: 'id' id: 'a' }
					{ type: 'id' id: 'b' }
					{ type: 'num' num: '30' }
				]}
			]}
			{ type: 'call' id: 'f1' args: [] }
		]}
	]
}


/:src
	a := 10
	fun f1 {
		b := 20
		log a b 30
	}
src:/

/:target
	#include <stdio.h>
	int vars[4], *var = &vars[3];
	int envs[4], *env = &envs[3];

	void f1() {
		var -= 1; // alocate locals
		var[0] = 20;
		printf("%i %i\n", var[0], env[0]);
		var += 1;
	}

	void main() {
		var -= 1; // alocate locals
		var[0] = 10;
		*--env = var[0];
		f1();
		env++;
		var += 1;
	}
target:/

enum mode {
	CODE
}

enum uplink { REF DEF GET FUN CALL ID ENVSIZE DECLS FIND }
enum decl { LOCAL ENV FUN }

fun map_semi a {
	re := []
	each a {
		re += item
		re += ';'
	}
	ret re
}

fun map_com a {
	re := []
	each a {
		if index > 0 re += ', '
		re += item
	}
	ret re
}

//fun not_nil y {
	//ret filter(y lam x = x != nil)
//}

fun c_block up block {
	block = map(block, lam x = c_unit(up x))
	ret lam m {
		re := map(block lam x = x(m))
		ret re
	}
}

fun c_prog up node {
	main := c_fun(up {
		type: 'fun'
		id: 'main'
		block: node.block
	})
	ret lam m {
		if m == mode.CODE {
			ret [
				'#include <stdio.h>\n'
				'int vars[4], *var = &vars[3];'
				'int envs[4], *env = &envs[3];'
			] + main(m)
		}
	}
}

fun c_unit up node {
	if node.type == 'define' ret c_define(up node)
	but node.type == 'log' ret c_log(up node)
	but node.type == 'fun' ret c_fun(up node)
	but node.type == 'call' ret c_call(up node)
	else throw ('what is ' + node.type + '?')
}

fun c_fun up node {
	up(uplink.FUN handler)

	refs := {} // value = env address
	decls := {}
	env_size := 0

	fun handler u a {
		if u == uplink.REF { refs[a] = 1 }
		but u == uplink.CALL { refs[a] = 1 }
		but u == uplink.DEF { decls[a] = { type: decl.LOCAL id: a } }
		but u == uplink.FUN { id := a(uplink.ID) decls[id] = { type: decl.FUN id, handler: a } }
		but u == uplink.GET { ret decls[a] }
		but u == uplink.ID { ret node.id }
		but u == uplink.ENVSIZE { ret env_size }
		but u == uplink.DECLS { ret decls }
		but u == uplink.FIND {
			if the decls[a] ret it
			ret up(uplink.FIND a)
		}
	}
	code := filter(node.block lam x = x.type != 'fun')
	child_funs := filter(node.block lam x = x.type == 'fun')

	child_funs = c_block(handler child_funs)
	code = c_block(handler code)

	decl_keys := keys(decls)
	stack_addr := 0
	local_count := 0
	each decl_keys {
		d := decls[item]
		if d.type == decl.LOCAL {
			d.stack_addr = stack_addr
			stack_addr += 1
			local_count += 1
		}
	}
	r := keys(refs)
	each r {
		init the decls[item] {
			it = { type: decl.ENV id: item env_addr: env_size }
			env_size += 1
			up(uplink.REF item)
		}
	}
	ret lam m {
		if m == mode.CODE {
			alloc := 'var -= ' + local_count
			dealloc := 'var += ' + local_count
			body := flatten([map_semi([] + alloc + code(m) + dealloc)]) * ''
			ret flatten(child_funs(m)) +[ 'void ' node.id '()' ' {' body '}' ]
		}
	}
}

fun omap o prop val {
	k := keys(o)
	re := []
	each k {
		x := o[item]
		if x[prop] == val re += x
	}
	ret re
}

fun copy_env sdecl ddecl {

}

fun var_addr d {
	if d.type == decl.LOCAL {
		ret 'var[' + d.stack_addr + ']'
	}
	but d.type == decl.ENV {
		ret 'env[' + d.env_addr + ']'
	}
	but d.type == decl.FUN {
		throw ('var_addr(decl.FUN) is not possible')
	}
}

fun c_call up node {
	up(uplink.CALL node.id)

	ret lam m {
		if m == mode.CODE {
			caller := up
			callee := up(uplink.GET node.id).handler
			env_size := callee(uplink.ENVSIZE)
			callee_decls := omap(callee(uplink.DECLS) 'type' decl.ENV)
			caller_decls := caller(uplink.DECLS)
			copy_len := len callee_decls
			enter := []
			each callee_decls {
				env_addr := item.env_addr
				src_addr := var_addr(caller_decls[item.id])
				enter += 'env[' + (env_addr - copy_len) + '] = ' + src_addr + ';'
			}
			enter += ['env -= ' + copy_len + ';']
			leave := 'env += ' + copy_len
			ret [ enter callee(uplink.ID) '();' [leave] ]
		}
		ret ''
	}
}

fun c_define up node {
	up(uplink.DEF node.id)
	x := c_expr(up node.expr)
	ret lam m {
		if m == mode.CODE {
			d := up(uplink.GET node.id)
			ret [var_addr(d) ' = ' x(m)]
		}
	}
}

fun c_id up node {
	up(uplink.REF node.id)
	ret lam m {
		if m == mode.CODE {
			d := up(uplink.GET node.id)
			ret var_addr(d)
		}
	}
}

fun c_num up node {
	ret lam m {
		if m == mode.CODE { ret node.num+'' }
	}
}

fun c_primary up node {
	if node.type == 'num' ret c_num(up node)
	but node.type == 'id' ret c_id(up node)
}

fun c_expr up node {
	ret c_primary(up node)
}

fun c_list up list {
	list = map(list lam x = c_expr(up x))
	ret lam m {
		ret map(list lam x = x(m))
	}
}

fun c_log up node {
	list := c_list(up node.list)
	ret lam m {
		c := list(m)
		if m == mode.CODE {
			ret [
				'printf("'
					map_com(map(c lam x = '%i'))
					'", '
					map_com(c)
				')'
			]
		}
	}
}

fun beautify_c code {
	indent := 0
	fun tab indent line {
		t := ''
		while len t < indent*3 t += ' '
		ret t + line
	}
	line := ''
	re := ''
	each code {
		if item == ord '{' { line += '{\n'; re += tab(indent line); line = '';indent += 1 }
		but item == ord ';' { re += tab(indent line+';\n'); line = '' }
		but item == ord '}' { indent -= 1; line += '}\n'; re += tab(indent line); line = '' }
		else line += chr item
	}
	re += tab(indent line)
	ret re
}

ast = ast//.block[1].block[1]
//are ast
code := c_prog(lam {}, ast)(mode.CODE)
log
//log flatten(code)*''
save('/tmp/tr.c' beautify_c(code*''))
log hilite(beautify_c(code*''))

