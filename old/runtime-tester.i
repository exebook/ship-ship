incl 'error'
incl 'lex'
incl 'parser'
incl 'compiler'
incl 'c_back'
incl 'cc'

incl 'incl/hilite'
incl 'incl/argv'

fun parse_test_file s {
	exact_names := cli_arg.list

	fun filter_by_key test {
		each key_filter {
			key := item
			if (key find test.labels) >= 0 ret true
		}
		ret false
	}

	key_filter := []
	each ['k' 'key'] use the cli_arg.keys[item] key_filter += it
	all_keys := {}
	s /= '\n'
	Z := []  T := []
	mode := -1
	cur_test := nil
	each s {
		if len item == 0 cont
		if item[0] == ord '#' {
			head := item[1..] / ' '
			key := head[0]
			id := head[1]
			labels := head[2..]
			each labels {
				init the all_keys[item] it = 0
				inc it
			}
			if key == 'code' {
				cur_test = { id, labels, code: [], output: [] }
				T += cur_test
				mode = 0
			}
			but key == 'output' mode = 1
			but key == 'end' @
		}
		but item[0:2] == '//' {
		}
		but mode == 0 {
			cur_test.code += item
		}
		but mode == 1 {
			cur_test.output += item
		}
	}
	if len the exact_names > 0 {
		Z = T T = []
		each Z {
			if (item.id find it) >= 0 T += item
		}
	}

	if len key_filter > 0 {
		Z = T T = []
		each Z {
			if filter_by_key(item) T += item
		}
	}

	each T {
		item.code *= '\n'
		item.output *= '\n'
	}
	each ['l' 'last']
		use the cli_arg.keys[item]
			T = T[-parse_int(it[0])..]
	use cli_arg.keys['1'] T = [T[-1]]
	use cli_arg.keys['keys'] {
		log all_keys
		kill
	}
	ret T
}

fun cmp_by_token a b { // return diff position or nil if identical
	try {
		A := lexer(load_src_str(a, 'a'))
		B := lexer(load_src_str(b, 'b'))
	}
	catch {
		e := error
		if is_object(e) and e.debug log e.debug
		else log 'FATAL cmp_by_token error:' e
		kill
	}
	max := len A.tokens
	if max < the len B.tokens max = it
	i := 0
	while i < max {
		init A.tokens[i] a = '<EOF>' else a = A.tokens[i].s
		init B.tokens[i] b = '<EOF>' else b = B.tokens[i].s
		if a != b ret B.tokens[i].y // return actual output first different line number
		inc i
	}
}

fun esc_title s {
	ret s / '\n' * '\\n'
}

fun frame title id {
	x := 60
	s := ' '+ title +': "'+ esc_title(id) +'" '
	loop {
		if len s < x s = '─'+ s
		if len s < x s = s +'─'
		if len s >= x @
	}
	ret s
}

fun process_options {
	supported_keys := {
		'1':1 k:1 h:1 help:1 o:1 d:1 s:1 x:1 src:1 diff:1 l:1 last:1 k:1 key:1 keys:1
	}
	bad_options := []

	each keys(cli_arg.keys) {
		if supported_keys[item] != 1 {
			help_flag = true
			bad_options += item
		}
	}

	if cli_arg.keys['h'] or cli_arg.keys['help'] help_flag = true

	if help_flag {
		help := ` t - Ship runtime test utility

	Usage:
		t Options || Tests

		By default all tests are executed unless test identifiers are provided.

	Tests:
		one or more string identifiers, for example: "bootstrap parse-list"
		tests and options can interleave, for example:

		t bootstrap -x parse-list --src

	Options:
		test | string id of a test, for example: parse-list
		-h --help | show this help message
		-s --src | show source for a failed test
		-d --diff | show diff for failed
		-o | show actual output for failed
		-x | show expected output for failed
		-l --last <number> | run only last <number> of tests
		-1 | run only one last test
		-k --key <key id> | filter only test matching keywords (multiple allowed)
		--keys | list all keys (labels)
	`
		log column_tabify(help)
		each bad_options log 'unknown option:' item
		kill
	}
}

fun main1 {
	process_options()
	try {
		a := load('data/runtime.ship')
	}
	catch {
		log error
		ret
	}
	a = parse_test_file(a)
	lex_opt := { space: lex_option_space.property }
	lexer = new_lexer(lex_opt)
	each a {
		item.actual = run(item)
		n := cmp_by_token(item.output, item.actual)
		use n {
			log ~red item.id ~~ item.labels*' '
			if cli_arg.keys['s'] or cli_arg.keys['src'] {
				log ~white frame('source', item.id)
				log hilite(item.code)
			}
			if cli_arg.keys['d'] or cli_arg.keys['diff'] {
				log ~red frame('diff', item.id)
				show_side_by_side(item.output, item.actual, n)
			}
			if cli_arg.keys['o'] {
				log ~red frame('output', item.id)
				log hilite(item.actual)
			}
			if cli_arg.keys['x'] {
				log ~green frame('expected', item.id)
				log hilite(item.output)
			}
		}
		else {
			log ~green item.id ~~ item.labels*' '
		}
	}
}

fun show_side_by_side X A n {
	eof := '__EOF__'//'     .      .      .'
	arrow := [' <─── ' 'expected  ' '  actual' ' ───> ']
	arrow_color := [0 40 9 0]
	arrow_size := 0 each arrow arrow_size += len item
	gap := '' while len gap < arrow_size gap += ' '
	X /= '\n' while X[-1] == '' pop X
	A /= '\n' while A[-1] == '' pop A
	while len X < len A X += eof
	while len A < len X A += eof
	max := 0
	each X { if the len item > max max = it }
	each X { while len item < max item += ' ' }
	max = 0
	each A { if the len item > max max = it }
	each A { while len item < max item += ' ' }
	each X {
		if index == n {
			s := ''
			each arrow s += color(arrow_color[index], item)
		}
		else s = gap
		a := hilite(item)
		a /= eof
		a *= color(22, '.  .  .')
		b := hilite(A[index])
		item = '| ' + a +' | '+ s +' | '+ b +' |'
	}
	log X * '\n'
}

fun uncolor a {
	R := ''
   each a {
      if item == 0x1b {
         while item != ord 'm' index += 1
      }
      else R += chr item
   }
   ret  R
}


fun run test {
	save('test-exe.c', test.code)
	o := system('/v/ship/bin/ship test-exe.c')
	o = uncolor(o)
	ret o
}

lexer := nil
help_flag := false
cli_arg := load_argv(['k' 'key' 'l' 'last'])
main1()
