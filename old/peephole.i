incl map
incl lex 'error' hilite
incl flatten cc

hilite_theme = [171 248 0 1 3 196 21 100 95]

fun test {
	ret ['test_range()' ';']
}

//  test

b := map([
	//func('f' 1 [
		//c_ret(c_string('abc'))
	//])
	//func('f1' 2 [
		//varset(0 call('f' []))
		//varset(1 arr_literal([con('11')con('22')con('33')]))
		//varset(0 add(varget(0) c_string('XYZ')))
		//add_to(varget(1) varget(0))
		//add_to(varget(0) con('55'))
		//'',[
			//'printf("ok\\n")'';'
		//]
		//varset(1 c_string('77'))
		//varset(0 add(varget(0) c_string('77')))
		//varset(0 add(varget(0) con('77')))
		//c_log([ varget(0) varget(1) ])
		//c_gc_run()
		//'array_compact(_v0)' ';'
		//c_gc_run()

		//c_log([ varget(0) ])
		//c_loop([
			//varset(0 add(varget(0) con('1')) )
			//c_if(cmp(varget(0) con('2')), [c_cont()] )
			//c_loop([
			//])
			//c_if(cmp(varget(0) con('5')), [c_break()] )
			//c_log([cmp(varget(0) con('5')) varget(0) con('555')])
		//])
	//])
	c_main(2 [], [
		c_log([c_con('555')])
	], [c_runtime_end()])
] lam x = x.statem())

program_code := code_flatten([templ1 b templ2])
log hilite(program_code)
save('/tmp/peep.c' program_code)
