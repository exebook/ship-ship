fun create_fun parser_node {
	cell_count := 0
	temp_count := 0
	ids := {}
	ret {
		new_temp: lam msg {
			inc temp_count;
			use msg ret '__'+ msg +'_t'+ temp_count
			ret '__t'+ temp_count
		}
		new_cell: lam {
			inc cell_count;
			ret cell_count
		}
		add_id: lam id { ids[id] = { id } }
		id: lam { ret parser_node.head.id }
		node: parser_node
	}
}

/:rem
function create_function(f) {
	if (f.head.type == undefined) f.head.type = 'var'
	f.code = []
	f.low_vars_decl_code = []
	f.c_param_code = []
	f.temp_var = 0
	f.eachs = []
	f.var_arg_count = 0
	f.var_count = 0
	f.low_level = false
	f.var_func = false
	f.low_level_args_count = 0

	f.env = {} // contains { num, v }
	// just record all id's used in a function but not declared in it (declared above lex/closure)
	f.env_count = 0 // populates env[..].num
	f.callers = new Set
	f.lams = []
	f.match_stack = []
	if (f.head.type != 'var') {
		f.low_level = true
	}
	f.trys = 0
}
rem:/
