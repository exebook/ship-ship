incl 'incl/load-ast-tests'
incl 'incl/argv'
incl 'incl/hilite'
incl 'incl/print'

incl 'error'
incl 'lex'
incl 'parser'

fun print_help {
	log 'NAST: new AST test suite'
	log 'nast [options]'
	log '   -e: print extended data for failed test'
	log '   -l: show lexed'
}

options := {
	expand_shame: false,
	show_lexed: false,
}

try {
	tests := load_parse_tests()
	nast_init_args()
	test_ast()
}
catch {
	show_error(error)
//	show_trace((error).stack)
}
fun nast_init_args {
	cli_arg := load_argv([])
	use cli_arg.keys['h'] {
		print_help()
		kill
	}
	use cli_arg.keys['e'] {
		options.expand_shame = true
	}
	use cli_arg.keys['l'] {
		options.show_lexed = true
	}
}

fun show_ok test {
	log ~green '"'+ esc_title(test.src) +'"'
}

fun esc_title s {
	ret s / '\n' * '\\n'
}

fun frame title id {
	x := 60
	s := ' '+ title +': "'+ esc_title(id) +'" '
	loop {
		if len s < x s = '─'+ s
		if len s < x s = s +'─'
		if len s >= x @
	}
	ret s
}

fun show_error_cmp test parsed actual {
	log
	if options.show_lexed {
		log ~4 'lexed:' shame(parsed.lexed, lam a { ret a }, 3)
	}
	log ~pink actual
	log ~6 test.result
	log '-----------------------------------'
	log ~pink frame('actual', test.src)
	print_node(actual)
	log ~6 frame('expected', test.src)
	print_node(test.result)
	log ~red '--------- faled ----------'
	log hilite(test.src)
}

fun run_single_parse_test lex parse test_no test {
	lexed := test_lex(lex, test.src)

	parsed := test_parse(parse, lexed)

	init parsed {
		log ~red 'parser returned nil'
		kill
	}
	if not is_object(parsed) { // todo: use 'not' instead of '== false'
		log ~red 'parser returned unrecognized result'
		kill
	}
	if parsed.error {
		e := parsed.error
		if is_object(e) {
			if test.result and test.result.error_msg {
				token := e.cp.lexed.tokens[e.cp.i]
				actual_error := { x: token.x y: token.y error_msg: e.msg }
				if shame(actual_error) == shame(test.result) {
					A = 0
					B = 0
					//kill
				}
				else {
					log ~1 'parser returned error that does not match the expected result'
					log ~2 'expected error:' test.result
					log ~3 'actual error:' actual_error
					kill
				}
			}
			else {
				log 'unexpected parser error in test:'
				show_error(e)
				kill
			}
		}
		else {
			show_error(e)
			log 'run_single_parse_test: incomplete error structure'
			kill
		}
	}
	else {
		if test.result and is_object(test.result) and test.result.error_msg {
			if options.show_lexed {
				log ~4 'lexed:' shame(parsed.lexed, lam a { ret a }, 3)
			}
			log ~pink frame('test', test.src)
			log ~pink 'parser returned OK, but error was expected'
			kill
		}

		if is_array(test.result) {
			actual := parsed.ast  // 'actual' is what we compare to 'test.result'
		}
		else {
			actual = parsed
		}

		fun filter k v {
			if k == 'cp' and is_object(v) ret v.i
			ret v
		}

		sh_opt := { filter, condensed: false, show_dup_strings: true }
		A := shame(actual, sh_opt)
		B := shame(test.result, sh_opt)
	}

	if A == B {
		show_ok(test)
	}
	else {
		show_error_cmp(test, parsed, actual)
		kill
	}
}

fun test_parse parse lexed {
	try {
		ret parse(lexed)
	}
	catch {
		ret { 'error': error }
	}
}

fun test_lex lexer src {
	try {
		src = { text: src, fname: '__test__'}
		ret lexer(src)
	}
	catch {
		log
		log 'ast-test: lexer error:' ~pink error
		show_trace()
		kill
	}
}

fun test_ast {
	lexer := new_lexer({ space: lex_option_space.property })
	parser := new_parser()

	i := 0
	each tests {
		test := {
			src: item.src
			result: item.result
		}
		run_single_parse_test(lexer, parser, index, test)
	}
}
