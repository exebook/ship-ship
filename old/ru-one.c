#define CARG_MAX 3
#define CVAR_PREFIX _v
#define CARG_PREFIX _a
#define CARG_PTR _aptr
#define CARG_COUNT _acount
#define CARG_ENV _env
#define __arg_decl __env_t *_env, short _acount, val _a0,val _a1,val _a2, val *_aptr
#define CARG_MAX 3
#define __call_n(f, e, n, a...) \
__builtin_choose_expr(n==0, f(e,n,__nil,__nil,__nil,0), \
__builtin_choose_expr(n==1, f(e,n,((val[]){a})[0],__nil,__nil,0), \
__builtin_choose_expr(n==2, f(e,n,((val[]){a})[0],((val[]){a})[1],__nil,0), \
__builtin_choose_expr(n==3, f(e,n,((val[]){a})[0],((val[]){a})[1],((val[]){a})[2],0), \
__builtin_choose_expr(n>3, f(e,n,((val[]){a})[0],((val[]){a})[1],((val[]){a})[2],&(((val[]){a})[3])), \
f(0,0,0,0,0,0) \
)))))
#define __call_v(f, e, n, vec) f(e, n, n>0?vec[0]:__nil, n>1?vec[1]:__nil, n>2?vec[2]:__nil,n>=3?&vec[3]:0)
#define __arg(n) n > _acount ? __nil : \
__builtin_choose_expr(n==0, _a0, \
__builtin_choose_expr(n==1, _a1, \
__builtin_choose_expr(n==2, _a2, \
_aptr[n-3] \
)))

#include "ru.c"
