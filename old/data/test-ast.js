'a() = 1',
'lvalue must precede =',
//'a ( ) = 1\n      ^--- lvalue must precede =    ',

'a[1] := 2',
//'a [ 1 ] := 2\n        ^--- id must precede :=     ',
'id must precede :=',

'1 + 2',
{type:'expr_stat',expr:{type:'expr',stack:[{type:'int',num:1},{type:'int',num:2},{type:'operator',operator:lex.names.plus}]}},

'a[1] = 2',
{type:'assign',operation:lex.names.eq,operation_i:4,lvalue:{type:'lvalue',head:{type:'id',id:'a'},tail:[{type:'index',left:{type:'int',num:1}}]},expr:{type:'int',num:2}},

'~ 1 + - 2 * 3 + 4',
{type:'expr_stat',expr:{type:'expr',stack:[{type:'prefix',operator:lex.names.tilde,primary:{type:'int',num:1}},{type:'prefix',operator:lex.names.minus,primary:{type:'int',num:2}},{type:'int',num:3},{type:'operator',operator:lex.names.mul},{type:'operator',operator:lex.names.plus},{type:'int',num:4},{type:'operator',operator:lex.names.plus}]}},


'1.23 + 5 * 3 - 4',
{type:'expr_stat',expr:{type:'expr',stack:[{type:'float',num:1.23},{type:'int',num:5},{type:'int',num:3},{type:'operator',operator:lex.names.mul},{type:'operator',operator:lex.names.plus},{type:'int',num:4},{type:'operator',operator:lex.names.minus}]}},

'(1 + 2) 3',
[{type:'expr_stat',expr:{type:'group',expr:{type:'expr',stack:[{type:'int',num:1},{type:'int',num:2},{type:'operator',operator:lex.names.plus}]}}},{type:'expr_stat',expr:{type:'int',num:3}}],

'abc (1 + 2) 3',
[{type:'expr_stat',expr:{type:'value',head:{type:'id',id:'abc'},tail:[{type:'call',args:[{type:'expr',stack:[{type:'int',num:1},{type:'int',num:2},{type:'operator',operator:lex.names.plus}]}]}]}},{type:'expr_stat',expr:{type:'int',num:3}}],

'i + 3',
{type:'expr_stat',expr:{type:'expr',stack:[{type:'id',id:'i'},{type:'int',num:3},{type:'operator',operator:lex.names.plus}]}},
// replace ' with " because JSON will do that
'(2 + i +)',
'parse group: expected ")"',

'f(1,2',
'parsing call: expected ")", got EOF',

'f(1,2 3',
'parsing call: expected ")"',

'f(1,2)',
{type:'expr_stat',expr:{type:'value',head:{type:'id',id:'f'},tail:[{type:'call',args:[{type:'int',num:1},{type:'int',num:2}]}]}},

'(1, )',
'parse group: expected ")"',

'(1, 2)',
'parse group: expected ")"',

'1 + 2 456',
[{type:'expr_stat',expr:{type:'expr',stack:[{type:'int',num:1},{type:'int',num:2},{type:'operator',operator:lex.names.plus}]}},{type:'expr_stat',expr:{type:'int',num:456}}],

'f(f(1))',
{type:'expr_stat',expr:{type:'value',head:{type:'id',id:'f'},tail:[{type:'call',args:[{type:'value',head:{type:'id',id:'f'},tail:[{type:'call',args:[{type:'int',num:1}]}]}]}]}},

'f()f()',
[{type:'expr_stat',expr:{type:'value',head:{type:'id',id:'f'},tail:[{type:'call',args:[]}]}},{type:'expr_stat',expr:{type:'value',head:{type:'id',id:'f'},tail:[{type:'call',args:[]}]}}],

'a += 1',
{type:'assign',operation:lex.names.plus_eq,operation_i:1,lvalue:{type:'id',id:'a'},expr:{type:'int',num:1}},

'1',
{type:'expr_stat',expr:{type:'int',num:1}},

'1 plus 2',
{type:'expr_stat',expr:{type:'expr',stack:[{type:'int',num:1},{type:'int',num:2},{type:'operator',operator:lex.names.plus}]}},

'1+2',
{type:'expr_stat',expr:{type:'expr',stack:[{type:'int',num:1},{type:'int',num:2},{type:'operator',operator:lex.names.plus}]}},

'1+2+3',
{type:'expr_stat',expr:{type:'expr',stack:[{type:'int',num:1},{type:'int',num:2},{type:'operator',operator:lex.names.plus},{type:'int',num:3},{type:'operator',operator:lex.names.plus}]}},

'1+2+3+4',
{type:'expr_stat',expr:{type:'expr',stack:[{type:'int',num:1},{type:'int',num:2},{type:'operator',operator:lex.names.plus},{type:'int',num:3},{type:'operator',operator:lex.names.plus},{type:'int',num:4},{type:'operator',operator:lex.names.plus}]}},

'1+2+3+4+5',
{type:'expr_stat',expr:{type:'expr',stack:[{type:'int',num:1},{type:'int',num:2},{type:'operator',operator:lex.names.plus},{type:'int',num:3},{type:'operator',operator:lex.names.plus},{type:'int',num:4},{type:'operator',operator:lex.names.plus},{type:'int',num:5},{type:'operator',operator:lex.names.plus}]}},

'[]',
{type:'expr_stat',expr:{type:'array_literal',items:[]}},

'[1]',
{type:'expr_stat',expr:{type:'array_literal',items:[{type:'int',num:1}]}},

'[1,2]',
{type:'expr_stat',expr:{type:'array_literal',items:[{type:'int',num:1},{type:'int',num:2}]}},

'[1,2,3]',
{type:'expr_stat',expr:{type:'array_literal',items:[{type:'int',num:1},{type:'int',num:2},{type:'int',num:3}]}},

'[1,2,3,4]',
{type:'expr_stat',expr:{type:'array_literal',items:[{type:'int',num:1},{type:'int',num:2},{type:'int',num:3},{type:'int',num:4}]}},

'[, 1]',
{type:'expr_stat',expr:{type:'array_literal',items:[{type: 'nil'},{type:'int',num:1}]}},

'[1, ]',
{type:'expr_stat',expr:{type:'array_literal',items:[{type:'int',num:1},{type: 'nil'}]}},

'[1,,2]',
{type:'expr_stat',expr:{type:'array_literal',items:[{type:'int',num:1},{type: 'nil'},{type:'int',num:2}]}},

'[1 2 3]',
{type:'expr_stat',expr:{type:'array_literal',items:[{type:'int',num:1},{type:'int',num:2},{type:'int',num:3}]}},

'[1,,]',
{type:'expr_stat',expr:{type:'array_literal',items:[{type:'int',num:1},{type:'nil'},{type:'nil'}]}},

'[,,1]',
{type:'expr_stat',expr:{type:'array_literal',items:[{type:'nil'},{type:'nil'},{type:'int',num:1}]}},

'a = nil',
{type:'assign',operation:lex.names.eq,operation_i:1,lvalue:{type:'id',id:'a'},expr:{type:'nil'}},

'a := nil',
{"type":"define","id":"a","expr":{"type":"nil"}},

'a := 1+2',
{"type":"define","id":"a","expr":{"type":"expr","stack":[{type:'int',"num":1},{type:'int',"num":2},{"type":"operator","operator":lex.names.plus}]}},

'1+2*3+4',
{type:'expr_stat',expr:{type:'expr',stack:[{type:'int',num:1},{type:'int',num:2},{type:'int',num:3},{type:'operator',operator:lex.names.mul},{type:'operator',operator:lex.names.plus},{type:'int',num:4},{type:'operator',operator:lex.names.plus}]}},

'id',
{type:'expr_stat',expr:{type:'id',id:'id'}},

'123',
{type:'expr_stat',expr:{type:'int',num:123}},

'"ABC"',
{type:'expr_stat',expr:{type:"str",str:"ABC"}},

'(1/1)',
{type:'expr_stat',expr:{type:'group',expr:{type:'expr',stack:[{type:'int',num:1},{type:'int',num:1},{type:'operator',operator:lex.names.div}]}}},

'nil',
{type:'expr_stat',expr:{type:'nil'}},

'x[1]',
{type:'expr_stat',expr:{type:'lvalue',head:{type:'id',id:'x'},tail:[{type:'index',left:{type:'int',num:1}}]}},

'x[1][2]',
{type:'expr_stat',expr:{type:'lvalue',head:{type:'id',id:'x'},tail:[{type:'index',left:{type:'int',num:1}},{type:'index',left:{type:'int',num:2}}]}},

'x[1][2][3]',
{type:'expr_stat',expr:{type:'lvalue',head:{type:'id',id:'x'},tail:[{type:'index',left:{type:'int',num:1}},{type:'index',left:{type:'int',num:2}},{type:'index',left:{type:'int',num:3}}]}},

'x[1+1][2+2][3+3]',
{type:'expr_stat',expr:{type:'lvalue',head:{type:'id',id:'x'},tail:[{type:'index',left:{type:'expr',stack:[{type:'int',num:1},{type:'int',num:1},{type:'operator',operator:lex.names.plus}]}},{type:'index',left:{type:'expr',stack:[{type:'int',num:2},{type:'int',num:2},{type:'operator',operator:lex.names.plus}]}},{type:'index',left:{type:'expr',stack:[{type:'int',num:3},{type:'int',num:3},{type:'operator',operator:lex.names.plus}]}}]}},

'{a:1}',
{type:'expr_stat',expr:{type:'object_literal',items:{a:{type:'int',num:1,name:{}}}}},

'{a:1,b:2}',
{type:'expr_stat',expr:{type:'object_literal',items:{a:{type:'int',num:1,name:{}},b:{type:'int',num:2,name:{}}}}},

'{a:1,b:2,c:3}',
{type:'expr_stat',expr:{type:'object_literal',items:{a:{type:'int',num:1,name:{}},b:{type:'int',num:2,name:{}},c:{type:'int',num:3,name:{}}}}},

'{a:1,b:2,c:3,d:4}',
{type:'expr_stat',expr:{type:'object_literal',items:{a:{type:'int',num:1,name:{}},b:{type:'int',num:2,name:{}},c:{type:'int',num:3,name:{}},d:{type:'int',num:4,name:{}}}}},

'{a:1+2}',
{type:'expr_stat',expr:{type:'object_literal',items:{a:{type:'expr',stack:[{type:'int',num:1},{type:'int',num:2},{type:'operator',operator:lex.names.plus}],name:{}}}}},

'fun { a := 1 }',
{type:'func',body:[{type:'define',id:'a',expr:{type:'int',num:1}}]},

'fun',
'parse fun: function header or body expected',

'fun a',
'parse fun: function body expected',

'fun { a := 1; b := 2 }',
{type:'func',body:[{type:'define',id:'a',expr:{type:'int',num:1}},{type:'define',id:'b',expr:{type:'int',num:2}}]},

'fun x {}',
{type:'func',head:{name:'x',params:[]}},

'fun x a {}',
{type:'func',head:{name:'x',params:[{type:'param',id:'a'}]}},

'fun x a b {}',
{type:'func',head:{name:'x',params:[{type:'param',id:'a'},{type:'param',id:'b'}]}},

'fun x a b c {}',
{type:'func',head:{name:'x',params:[{type:'param',id:'a'},{type:'param',id:'b'},{type:'param',id:'c'}]}},

'fun x a b c int {}',
{type:'func',head:{name:'x',params:[{type:'param',id:'a'},{type:'param',id:'b'},{type:'param',id:'c'}],type:'int'}},

'fun x a b c u8 {}',
{type:'func',head:{name:'x',params:[{type:'param',id:'a'},{type:'param',id:'b'},{type:'param',id:'c'}],type:'u8'}},

'fun x a b c defg {}',
{type:'func',head:{name:'x',params:[{type:'param',id:'a'},{type:'param',id:'b'},{type:'param',id:'c'},{type:'param',id:'defg'}]}},

'fun q{ ret }',
{type:'func',head:{name:'q',params:[]},body:[{type:'return'}]},

'fun q{ ret 1+2 }',
{type:'func',head:{name:'q',params:[]},body:[{type:'return',expr:{type:'expr',stack:[{type:'int',num:1},{type:'int',num:2},{type:'operator',operator:lex.names.plus}]}}]},

'fun sum a b { ret a + b }',
{type:'func',head:{name:'sum',params:[{type:'param',id:'a'},{type:'param',id:'b'}]},body:[{type:'return',expr:{type:'expr',stack:[{type:'id',id:'a'},{type:'id',id:'b'},{type:'operator',operator:lex.names.plus}]}}]},

'a=[1,2,3]',
{type:'assign',operation:lex.names.eq,operation_i:1,lvalue:{type:'id',id:'a'},expr:{type:'array_literal',items:[{type:'int',num:1},{type:'int',num:2},{type:'int',num:3}]}},

'a[1] = b[2]',
{type:'assign',operation:lex.names.eq,operation_i:4,lvalue:{type:'lvalue',head:{type:'id',id:'a'},tail:[{type:'index',left:{type:'int',num:1}}]},expr:{type:'lvalue',head:{type:'id',id:'b'},tail:[{type:'index',left:{type:'int',num:2}}]}},

'f()',
{type:'expr_stat',expr:{type:'value',head:{type:'id',id:'f'},tail:[{type:'call',args:[]}]}},

'f(1)',
{type:'expr_stat',expr:{type:'value',head:{type:'id',id:'f'},tail:[{type:'call',args:[{type:'int',num:1}]}]}},

'f(1,2)',
{type:'expr_stat',expr:{type:'value',head:{type:'id',id:'f'},tail:[{type:'call',args:[{type:'int',num:1},{type:'int',num:2}]}]}},

'f(1,2,3)',
{type:'expr_stat',expr:{type:'value',head:{type:'id',id:'f'},tail:[{type:'call',args:[{type:'int',num:1},{type:'int',num:2},{type:'int',num:3}]}]}},

'f(1,2,3,4)',
{type:'expr_stat',expr:{type:'value',head:{type:'id',id:'f'},tail:[{type:'call',args:[{type:'int',num:1},{type:'int',num:2},{type:'int',num:3},{type:'int',num:4}]}]}},

'f(1,2+3)',
{type:'expr_stat',expr:{type:'value',head:{type:'id',id:'f'},tail:[{type:'call',args:[{type:'int',num:1},{type:'expr',stack:[{type:'int',num:2},{type:'int',num:3},{type:'operator',operator:lex.names.plus}]}]}]}},

'x=a[1+1]',
{type:'assign',operation:lex.names.eq,operation_i:1,lvalue:{type:'id',id:'x'},expr:{type:'lvalue',head:{type:'id',id:'a'},tail:[{type:'index',left:{type:'expr',stack:[{type:'int',num:1},{type:'int',num:1},{type:'operator',operator:lex.names.plus}]}}]}},

'2 + -1',
{type:'expr_stat',expr:{type:'expr',stack:[{type:'int',num:2},{type:'prefix',operator:lex.names.minus,primary:{type:'int',num:1}},{type:'operator',operator:lex.names.plus}]}},

'1 or 2',
{type:'expr_stat',expr:{type:'expr',stack:[{type:'int',num:1},{type:'int',num:2},{type:'operator',operator:lex.names.or}]}},

'1 and 2 and 3 and 4',
{type:'expr_stat',expr:{type:'expr',stack:[{type:'int',num:1},{type:'int',num:2},{type:'operator',operator:lex.names.and},{type:'int',num:3},{type:'operator',operator:lex.names.and},{type:'int',num:4},{type:'operator',operator:lex.names.and}]}},

'1 || 1',
{type:'expr_stat',expr:{type:'expr',stack:[{type:'int',num:1},{type:'int',num:1},{type:'operator',operator:lex.names.pipe_pipe}]}},

'1 && 2 && 3 && 4',
{type:'expr_stat',expr:{type:'expr',stack:[{type:'int',num:1},{type:'int',num:2},{type:'operator',operator:lex.names.and},{type:'int',num:3},{type:'operator',operator:lex.names.and},{type:'int',num:4},{type:'operator',operator:lex.names.and}]}},

'f([1,2,3])',
{type:'expr_stat',expr:{type:'value',head:{type:'id',id:'f'},tail:[{type:'call',args:[{type:'array_literal',items:[{type:'int',num:1},{type:'int',num:2},{type:'int',num:3}]}]}]}},

'y=a[sum(0,2 minus 1)]',
{type:'assign',operation:lex.names.eq,operation_i:1,lvalue:{type:'id',id:'y'},expr:{type:'lvalue',head:{type:'id',id:'a'},tail:[{type:'index',left:{type:'value',head:{type:'id',id:'sum'},tail:[{type:'call',args:[{type:'int',num:0},{type:'expr',stack:[{type:'int',num:2},{type:'int',num:1},{type:'operator',operator:lex.names.minus}]}]}]}}]}},

'1 * 2 ** 3',
{type:'expr_stat',expr:{type:'expr', stack:[{type:'int',num:1},{type:'int',num:2},{type:'int',num:3},{type:'operator',operator:lex.names.pow},{type:'operator',operator:lex.names.mul}]}},

'1 + 2  % 3',
{type:'expr_stat',expr:{type:'expr', stack:[{type:'int',num:1},{type:'int',num:2},{type:'int',num:3},{type:'operator',operator:lex.names.percent},{type:'operator',operator:lex.names.plus}]}},

'1 SHR 2 - 3 SHL 4',
{type:'expr_stat',expr:{type:'expr', stack:[{type:'int',num:1},{type:'int',num:2},{type:'int',num:3},{type:'operator',operator:lex.names.minus},{type:'operator',operator:lex.names.SHR},{type:'int',num:4},{type:'operator',operator:lex.names.SHL}]}},

'1 >= 2 SHR 3',
{type:'expr_stat',expr:{type:'expr', stack:[{type:'int',num:1},{type:'int',num:2},{type:'int',num:3},{type:'operator',operator:lex.names.SHR},{type:'operator',operator:lex.names.right_eq}]}},

'1 == 2 <= 3 != 4 < 5',
{type:'expr_stat',expr:{type:'expr',stack:[{type:'int',num:1},{type:'int',num:2},{type:'int',num:3},{type:'operator',operator:lex.names.left_eq},{type:'operator',operator:lex.names.eq_eq},{type:'int',num:4},{type:'int',num:5},{type:'operator',operator:lex.names.left},{type:'operator',operator:lex.names.not_eq}]}},

'1 && 2 > 3 and 4',
{type:'expr_stat',expr:{type:'expr', stack:[{type:'int',num:1},{type:'int',num:2},{type:'int',num:3},{type:'operator',operator:lex.names.right},{type:'operator',operator:lex.names.and},{type:'int',num:4},{type:'operator',operator:lex.names.and}]}},

'1 > 2 or 3 < 4',
{type:'expr_stat',expr:{type:'expr', stack:[{type:'int',num:1},{type:'int',num:2},{type:'operator',operator:lex.names.right},{type:'int',num:3},{type:'int',num:4},{type:'operator',operator:lex.names.left},{type:'operator',operator:lex.names.or}]}},

'1 & 2 + 3 | 4',
{type:'expr_stat',expr:{type:'expr', stack:[{type:'int',num:1},{type:'int',num:2},{type:'int',num:3},{type:'operator',operator:lex.names.plus},{type:'operator',operator:lex.names.amp},{type:'int',num:4},{type:'operator',operator:lex.names.pipe}]}},

'1 AND 2 + 3 OR 4',
{type:'expr_stat',expr:{type:'expr', stack:[{type:'int',num:1},{type:'int',num:2},{type:'int',num:3},{type:'operator',operator:lex.names.plus},{type:'operator',operator:lex.names.AND},{type:'int',num:4},{type:'operator',operator:lex.names.OR}]}},

'1 & 2 | 3 ^ 4',
{type:'expr_stat',expr:{type:'expr', stack:[{type:'int',num:1},{type:'int',num:2},{type:'operator',operator:lex.names.amp},{type:'int',num:3},{type:'int',num:4},{type:'operator',operator:lex.names.caret},{type:'operator',operator:lex.names.pipe}]}},

//'log',
//{type:'log',list:[]},

'log 1\n',
{type:'log',list:[{type:'int',num:1}]},

'log 2',
{type:'log',list:[{type:'int',num:2}]},

'log 1 2\n',
{type:'log',list:[{type:'int',num:1},{type:'int',num:2}]},

'log ~~',
{type:'log',list:[{type:'color',num: -1}]},

'log ~123',
{type:'log',list:[{type:'color',num: 123}]},

'log ~r',
{type:'log',list:[{type:'prefix',operator:lex.names.tilde,primary:{type:'id',id:'r'}}]},

'log ~purple "Message: " "hey" ~white "White!" ~red "Red!" ~blue "Blue!" ~~ "no color"',
{type:'log',list:[{type:'color',num:93},{type:'str',str:'Message: '},{type:'str',str:'hey'},{type:'color',num:15},{type:'str',str:'White!'},{type:'color',num:196},{type:'str',str:'Red!'},{type:'color',num:20},{type:'str',str:'Blue!'},{type:'color',num:-1},{type:'str',str:'no color'}]},

'log 1\nlog 2\n',
[{type:'log',list:[{type:'int',num:1}]},{type:'log',list:[{type:'int',num:2}]}],

'log 1\n(2 + 3)',
[{type:'log',list:[{type:'int',num:1}]},{type:'expr_stat',expr:{type:'group',expr:{type:'expr',stack:[{type:'int',num:2},{type:'int',num:3},{type:'operator',operator:lex.names.plus}]}}}],

'are a',
{type:'are',list:[{type:'id',id:'a'}]},

'c:=1 are c',
[{type:'define',id:'c',expr:{type:'int',num:1}},{type:'are',list:[{type:'id',id:'c'}]}],

'are 1 2+3',
{type:'are',list:[{type:'int',num:1},{type:'expr',stack:[{type:'int',num:2},{type:'int',num:3},{type:'operator',operator:lex.names.plus}]}]},

`a := [
	1,2,3
]`,
{type:'define',id:'a',expr:{type:'array_literal',items:[{type:'int',num:1},{type:'int',num:2},{type:'int',num:3}]}},

'loop {}',
{type:'loop'},

'loop { log 1; }',
{type:'loop',block:[{type:'log',list:[{type:'int',num:1}]}]},

'loop { log 1 }',
{type:'loop',block:[{type:'log',list:[{type:'int',num:1}]}]},

'loop { a := 555; log a }',
{type:'loop',block:[{type:'define',id:'a',expr:{type:'int',num:555}},{type:'log',list:[{type:'id',id:'a'}]}]},

'loop 2 {}',
{type:'loop',expr:{type:'int',num:2}},

'a := true',
{type:'define',id:'a',expr:{type:'true'}},

'while true {}',
{type:'while',expr:{type:'true'}},

'while a < 1 { log a }',
{type:'while',expr:{type:'expr',stack:[{type:'id',id:'a'},{type:'int',num:1},{type:'operator',operator:lex.names.left}]},block:[{type:'log',list:[{type:'id',id:'a'}]}]},

'while a < 1 log a',
{type:'while',expr:{type:'expr',stack:[{type:'id',id:'a'},{type:'int',num:1},{type:'operator',operator:lex.names.left}]},block:[{type:'log',list:[{type:'id',id:'a'}]}]},



'loop 5 {}',
{type:'loop',expr:{type:'int',num:5}},

'loop 5 { log; }',
{type:'loop',expr:{type:'int',num:5},block:[{type:'log',list:[]}]},


'loop 5 { log lap }',
{type:'loop',expr:{type:'int',num:5},block:[{type:'log',list:[{type:'lap'}]}]},

'loop 3 { if 1 {} }',
{type:'loop',expr:{type:'int',num:3},block:[{type:'if',expr:{type:'int',num:1}}]},

'loop 3 { loop 5 {} }',
{type:'loop',expr:{type:'int',num:3},block:[{type:'loop',expr:{type:'int',num:5}}]},

'loop 5+5 { a := 1 }',
{type:'loop',expr:{type:'expr',stack:[{type:'int',num:5},{type:'int',num:5},{type:'operator',operator:lex.names.plus}]},block:[{type:'define',id:'a',expr:{type:'int',num:1}}]},

'loop 5+5 { a := 1; b := 2 }',
{type:'loop',expr:{type:'expr',stack:[{type:'int',num:5},{type:'int',num:5},{type:'operator',operator:lex.names.plus}]},block:[{type:'define',id:'a',expr:{type:'int',num:1}},{type:'define',id:'b',expr:{type:'int',num:2}}]},



'if 1+2 { 3 } but 4 { 5 } else { 6 }',
{type:'if',expr:{type:'expr',stack:[{type:'int',num:1},{type:'int',num:2},{type:'operator',operator:lex.names.plus}]},block:[{type:'expr_stat',expr:{type:'int',num:3}}],cont:{type:'but',expr:{type:'int',num:4},block:[{type:'expr_stat',expr:{type:'int',num:5}}],cont:{type:'else',block:[{type:'expr_stat',expr:{type:'int',num:6}}]}}},

'if false { log \'false\' }',
{type:'if',expr:{type:'false'},block:[{type:'log',list:[{type:'str',str:'false'}]}]},

'if 0 {}',
{type:'if',expr:{type:'int',num:0}},

'if 0 { log \'false\' }',
{type:'if',expr:{type:'int',num:0},block:[{type:'log',list:[{type:'str',str:'false'}]}]},

'if 0 {} else {}',
{type:'if',expr:{type:'int',num:0},cont:{type:'else'}},

'if 0 {2} else {3}',
{type:'if',expr:{type:'int',num:0},block:[{type:'expr_stat',expr:{type:'int',num:2}}],cont:{type:'else',block:[{type:'expr_stat',expr:{type:'int',num:3}}]}},

'if 0 {} else { if 1 {} }',
{type:'if',expr:{type:'int',num:0},cont:{type:'else',block:[{type:'if',expr:{type:'int',num:1}}]}},

'if 0 {} but 1 {}',
{type:'if',expr:{type:'int',num:0},cont:{type:'but',expr:{type:'int',num:1}}},

'if 0 {} but 1 {} else {}',
{type:'if',expr:{type:'int',num:0},cont:{type:'but',expr:{type:'int',num:1},cont:{type:'else'}}},

'if 0 {1 2} but 3 {4 5} else {6 7}',
{type:'if',expr:{type:'int',num:0},block:[{type:'expr_stat',expr:{type:'int',num:1}},{type:'expr_stat',expr:{type:'int',num:2}}],cont:{type:'but',expr:{type:'int',num:3},block:[{type:'expr_stat',expr:{type:'int',num:4}},{type:'expr_stat',expr:{type:'int',num:5}}],cont:{type:'else',block:[{type:'expr_stat',expr:{type:'int',num:6}},{type:'expr_stat',expr:{type:'int',num:7}}]}}},

'if 0 {} but 1 {} but 2 {} but 3 { 4 }',
{type:'if',expr:{type:'int',num:0},cont:{type:'but',expr:{type:'int',num:1},cont:{type:'but',expr:{type:'int',num:2},cont:{type:'but',expr:{type:'int',num:3},block:[{type:'expr_stat',expr:{type:'int',num:4}}]}}}},



'f(lam {})',
{type:'expr_stat',expr:{type:'value',head:{type:'id',id:'f'},tail:[{type:'call',args:[{type:'lam',head:{}}]}]}},

'f(lam {log;})',
{type:'expr_stat',expr:{type:'value',head:{type:'id',id:'f'},tail:[{type:'call',args:[{type:'lam',head:{},body:[{type:'log',list:[]}]}]}]}},

'f(lam cb {})',
{type:'expr_stat',expr:{type:'value',head:{type:'id',id:'f'},tail:[{type:'call',args:[{type:'lam',head:{params:[{type:'param',id:'cb'}]}}]}]}},

'f(lam cb = cb())',
{type:'expr_stat', expr:{type:'value',head:{type:'id',id:'f'},tail:[{type:'call',args:[{type:'lam',params:[{type:'param',id:'cb'}],expr:{type:'value',head:{type:'id',id:'cb'},tail:[{type:'call',args:[]}]}}]}]}},


'a := lam { 1 }',
{type:'define',id:'a',expr:{type:'lam',head:{},body:[{type:'expr_stat',expr:{type:'int',num:1}}]}},

'a := lam = 1',
{type:'define',id:'a',expr:{type:'lam',expr:{type:'int',num:1}}},

'f := lam { 1;2 }',
{type:'define',id:'f',expr:{type:'lam',head:{},body:[{type:'expr_stat',expr:{type:'int',num:1}},{type:'expr_stat',expr:{type:'int',num:2}}]}},

'f := lam a b { a; b }',
{type:'define',id:'f',expr:{type:'lam',head:{params:[{type:'param',id:'a'},{type:'param',id:'b'}]},body:[{type:'expr_stat',expr:{type:'id',id:'a'}},{type:'expr_stat',expr:{type:'id',id:'b'}}]}},

'lam x y { ret x.main + 5 }',
{type:'expr_stat',expr:{type:'lam',head:{params:[{type:'param',id:'x'},{type:'param',id:'y'}]},body:[{type:'return',expr:{type:'expr',stack:[{type:'lvalue',head:{type:'id',id:'x'},tail:[{type:'member',id:'main'}]},{type:'int',num:5},{type:'operator',operator:lex.names.plus}]}}]}},

'lam x y = x.main + 5',
{type:'expr_stat',expr:{type:'lam',params:[{type:'param',id:'x'},{type:'param',id:'y'}],expr:{type:'expr',stack:[{type:'lvalue',head:{type:'id',id:'x'},tail:[{type:'member',id:'main'}]},{type:'int',num:5},{type:'operator',operator:lex.names.plus}]}}},

'lam x = 5',
{type:'expr_stat',expr:{type:'lam',params:[{type:'param',id:'x'}],expr:{type:'int',num:5}}},

'lam a,b {}',
{type:'expr_stat',expr:{type:'lam',head:{params:[{type:'param',id:'a'},{type:'param',id:'b'}]}}},

'lam 1 { ret a }',
'parser error: statement expected, keyword found',

'each list {}',
{type:'each',iterable:{type:'id',id:'list'}},

'each list { log item index }',
{type:'each',iterable:{type:'id',id:'list'},block:[{type:'log',list:[{type:'each_item'},{type:'each_index'}]}]},

'each list { log item 2 index 3 }',
{type:'each',iterable:{type:'id',id:'list'},block:[{type:'log',list:[{type:'each_item',level:2},{type:'each_index',level:3}]}]},

'name t u mini ref re int',
{type:'name',names:[{id:'t'},{id:'u'},{id:'mini'},{id:'ref'},{id:'re'}],nametype:'int'},

'a := NOT 3',
{type:'define',id:'a',expr:{type:'prefix',operator:lex.names.NOT,primary:{type:'int',num:3}}},

'var a; a = 555',
[{type:'var',vars:[{id:'a'}]},{type:'assign',operation:lex.names.eq,operation_i:4,lvalue:{type:'id',id:'a'},expr:{type:'int',num:555}}],

'var a\n a = 555',
[{type:'var',vars:[{id:'a'}]},{type:'assign',operation:lex.names.eq,operation_i:3,lvalue:{type:'id',id:'a'},expr:{type:'int',num:555}}],

'ret b := 2',
'id must precede :=',

'ret \nb := 2',
[{type:'return'},{type:'define',id:'b',expr:{type:'int',num:2}}],

'ret b \n:= 2',
'id must precede :=',

'halt',
{type:'halt'},

'a == b',
{type:'expr_stat',expr: {type:'expr',stack:[{type:'id',id:'a'},{type:'id',id:'b'},{type:'operator',operator:lex.names.eq_eq}]}},

'if true ret',
{type:'if',expr:{type:'true'},block:[{type:'return'}]},

'len a',
{type:'expr_stat', expr: {type:'len',object:{type:'id',id:'a'}}},

'x := len [1 2 3] + 5',
{type:'define',id:'x',expr:{type:'expr',stack:[{type:'len',object:{type:'array_literal',items:[{type:'int',num:1},{type:'int',num:2},{type:'int',num:3}]}},{type:'int',num:5},{type:'operator',operator:lex.names.plus}]}},

'a[',
'parse index: "..", ":" or "]" expected',

'a[]',
'range expression expected',

'a[1]',
{type:'expr_stat',expr:{type:'lvalue',head:{type:'id',id:'a'},tail:[{type:'index',left:{type:'int',num:1}}]}},

'a[..]',
'parse index: cannot omit both .. expressions',

'a[1..]',
{type:'expr_stat',expr:{type:'lvalue',head:{type:'id',id:'a'},tail:[{type:'index',delim:'..',left:{type:'int',num:1}}]}},

'a[..1]',
{type:'expr_stat',expr:{type:'lvalue',head:{type:'id',id:'a'},tail:[{type:'index',delim:'..',right:{type:'int',num:1}}]}},

'a[1..2]',
{type:'expr_stat',expr:{type:'lvalue',head:{type:'id',id:'a'},tail:[{type:'index',delim:'..',left:{type:'int',num:1},right:{type:'int',num:2}}]}},

'a[:]',
'parse index: the : must be preceeded by an expression',

'a[1:]',
'parse index: the : must be followed by an expression',

'a[:1]',
'parse index: the : must be preceeded by an expression',

'a[1:2]',
{type:'expr_stat',expr:{type:'lvalue',head:{type:'id',id:'a'},tail:[{type:'index',delim:':',left:{type:'int',num:1},right:{type:'int',num:2}}]}},

'inc a()',
'cannot incement a rvalue',

'inc log',
'lvalue expected',

'inc a',
{type:'inc', value:{type:'id', id:'a'}},

'dec a[1]',
{type:'dec',value:{type:'lvalue',head:{type:'id',id:'a'},tail:[{type:'index',left:{type:'int',num:1}}]}},

'a:=[1,2,3]; log pop 1 a',
'cannot parse pop argument as a value',

'a := 10_000_000',
{type:'define',id:'a',expr: {type:'int',num:10000000}},

'init p { p = 1 }',
{type:'init',value:{type:'id',id:'p'},block:[{type:'assign',operation:lex.names.eq,operation_i:4,lvalue:{type:'id',id:'p'},expr:{type:'int',num:1}}]},

'use p p = 1',
{type:'use',value:{type:'id',id:'p'},block:[{type:'assign',operation:lex.names.eq,operation_i:3,lvalue:{type:'id',id:'p'},expr:{type:'int',num:1}}]},

'kill',
{type:'kill'},

'try { throw 1 } catch { error }',
{type:'try',try_block:[{type:'throw',primary:{type:'int',num:1}}],catch_block:[{type:'expr_stat',expr:{type:'error'}}]},

'{a: {},}',
{type:'expr_stat',expr:{type:'object_literal',items:{a:{type:'object_literal',items:{},name:{}}}}},

'log { "abc" }.abc',
'shorthand property assignment must be an <id>, but got <str>',

'"\\\\"',
{type:'expr_stat',expr:{type:'str',str:'\\'}},

'"\\n"',
{type:'expr_stat',expr:{type:'str',str:'\n'}},

]

var R = []
for (var i = 0 ; i < s.length; i += 2) {
	R.push(s[i])
	R.push({})
}

log=console.log
log(JSON.stringify(R,0,'   '))
