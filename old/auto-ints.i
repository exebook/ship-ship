// forgot what it does, seems to be checking if a variable can be represented as int
incl map
defs := {
	a: [1 'b' 'c']
	b: [1 'c']
	c: [1 'a' 'b']
	z: [1 'a' 'x']
	x: ['?']
}

fun question x { ret ('?' find defs[x]) >= 0 }
fun intersect a b { // return a[] which are also in [b]
	re := []
	each a if (item find b) >= 0 re += item
	ret re
}
fun dissect a b { // return a[] which is not in b[]
	re := []
	each a if (item find b) < 0 re += item
	ret re
}

all := keys(defs)
bads := ['?']
good := all
N := 0
loop {
	dirty := filter(good lam x = len intersect(defs[x] bads) > 0)
	if len dirty == 0 break
	good = dissect(good dirty)
	bads += dirty
}
are 'END' good bads

