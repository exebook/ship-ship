val ex_add_var(val a, val b) {
	expr_push(a);
	expr_push(b);
	op_add();
	return expr_pop();
}

#define ex_add_i64(a, b) ((int64_t)a + (int64_t)b)
#define ex_add_f64(a, b) ((double)a + (double)b)

val ex_cout() {
	enter_dynamic(0);
	int count = arg_count();
	for (int n = 0; n < count; n++) {
		if (n > 0) printf(" ");
		show(arg_dyn(n));
	}
	leave_dynamic();
	return cell_let(555);
}

val ex_log() {
	ex_cout();
	printf("\n");
	return cell_let(555);
}

val ex_time_begin() {
	enter_dynamic(0);
	begin();
	leave_dynamic();
	return cell_let(555);
}

val ex_time_end() {
   arg_alloc(0);
   op_time_end();
   expr_pop();
	return cell_let(555);
}

val ex_greater(val a, val b) {
	if (cell_is_number(a)) {
		if (cell_is_number(b)) {
		printf("AA %i %i\n", (int)cell_get(a), (int)cell_get(b));
			val re = cell_get(a) > cell_get(b);
			re = cell_let(re);
			msg_val("re", re);
			return re;
		}
		else
			return cell_false();
	}
	expr_push(a);
	expr_push(b);
	op_cmp(2);
	return expr_pop();
}