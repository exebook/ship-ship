#define KEY_LENGTH_TYPE uint8_t

typedef int (*__dic_enum_func)(void *key, int count, val *value, void *user);

typedef struct struct_keynode {
	struct struct_keynode *next;
	char *key;
	KEY_LENGTH_TYPE len; // * sizeof(char)
	val value;
} keynode;

typedef struct {
	keynode **table;
	int length, count; // think of length as of table capacity, count is for actual keys
	double growth_treshold;
	double growth_factor;
	val *value;
} dictionary;

#define hash_func meiyan

static inline uint32_t meiyan(const char *key, int count) {
	typedef uint32_t* P;
	uint32_t h = 0x811c9dc5;
	while (count >= 8) {
		h = (h ^ ((((*(P)key) << 5) | ((*(P)key) >> 27)) ^ *(P)(key + 4))) * 0xad3e7;
		count -= 8;
		key += 8;
	}
	#define tmp h = (h ^ *(uint16_t*)key) * 0xad3e7; key += 2;
	if (count & 4) { tmp tmp }
	if (count & 2) { tmp }
	if (count & 1) { h = (h ^ *key) * 0xad3e7; }
	#undef tmp
	return h ^ (h >> 16);
}

keynode *__keynode_new(char*k, int l) {
	keynode *node = malloc(sizeof(keynode));
	node->len = l;
	node->key = malloc(l);
	memcpy(node->key, k, l);
	node->next = 0;
	node->value = -1;
	return node;
}

void __keynode_free(keynode *node) {
	free(node->key);
	free(node);
}

void __keynode_delete_chain(keynode *node) {
	free(node->key);
	if (node->next) __keynode_delete_chain(node->next);
	free(node);
}

dictionary* __dic_new(int initial_size) {
	dictionary* dic = malloc(sizeof(dictionary));
	if (initial_size == 0) initial_size = 10;
	dic->length = initial_size;
	dic->count = 0;
	dic->table = calloc(sizeof(keynode*), initial_size);
	dic->growth_treshold = 2.0;
	dic->growth_factor = 10;
	return dic;
}

void __dic_delete(dictionary* dic) {
	for (int i = 0; i < dic->length; i++) {
		if (dic->table[i])
			__keynode_delete_chain(dic->table[i]);
	}
	free(dic->table);
	dic->table = 0;
	free(dic);
}

void __dic_reinsert_when_resizing(dictionary* dic, keynode *k2) {
	int n = hash_func(k2->key, k2->len) % dic->length;
	if (dic->table[n] == 0) {
		dic->table[n] = k2;
		return;
	}
	keynode *k = dic->table[n];
	k2->next = k;
	dic->table[n] = k2;
}

void __dic_resize(dictionary* dic, int newsize) {
	int o = dic->length;
	keynode **old = dic->table;
	dic->table = calloc(sizeof(keynode*), newsize);
	dic->length = newsize;
	for (int i = 0; i < o; i++) {
		keynode *k = old[i];
		while (k) {
			keynode *next = k->next;
			k->next = 0;
			__dic_reinsert_when_resizing(dic, k);
			k = next;
		}
	}
	free(old);
}

int __dic_add(dictionary* dic, void *key, int keyn) {
	int n = hash_func((const char*)key, keyn) % dic->length;
	if (dic->table[n] == 0) {
		dic->table[n] = __keynode_new((char*)key, keyn);
		dic->value = &dic->table[n]->value;
		dic->count++;
		return 0;
	}
	else {
		double f = (double)dic->count / (double)dic->length;
		if (f > dic->growth_treshold) {
			__dic_resize(dic, dic->length * dic->growth_factor);
			return __dic_add(dic, key, keyn);
		}
	}
	keynode *k = dic->table[n];
	while (k) {
		if (k->len == keyn && memcmp(k->key, key, keyn) == 0) {
			dic->value = &k->value;
			return 1;
		}
		k = k->next;
	}
	dic->count++;
	keynode *k2 = __keynode_new((char*)key, keyn);
	k2->next = dic->table[n];
	dic->table[n] = k2;
	dic->value = &k2->value;
	return 0;
}

int __dic_find(dictionary* dic, void *key, int keyn) {
	int n = hash_func((const char*)key, keyn) % dic->length;
	__builtin_prefetch(dic->table[n]);
	keynode *k = dic->table[n];
	if (!k) return 0;
	while (k) {
		if (k->len == keyn && !memcmp(k->key, key, keyn)) {
			dic->value = &k->value;
			return 1;
		}
		k = k->next;
	}
	return 0;
}

void __dic_each(dictionary* dic, __dic_enum_func f, void *user) {
	for (int i = 0; i < dic->length; i++) {
		if (dic->table[i] != 0) {
			keynode *k = dic->table[i];
			while (k) {
				if (!f(k->key, k->len, &k->value, user)) return;
				k = k->next;
			}
		}
	}
}

/*
	example:

	struct keynode *k = __dic_remove(dic, "abc", 3);
	if (k) {
		free_my_value(k->value);
		__keynode_free(k);
	}
*/

keynode* __dic_remove(dictionary* dic, void *key, int keyn) {
	int n = hash_func((const char*)key, keyn) % dic->length;
	keynode *k = dic->table[n], *prev = 0;

	if (!k) return 0;

	while (k) {
		if (k->len == keyn && !memcmp(k->key, key, keyn)) {
			dic->value = 0;
			if (prev) {
				prev->next = k->next;
			}
			else {
				dic->table[n] = k->next;
			}
			dic->count--;
			return k;
		}
		prev = k;
		k = k->next;
	}
	return 0;
}

/*
	copying
	note that values are just copied with =, if you allocate them it is
	up to you to handle duplicate references
*/
keynode *__keynode_copy(keynode* src) {
	keynode *dest = malloc(sizeof(keynode));
	dest->len = src->len;
	dest->key = strndup(src->key, src->len);
	dest->value = src->value;
	return dest;
}

dictionary* __dic_copy(dictionary* src) {
	dictionary* dest = malloc(sizeof(dictionary));
	*dest = *src; // copy fields
	dest->table = calloc(sizeof(keynode*), dest->length);

	for (int i = 0; i < src->length; i++) {
		if (src->table[i]) {
			keynode *ks, *kd;
			ks = src->table[i];
			kd = __keynode_copy(ks);
			dest->table[i] = kd;
			while (ks->next) {
				ks = ks->next;
				kd->next = __keynode_copy(ks);
				kd = kd->next;
			}
		}
	}
	return dest;
}

