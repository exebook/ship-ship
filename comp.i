incl pargen
incl map tool
incl flatten cc
incl comp_er sy_expr sy_value sy_assign sy_fun sy_charmap

enum mode {
	CODE NODE TYPE
}

enum uplink {
	//TYPE_GET TYPE_NEW
	NAME_GET NAME_SET NAME_POS REC_TYPE
	REF DEF GET FUN ID ENVSIZE DECLS FIND FUNCODE NODE ENV GETU TMP
	C_ID C_ID_MAKE FLY AFTER MAKE_PATH NEED NEED_UID SCAN_ENV
	ENUM_SET ENUM_GET TRY_DEPTH PREV THE IT THAT
	C_GLOBAL VARSET IN_CATCH EACH_ID EACH_UNIQ SRC RET_TYPE
}

enum decl { FUN LOCAL ENV FLY ARG FLYARG ENUM }

fun find1 where what {
	// using this instead of ugly A find B
	ret what find where
}

fun line_info node f {
	init node throw 'line_info argument is nil'
	tok := pos_tok(node.pos)
	line := tok[5]
	file := tok[0]
	ret c_line(line file f)
}

fun _sy_each up list f {
	//each list item = f(up item)
	//ret lam m { each list item = item(m) ret list }
	list = map(list lam x = f(up x))
	ret lam m = map(list lam x = x(m))
}

//fun sy_block up node {
	//ret _sy_each(up node.block sy_statem)
//}

fun sy_incl up node {
	b := _sy_each(up node.block sy_statem)
	ret lam m {
		if m == mode.CODE {
			ret c_block(b(m))
		}
	}
}

fun sy_log up node {
	list := _sy_each(up node.args sy_expr)
	ret lam m {
		if m == mode.CODE {
			c := list(m)
			ret line_info(node c_log(c))
		}
	}
}

fun sy_cout up node {
	list := _sy_each(up node.args sy_expr)
	ret lam m {
		if m == mode.CODE {
			c := list(m)
			ret line_info(node c_cout(c))
		}
	}
}

fun sy_are up node {
	list := _sy_each(up node.args sy_expr)
	ret lam m {
		if m == mode.CODE {
			c := list(m)
			l := []
			each node.args {
				if item.type == types.string {
					s = item.str
					if index > 0 s = ' ' + s
					l += c_string(s)
				}
				else {
					s := item.source
					if index > 0 s = ' ' + s
					if index == 0 s = s + ' '
					l += [c_color_term(4, s)]
					l += c[index]
				}
			}
			ret line_info(node c_log(l))
		}
	}
}

fun sy_ifst up node {
	x := sy_expr(up node.expr)
	b := _sy_each(up node.block sy_statem)
	use node.else {
		e := _sy_each(up node.else.block sy_statem)
	} else e = lam = []
	ret lam m {
		if m == mode.CODE ret c_if(x(m) b(m) e(m))
	}
}

fun loop_handler up node mod_vars {
	ret lam u a {
		if u == uplink.VARSET {
			each mod_vars if item == a ret
			mod_vars += a
		}
		ret up(u a)
	}
}

fun hold_vars mod_vars {
	re := []
	each mod_vars {
		d := item
		if d.type == decl.LOCAL re += c_varget(d.stack_addr)
		but d.type == decl.ARG re += c_argget(d.arg_addr)
		but d.type == decl.FLYARG re + c_flyargget(d.arg_addr)
		but d.type == decl.ENV re += c_envget(d.env_addr)
		but d.type == decl.FLY re += c_flyget(d.stack_addr)
	}
	each re item = c_loop_hold(item)
	ret re
}

fun sy_loop up node {
	mod_vars := []
	b := _sy_each(loop_handler(up node mod_vars) node.block sy_statem)
	ret lam m {
		if m == mode.CODE ret c_loop(b(m) hold_vars(mod_vars))
	}
}

fun sy_while up node {
	mod_vars := []
	x := sy_expr(up node.expr)
	b := _sy_each(loop_handler(up node mod_vars) node.block sy_statem)
	ret lam m {
		if m == mode.CODE ret c_while(x(m) b(m) hold_vars(mod_vars))
	}
}

fun sy_break up node {
	ret lam m {
		if m == mode.CODE ret c_break()
	}
}

fun sy_cont up node {
	ret lam m {
		if m == mode.CODE ret c_cont()
	}
}

fun sy_each up node {
	mod_vars := []
	if node.iterable.type == types.each_range {
		iter := [
			sy_expr(up node.iterable.a)
			sy_expr(up node.iterable.b)
		]
	}
	else {
		iter = sy_expr(up node.iterable)
	}
	each_id := up(uplink.EACH_UNIQ)
	loop_h := loop_handler(up node mod_vars)
	b := _sy_each(handler node.block sy_statem)

	fun handler u a {
		if u == uplink.EACH_ID {
			level := a
			if level == 0 ret each_id
			else ret up(u level - 1)
		}
		else ret loop_h(u a)
	}

	ret lam m {
		if m == mode.CODE {
			if is_array(iter) each iter item = item(m)
			else iter = iter(m)
			ret c_each(iter b(m) each_id hold_vars(mod_vars))
		}
	}
}

fun sy_item up node {
	each_id := up(uplink.EACH_ID node.level)
	init each_id {
		if node.level == 0 throw stop(node.pos tem(E015_1 'item'))
		else throw stop(node.pos tem(E014_2 'item' node.level))
	}

	ret lam m {
		if m == mode.CODE {
			ret c_item(each_id)
		}
	}
}

fun sy_each_index up node {
	each_id := up(uplink.EACH_ID node.level)
	init each_id {
		if node.level == 0 throw stop(node.pos tem(E015_1 'index'))
		else throw stop(node.pos tem(E014_2 'index' node.level))
	}

	ret lam m {
		if m == mode.CODE {
			ret c_index(each_id)
		}
	}
}

fun sy_kill up node {
	if node.expr x := sy_expr(up node.expr)
	ret lam m {
		if m == mode.CODE {
			if x x = x(m) else x = c_nil()
			ret c_kill(x)
		}
	}
}

fun sy_try up node {
	b_try := _sy_each(lam u a {
			if u == uplink.TRY_DEPTH ret up(uplink.TRY_DEPTH) + 1
			else ret up(u a)
		}
		node.b_try sy_statem
	)

	b_catch := _sy_each(lam u a {
			if u == uplink.IN_CATCH ret true
			else ret up(u a)
		}
		node.b_catch sy_statem
	)

	ret lam m {
		if m == mode.CODE ret c_try_catch(b_try(m) b_catch(m))
	}
}

fun sy_throw up node {
	e := sy_expr(up node.expr)
	ret lam m {
		if m == mode.CODE ret line_info(node c_throw(e(m)))
	}
}

fun sy_enum up node {
	e := up(uplink.ENUM_SET node)

	if node.list {
		count := 0
		if len e.items > 0 { // multiple definitions of enum extend it
			each e.items {
				if item > count count = item
			}
			inc count
		}
		each node.list {
			if e.items[item.id] {
				throw stop(item.pos tem('already in <^>' node.id))
			}
			else {
				if item.num != -1 count = item.num
				e.items[item.id] = count
				inc count
			}
		}
	}
	but node.expr {
		up(uplink.AFTER lam stage {
			if stage == stag.SCOPE {
				used := {}
				elems := []
				stack := []
				if node.expr.type == types.expr stack = node.expr.stack
				if node.expr.type == types.id stack = [node.expr]

				each stack {
					if item.type == types.id {
						part := up(uplink.ENUM_GET item.id)
						if part {
							p := []
							each part.items {
								p += [[cle item]]
							}
							sort(p lam a b = a[1] > b[1])
							each p {
								el := item[0]
								if used[el] {
									throw stop(node.expr.pos tem(E007_3 used[el] part.name el))
								}
								elems += [[ el cle part.name ]]
								used[el] = part.name
							}
						}
						else throw stop(item.pos tem('enum <^> was not defined' item.id))
					}
					but item.type == types.operator and item.op == Token['+'] {
						//just ignore
					}
					else throw stop(item.pos tem('enum expression only expects <id> and <+>'))
				}

				// merge values
				count := 0
				used = {}
				each elems {
					value := item[1]
					if used[value] {
						value = count
					}
					else {
						if value > count count = value
					}
					used[value] = 1
					e.items[item[0]] = value
					inc count
				}
			}
		})
	}

	ret lam m {
		if m == mode.CODE ret c_nop()
	}
}

fun sy_langc up node {
	each node.exprs {
		item[2] = sy_expr(up item[1])
	}

	ret lam m {
		if m == mode.CODE {
			each node.exprs {
				node.src[item[0]] = item[2](m).expr()
			}
			if node.global {
				up(uplink.C_GLOBAL line_info(node c_langc(node.src)))
				ret c_nop()
			}
			else {
				ret line_info(node c_langc(node.src))
			}
		}
	}
}

fun sy_langc_expr up node {
	each node.exprs {
		item[2] = sy_expr(up item[1])
	}

	ret lam m {
		if m == mode.CODE {
			each node.exprs {
				node.src[item[0]] = item[2](m).expr()
			}
			ret line_info(node c_langc_expr(c_langc(node.src)))
		}
	}
}

fun sy_langc_shadow up node {
	ret lam m {
		if m == mode.CODE {
			ret c_nop()
		}
	}
}

fun sy_rec up node {
	ids := map(node.ids lam x = x.id)
	list := map(node.list lam x = x.id)

	ty := 'rec-' + list * '-'
	each ids {
		nam := up(uplink.NAME_GET item)
		init nam = up(uplink.NAME_SET, [node.pos item ty])
	}
	ret lam m {
		if m == mode.CODE ret c_nop()
	}
}

fun sy_statem up node {
	re := nil
	if node.type == types.log re = sy_log(up node)
	but node.type == types.cout re = sy_cout(up node)
	but node.type == types.are re = sy_are(up node)
	but node.type == types.fun re = sy_fun(up node)
	but node.type == types.expr_statem re = sy_expr_statem(up node)
	but node.type == types.decl re = sy_define(up node)
	but node.type == types.assign re = sy_assign(up node)
	but node.type == types.ret re = sy_ret(up node)
	but node.type == types.if_st re = sy_ifst(up node)
	but node.type == types.loop re = sy_loop(up node)
	but node.type == types.break re = sy_break(up node)
	but node.type == types.cont re = sy_cont(up node)
	but node.type == types.inc re = sy_inc(up node Keys['+'])
	but node.type == types.dec re = sy_inc(up node Keys['-'])
	but node.type == types.op_assign re = sy_op_assign(up node)
	but node.type == types.each re = sy_each(up node)
	but node.type == types.each_range re = sy_each(up node)
	but node.type == types.kill re = sy_kill(up node)
	but node.type == types.while re = sy_while(up node)
	but node.type == types.try re = sy_try(up node)
	but node.type == types.throw re = sy_throw(up node)
	but node.type == types.enum re = sy_enum(up node)
	but node.type == types.charmap re = sy_charmap(up node)
	but node.type == types.incl re = sy_incl(up node)
	but node.type == types.langc re = sy_langc(up node)
	but node.type == types.langc_shadow re = sy_langc_shadow(up node)
	but node.type == types.rec re = sy_rec(up node)
	else throw stop(node.pos 'sy_statem: cannot handle <' + types[node.type] + '>')
	ret re
}

fun compile fname src c_file_name options loader {
	shadow_list := []
	fun p_handler u a {
		if u == parse_link.C_SHADOW shadow_list += a
	}
	ast := ret_ast(parse_source(p_handler fname src loader))
	options.c_shadow_list = shadow_list
	code := sy_prog(handler ast options)(mode.CODE).statem()
	fun handler u a {
		if u == uplink.SRC ret src
		else {
			log 'top handler ignores:' uplink[u]+'('+shame(a)+')'
		}
	}
	flat := code_flatten(code c_file_name)
	ret flat
}
