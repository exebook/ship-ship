int __size_of(val a) {
	return __leaf(a)->size;
}

val __string_alloc(int size) {
	leaf *l = leaf_new(leaf_str);
	l->size = size;
	//l->s = Alloc(size * sizeof(uchar));
	int N = size * sizeof(uchar);
	l->s = size ? Alloc(N) : 0;
	return __mk_val(l); // already rooted
}

val __string_ascii(char *s) {
	int len = strlen(s);
	val re = __string_alloc(len);
	uchar *d = __leaf(re)->s;
	char *e = s + len;
	while (s < e) {
		*d++ = *s++;
	}
	return re; // already rooted
}

val __string_uchar(uchar* p, int size) {
	val re = __string_alloc(size);
	if (size > 0) {
		memcpy(__leaf(re)->s, p, size * sizeof(uchar));
	}
	//printf("new uchar[%lx]: ", re);
	//__print(re, 0);
	//printf("\n");
	return re; // already rooted
}

val __string_utf_size(char *s, int size) {
	if (size == 0) return __string_alloc(0);
	uchar* d = malloc(size * sizeof(uchar));
	int n = utf2w((unsigned char*)s, (unsigned char*)&s[size], d, &d[size]);
	val ret = __string_uchar(d, n);
	free(d);
	return ret; // already rooted
}

val __string_utf(char *s) {
	int n = strlen(s);
	return __string_utf_size(s, n); // already rooted
}

//void trav(leaf *a, int d) {
	//if (a->type != leaf_str) {
		//printf("trav fail: %p, %s\n", a, __type_of_leaf(a));
		//exit(0);
	//}
	//if (a->pair) {
		//trav(a->left, d + 1);
		//trav(a->right, d + 1);
	//}
	//else {
		//printf("|");
		//for (int i = 0; i < d; i++) printf(" ");
		//printf("'");
		//for (int i = 0; i < a->size; i++) printf("%c", a->s[i]);
		//printf("'");
		//printf("\n");
	//}
//}

void __string_compact(val a) {
	if (!__is_heap(a)) {
		printf("compacting not a string\n");
		exit(0);
	}
	leaf *l = __leaf(a);

	if (l->type != leaf_str) {
		printf("compacting not a string %s\n", __type_of_leaf(l));
		exit(0);
	}

	if (!l->pair) return;
	__size_t length = l->size;
	const int stack_size = length + 3;

	uchar
		*dest = Alloc(length * sizeof(uchar)),
		*write_pos = dest;
	leaf
		*C,
		**stack = malloc(stack_size * sizeof(void*)),
		**stack_end = &stack[stack_size],
		**sp = stack_end;

	*--sp = l;

	while (1) {
		if (sp > stack_end) {
			printf("endu!\n");
			exit(0);
		}
		C = *sp++;
		if (C->type != leaf_str) {
			printf("String pair contains something else\n");
			printf("%s %p\n", __type_of_leaf(C), C);
			exit(0);
		}
		if (C->pair) {
			*--sp = C->right;
			*--sp = C->left;
			if (sp < stack) {
				printf("crashu!\n");
				exit(0);
			}
		}
		else {
			if (C->size > 0) {
				memcpy(write_pos, C->s, C->size * sizeof(uchar));
			}
			write_pos += C->size;
			if (sp == stack_end) {
				free(stack);
				break;
			}
		}
	}
	l->s = dest;
	l->pair = 0;
}

val __array_join(val a, val delim) {
	__enter();
	if (!__is_array(a)) {
		__throw(__tem(E019_1, __type_of(a)));
	}
	if (!__is_string(delim)) { // && !__is_num(delim)) {
		__throw(__tem(E020_1, __type_of(delim)));
	}
	val ret;
	leaf *al = __leaf(a);
	__size_t a_size = al->size, s_size = 0;
	val *p = al->p;
	ret = __string_alloc(0);
	__hold(&ret);

	for (int i = 0; i < a_size; i++) {
		__loop_mark();
		val v = p[i];
		if (i > 0 && __len(delim) > 0) {
			ret = __string_concat(ret, delim);
		}
		if (__is_num(v)) {
			v = __stringize_num(v);
		}

		if (__is_string(v)) {
			ret = __string_concat(ret, v);
		}
		else if (__is_nil(v)) {
			// do nothing
		}
		else {
			__throw(__tem(E021_1, __type_of(v)));
		}
		__loop_release();
	}
	__return(ret);
}

val __str_empty() {
	return __string_alloc(0);
}

val __stringize_num(val a) { //create_str_from_num
	char s[25] = "NaN";
	if (__is_num(a)) {
		sprintf(s, "%.16G", __get(a));
	}
	val ret = __string_ascii(s);
	return ret; // already rooted
}

val __int_to_str(int a) {
	return __stringize_num(__let_i(a));
}

int __str_find_at(val where, val what, int at) {
	__string_compact(where);
	__string_compact(what);
	leaf *a = __leaf(where);
	leaf *b = __leaf(what);
	uchar *s = a->s;
	int size = a->size;
	s += at;
	size -= at;
	uchar *p = memmem(s, size * sizeof(uchar), b->s, b->size * sizeof(uchar));
	return p ? p - a->s : -1;
}

val __split(val a, val b) {
	if (!__is_string(a)) {
		__throw(__str("split(A B) <- A must be a string"));
	}
	__enter();
	val ret;
	val part;

	ret = __array_allocopy(0, 0);
	__string_compact(a);
	leaf *l = __leaf(a);
	int str_size = l->size;

	if (str_size == 0) {
		if (__leaf(b)->size > 0) __array_push(ret, __string_alloc(0));
		return ret;  // already rooted by __array_allocopy
	}
	int string_size_wide = str_size * sizeof(uchar);
	uchar *p = l->s, *e = p + str_size;
	uchar *part_start = p;

	leaf *L = __leaf(b);
	uchar *separator_ptr = L->s;
	int sep_size = L->size;
	int separator_size = sep_size * sizeof(uchar);

	while (1) {
		__loop_mark();
		if (separator_size == 0) {
			p++;
		}
		else {
			while (1) { // hack to support wide_memmem:
				uchar *p1 = p;
				p = memmem(p, string_size_wide, separator_ptr, separator_size);
				if (((uintptr_t)p&1) == 0) break; // if the found pointer is even then we're done, otherwise try next byte

				p = (uchar*)(((uintptr_t)p) + 1);
				string_size_wide -= ((p-p1) / 2);
			}
		}
		uchar *part_end = p ? p : e;
		int part_size = part_end - part_start;
		part = __string_uchar(part_start, part_size);
		__array_push(ret, part);
		if (p == 0) {
			break;
		}
		p += sep_size;
		str_size -= (sep_size + part_size);
		string_size_wide = str_size * sizeof(uchar);
		if (p >= e) {
			if (separator_size > 0) {
				part = __string_alloc(0);
				__array_push(ret, part);
			}
			break;
		}
		part_start = p;
		__loop_release();
	}
	__leave();
	return __rooted(ret);
}

val __string_concat(val a, val b) {
	if (!__is_string(a)) {
		__throw(__concat(__str("__string_concat first operand is "), __type_of(a)));
	}
	if (!__is_string(b)) {
		__throw(__concat(__str("__string_concat second operand is "), __type_of(b)));
	}
	leaf
		*la = __leaf(a),
		*lb = __leaf(b);

	if (lb->size == 0) {
		return a;
	}
	if (la->size == 0) {
		return b;
	}
	leaf *l = leaf_new(leaf_str);
	l->pair = 1;
	l->size = la->size + lb->size;
	l->left = la;
	l->right = lb;
	return __mk_val(l); // already rooted by leaf_new()
}

val __utf(val a) {
	if (__is_heap(a) && __leaf(a)->type == leaf_str) {
		__string_compact(a);
		leaf *al = __leaf(a);
		int n = al->size;
		leaf *re = leaf_new(leaf_pchar);
		if (n == 0) {
			re->size = 0;
			re->utf = Alloc(1);
			re->utf[0] = 0;
			return __mk_val(re);  // already rooted by leaf_new
		};
		char *utf = Alloc(n * 5); //use alloca if n < 100ish
		re->size = w2utf(utf, n * 5, al->s, al->size);
		re->utf = Realloc(utf, re->size + 1, n * 5);
		//re->p[re->size] = 0;
		return __mk_val(re);  // already rooted by leaf_new
	}
	else {
		__throw(__str("utf: argument is not a string"));
	}
	return __nil;
}

uchar *__uchar(val a) {
	__string_compact(a);
	return __leaf(a)->s;
}

char *__pchar(val a) { //result will be freed on the next GC cycle
	val b = __utf(a);
	if (__is_nil(b)){
		static char *x = "";
		return x;
	};
	return __leaf(b)->utf;
}

char *__pchar_int(val a) { // not disturbing the GC
	__string_compact(a);
	leaf *al = __leaf(a);
	int n = al->size;
	if (n == 0) return 0;
	char *utf = Alloc(n * 5);
	w2utf(utf, n * 5, al->s, n);
	return utf;
}

val __char_at(val s, int index) {
	if (!__is_string(s)) {
		__throw(__str("char_at: not a string"));
		return 0;
	}
	__string_compact(s);
	leaf *l = __leaf(s);
	if (index < 0) {
		index = l->size + index;
		if (index < 0) return __nil;
	}
	if (index >= l->size) return __nil;
	return __string_uchar((uchar[1]){ (uchar)l->s[index] }, 1);
}

uchar __charcode_at(val s, int index) {
	if (!__is_string(s)) {
		__throw(__str("charcode_at: not a string"));
		return 0;
	}
	__string_compact(s);
	leaf *l = __leaf(s);
	if (index < 0) {
		index = l->size + index;
		if (index < 0) {
			__throw(__str("charcode_at: out of bounds"));
			return 0;
		}
	}
	if (index >= l->size) {
		__throw(__str("charcode_at: out of bounds"));
		return 0;
	}
	return l->s[index];
}

val __parse_num(val s, int base) {
	if (!__is_string(s)) {
		__throw(__str("parse_num: argument is not a string"));
	}
	__string_compact(s);
	if (base <= 0) base = 10;
	char t[64];
	leaf *l = __leaf(s);
	int size = l->size >= 63 ? 63 : l->size;
	int i, dot = 0;
	for (i = 0; i < size; i++) {
		t[i] = l->s[i]; if (t[i] == '.') dot = 1;
	}
	t[i] = 0;
	return dot
		? __let(strtod(t, 0))
		: __let((int32_t)strtol(t, 0, base));
}

val __trim(val s) {
	if (!__is_string(s)) {
		__throw(__str("__trim: argument is not a string"));
	}
	__string_compact(s);
	leaf *l = __leaf(s);
	if (l->size == 0) return s;
	uchar *p = l->s;
	int a = l->size - 1;
	int b = 0;

	for (int i = 0; i < l->size; i++) if (p[i] != 32 && p[i] != 9) { a = i; break; }
	for (int i = l->size - 1; i >= 0; i--) if (p[i] != 32 && p[i] != 9) { b = i; break; }
	val re = __string_uchar(&p[a], b-a + 1);
	return re;
}

val __template(val str, val arr) {
	str = __split(str, __str("^"));
	if (__len(str) != __len(arr) + 1) {
		__log(str, arr);
		__fatal_error("internal error string.c:__template()\n");
	}
	val re = __array(__index_get(str, 0));
	for (__size_t i = 0; i < __len(arr); i++) {
		re = __val_add(re, __index_get(arr, i));
		re = __val_add(re, __index_get(str, i + 1));
	}
	return __array_join(re, __str(""));
}

val __string_from_charcode(int code) {
	return __string_uchar((uchar [1]) { code }, 1);
}

val __chr(val expr) {
	if (__is_num(expr)) return __string_from_charcode(__get_i(expr));
	if (__is_array(expr)) {
		val le = __len(expr);
		val re = __string_alloc(le);
		uchar *p = __leaf(re)->s;
		__iterate_array(i, expr) {
			val charcode = i_items[i_index];
			if (!__is_num(charcode)) {
				__throw(__str("chr: argument array must only have numbers"));
			}
			p[i_index] = (uchar)__get(charcode);
		}
		return re;
	}
	__throw(__str("chr: argument must be a number or an array of numbers"));
	return __nil;
}

val __ord(val string) {
	return __let_i(__charcode_at(string, 0));
}
