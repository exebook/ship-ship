#define HEAP_FLAG 0x0001000000000000ULL
#define INT_MASK 0xFFFFFFFF
#define PTR_MASK 0x0000FFFFFFFFFFFFULL

#define __is_int(a) (((a) & HEAP_FLAG) == 0)

#define __nil (HEAP_FLAG)
#define __is_nil(a) ((a) == HEAP_FLAG)
#define __false 0
#define __true 1

// simple macros will crash due do double evaluation
#define __is_num(a) ({ val __is_num1 = a; __is_int(__is_num1) || __is_big(__is_num1); })
#define __is_array(a) ({ val __is_ar1 = a; __is_heap(__is_ar1) && __leaf(__is_ar1)->type == leaf_arr; })
#define __is_string(a) ({ val __is_st1 = a; __is_heap(__is_st1) && __leaf(__is_st1)->type == leaf_str; })
#define __is_object(a) ({ val __isobj1 = a; __is_heap(__isobj1) && __leaf(__isobj1)->type == leaf_obj; })
#define __is_rec(a) ({ val __isrec1 = a; __is_heap(__isrec1) && __leaf(__isrec1)->type == leaf_rec; })
#define __is_fun(a) ({ val __is_fn1 = a; __is_heap(__is_fn1) && __leaf(__is_fn1)->type == leaf_fun; })
#define __is_big(a) ({ val __isbig1 = a; __is_heap(__isbig1) && __leaf(__isbig1)->type == leaf_big; })
#define __is_fly(a) ({ val __isref1 = a; __is_heap(__isref1) && __leaf(__isref1)->type == leaf_fly; })
#define __let_i(a) (val)(a & INT_MASK)
#define __let(a) __int32_ternary((a), ((val)(a) & INT_MASK), __new_big(a))

#define __get_i(a) (int32_t)((a) & INT_MASK)
#define __get_d(a) __leaf(a)->big

//do these two macros have to be functional? YES

#define __get(a) ({ val a1 = a; __is_nil(a1) ? 0 : __is_int(a1) ? __get_i(a1) : __get_d(a1); })
#define __float(a) ({ val a1 = a; __is_int(a1) ? (double)(a1) : __get_d(a1); })

#define __round(expr) ({ \
	val __round_a_ = expr; \
	__is_int(__round_a_) ? __get_i(__round_a_) : (int)__get_d(__round_a_); \
})

