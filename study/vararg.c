#include <stdarg.h>
/*
	fun f a b {
		log a b
		log args
	}
*/
void f(int count, ...) {
	printf("-----------\n");
	va_list ap;
	va_start(ap, count);
	for (int j = 0; j < count; j++) {
		printf("%i\n", va_arg(ap, int));
	}
	va_end(ap);
}

int main() {
	f(0);
	f(1, 100);
	f(2, 100, 200);
	f(3, 100, 200, 300);
	f(4, 100, 200, 300, 400);
}

