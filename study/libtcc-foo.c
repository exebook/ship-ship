#include <tcclib.h>
/* include the "Simple libc header for TCC" */
int add(int a, int b) {
	return a + b;
}
int fib(int n)
{
    if (n <= 2)
        return 1;
    else
        return fib(n-1) + fib(n-2);
}

int foo(int n)
{
    printf("Hello World!\n");
    printf("fib(%d) = %d\n", n, fib(n));
    printf("add(%d, %d) = %d\n", n, 2 * n, add(n, 2 * n));
    return 0;
};

int main() {
	foo(32);
}

