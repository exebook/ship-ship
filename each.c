//TODO there is one way to make it faster, the iterable should have be marked as 'iterating' while inside 'each {}' and then any attempt to modify it should throw

typedef struct {
	char type;
	union {
		val iterable;
		keynode **kv;
	};
	__size_t index, size;
} __each_iter;


keynode **__each_object_serialize(val object) {
	leaf *l = __leaf(object);
	dictionary *d = l->d;
	keynode **p, **re = Alloc(sizeof(keynode*) * l->d->count);
	p = re;

	for (int i = 0; i < d->length; i++) {
		if (d->table[i] != 0) {
			keynode *k = d->table[i];
			while (k) {
				*p++ = k;
				k = k->next;
			}
		}
	}
	return re;
}

void __each_begin(__each_iter *q, val a) {
	q->index = -1;

	if (__is_array(a)) {
		q->type = 0;
		q->size = __leaf(a)->size;
		q->iterable = a;
	}
	else if (__is_string(a)) {
		q->type = 1;
		q->size = __leaf(a)->size;
		q->iterable = a;
	}
	else if (__is_object(a)) {
		q->type = 2;
		q->size = __leaf(a)->d->count;
		q->kv = __each_object_serialize(a);
	}
	else if (__is_num(a)) {
		q->type = 3;
		q->size = __get_i(a);
	}

	else {
		__throw(__tem("each iterable cannot be <^>", __type_of(a)));
	}
}

char __each_next(__each_iter *q) {
	q->index++;
	return q->index == q->size ? 0 : 1;
}

val __each_item(__each_iter *q) {
	if (q->type == 0) {
		return __array_get(q->iterable, q->index);
	}
	else if (q->type == 1) {
		return __index_get(q->iterable, q->index);
	}
	else if (q->type == 2) {
		return q->kv[q->index]->value;
	}
	else if (q->type == 3) {
		return q->index;
	}
	else if (q->type == 4) {
		return q->index;
	}
	return __nil;
}

val *__each_item_ref(__each_iter *q) {
	if (q->type == 0) {
		return __array_at(q->iterable, q->index);
	}
	else if (q->type == 1) {
		__throw(__str(E022_0));
	}
	else if (q->type == 2) {
		return &q->kv[q->index]->value;
	}
	else if (q->type == 3) {
		__throw(__str("numeric item is not assignable"));
	}
	else if (q->type == 4) {
		__throw(__str("ranged item is not assignable"));
	}
	__throw(__str("__each_item_ref error"));
	return 0;
}

val __each_index(__each_iter *q) {
	if (q->type < 2) {
		return __let_i(q->index);
	}
	else if (q->type == 3) {
		return q->index;
	}
	else if (q->type == 4) {
		return q->index;
	}
	else {
		keynode *k = q->kv[q->index];
		return __string_uchar((uchar*)k->key, k->len / 2);
	}
}

//*********************** EACH RANGED
// for the future, we can know at compilation time if item/index belongs
// to each_ranged, which means they can be compiled as a separate loop and
// without dynamic q->type checking
// this will probably make each.. somewhat faster

//typedef struct {
	//__size_t A, B;
//} __each_range_iter;

void __each_range_begin(__each_iter *q, __size_t a, __size_t b) {
	q->type = 4;
	q->index = a - 1;
	q->size = b + 1;
}
//
//char __each_range_next(__each_range_iter *q) {
	//return q->A++ == q->B ? 0 : 1;
//}
//
//val __each_range_item(__each_range_iter *q) {
	//return q->A;
//}
//
//val *__each_range_item_ref(__each_range_iter *q) {
	//__throw(__str("__each_range_item_ref error"));
	//return 0;
//}
//
//val __each_range_index(__each_range_iter *q) {
	//return q->A;
//}
