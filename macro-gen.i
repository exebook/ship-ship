fun macro_gen {
	fun generate__arg_decl M {
		a := []
		while len a < M a += 'val '+CARG_PREFIX + len a
		x := ''
		if len a > 0 x = a * ',' + ', '
		ret '#define __arg_decl __env_t *'+CARG_ENV+', val __this, short '+CARG_COUNT+', '+x+'val *'+CARG_PTR+'\n'
	}
/:
	types of call macros
	__call() convenient general use base version, only passes arguments, this and env are nil
	__call_ex() same as __call but allows to pass env and this
	__call_n() what __call compiles to, not supposed to be used by users
	__call_v() used by the compiler, accepts a vector of arguments (val[])

	if you write a high-level function in C by hand, you access arguments with
		__arg(n) where n the arg number
		also you declare such function and return something as in this example:

		val my_plus(__arg_decl) {
			__enter();
			val x = __arg(0) + __arg(1);
			__return(x);
		}

		__log(__call(my_plus, 10, 20));

:/
	fun generate__call_v M {
		fun regs {
			if M == 0 ret ''
			re := []
			while len re < M re += 'n>'+ len re +'?vec['+len re+']:__nil'
			ret re * ', ' + ','
		}
		ret '#define __call_v(f, e, this, n, vec) f(e, this, n, '+ regs() +'n>='+M+'?&vec['+M+']:0)\n'
	}

	fun generate__call_n M {
		fun regs N max {
			if max == 0 ret ''
			re := []
			while len re < N re += cc(A '['len re']')
			while len re < max re += '__nil'
			ret re * ',' + ','
		}
		A := '((val[]){a})'
		cc := lam = args * ''
		a := [] b  := []

		// https://gcc.gnu.org/bugzilla//show_bug.cgi?id=46073
		// seems like GCC have this this little annoyance fixed but not Clang

		a += '#define __call_n(f, e, this, n, a...) \\'
		a += '_Ignore_array_bounds \\'

		while len b <= M {
			b += cc('__builtin_choose_expr(n=='len b', f(e, this, n,' regs(len b M) '0), \\')
		}
		a += b
		a += cc('__builtin_choose_expr(n>'M', f(e, this, n,' regs(M M) '&('A'['M'])), \\')
		z := []
		while len z < CARG_MAX z += '0'
		f := 'f(0,0,0,0'
		if len z > 0 f += ','+ z * ','
		f += ') \\'
		a += f
		e := ''; while len e < M e += ')'
		a += '))'+e + '\\'
		a += '_Unignore_array_bounds'

		ret a*'\n'+'\n'
	}

	fun generate__arg M {
		re := [
			'#define __arg_count '+CARG_COUNT+'\n'
			'#define __arg(n) (n < 0 || n > '+CARG_COUNT+') ? __nil : \\'
		]
		a := []
		while len a < M a += '__builtin_choose_expr(n=='+len a+', '+CARG_PREFIX+len a+', \\'
		re += a
		re += CARG_PTR+'[n-'+M+'] \\'
		a = ''
		while len a < M a += ')'
		re += a + ''

		argn := '#define __argn(n) ((n < 0 || n > '+CARG_COUNT+') ? __nil :'
		a = []
		while len a < M a += 'n=='+len a+' ? '+CARG_PREFIX+len a+' :'
		argn += ' ' + a * ' ' + CARG_PTR+'[n-'+M+'])'
		re += argn

		ret re * '\n'+'\n'
	}

	re := ''
	re += generate__arg_decl(CARG_MAX)
	re += '#define CARG_MAX '+CARG_MAX+'\n'
	re += generate__call_n(CARG_MAX)
	re += generate__call_v(CARG_MAX)
	re += generate__arg(CARG_MAX)
	ret re
}
