char __cmp_str(val a, val b) {
	__string_compact(a);
	__string_compact(b);

	leaf
		*al = __leaf(a),
		*bl = __leaf(b);

	if (al == bl) return 0;

	uchar
		*pa = al->s,
		*pb = bl->s,
		*ea = al->s + al->size,
		*eb = bl->s + bl->size;

	while (pa < ea && pb < eb) {
		if (*pa == *pb) {
			pa++; pb++;
			continue;
		}
		if (*pa < *pb) return -1;
		return 1;
	}
	int ax, bx;
	ax = __len(a);
	bx = __len(b);
	if (ax == bx) return 0;
	if (ax < bx) return -1;
	return 1;
}



val __cmp(val a, val b, char op) { // == 0, < 1, > 2, <= 3, >= 4, != 5
	if (a == b && op == 0) return 1;
	if (__is_nil(a)) {
		if (__is_nil(b)) switch (op) {
			case 0: return __let(1);
			case 1: return __let(0);
			case 2: return __let(0);
			case 3: return __let(1);
			case 4: return __let(1);
			case 5: return __let(0);
		}
		else return __let(op == 5 ? 1 : 0);
	}
	else if (__is_heap(a)) {
		if (!__is_heap(b)) return __let(op == 5 ? 1 : 0);
		leaf
			*la = __leaf(a),
			*lb = __leaf(b);

		if (la->type != lb->type) return __let(op == 5 ? 1 : 0);

		switch (la->type) {
			case leaf_str: {
				char c = __cmp_str(a, b);
				switch (op) {
					case 0: return __let(c == 0);
					case 1: return __let(c == -1);
					case 2: return __let(c == 1);
					case 3: return __let(c == 0 || c == -1);
					case 4: return __let(c == 0 || c == 1);
					case 5: return __let(c != 0);
				}
			}
			case leaf_fun: {
				return __let((la->f == lb->f));
			}
			case leaf_obj: {
				switch (op) {
					case 0: return __let(la == lb);
					case 1: return __let(0);
					case 2: return __let(0);
					case 3: return __let(1);
					case 4: return __let(1);
					case 5: return __let(la != lb);
				}
			}
			default: return __let(0);
		}
	}
	else if __is_int(a) {
		if (!__is_num(b)) return __let(op == 5 ? 1 : 0);
		switch (op) {
			case 0: return __eq(a, b);
			case 1: return __lt(a, b);
			case 2: return __gt(a, b);
			case 3: return __le(a, b);
			case 4: return __ge(a, b);
			case 5: return __ne(a, b);
		}
		return 0;
	}
	return __let(0);
}


val __find(val where, val what, __size_t at) {
	if (__is_string(where)) {
		if (!__is_string(what)) __throw(__tem(E016_1, __type_of(what)));
		return __let_i(__str_find_at(where, what, at));
	}
	else if (__is_array(where)) {
		return __is_string(what)
			? __array_find_string(where, what, at)
			: __array_find_val(where, what, at);
	}
	__throw(__tem(E017_1, __type_of(where)));
	return __nil; // never reach here
}
