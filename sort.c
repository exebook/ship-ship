void __swap_p(val *a, val *b) {
	val c = *a;
	*a = *b;
	*b = c;
}

int64_t __sort_cmp_(val *a, val *b, val f) {
	if (__is_fun(f)) {
		val re = __closure_call(f, *a, *b);
		if (!__is_num(re)) {
			__throw(__str("sort filter function must return a number"));
		}
		return re;
	}
	else {
		return __cmp(*a, *b, 2); //a > b
	}
}

void __dual_quicksort(val *left, val *right, val f) {
	if (right <= left) return;
	if (__sort_cmp_(left, right, f)) {
		__swap_p(left, right);
	}
	val
		*p = left,
		*q = right,
		*l = left + 1,
		*g = right - 1,
		*k = l;
	while (k <= g) {
		__loop_mark();
		if (__sort_cmp_(p, k, f)) {
			__swap_p(k, l++);
		}
		else if (__sort_cmp_(k, q, f)) {
			while (__sort_cmp_(g, q, f) && k < g) g--;
			__swap_p(k, g--);
			if (__sort_cmp_(p, k, f)) __swap_p(k, l++);
		}
		k++;
		__loop_release();
	}
	l--; g++;

	__swap_p(left, l); __swap_p(right, g);

	__dual_quicksort(left, l - 1, f);
	__dual_quicksort(l + 1, g - 1, f);
	__dual_quicksort(g + 1, right, f);
}

void __sort(val a, val f) {
	if (__len(a) < 2) return;

	if (!__is_array(a)) {
		__throw(__str("sort: first arg is not an array"));
	}

	//val f = __arg(1);
	//__log(f);

	if (!__is_nil(f) && !__is_fun(f)) {
		__throw(__str("sort: second arg must be a function or nil"));
	}

	__enter();
	leaf *l = __leaf(a);
	val *p = l->p;
	__dual_quicksort(p, & p[l->size - 1], f);
	__leave();
}

