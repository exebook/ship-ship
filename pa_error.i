color_error := false

color_optional := lam clr text {
	if color_error ret color(clr text)
	ret text
}

fun stop A msg {
	ret {
		kind: 'stop'
		token_pos: A[0]
		tokens: A[1]
		msg: msg
	}
}

fun stop2 A msg B msg_b {
	S := stop(A msg)
	S.prev = stop(B msg_b)
	ret S
	//ret stop(A msg) + { prev: stop(B msg_b) }
}

fun dev_error msg = { kind: 'dev' msg }

fun nodestr node {
	ret shame(node, { filter: lam k x {
		if k == 'type' ret '~'+types[x]
		but k == 'sym' ret Token[x]+':'+x
		but k == 'pos' ret '@'+x[0]
		else ret x }
	})
}

prn_ast := lam a indent id {
	log nodestr(a)
	ret
	if is_array(a) {
		log ~purple tt k ~green '['
		each a {
			prn_ast(item indent + 2 index+'')
		}
		log ~green tt + ']'
	}
	fun tab x { re := '' while len re < x*3 re += ' ' ret re }
	t := tab(indent)
	tt := tab(indent + 1)
	use id id += ' '
	init id = ''
	cout ~gray t +id ~purple '('a.pos[0]') ' types[a.type] ~green ' {\n'

	each keys(a) {
		k := item
		x := a[k]
		if k == 'pos' or k == 'type' cont
		if is_object(x) and x.type != nil {
			prn_ast(x indent + 1 k)
		}
		but is_array(x) {
			log ~purple tt k ~green '['
			each x {
				prn_ast(item indent + 2 index+'')
			}
			log ~green tt + ']'
		}
		but x == nil {
			log k+':nil'
		}
		else {
			if k == 'op' {
				log tt + k + '' ~red Keys[x]
			}
			else {
				log tt + k + '' ~3 x
			}
		}
	}
	log ~green t + '}'
}

past := fun a id {
	re := []
	if is_array(a) {
		re += color_optional(5 ' [. ')
		each a re += past(item)
		re += color_optional(5 ' .] ')
		ret re * ''
	}
	if id and len id > 0 re += color_optional(14 id+'') + ' '
	re += [ color_optional(5 types[a.type]) color_optional(2 '{')]

	skip_len := 0
	each keys(a) {
		k := item
		x := a[k]
		if k == 'pos' or k == 'type' cont
		if skip_len > 0 re += ' '
		if is_object(x) and x.type != nil {
			re += past(x k)
		}
		but is_array(x) {
			re += color_optional(5 k) + color_optional(2 '[')
			each x {
				if index > 0 re += ','
				re += past(item '')
			}
			re += color_optional(2 ']')
		}
		but x == nil {
			re += k+':nil'
		}
		but k == 'op' {
			re += k +' '+ color_optional(1 ''+Keys[x])
		}
		else {
			re += k +' '+ color_optional(3 x+'')
		}
		inc skip_len
	}
	re += color_optional(2 '}')
	ret re * ''
}

fun error_source_at src pos {
	fun get_leading_spaces s {
		re := 0
		each s if item <= 32 re += 1 else break
		ret re
	}
//	pos := token[1]
	line := 0 line_start := 0
	i := 0
	while i < pos {
		if src[i] == 10 {
			inc line
			line_start = i + 1
		}
		inc i
	}
	lines := src / '\n'
	src_line := lines[line]
	ptr := pos - line_start - get_leading_spaces(src_line)
	src_line = trim(src_line)
	caret := '' while len caret < ptr caret += ' '
	ret [src_line caret line]
}

fun source_error src tokens e {
	tok := tokens[e.token_pos]
	if tok == nil { // handle EOF
		tok = tokens[-1]
		tokens += [tok]
		tok[1] = len src
	}
	x := error_source_at(src tok[1])
	ret [
		tok[0] + ': ' + (x[2]+1)
		'  ' + x[0]
		'  ' + color_optional(1 x[1] + '^') + e.msg
	] * '\n'
}

fun show_stop e {
	n := e.token_pos
	if n >= len e.tokens {
		n = len e.tokens - 1
		if n < 1 {
			log 'empty file error'
		}
	}
	n2 := e.token_pos
	if n2 >= len e.tokens {
		n2 = len e.tokens - 1
		if n2 < 1 {
			log 'empty file error'
		}
	}
	E1 := source_error(e.tokens[n][6] e.tokens e)
	if e.prev E2 := source_error(e.prev.tokens[n2][6] e.prev.tokens e.prev)
	if E2 ret E2 +'\n'+ E1
	ret E1
}

fun lexer_error fname src pos msg {
	x := error_source_at(src pos)
	ret [
		fname + ': ' + (x[2]+1)
		x[0]
		color_optional(1 x[1] + '^') + msg
	] * '\n'
}

fun line_at_pos pos {
	tok := pos[1][pos[0]][tokrec.line_no]
	ret tok
}

