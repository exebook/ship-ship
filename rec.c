// breakdown of a rec type (internally rec is an array)
#define REC_SIZE 0
#define REC_NAME 1
#define REC_FIRST 2

#define __rec(ty, items...) __rec__(ty, sizeof(((val[]){ items })), ((val[]){ items }), 0)
#define __rec_get_type(rec_type) __array_get(__global.rec_type_arr, rec_type)

val __rec_new(int rec_type, __size_t count, val *items) {
	leaf *l = leaf_new_rec(count, rec_type);
	//for (int i = 0; i < count; i++) {
		//l->items[i] = items[i];
	//}
	memmove(l->items, items, count * sizeof(val));
	return __mk_val(l);
}

val __rec__(int rec_type, __size_t count, val *items) { // this is for Ship/C API __rec
	val ty = __rec_get_type(rec_type);
	if (!__is_array(ty)) {
		__throw(__str("__rec: unknown rec_type"));
	}
	int r_size = __get_i(__array_get(ty, REC_SIZE));
	if (r_size != count) {
		val r_name = __array_get(ty, REC_NAME);
		__throw(__concat(__str("__rec: size mismatch for "), r_name));
	}
	return __rec_new(rec_type, count, items);
}

val __rec_get(val rec, val ix) {//int rec_type shall I check record type here?
	leaf *l = __leaf(rec);
	if (l->type == leaf_rec) {
		return l->items[ix];
	}
	else {
		__throw(__str("__rec_get: not a record"));
	}
	return __nil;// silencer
}

void __rec_set(val rec, val ix, val value) {
	leaf *l = __leaf(rec);
	if (l->type == leaf_rec) {
		l->items[ix] = value;
	}
	else {
		__throw(__str("__rec_set: type mismatch"));
	}
}

val __rec_to_object(val a) {
	__enter();
	leaf *l = __leaf(a);
	val ty = __rec_get_type(l->rectype);
	if (!__is_array(ty)) {
		__throw(__str("__rec: unknown rec_type"));
	}
	int r_size = __get_i(__array_get(ty, REC_SIZE));
	val re = __object();
	for (int i = 0; i < r_size; i++) {
		val nam = __array_get(ty, i + REC_FIRST);
		__property_set(re, nam, l->items[i]);
	}
	__return(re);
}
