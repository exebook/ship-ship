//fun _get_env_src d {
	//else throw '_get_env_src problem, ' +d.id+ ' is ' + decl[d.type]
//}

fun _create_env up node s {
	ee := s.handler(uplink.ENV)
	if ee == nil {
		throw stop(node.pos tem(E004_1 s.handler(uplink.ID)))
		//todo: in fact builtins could be referenced
	}
	dd := nil
	re := []
	each ee {
		uid := item.uid
		d := up(uplink.GETU uid)
		x := nil
		if d.type == decl.FLY x = c_varget(d.stack_addr)
		but d.type == decl.FLYARG x = c_argget(d.arg_addr)
		but d.type == decl.ENV {
			d = up(uplink.GETU item.uid)
			init dd = up(uplink.ENV)
			x = c_envdirect(dd[item.uid].env_addr)
		}
		else {
			throw '_creat_env ??'
		}
		re[item.env_addr] = x
	}
	ret re
}

fun sy_const id {
	ret lam m {
		if m == mode.CODE ret c_const(id)
	}
}

fun sy_id up node {
	if node.id == 'nil' ret sy_const('nil')
	if node.id == 'true' ret sy_const('true')
	if node.id == 'false' ret sy_const('false')

	d := nil
	enu := nil

	up(uplink.REF node)
	up(uplink.AFTER lam stage {
		// TODO: add if stage==1 here
		//if stage == stag.SCOPE
		d = up(uplink.GET node.id)
		init d {
			d = up(uplink.FIND node.id)
		}
		init d {
			enu = up(uplink.ENUM_GET node.id)
			init enu throw stop(node.pos tem(E011_1 node.id))
		}
	})

	ret lam m {
		if m == mode.NODE {
			ret node
		}
		but m == mode.TYPE {
			ty := up(uplink.NAME_GET node.id)
			init ty = 'var'
			ret ty
		}
		but m == mode.CODE {
			if enu ret c_enumap(enu)
			if d.type == decl.LOCAL ret c_varget(d.stack_addr)
			but d.type == decl.ARG ret c_argget(d.arg_addr)
			but d.type == decl.FLYARG ret c_flyargget(d.arg_addr)
			but d.type == decl.ENV ret c_envget(d.env_addr)
			but d.type == decl.FLY ret c_flyget(d.stack_addr)
			but d.type == decl.FUN ret c_closure(d.handler(uplink.C_ID), _create_env(up node d))
			else throw stop(node.pos 'sy_id unhandled')
		}
	}
}

fun sy_args up node {
	ret lam m {
		if m == mode.CODE {
			ret c_args()
		}
	}
}

fun sy_this up node {
	ret lam m {
		if m == mode.CODE {
			ret c_this()
		}
	}
}

fun sy_it up node {
	re := up(uplink.IT)
	if re {
		ret sy_expr(up re)
	}
	ret lam m {
		if m == mode.CODE {
			ret c_nil()
		}
	}
}

fun sy_that up node {
	re := up(uplink.THAT)
	if re ret sy_expr(up re)
	ret lam m {
		if m == mode.CODE {
			ret c_nil()
		}
	}
}

fun sy_error up node {
	init up(uplink.IN_CATCH) throw stop(node.pos E012_0)
	ret lam m {
		if m == mode.CODE {
			ret c_error()
		}
	}
}

fun show_fun h locals env refs {
	id := h(uplink.C_ID)
	a := []
	each locals {
		addr := ''
		if item.type == decl.LOCAL or item.type == decl.FLY addr = ':'+item.stack_addr+''
		a += cle + ' ' + color(1 decl[item.type]) + addr
	}
	b := []
	each env {
		b += cle + ':'+item.env_addr+''
	}
	log ~blue 'fun' id '{' ~~ a*'  ' ~blue '}' b*' ' ~green keys(refs)*' '
}

fun sy_ret up node {
	x := sy_const('nil')
	use node.expr x = sy_expr(up node.expr)
	try_depth := up(uplink.TRY_DEPTH)

	up(uplink.AFTER lam stage {
		if stage == stag.TYPE {
			//log 'ret from' up(uplink.ID) nodestr(node)
			up(uplink.RET_TYPE x(mode.TYPE))
		}
	})

	ret lam m {
		if m == mode.TYPE {
			if x ret x(m) else ret 'var'
		}
		but m == mode.CODE {
			if try_depth == 0 ret c_ret(x(m))
			else ret statems([
				c_unwind_try_stack(try_depth)
				c_ret(x(m))
			])
		}
	}
}

fun sy_lam up node {
	node.id = up(uplink.C_ID_MAKE 'lam')
	f := sy_fun(up node)
	def := up(uplink.GET node.id)

	ret lam m {
		if m == mode.CODE {
			f(m)
			re := c_closure(def.handler(uplink.C_ID), _create_env(up node def))
			ret re
		}
	}
}

fun stop_fun_type_mismatch up pos expected actual {
	id := ((expected / '-')[1] / '>')[0]
	expected /= ':'
	actual /= ':'

	err := nil
	if expected[0] == actual[0] {
		if actual[1] == 'nil' {
			err = tem(E019_3 id 'nil' expected[1])
		}
		but expected[1] == 'nil' {
			err = tem(E019_3 id actual[1] 'nil')
		}
		else {
			err = tem(E019_3 id actual[1] expected[1])
		}
	}

	ret stop2(pos err up(uplink.NAME_POS id) tem(E020_1 id))
}

enum stag { SCOPE CALL TYPE }

fun sy_fun up node {
	refs := {}
	locals := {}
	env := {}
	stack_size := 0
	recursing := 0
	fuid := up(uplink.C_ID_MAKE, [node.id])
	each_uniq := 0
	_the := nil
	_that := nil
	fun_ret_ty := nil

	up(uplink.FUN handler)

	block := _sy_each(handler node.block sy_statem)

	//cout ~2
	//log show_stop(stop(node.pos tem('fun ^ detected' node.id)))
	//cout ~~

	fun set_fun_type ty {
		//log ~1 'ret' node.id node.pos[0] ty
		if fun_ret_ty and fun_ret_ty != ty {
			throw stop(node.pos tem('fun ^ returns ^ but must return ^' node.id ty fun_ret_ty))
		}
		fun_ret_ty = ty
	}

	fun check_siggy {
		init fun_ret_ty = 'nil'
		siggy := 'fun-'+ node.id +'>'+ map(node.args lam x = x.id)*',' +':'+ fun_ret_ty
		//log ~1 'new siggy' siggy 'at line' line_at_pos(node.pos)
		sig := up(uplink.NAME_GET node.id)
		init sig {
			// TODO: handle ellipsis ... functions here somehow
			up(uplink.NAME_SET, [node.pos node.id siggy])
		}
		else if sig != siggy {
			throw stop_fun_type_mismatch(up node.pos sig siggy)
			//throw stop(node.pos fun_type_mismatch_message(up sig siggy))
		}
	}

	up(uplink.AFTER lam stage {
		//each afters item(stage)
		if stage == stag.SCOPE scope_analysis()
		but stage == stag.CALL call_analysis()
		but stage == stag.TYPE check_siggy()
	})

	each locals {
		d := item
		if d.type == decl.LOCAL {
			d.stack_addr = stack_size
			stack_size += 1
		}
	}

	a := node.args
	each a {
		init the locals[item.id] {
			it = {
				type: decl.ARG
				id: item.id
				uid: fuid+'.'+item.id
				arg_addr: index
				node: item
			}
		}
		else {
			var_shadowed := locals[item.id]
			throw stop(var_shadowed.node.pos 'variable definition shadows argument')
		}
	}

	fun make_path {
		re := up(uplink.MAKE_PATH)
		re[fuid] = 1
		ret re
	}

	fun need id {
		loc := locals[id]
		if loc {
			if loc.type == decl.LOCAL loc.type = decl.FLY
			but loc.type == decl.ARG loc.type = decl.FLYARG
			but loc.type == decl.FLY or loc.type == decl.FLYARG {}
			else {
				throw stop(loc.node.pos 'need: reference is ' + decl[loc.type])
			}
			ret { id, uid: fuid + '.' + id, fuid }
		}
		else {
			each env if item.id == id ret item
			e := up(uplink.NEED id)

			init env[e.uid] {
				re := {
					type: decl.ENV
					id: e.id
					uid: e.uid
					fuid: e.fuid
					env_addr: len env
				}
				env[e.uid] = re
				ret re
			}
			else throw 'need(id) error'
			ret e
		}
	}

	fun need_uid uid {
		each locals {
			id := cle
			cur_uid := fuid + '.' + id
			if cur_uid == uid {
				loc := locals[id]
				if loc.type == decl.LOCAL loc.type = decl.FLY
				but loc.type == decl.ARG loc.type = decl.FLYARG
				but loc.type == decl.FLY or loc.type == decl.FLYARG {}
				ret { id, uid, fuid }
			}
		}
		e := up(uplink.NEED_UID uid)
		init env[uid] {
			re := {
				type: decl.ENV
				id: e.id
				uid: e.uid
				fuid: e.fuid
				env_addr: len env
			}
			env[uid] = re
		}
		ret e
	}

	fun scan_env path {
		ee := {}
		each env use path[item.fuid] ee[cle] = 1

		each refs {
			d := handler(uplink.FIND cle)
			if d.type == decl.FUN {
				inc recursing
				e := d.handler(uplink.SCAN_ENV path)
				dec recursing
				each e ee[cle] = 1
			}
		}
		ret ee
	}

	fun after stage {
		if stage == stag.SCOPE scope_analysis()
		but stage == stag.CALL call_analysis()
	}

	fun remove_enums_from_locals {
		tmp := {}
		each refs {
			if handler(uplink.ENUM_GET cle) == nil tmp[cle] = item
		}
		refs = tmp
	}

	fun scope_analysis {
		remove_enums_from_locals()
		each refs {
			d := handler(uplink.FIND cle)
			init d {
				throw stop(item.pos 'scope: undeclared identifier <'+cle+'>')
			}
			if d.type != decl.FUN {
				init locals[cle] e := need(cle)
			}
		}
	}

	fun call_analysis {
		ee := {}
		each env ee[cle] = 1

		each refs {
			d := handler(uplink.FIND cle)
			init d {
				throw stop(item.pos 'this should never happen')
			}
			if d.type == decl.FUN {
				mp := up(uplink.MAKE_PATH)
				inc recursing
				e := d.handler(uplink.SCAN_ENV mp)
				dec recursing
				each e ee[cle] = 1
			}
		}

		each ee need_uid(cle)
		//show_fun(handler locals env refs)
	}

	fun handler u a { // FUNCTION
		if u == uplink.ID ret node.id
		but u == uplink.C_ID ret fuid
		but u == uplink.NODE ret node
		//but u == uplink.AFTER ret up(u a)
		but u == uplink.ENUM_SET {
			use locals[a.id] {
				throw stop(a.pos tem(E005_1 a.id))
			}
			ret up(u a)
		}
		//but u == uplink.ENUM_GET ret up(u a)
		//but u == uplink.ENUM_ALLOC ret up(u a)
		but u == uplink.REF {
			init refs[a.id] = a
		}
		but u == uplink.DEF {
			if up(uplink.ENUM_GET a.id) throw stop(a.pos tem(E006_1 a.id))
			if locals[a.id] ret locals[a.id]
			locals[a.id] = { type: decl.LOCAL id: a.id, uid: fuid+'.'+a.id node: a }
		}
		but u == uplink.FUN {
			id := a(uplink.ID)
			locals[id] = { type: decl.FUN id, handler: a, node: a(uplink.NODE) }
		}
		but u == uplink.RET_TYPE {
			set_fun_type(a)
		}
		but u == uplink.C_ID_MAKE ret up(u, [node.id] + a)
		but u == uplink.NEED ret need(a)
		but u == uplink.NEED_UID ret need_uid(a)
		but u == uplink.GET {
			use the locals[a] ret it
			each env if item.id == a ret item
			//throw 'strange ' + a
		}
		but u == uplink.GETU {
			each locals if item.uid == a ret item
			each env if item.uid == a ret item
		}
		but u == uplink.FIND {
			if the locals[a] ret it
			ret up(uplink.FIND a)
		}
		but u == uplink.SCAN_ENV {
			if not recursing ret scan_env(a)
			ret {}
		}
		but u == uplink.MAKE_PATH ret make_path()
		but u == uplink.FUNCODE ret up(u a)
		but u == uplink.ENVSIZE ret len env
		but u == uplink.DECLS ret locals
		but u == uplink.ENV ret env
		//but u == uplink.FLY { make_fly(a) }
		but u == uplink.TMP {
			tmp_addr := stack_size
			stack_size += 1
			ret tmp_addr
		}
		but u == uplink.EACH_ID ret nil
		but u == uplink.EACH_UNIQ {
			inc each_uniq
			ret each_uniq
		}
		but u == uplink.TRY_DEPTH ret 0
		but u == uplink.THE {
			_that = _the
			_the = a
		}
		but u == uplink.IT ret _the
		but u == uplink.THAT ret _that
		but u == uplink.VARSET ret
		but u == uplink.IN_CATCH ret nil
		else ret up(u a)
		//throw stop(node.pos tem('handler cannot handle ^', u))
	}

	fun map_vars {
		re := []
		loc := []
		each locals loc += item
		sort(loc lam a b = a.stack_addr > b.stack_addr)
		// sorting is needed because keys order in locals changes
		// when flyvars are being propagated

		each loc {
			if item.type == decl.LOCAL re += 0
			if item.type == decl.FLY re += 1
		}
		while len re < stack_size re += 0
		ret re
	}

	fun flyargs {
		re := []
		each locals {
			if item.type == decl.FLYARG {
				re += item.arg_addr
			}
		}
		ret re
	}

	ret lam m {
		if m == mode.CODE {
			up(uplink.FUNCODE, c_fun(fuid map_vars() flyargs() block(m)))
			ret c_rem('fun '+node.id + '')
		}
	}

}

enum C_ID_KIND { std builtin fun enum }

//enum tt { any num str fun lit_obj }

fun sy_prog up node options {
	if node == nil or node.type != types.program
		throw stop(node.pos, 'program node expected')

	unames := {}
	uname_pos := {} // position where a name was defined in source code
	recs := {}
	//type_list := [ {s:'any'} {s:'lit_obj'} ] //{s:'.num'} {s:'.str'} {s:'.fun'}
	//type_map := {}// any: 0 num: 1 str: 2 fun: 3 obj_lit: 4 str_lit: 5 num_lit: 6 fun_lit: 7 }
	//each type_list {
		//item.n = index
		//type_map[item.s] = item
	//}
	//type_list := { any: {}, lit_obj: {} }

	funcs := []
	afters := []
	enums := {}
	//enum_count := 0
	c_global := []

	// TODO: put here all C functions used by ru.c and stdlib
	std_names := 'main printf malloc free realloc sprintf memcpy strcpy strlen fork getpid random'

	builtin_names := 'time_ms gc_test_run parse_num trim find push pop put pull type_name is_string is_array is_number is_object is_fun is_nil shame honor floor round ceil abs sort charcode_at force_gc error_trace stack_trace enable_trace show_runtimes fcolor bcolor load save delete __try_stack keys values bind system execa argv'
	c_global_ns := map_each(std_names / ' ' C_ID_KIND.std)
	each options.c_shadow_list c_global_ns[item] = C_ID_KIND.std
	builtins := { }
	each builtin_names / ' ' {
		id := item
		c_id := ' __ship_runtime_'+ id
		c_global_ns[id] = C_ID_KIND.builtin // builtin function
		builtins[id] = { type: decl.FUN id: id, handler: builtin_handler(id c_id) }
	}

	fun c_make_uniq_id base_id cid_type {
		c_id := base_id
		n := 0
		while c_global_ns[c_id] != nil {
			inc n
			c_id = base_id + n
		}
		init c_global_ns[c_id] = cid_type
		ret c_id
	}

	fun handler u a { // PROGRAM
		if u == uplink.FUNCODE { funcs += a }
		but u == uplink.AFTER { afters += a }
		but u == uplink.MAKE_PATH ret {}
		but u == uplink.FUN {
			if a(uplink.ID) == 'program' ret
			else log 'sy_prog handler: unexpected FUN ' + a(uplink.ID)
		}
		but u == uplink.FIND { ret builtins[a] }
		but u == uplink.C_ID_MAKE {
			if len a > 1 a = a[1..]
			base_id := a * '_'
			ret c_make_uniq_id(base_id C_ID_KIND.fun)//2 is normal function
		}

		but u == uplink.ENUM_SET {
			init enums[a.id] = {
				name: a.id
				is_free: a.is_free
				items: {}
				cid: c_make_uniq_id('enum_' + a.id C_ID_KIND.enum)
				reflect: false
			}
			//else throw stop(node.pos 'enum <'+a.id+'> is already defined')
			ret enums[a.id]
		}
		but u == uplink.ENUM_GET ret enums[a]

		but u == uplink.C_GLOBAL c_global += a

		but u == uplink.NAME_GET {
			ret unames[a]
		}
		but u == uplink.NAME_SET {
			unames[a[1]] = a[2]
			uname_pos[a[1]] = a[0]
			ret unames[a[1]]
		}
		but u == uplink.NAME_POS {
			ret uname_pos[a]
		}
		but u == uplink.REC_TYPE {
			if recs[a] == nil {
				tmp := len recs
				recs[a] = tmp
				// recs[a] = len recs // compiles wrongly by boot0, try ship -x 'a:={} a["b"]=len a; log a'
			}
			ret recs[a]
		}

		//but u == uplink.TYPE_GET {
			//if is_number(a) ret type_list[a]
			//ret type_map[a]
		//}
		//but u == uplink.TYPE_NEW {
			//use type_map[a] throw 'duplicate type ' + a
			//// TODO display node of duplicate-type error
			//// or better never use TYPE_NEW before TYPE_GET was used to check the type
			//nt := { n: len type_map, s: a }
			//type_map[a] = nt
			//type_list += nt
			//ret nt
		//}
		else up(u a)
	}

	fun builtin_handler id c_id {
		init c_id = id
		ret lam u a {
			if u == uplink.ID ret id
			but u == uplink.C_ID ret c_id
			but u == uplink.DECLS ret { }
			but u == uplink.SCAN_ENV ret {}
			but u == uplink.ENVSIZE ret 0
			but u == uplink.ENV ret nil
			else {
				throw 'builtin_handler: ' + uplink[u]
			}
		}
	}

	prog := sy_fun(handler, { pos: node.pos type: types.fun block: node.block, id: 'program' args:[] })

	each afters item(stag.SCOPE)
	each afters item(stag.CALL)
	each afters item(stag.TYPE)

	ret lam m {
		if m == mode.CODE {
			forwards := []
			each c_global_ns
				if item == C_ID_KIND.fun forwards += c_forward_decl(cle)
			forwards += c_forward_decl('program_f')
			funcs += prog(m)
			each enums {
				if item.reflect forwards += c_enum_decl(item)
			}
			block := [c_call('program' [])]
			inits := []
			if options.signals inits += [c_default_signal_handlers()]
			if options.test_mode inits += [c_test_mode_setup()]
			if options.trace_disable inits += [c_disable_traces()]
			if options.show_runtimes inits += [c_show_runtimes()]

			rec_type_list := []
			each keys(recs) {
				ry := (item / '-')
				ry = [len ry-1 ry[0]] + ry[1..]
				inits += [c_register_rec(ry)]
			}
			ret c_program(forwards + c_global + flatter.eol + c_rem('user code begin') + funcs, inits block)
		}
	}
}

/:
x := 1
fun f a b {
	y := 2
	z := 3
	log a b x y z
	ret fun g {
		log a x
	}
}
f(4 5)()
:/

