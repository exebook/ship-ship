code multi-assign basic assign
	a b c := nil
	are a b c
	a b c = [10 20 30]
	are a b c

output
	a nil  b nil  c nil
	a 10  b 20  c 30

code multi-define basic
	a b c := [2 4 6]
	are a b c
	x y z := 33
	are x y z

output
	a 2  b 4  c 6
	x 33  y 33  z 33

code if-expr basic if
	log if 1 'one' else 'two'
	log if 0 'yes' else 'no'

output
	one no

code multi-assign-value basic assign
	a := {}
	a.b a.c = 1
	log a

output
.test.i: 2
  a.b a.c = 1
  ^E010_0
  //{ a: 1 b: 1 }

code else-but if
f := lam x {
	if x==1 log 'one ='x
	but x==2 log 'two ='x
	but x==3 log 'three ='x
	else log 'other ='x
}

f(0) f(1) f(2) f(3)

f = lam x {
	if x==1 log 'one ='x
	but x==2 log 'two ='x
	but x==3 log 'three ='x
	else log 'other ='x
}

f(0) f(1) f(2) f(3)

output

other = 0
one = 1
two = 2
three = 3
other = 0
one = 1
two = 2
three = 3


code closure-minimal closure
	fun e a {
		b := a
		fun closure {
			log b
		}
		ret closure
	}

	a := e(555)
	b := e(777)
	a()
	b()

output
	555
	777

code closure-gc closure
	fun counter a {
		gc_test_run()
		b := a
		gc_test_run()
		fun closure {
			gc_test_run()
			b = b + 1
			gc_test_run()
			ret b
		}
		gc_test_run()
		ret closure
	}

	gc_test_run()
	a := counter(555)
	gc_test_run()
	log a()
	gc_test_run()
	log a()
	gc_test_run()

output
	556 557

code closure-getter-setter closure
fun env a {
	b := a
	fun set a {
		b = a
	}
	fun get {
		ret b
	}
	ret [get set]
}

a := env(100)
log a[0]()
a[1](200)
log a[0]()

output
100 200

code closure-var-params closure

fun env a b {
	c := a + b
	d := 400
	log a b c d
	fun closure {
		inc a
		inc b
		inc c
		inc d
		log a b c d
		ret [a, b, c, d]
	}
	ret closure
}

a := env(100, 200)
log a()
log a()

output
100 200 300 400
101 201 301 401
[101, 201, 301, 401]
102 202 302 402
[102, 202, 302, 402]

code closure-nested closure
fun A {
	a := 100
	fun B {
		b := 200
		fun C {
			c := 300
			log a b c
		}
		ret C
	}
	ret B
}

A()()()
output
100 200 300

code closure-google-1 closure
fun f a b {
	c := a + 2
	d := b + 2
	fun fu {
		log a c
		ret a + c
	}
	ret fu
}

log f(1, 2)()
output
1 3 4

code closure-google-2 closure
fun f a b {
  c := a + 2
  d := b + 2
  fun fu {
    log a b c d
    ret a + b + c + d
  };
  ret fu
}

log f(1, 2)()
output
1 2 3 4 10

code closure-google-3 closure
fun f a b {
   c := a + 2
   d := b + 2
   fun fu {
      log a b c d fu
      if fu != nil {
         ret a + b + c + d
      }
   }
   ret fu
}
log f(1, 2)()

output
1 2 3 4 fun 10

code closure-google-4 closure
some_global := nil

fun f a b {
	fun fu a b {
		c := 3
		d := 4
		fun r1 {
			c := 3
			d := 4
			log a fu // 1, fun
			some_global = a
			ret fu
		}
		ret r1
	}
	ret fu(a, b)
}

f(1, 2)();

output
1 fun

code fun-local-names closure
fun A {
	fun F {
		log 'a'
	}
	F()
	ret F
}

fun B {
	fun F {
		log 'b'
	}
	F()
	ret F
}

A()()
B()()
output
a a b b

code funvar-in-expr closure

fun fu{} if fu!=nil log 1
output
1

code forward-funcs closure

fu6()

fun fu6 {
	fu4() // empty body func call
	fu()()()
}

fun fu {
	log 'fu'
	fu2()
	ret fu3
	fun fu3 {
		log 'fu3'
		f := fu5
		ret f
	}
}

fun fu2 {
	log 'fu2'
}

fun fu4 {
}

fun fu5 {
	log 'fu5'
}
output
fu
fu2
fu3
fu5


code closure-global-var closure
m := 1
fun A {
	a := 2
	fun B {
		b := 3
		fun C {
			c := 4
			m = 5
			a = 6
			b = 7
			c = 8
			are c
		}
		C()
		are a b
	}
	are a
	ret B
}

A()()
log m
output
a 2
c 8
a 6  b 7
5

code closure-propagate-env-to-callers closure
a := 0 b := 10
fun A {
	inc a
	log a
	if a < 2 B()
}

fun B {
	inc b
	log b
	if b < 12 A()
}

A()
log a b
output
1 11 2 2 11

code closure-make-caller-varfun closure
d := 3
fun A {
	inc d
	fun C {
		inc d
		log d
	}
	C()
}

fun B {
	d := 10
	A()
}

B()
output
5

code closures-complex-func-tree closure

n := 2
fun D {
   inc n
   if n > 3 ret
   log 'D' n
   d := 3 e := 4
   fun A {
      log 'A'
      a := 5
      fun C {
         log 'C'
         c := 6
         log d
         ret D
      }
      C()()
      D()
   }
   fun B {
      log 'B'
      b := 7
      ret A
   }
   B()()
}
D()
output
D 3 B A C 3

code closure-ignore-owner-var closure
a := 1 // not used but caused crash in older versions
fun A a {
	fun C {
		inc a
	}
	C()
	log a
}

A(444)
output
445

code closure-more-scopes closure

f := nil
fun A {
	a := 10
	fun B {
		// no a here
		fun C {
			a := 30
			fun D {
				a := 40
				f = B
				log 'D' a
				inc a
			}
			D()
			log 'C' a
			inc a
      }
      C()
      log 'B' a
      inc a
   }
   B()
   log 'A' a
   inc a
}
A()
log;
f()
output

D 40
C 30
B 10
A 11

D 40
C 30
B 12

code closure-forward closure
d := 1
fun B {
	d = d + 2
	ret D()
}
fun D {
	log d
}

B()

output
3

code closure-nested-main closure

d := 1
fun D {
	fun A b {
		a := b + d
		inc a
		fun C {
			inc a
			ret a
		}
		ret C
	}
	ret A
}
fun B {
	ret D()
}

c := D()(100)
log c()
log c()

d = 100
b := B()(200)
log b()

d = 1000
e := B()(300)
log e()

output
103
104
302
1302

code closures-direct-call-bug closure
a := 1
fun A a {
	fun B {
		log 'ok'
		ret A
	}
	ret B
}
A(a)() // this has a bug
output
ok

code implicit-environment closure
d := 5
fun D {
	log d
}
fun C {
	d := 3
	fun A {
		log d
	}
	fun B {
		d := 10
		A()
		D()
	}
	B()
}
C()

output
3 5

code lvalue basic
i := 1; i += 2; log i
output
3

code lvalue-index index assign array value
a := [1]; a[0] += 2; log a
output
[3]

code string-basic string basic
a := "hello" b := "world"
c := a + " " + b
log c
output
hello world

code indexing-pre-space index array lexer error
a := [1 2 3]
x := a [2]

output error
.test.i: 2
   x := a [2]
          ^ E002_0

code lvalue-var-var basic value
b := 0; a := [1]; a[b] += 2; log a
output
[3]

code lvalue-var-str-var value
b := "1"; a := [1,2,3]; a[b] += 2; log a
output
E004

//code bit-index
	//a := 0
	//a[0] += 1
	//are a
	//a[5] = 42
	//are a
	//b := 1
	//b[0] -= 1
	//are b b[0]-1
	//b[3] -= 1
	//are b
	//b[16] = 2
	//are b
//output
	//a 1
	//a 33
	//b 0  b[0]-1 -1
	//b 8
	//b 65544

code int-plus-str string value
	ii := 100
	ii += 'asd'
	log ii

output
	100asd


code opt-comma-list syntax array

log []
log [1]
log [, 1]
log [1, ]
log [1,,2]
log [1,2,3]
log [1 2 3]
log [1,,]
log [,,1]
log [,,]

output

[]
[1]
[nil, 1]
[1, nil]
[1, nil, 2]
[1, 2, 3]
[1, 2, 3]
[1, nil, nil]
[nil, nil, 1]
[nil, nil, nil]

code set-item-of-nil array value
	a:=[1,[1]]; a[2][10] = 555; log a

output
	E008 nil

code plus-push array
	a := [1,2,3]
	a = a + 4
	log a

output
	[1,2,3,4]

code plus-assign-push value array
	a := [1,2,3]
	a += 4
	log a

output
	[1,2,3,4]

code plus-assign-item-push array value
	a := [[1]]; a[0] += 2; log a
	a += [[3]]
	log a
	a += [[[[4]]]]
	log a

output
	[[1, 2]]
	[[1, 2],[3]]
	[[1, 2],[3],[[[4]]]]


code compare expr compare
	log 1 == 1 1 == 2 1 < 2 2 < 1 1 > 2 2 > 1 1 <= 2 2 <= 1 1 >= 2 2 >= 1 1 != 2, 2 != 2
	a := 1
	b := 2
	log a == a a == b a < b b < a a > b b > a a <= b b <= a a >= b b >= a a != b b != b
	log 'a'=='b' 'a'=='a' 'a'<'b' 'a'>'b' 'b'<'a' 'b'>'a' 'a'>='b' 'b'>='a' 'a'<='b' 'b'<='a' 'a'!='b' 'a'!='a'
	log 'abc'<'z' 'abc'<='z' 'a'>='zxy'
	c := {}
	log c == 1, c != 1

output
	1 0 1 0 0 1 1 0 0 1 1 0
	1 0 1 0 0 1 1 0 0 1 1 0
	0 1 1 0 0 1 0 1 1 0 1 0
	1 1 0
	0 1

code compare-nil compare expr
	log nil<nil nil==nil nil>nil nil>=nil nil<=nil
	a := 1; log a == nil, a != nil

output
	0 1 0 1 1
	0 1

code bitwise bits expr
	log 1 SHL 7
	log 128 SHR 2
	a := 1; b := 7; log a SHL b
	a = 128; b = 1; log a SHR b

output
	128 32 128 64

code ranges array range
	a := [0,1,2,3,4,5,6,7,8,9,10]
	log a[-1] a[-2]
	log a[5]
	log a[2:2]
	log a[2..5]
	log a[3..]
	log a[..2]
	log a[..-5]
	log a[-2..-1]
	log a[0:-1]
	a = 'ABC'
	log a[..-2] a[-2..-1] a[-3:2]
	a = '01234'
	log a[2:-2]
	log [1,2,3][-4..]

output
	10 9
	5
	[2, 3]
	[2, 3, 4, 5]
	[3, 4, 5, 6, 7, 8, 9, 10]
	[0, 1, 2]
	[0,1,2,3,4,5,6]
	[9,10][0]
	AB BC AB 12
	[1,2,3]


code concat array
	a := [0,1,2,3]
	b := [5,6,7]
	log a + b
	log a + [1]
	log [1] + b
	a += 55
	log a
	a += b
	log a b
	put(a 555)
	put(a 333)
	log a

output
	[0, 1, 2, 3, 5, 6, 7]
	[0, 1, 2, 3, 1]
	[1, 5, 6, 7]
	[0, 1, 2, 3, 55]
	[0, 1, 2, 3, 55, 5, 6, 7] [5, 6, 7]
	[333, 555, 0, 1, 2, 3, 55, 5, 6, 7]

code inc-dec value basic
	a := 200
	i := 100
	b := [0,1,2,3]
	inc i
	inc a
	inc b[2]
	log i a b
	dec i
	dec a
	dec b[3]
	log i a b

output
	101 201 [0, 1, 3, 3]
	100 200 [0, 1, 3, 2]

code each-item-assign array each loop
	a := [1,2,3] each a item += 1; log a

output
	[2, 3, 4]

code each-2 array each loop
	a := [1,2,3]; b := [a,a,a] each b[1] item += 1; log b

output
	[[#0 2, 3, 4], *0, *0]

code each-item array each loop
	each [10,20,30] log index item

output
	0 10
	1 20
	2 30

code chr string
	a := 66
	log chr 65 chr a
	log chr 65 + chr 66 + chr 67

output
	A B
	ABC

code str-index string
	S := 'ABCD'
	are charcode_at(S 0) charcode_at(S)
	are S[1] S[-1] S[99]
	are chr 67 ord 'Z'

output
	charcode_at(S 0) 65  charcode_at(S) 65
	S[1] B S[-1] D S[99] nil
	chr 67 C  ord Z 90

code str-range string range
	a := 'abcdefg' log a[1..3]
output
	bcd

code each-str-item-err string error
a:='qwe'; each a item = 1

output
E022

code each-len-item-str-err string each loop
a:='qwe'
each a log len item
output
1 1 1


code trim string
log len ("<"+trim("   5   ")+">")
output
3

code builtin-ref-error error builtin
log pull
output
.test.i: 1
	log pull
		^ E004_1 pull

code push-panic error array
b := 1
push(b 555)
output
E012

code push array
a:=[111,222,333]
log a[push(a 555)] a
output
555 [111, 222, 333, 555]

code put array
a:=[2]; put(a 1); log a
output
[1, 2]

code put-panic array error
a:=[]; put(1 a); log a
output
E013

code pop array
a:=[1,2,3]; log pop(a) a
output
3 [1, 2]

code pop-panic array error
b := 1; pop(b)
output
E014

code pull array
a:=[1 2 3]
log pull(a) a
log pull(a) a
log pull(a) a

output
1 [2, 3]
2 [3]
3 []

code pull-panic array error
a:=1; log pull(a)
output
E015

code find-in-str string find
a := "---abc---123--abc"
log find(a 'abc')
log find(a 'abc' 5)
log find(a 'qwe')
log find(a 123)
output
3
14
-1
E016 number

code find-in-arr array find
b := [10 20 30 40 50 30]
log find(b 30)
log find(b 30 4)
log find(b 'asd')
log find(b 99)
log find(b nil)
output
2
5
-1
-1
-1

code find-in-nil find
log find(nil 30)
output
E017 nil

code split array string
a := "a++b++++"
log a / "+"
a = "a+b"
log a / "+"
a = "ab"
log a / "+"
output
["a", "", "b", "", "", "", ""]
["a", "b"]
["ab"]


code join array string
log [1,2,,'aaa'] * '+'
log [1,2,'bbb',' ',] * '+'
log [1,2,,'ccc'] * ''
output
1+2++aaa
1+2+bbb+ +
12ccc

code join-panic array error
log [1,2,,'qqq'] * nil
output
E020 nil

code init
fun fu a {init a { a=5 }; log a}; fu(nil); fu(111)
output
5
111

code init-eq-sugar init
fun f a {
	init a = 'default'
	log a
}
f(nil) f(5)
output
default
5


code cont-each array each loop
	a:=[1,2,3,4,5,6,7]; each a { if item != 5 { log "ok" item; cont}  log item  }
output
	ok 1
	ok 2
	ok 3
	ok 4
	5
	ok 6
	ok 7

code are log
	a:=1; b:='ABC'; are a b[1]
output
	a 1  b[1] B

code is expr
	i:=1; a:=[]; log not is_string(a)
	log is_string(a)
	a = ''
	log is_string(a) is_array(a)
output
	1 0 1 0

code math builtin
	a := 25./4
	log a round(a) floor(a) ceil(a)
	L := [1 1.1 1.4 1.5 1.6 1.9 2.0]
	each L {
		log item round(item) floor(item) ceil(item)
	}
	log abs(5) abs(0) abs(-5)
	// TODO: trunc abs sin cos cosh exp tan tanh sinh log log10 sqrt pow

output
	6.25 6 6 7
	1 1 1 1
	1.1 1 1 2
	1.4 1 1 2
	1.5 2 1 2
	1.6 2 1 2
	1.9 2 1 2
	2 2 2 2
	5 0 5

code lexical closure
	a := 1
	b := nil
	b = 2
	fun fu {
		a := 3
		are a b
		b = 5
	}
	fu()
	are a b
	// lexical scope for arguments
	fun A a {
		fun B {
			log a + 1
		}
		fun C {
			inc a
		}
		C()
		B()
	}

	A(444)

output
	a 3  b 2
	a 1  b 5
	446

code arg-order arg

	fun f1 a b c {
		d := 777
		e := 888
		g := 999
		are a b c d e g
	}
	fun f2 a b c {
		d := 444
		e := 555
		g := 666
		are a b c d e g
		f1(d, e, g)
	}
	f2(111, 222, 333)

output
	a 111 b 222 c 333 d 444 e 555 g 666
	a 444 b 555 c 666 d 777 e 888 g 999

code literal-initial-value value
	log [0,1,2,3,4][1:2]
output
	[1,2]

code arg-order-2 arg

count := 555
ii := 500

fun var_func a b var {
	c := 333
	d := 444
	log a b c d count
	dec count
	are count
}

fun int_func a b int {
	c := 300
	d := 400
	log a b c d ii
	dec ii
	are ii
}

var_func(111, 222)
count = 500
int_func(100, 200)

output
111 222 333 444 555
count 554
100 200 300 400 500
ii 499


code lexical-lvalue closure value
fun f1 {
	a = 500
	log a
	inc a
	log a
	a += 2
	log a
}
a := 400
f1()
log 'final' a
output
500 501 503 final 503

code args-index-count arg
fun f1 a b {
	log args[-1] args[0] args[1] args[2]
	log len args
}
f1(100, 200)
a := f1
a(10, 20, 30)
a(10)

output
nil 100 200 nil
2
nil 10 20 30
3
nil 10 nil nil
1
// TODO: should len in last one be 1? how to achieve that? we are pushing extra nils on the stack...


code return-leave-code basic return
b := 'hello'

fun int_plain_int {
	ret 300
}

fun int_leave_int {
	a := 100
	ret 200
}

fun int_plain_var_expr {
	b := 'hello'
	ret len b
}

fun int_leave_var_expr {
	b := 'world!'
	ret len b
}

fun var_plain_int {
	ret 300
}

fun var_leave_int {
	a := 100
	ret 200
}

fun var_plain_var_expr {
	ret len b
}

fun var_leave_var_expr {
	b := 'world!'
	ret len b
}

log int_plain_int()
log int_leave_int()
log int_plain_var_expr()
log int_leave_var_expr()

log var_plain_int()
log var_leave_int()
log var_plain_var_expr()
log var_leave_var_expr()

output
300
200
5
6
300
200
5
6

code add-nil-args arg
fun f1 {
   log 'hi'
}
fun f2 a b {
   log 'hi' a b
}
a := f1
a()
a = f2
a(5, 6)
a(100)
a()

output
hi
hi 5 6
hi 100 nil
hi nil nil

code if-index if each loop
a := [0 1 2 3]

each a {
	if index > 1 log index
}
output
2 3

code find-in-array array find

a := ['qwe','asd','zxc'] log find(a 'asd'), find(a []) find(a 'iop') find(a 100)
a = [0,1,2,3,4,5] log find(a 3) find(a 30)

output
1 -1 -1 -1
3 -1

code args-as-array arg array

fun _f {
	are args[1..] // implicit convertion to array
	are args // convert to array
	are args[2] args[1] args[0] // quick indexed access
	are len args // quick access to the count
	a := args // convert to array
	are a[1..] // implicit convert to array
	log [args][0][1..] // trick to convert to array
	are args[1:-2]
	are args[..1]
	log args + 1
}

f := _f

f(0, 1, 2)

output
args[1..] [1, 2]
args [0, 1, 2]
args[2] 2 args[1] 1 args[0] 0
len args 3
a[1..] [1, 2]
[1, 2]
args[1:-2] [0, 1]
args[..1] [0, 1]
[0, 1, 2, 1]

code array-beyond-end array nil
a := []
a[2] = 'ok'
are a
output
a [nil, nil, 'ok']


code mix-arg-type arg
a := 5 b := 6 c := 7 d := 8

fun f_a a { log a }
f_a(1)

fun f_i i { log i }
f_i(2)

fun f_ai a i { log a i }
f_ai(3, 4)

f_i(a)
f_a(a)

fun f_aibj a i b j { log a i b j }
f_aibj(1,2,3,4)
f_aibj(a,b,c,d)
output
1
2
3 4
5
5
1 2 3 4
5 6 7 8

code utf-strings string

a := 'привет'
log len a, "'"+a+"'"
b := a + a
log len b, "'"+b+"'"
b = 'привет мир'
c := b / ' '
log c
a = c * '_'
log "'"+a+"'"

output
6 'привет'
12 'приветпривет'
["привет", "мир"]
'привет_мир'

code lexical-arg closure
fun parent a {
	b := 7
	fun child {
		log a b
	}
	child()
}

parent(5)
output
5 7

code init-use init if
b := nil
fun F a {
   use a { log 'a was already set' }
   use b { log 'b was already set' }
   init a { a = 555 }
   init b { b = 777 }
   use a { log 'a is now set' }
   use b { log 'b is now set' }
   log a b
}
F(nil)
log '-----------'
F(123)

a := [0,1,2,[4,5,6]]
use a[3][1] { log 'use' }
init a[3][1] { log 'never' }
//init a[4][1] { log 'use' a[5][5] }

output
a is now set
b is now set
555 777
-----------
a was already set
b was already set
a is now set
b is now set
123 777
use
//use nil

code function-reference closure
fun A {
	fun B {
		log "B!"
	}
	fun C {
		ret B
	}
	ret C
}

A()()()
output

B!

code default-return-nil return
fun fu {
	if false ret 100
}
log fu()
output
nil

code lambda-minimal closure
f := lam { log 'ok' }
f()
output
ok

code lambda-calls-parent closure
a := 1
fun A a {
	ret lam {
		log 'ok' a
		ret A
	}
}
A(a)()
output
ok 1


code lambda-with-arg closure arg
f := lam a {
	log a * a
}

f(5)
output
25

code lambda-in-closure closure

name e var

d := 1
fun D {
	ret lam b {
		a := b + d
		inc a
		ret lam {
			inc a
			ret a
		}
	}
}

c := D()(100)
log c() c()

output
103 104

code lambda-in-array closure array
a := [
	lam { cout 1 }
	lam { cout 2 }
	lam { cout 3 }
]

a += a[0]

c := 0
while c < 3 {
	a += lam { cout c }
	inc c
}

c = 0
while c < 3 {
	a += (lam d { ret lam { cout d } })(c)
	inc c
}

each a {
	item()
}

output
1231333012

code lambda-eq closure
a := lam b c = 55
log a()
output
55

code item-multiply-eq each loop array value
a:=[2,2,2,2,2] each a { item *= index } log a
output
[0, 2, 4, 6, 8]

code complex-each-item each loop
a := [[1],[2]]
each a {
   log item[0]
}
output
1 2

code complex-each-item-error each loop
a := 'abc'
each a {
   log item[0]
}

output
a b c

code each-over-each each loop
each [[1 2 3],[4 5 6],[7 8 9]] {
	each item {
		log item
	}
}
output
1 2 3 4 5 6 7 8 9

code each-over-string each loop string
a:='123' each a log item
output
1 2 3

code kill basic
log 1 kill log 2
output
1

code is-detailed expr
a b c d f := nil
fun A {}
f = A
a = 1
b = []
c = 'a' + 'b'

cout is_number([])
cout is_number(f)
cout is_number(a)
cout is_number('qwe')
cout is_number(lam {})
cout is_number(nil)
cout is_number(d)
cout is_number([][0])
log

cout is_fun([])
cout is_fun(f)
cout is_fun(a)
cout is_fun('qwe')
cout is_fun(lam {})
cout is_fun(nil)
cout is_fun(d)
cout is_fun([][0])
log

cout is_array([])
cout is_array(f)
cout is_array(a)
cout is_array('qwe')
cout is_array(lam {})
cout is_array(b)
cout is_array(nil)
cout is_array(d)
cout is_array([][0])
log

cout is_string([])
cout is_string(f)
cout is_string(a)
cout is_string('qwe')
cout is_string(lam {})
cout is_string(c)
cout is_string(nil)
cout is_string(d)
cout is_string([][0])
log

cout is_nil([])
cout is_nil(f)
cout is_nil(a)
cout is_nil('qwe')
cout is_nil(lam {})
cout is_nil(c)
cout is_nil(nil)
cout is_nil(d)
cout is_nil([][0])
log

fun B {
	a := 'hello'
	log is_string(a), is_array(a)
	ret lam {
		log is_string(a), is_array(a)
	}
}

B()()
output
00100000
01001000
100001000
000101000
000000111
1 0
1 0

code try-catch-basic try
enable_trace(true)
fun sum a b {
	if is_number(a) and is_number(b) {
		ret a + b
	}
	throw { msg: 'cannot sum non numbers' stack: stack_trace() }
}

log sum(2, 3)

try {
	sum(4, 'a')
}
catch {
	log 'msg:' error.msg
	log 'stack:'
	each error.stack log item
	log 'et:'
	log error_trace() * '\n'
	log 'st:'
	log stack_trace() * '\n'
}

output
5
msg: cannot sum non numbers
stack:
  |.test.i:12  sum(4, 'a')
  |.test.i:6   throw { msg: 'cannot sum non numbers' stack: stack_trace() }
et:
  |.test.i:12  sum(4, 'a')
  |.test.i:6   throw { msg: 'cannot sum non numbers' stack: stack_trace() }
st:
  |.test.i:21  log stack_trace() * '\n'



code object-minimal object
c := {
	a:'привет'
	b: 2 + 2
	d: [1,2,3]
}
log c

output
{ a: "привет", b: 4, d:[1,2,3] }

code object-set-member object
a := {
	b: {
		c: 100
		d: 200
	}
}
log a.b.c
log a.b.d
a.b.c = 300
log a
log a.x
output
100
200
{ b: { c: 300, d: 200 } }
nil

code object-add-property object
a := { b: 100 }
a.c = 200
log a
b := a.c  // force normalization of properties array
log a
output
{ b: 100, c: 200 }
{ b: 100, c: 200 }


code object-ic object
disabled
//option: debug_IC
fun prn a {
   log a.c
}
fun up a {
   inc a.c
}
name n int
a := { c: 1 }
prn(a) // GET Hash 1
prn(a) // GET IC 1
up(a) // SET Hash
up(a) // SET IC
a.a = 55 // spoils IC by changing hidden type
prn(a) // GET Hash 3
prn(a) // GET Hash 3
up(a) // SET Hash
log a

output
GET Hash 1
GET IC 1
SET Hash
SET IC
SET Hash
GET Hash 3
GET Hash 3
SET Hash
{ a: 55, c: 4 }


code shorthand-property object
fun mk b { ret { b } }
log mk(123)
output
{ b: 123 }

code object-each-string object each loop string
a := {}
each 'abc' {
   a[item] = 1
   a[ord item] = 1
}
log a
output
{ a: 1, b: 1, c: 1, "97": 1, "98": 1, "99": 1 }

code value-in-each value each loop
a := []
each [1] {
	a[0] = 1
}
log a
output
[1]

code assignment-operators basic
a := 10
a -= 1
log a
a *= 5
log a
a /= 2
log a
a += 5
log a
output
9
45
22.5
27.5

code float-literals basic
a := 0.5
log a
output
0.5

code use-item init
each [0, 1, 2] { use item { log item } }
output
0 1 2

code member-call object
a := { f:nil, b: lam { log 'world' } } a.f = lam { cout 'hello ' }; a.f() a.b()
output
hello world

code sort-basic builtin sort

a := [0 4 5 1 7 4 2 5 8 5]
sort(a)
log a
a = ['a', 'z', 'y', 'u', '1', 'k', 'a', 'a', 'k', 'k', 'kk']
sort(a)
log a
a = ['z', 1,'a']
sort(a, lam a b {
   ret a+'' > b+''
})
log a
a = [5,1,6,3,2]
sort(a, lam a b {
   ret a < b
})
log a
output
[0, 1, 2, 4, 4, 5, 5, 5, 7, 8]
["1", "a", "a", "a", "k", "k", "k", "kk", "u", "y", "z"]
[1, "a", "z"]
[6, 5, 3, 2, 1]


code incl-basic incl
file lib
log 'log-lib'

fun sub {
	log 'lib-sub'
}

file main
log 'log-main'
incl lib
sub()
log 'main-end'
output
log-main
log-lib
lib-sub
main-end

code incl-error-lib incl
file lib
log 'log-lib' some_id

file main
log 'log-main'
incl lib
output
lib.i: 1
  log 'log-lib' some_id
               ^E011_1 some_id

code try-scope-scan try
try {
	a := 1
	throw 'boom'
}
catch {
	b := 2
}

log a b
output
1 2

code fs-load-error builtin
try {
   a := load('non/exiting/file')
   log len a
}
catch {
   log error
}

output
load("non/exiting/file")
No such file or directory

code fs-basic builtin
try {
   save('/tmp/fs-basic.tmp' '12345678')
   a := load('/tmp/fs-basic.tmp')
   log a
}
catch {
   log error
}
output
	12345678

code each-item-nested each loop
	each [[1 2] [3 4] [5 6]] each item {
		log index_for 1 item_for 1 index item
	}

	a := [[1 2] [3 4] [5 6]]

	each a each item {
		item_for 1[0] = 'x'
	}

	log a

	b := [[1 2] [3 4] [5 6]]

	each b each item {
		item_for 1 = '-'
	}

	log b

output
	0 [1,2] 0 1
	0 [1,2] 1 2
	1 [3,4] 0 3
	1 [3,4] 1 4
	2 [5,6] 0 5
	2 [5,6] 1 6
	[["x",2],["x",4],["x",6]]
	["-","-","-"]

code enum-basic enum
enum e { 'a' b }
log e.b e.a len e
a := 1
log e[0] e[a]
output
1 0 2
a b

code enum-bounds enum
enum e { a }
log e[100]
output
E023 100

code define-enum-conflict enum
e := 1
enum e { a b }
log e.b e.a e

output
.test.i: 2
enum e { a b }
      ^E005_1 e

code enum-define-conflict enum
enum e { a b }
log e.b e.a e
e := 1

output
.test.i: 3
  e := 1
  ^E006_1 e


code ord string
a := '0' log a ord a ord 'AZ'
output
0 48 65

code each-index-assign each loop value
disabled
a:=[0,1,2,3,4,5]
each a {
	log item
	if index == 2
		index = 4
}

output
0 1 2 5

code init-use-else init if
b := 1
a:=nil
init b { log 1 } else {log 2}
init a log 1 else {log 2}
use a { log 1 } else log 2
use b log 1 else log 2
output
2 1 2 1

code charmap-basic charmap
a := 'a+b+=c++d--'

each [1 3 6 9 1 9 0] {
	log charmap a item {
		'+': 'plus'
		'+=': 'add to'
		'++': 'increment'
		'--': 'decrement'
	}
}

output

plus
add to
increment
decrement
plus
decrement
nil

code item-in-charmap charmap
a := 'a+b+=c++d--'
each ' ' {
	log charmap a ord item - 31 {
		'+': 'plus'
		'+=': 'add to'
		'++': 'increment'
		'--': 'decrement'
	}
}
output
plus

code cmp-norm-string-and-pair expr
log 'a'+'a' == 'aa'
output
1


code charmap-miss-found-match-bounds charmap
a := 'a+b+=c++d--'
each [3 0 6 1 9, 100] {
	cout 'at ' item ': '
	f := charmap a item {
		'--': 'minus '
		'+' '+=' '++':
		'something'
	}
	init f = 'nothing'
	log f
}
output
at 3: something
at 0: nothing
at 6: something
at 1: something
at 9: minus
at 100: nothing

code map-reduce program
fun filter a f {
   R := []
   each a
      if f(item) R += item
   ret R
}
fun map a f {
   R := []
   each a R += f(item)
   ret R
}
fun reduce a f {
   if len a == 0 ret []
   if len a == 1 ret [a[0]]
   b := a[0]
   R := [b]
   each a {
      if index == 0 cont
      b = f(b, item)
      R += b
   }
   ret b
}
fun sum_ab a b {
   ret a + b
}
a := [0,1,2,3,4,5,6]
log filter(a, lam a { ret a > 3 } )
log map(a, lam a { ret a * 100 })
log reduce(a, lam a b { ret a + b })
output
[4, 5, 6]
[0, 100, 200, 300, 400, 500, 600]
21

code item-assign-member-index each loop value
a := [{type:1}]
each a { item.type = 2 }
log a
b := [[1]]
each b { item[0] = 2 }
log b
output
[{ type: 2 }]
[[2]]

code split-join-empty string array
s := 'a+b+c'
s = s / '+'
s = s * ''
log s

output
abc

code charmap-enum charmap enum
enum types { int var string double }

i := 0
s := 'double'
f := charmap s i {
	types
}

if f log 'found' f f == types.double

output
found 3 1

code concat-empty-strings string
	a := ''
	a += ''
	log a
	log 1
output
	1

code assign-to-int-param value arg
fun f1 i { i += 1; log i } f1(3)
output
4

code enum-with-str-and-keyword enum
disabled
	enum e { plus 'enum' a b }
	log e e.plus e.enum e.a e.b
output
	4 0 1 2 3

code enum-by-index enum
	enum e { '+' '-' z '>=' }
	log e['+'] e['-'] e['+'] e['>='] e['z']
output
	0 1 0 3 2

code enum-reflex-many enum
	enum e { q }
	enum d { w:1 }
	log e.q e[e.q]
	log d.w
	log d[d.w]
	log d[99]
output
	0 q
	1
	w
	E023 99

code enum-expr enum
	enum a { a b }
	enum b { x y }
	enum c = a + b
	are a.a == a.b
	are a.a == b.x
	are a.a == c.a
	are b.x == c.x

output
	a.a == a.b 0
	a.a == b.x 1
	a.a == c.a 1
	b.x == c.x 0

code enum-expr-error enum error
	enum x { a b }
	enum y { a }
	enum z = x + y
output
	.test.i: 3
	  enum z = x + y
				  ^E007_3 x y a

code enum-reflex-complex enum
	enum a { b c d }
	enum b { m n o }
	enum c { x y z }
	enum d = a + c

	are d[d.b] d[d.z]
output
	d[d.b] b d[d.z] z

code item-member-on-object array object
a := [ {} {} {} ]

each a {
	log item.type
}
output
nil nil nil

code enum-visibility enum
enum e { a }

fun A {
	fun B {
		enum e { b }
		log 'B:' e.a e.b
	}
	B()
	log 'A:' e.a e.b
}

A()

output
B: 0 1
A: 0 1

code charmap-error-mix charmap
enum e { a b c }

log charmap 'abc' i {
	e
	'c': 'C found'
}


output
.test.i: 5
  'c': 'C found'
  ^keyword or enum expected

code find-at find string
a := 'a + b + c + d'
i := 0
loop {
	i = find(a, '+', i)
	if i < 0 break
	log i
	inc i
}
output
2 6 10

code parse-int-float builtin
	log parse_num('123', 10)
	log parse_num('1.23')
	log parse_num('-123', 10)
	log parse_num('-1.23')
	log parse_num("20") + 1000
	log parse_num("20", 10) + 1000
	log parse_num("20", 16) + 1000
	log parse_num("1.23") + 1000
	log parse_num('101', 2)
output
	123
	1.23
	-123
	-1.23
	1020
	1020
	1032
	1001.23
	5

code parse-neg-to-var builtin
a := parse_num('-123', 10)
log a
output
-123


code charmap-char-escape charmap
enum e { '#\'' }
x := charmap '#\'' { e }
log if x != nil 'ok' else 'not'
output
ok

code shame-circular-object shame
b := {}
a := {b}
b.a = a

d := {}
c := {d}
d.c = c

b.d = d
log shame(a)
output
{#1 b:{a:*1,d:{#0 c:{d:*0}}}}

code shame-circular-array shame
a := [1,2,3]
a += [a]
log shame(a)
output
[#0 1, 2, 3, *0]


code shame-parse-array shame
a := '[#0 0, 1, 2, *0]'
h := honor(a)
log len h, h, h[0], h[3] h[3][1:2]

output
4 [#0 0, 1, 2, *0] 0 [#0 0, 1, 2, *0] [1, 2]

code shame-parse-basic-object shame
s := '{x:{c:{}}}'
log honor(s)
output
{x:{c:{}}}

code shame-parse-circular-object shame
s := '{#1 b: { d: {#0 c: { d: *0 } }, a: *1 } }'
log honor(s)
output
{#0 b: { a: *0, d: {#1 c: { d: *1 } } } }

code shame-parse-full shame
s := '{#1 b: { d: {#0 c: { d: *0, e: [1,2,3,*1] } }, a: *1 } x:5 }'
o := honor(s)
log o
log o.b.d.c.d.c.e[3].b.a.x
log honor('[#1 "string"#0, *0, *0, *1]')
log shame({a:'123'+'456'+'789'})
output
{#0 b:{a:*0,d:{#1 c:{d:*1,e:[1,2,3,*0]}}},x:5}
5
[#1 "string","string","string",*1]
{a:"123456789"}



code incl-error-in-lam incl
file lib
log 'log-lib'

fun q a {
}

sub := lam {
   log 'lib-sub'
   q()
}
file main
incl 'lib'

output
lib.i: 8
q()
 ^
argument count mismatch: q(a)

code incl-error-in-fun incl
file lib
	log 'log-lib'

	fun w {
		log 'lib-sub'
		w(1)
	}
file main
	incl 'lib'

output
	lib.i: 5
	  w(1)
		^argument count mismatch: w()

code set-or-get object range
disabled
a := {}
j := a['b':1]
log j
j = a['b':2]
log j
j = a['x']
log j
j = a['x':3]
log j
j = a['x':4]
log a
output
1 1 nil 3
{ b: 1, x: 3 }

code and-optimization expr
a := 100
fun modify_a {
	inc a
	ret 0
}
if modify_a() and modify_a() {
	log 'true'
}
log a
output
101

code try-catch-deep try
fun a {
	fun b {
		fun c {
			fun d {
				throw 555
			}
			d()
		}
		c()
	}
	b()
}

try a()
catch log 'caught' error

output
caught 555


code throw_src try
disabled
file main
a := 1
try {
	log a.b "\n"
}
catch {
	log error.line
	log error.msg
}
output
nil
__test__:3
   log a.b "\n"
         ^ not an object


code if-with-nil if
a=[]; if a log 1
a=nil; if a log 2
a={}; if a log 3
a=6; if a log 4
a:=0; if a log 5
output
1 3 4

code throw-error try
disabled
try {
   throw { msg: 'hello' }
}
catch {
   log (error).line
   log (error).msg
}
throw { hello: 'world' }
output
__test__: 2
   throw { msg: 'hello' }
   ^
hello
panic, unhandled runtime exception:
__test__: 8
throw { hello: 'world' }
^

code ret-from-each ret each loop
fun x {
	b := [2]
	a := [1]
	each a {
		each b {
			ret 'expr-stack-empty'
		}
	}
}

log x()
output
expr-stack-empty

code load-not-found builtin fs
try {
	z := load('@#@#$')
}
catch {
	log error
}
q := load('@#@#$')
output
load("@#@#$")
No such file or directory
load("@#@#$")
No such file or directory

code float-in-array-object-literal array object num
a := [1.32]
b := {c: 1.45}
log a b
output
[1.3200000000000001] { c: 1.45 }


code object-property-count object
a := {b:1, c:2, d:3}
log len a
output
3

code errors-in-honor shame
try {
	log honor('[]')
}
catch {
	log error
}
try {
	log honor('[ ]')
}
catch {
	log error
}
try {
	log honor('[}')
}
catch {
	log error
}
try {
	log honor('{]')
}
catch {
	log error
}

output
[]
[]
parse shame: unexpected token
parse shame: property name expected

code to-bool expr
a := { b: [] }
log 1 and a.b
log 1 or a.b
log 1 and a.c
log 1 or a.c
output
1 1 0 1

code lex_str-edge-cases lexer
fun q s {
	t := []
	each s t += ord item
	t = t * '-'
	log '"' + t + '"'
}
a := 'A\\nB'
b := 'A\nB'

c := 'A\\tB'
d := 'A\tB'

e := 'A\'B'
q(a) q(b) q(c) q(d) q(e)
log '\33;0\65\0x42\x43\x44;\\'
output
"65-92-110-66"
"65-10-66"
"65-92-116-66"
"65-9-66"
"65-39-66"
!0ABCD\

code honor-nil-shame-filter shame
J := honor('{a:1,b:"c",d:{e:3},f:nil}')

fun filter k v {
	if k == 'a' {
		ret {x:100}
	}
	if k == 'b' {
		ret 200
	}
	ret v
}
log shame(J, {filter:filter})

output
{a:{x:100},b:200,d:{e:3},f:nil}

code shame-options shame
J := honor('{a:1,b:"c",d:{e:3},f:nil}')

log shame(J, {indent:10})
log shame(J, {indent:2, json_mode: true, condensed: false})
log shame(J, {condensed: true})
log shame(J, {condensed: false})
log shame(J, {indent: 3, condensed: true})
log shame(J, {json_mode: true})
log shame(J, {json_mode: false})
log shame({a:"a h", b:"b e"}, {quote_strings: false})
log shame({a:"a h", b:"b e"}, {quote_strings: true})
s := 'asd'
log shame({a:s, b:s, c:s, d:[s,1,s,2,s,3]}, {show_dup_strings: true})
log shame({a:s, b:s, c:s, d:[s,1,s,2,s,3]}, {show_dup_strings: false})
log shame({a:'qwe', b:'qwe', c:'qwe', d:['qwe',1,'qwe',2,'qwe',3]}, {show_dup_strings: true})
log shame({a:'qwe', b:'qwe', c:'qwe', d:['qwe',1,'qwe',2,'qwe',3]}, {show_dup_strings: false})
o := {a:1}
log shame({a:o, b:o, c:o, d:[o,1,o,2,o,3]}, {json_mode: true})
log shame({a:o, b:o, c:o, d:[o,1,o,2,o,3]}, {json_mode: false})

output
{
          a:1,b:"c",d:{
                    e:3
          },f:nil
}
{
   "a": 1, "b": "c", "d": {
     "e": 3
  }, "f": null
}
{a:1,b:"c",d:{e:3},f:nil}
{ a: 1, b: "c", d: { e: 3 }, f: nil }
{
   a:1,b:"c",d:{
      e:3
   },f:nil
}
{"a":1,"b":"c","d":{"e":3},"f":null}
{a:1,b:"c",d:{e:3},f:nil}
{a:a h,b:b e}
{a:"a h",b:"b e"}
{a:"asd",b:"asd",c:"asd",d:["asd",1,"asd",2,"asd",3]}
{a:"asd"#0,b:*0,c:*0,d:[*0,1,*0,2,*0,3]}
{a:"qwe",b:"qwe",c:"qwe",d:["qwe",1,"qwe",2,"qwe",3]}
{a:"qwe",b:"qwe",c:"qwe",d:["qwe",1,"qwe",2,"qwe",3]}
//{a:"qwe"#0,b:*0,c:*0,d:[*0,1,*0,2,*0,3]} // this will change if we implement string pool
{"a":{#0 "a":1},"b":{"a":1},"c":{"a":1},"d":[{"a":1},1,{"a":1},2,{"a":1},3]}
{a:{#0 a:1},b:*0,c:*0,d:[*0,1,*0,2,*0,3]}

code shame-filter_val_bare shame
J := honor('{a:1,b:"c",d:{e:3},f:nil,g:0}')

log shame(J, {condensed: true})
log shame(J, {filter, filter_val_bare:0})
log shame(J, {filter, filter_val_bare:1})

fun filter k v {
	if k == 'a' {
		ret {x:100}
	}
	if k == 'b' {
		ret 200
	}
	if k == 'g' {
		ret 'h'
	}
	ret v
}

output
{a:1,b:"c",d:{e:3},f:nil,g:0}
{a:{x:100},b:200,d:{e:3},f:nil,g:"h"}
{a:{x:100},b:200,d:{e:3},f:nil,g:h}

code ret-from-try ret try

fun q {
	try {
		ret
	}
	catch {
	}
}

log 'try count:' __try_stack()
q()
log 'try count:' __try_stack()

output
try count: 0
try count: 0


code keys-values
a := {b:1 c:2 x:3 y:4}
are keys(a)
are values(a)

output
keys(a) ["y","b","c","x"]
values(a) [4,1,2,3]

code the-basic the
log it
a := {b: 1, c: [2,3,4,{d:5}]}
if a.c[3].d log '1>' a.c[3].d
if a.c[3].e log '2>' a.c[3].e
if the a.c[3].d log '3>' it
if the a.c[3].e log '4>' it
output
nil
1> 5
3> 5

code it-assign the
	a := 1
	b := the a
	are a b
	it = 5
	are a b
output
	a 1 b 1
	a 5 b 1

code each-index-level-in-value each loop
disabled
a := 'ABC:DEF'
each a {
	log chr item
	if item == ord ':' {
		b := 'X'
		are b
		each b {
			index 1 = index 1 + 1
		}
		each b {
			index 1 += 1
		}
	}
}
output
A B C :
b X
F

code more-the the
	a := 'faf'
	k := {}

	each a {
		use the k[item] {
			it += index
		}
		else {
			it = [index]
		}
	}

	log k
output
	{ a:[1], f:[0,2] }

code type_name expr
	log type_name(1)
	log type_name({})
	log type_name('')
	log type_name('1'+'2')
	log type_name([])
	log type_name(lam{})
output
	number
	object
	string
	string
	array
	fun

code assign-the the
	a := [100]
	the a[0] = 222
	log it
output
	222

code decl-the the
	the a := 222
	log it a
output
	222 222

code ic-non-existing-prop-bug object
	a := { b: 1 }

	fun f1 {
		log a.c
	}

	f1()
	f1()
	f1()

output
	nil nil nil

code langc-basic langc
	langc global {
		void global_c_func(val a) {
			__cout(a);
			printf(" world\n");
		}
	}

	fun c__sum a b {
		re := nil
		langc [
			$ a = __add($ a, $ b);
		]
		ret a
	}

	fun c_call_global {
		a := 'hello'
		langc {
			global_c_func($ a);
		}
	}

	c_call_global()
	log c__sum(5, 50)
	log c__sum('sh', 'ip')

output
	hello world
	55
	ship

code langc-undefined-id langc
	fun f {
		langc [
			$ a = __nil;
		]
	}

	f()
output
	.test.i: 3
		$ a = __nil;
		^E011_1 a

code langc-char-literal langc
	fun f {
		langc { printf("%c%c%c\n", 'a','\t','c'); }
	}

	f()
output
	a       c

code langc-throw langc try
	fun f x {
		langc [
			int a = __get($ x);
			if (a == 1) {
		]
				log '  throw'
				throw 'boom'
		langc [
			}
			else {
				printf("  pass\n");
			}
		]
	}

	fun go msg x {
		log msg
		try {
			f(x)
		}
		catch {
			log '  got it:' error
		}
	}
	go('one', 1)
	go('two', 2)

output
	one
	  throw
	  got it: boom
	two
	  pass

code langc-values langc
	fun q {
		a := { b: 111 c: 555 }

		ret lam {
			langc {
				__log($ a.c, $ ("abc" + '123'));
			}
		}
	}

	q()()

output
	555 abc123

code langc-as-expr langc
	a := langc { __let(222) }

	are a
	log if langc { fork() } 'PARENT ' else 'CHILD '

output
	a 222
	PARENT
	CHILD

code pull-normalize-arr array
s := '1 2 3'
log pull (s / ' ')
output
1

code it-member-and-that the
a := [{b:100},{b:200}]
x := the
a[0] log it.b
x = the
a[1] log it.b that.b
output
100
200 100

code shame-quote-dirty-property shame
a := {'/a/b.c':1, a:2}
a = shame(a)
log shame(a) // quote to enforce comparison character-by-character

output
'{a:2,"/a/b.c":1}'

code many-funcs-single-closure closure
fun env {
	c := 100
	fun a {
		c += 1
	}
	fun b {
		c += 100
	}
	fun d {
		log(c)
	}
	ret {a,b,d}
}

a := env()
a.a()
a.b()
a.d()

output
201

code colon-comment coment lexer
log 12/:rem rem:/34
output
12 34

code nil-property-name object nil
a := {}
a[nil] = 1
output
E001 nil

code nil-index
a := []
a[nil] = 1
output
E004

code comparing-objects object expr
a:={} b:=a c:={}
log a==b a!=b a<b a>b a<=b a>=b
log a==c a!=c a<c a>c a<=c a>=c
output
1 0 0 0 1 1
0 1 0 0 1 1

code modulo number expr
log 10 mod 3 11 mod 3
a := 10 b := 4
log a mod b
output
1 2
2

code each-over-expr expr each loop
each [1 2 3] + [4 5] log item
output
1 2 3 4 5

code it-error-cp the each loop
each [] log the item
log it
output
.test.i: 1
  each [] log the item
                  ^E015_1 item


code that-error-cp each the loop
each [] log the item
log the []
log that

output
.test.i: 1
  each [] log the item
                  ^E015_1 item

code mul_to-div_to number value
a := '1 2 3'
a /= ' '
log a
a *= '-' // var_imm case
log a

b := [[1]]
each b item[0] *= 4 // var case
log b
each b item[0] /= 2 // var case
log b

output
["1","2","3"]
1-2-3
[[4]]
[[2]]

code ret-from-throw ret try
fun f {
	try {
		throw 1
	}
	catch {
		ret
	}
}

f()
log 'ok'
output
ok

code nested-catch try
fun f {
	try {
		try {
			throw 1
		}
		catch {
			log 'catch 1'
			throw 2
		}
	}
	catch {
		log 'catch 2'
		ret
	}
}

f()
output
catch 1
catch 2

code IC-assign-new-prop object ic
fun mk {
	ret {a:100}
}

fun _use a {
	a.x = 555
	log a
}

_use(mk())
_use(mk())

output
{ a: 100, x: 555 }
{ a: 100, x: 555 }

code keywords-as-properties object
a := { name: 100 'log': 200 'if':300 'fun':400}
log a.name a['log'] a['if'] a['fun']

output
100 200 300 400

code enum-add enum
enum e1 { a b cde }
enum e2 { f g }
enum e = e1 + e2
fun f {
	log e
}
f()
log e

output
{ a: 0, b: 1, f: 3, g: 4, cde: 2 }
{ a: 0, b: 1, f: 3, g: 4, cde: 2 }

code variable-args-over arg
fun f0 a b {
	log a b
}
f := f0
f(1,2,3,4,5)
output
1 2

code args-without-commas arg
fun f a b {
	log a b
}
f(1 2)
output
1 2

code init-item init each loop
each [nil] init item { log 5 }
output
5

code this-basic-support this object
a := { b: lam { log this.c } c: 555 }
a.b()

output
555


code incl-twice incl
file lib
log 'log-lib'

fun sub {
	log 'lib-sub'
}

file main
log 'log-main'
incl lib
incl lib
sub()
fun f {
	incl lib
	sub()
}
log 'main-end'
f()

output
log-main
log-lib
log-lib
lib-sub
main-end
log-lib
lib-sub

code scan-env-in-lambdas closure
l0 := lam
x {
	fun A z {
		if z == 1 {
			ret lam { ret A }
		}
		log x
	}
	f := A(1)()
	f()
}
l0(100)

output
100

code chr-array string chr
log chr [65 66 67 68]
output
ABCD

code this-in-value-call-param this
a := {
	d: 200
	f: lam { log this.d }
}
b := {
	d: 404
	c: 5
}
a.f(b.c)
output
200

code this-passed-to-lam this
say := lam s {
   log s
}
fun f {
   say(this.c)
}
state := { f,c:5 }
state.f()
output
5

code split-string-empty-delim string
log ''/''
log ''/'a'
log 'a'/''
log 'ab'/''
log 'abc'/''
log 'abcd'/''
output
[]
[""]
["a"]
["a","b"]
["a","b","c"]
["a","b","c","d"]


code bind-basic bind
a := 999
fun f x y {
	are a this
	are x y
}
d := bind(f 111 222 333)
d(333)

output
a 999  this 111
x 222  y 333

code delete-property object
o := { a: 1 b: 2 c: 3 }
delete(o 'b')
log o

output
{ a: 1, c: 3 }


code is_type expr cell
fun f{}
log is_number(1) is_number([]) is_number({}) is_number('') is_number(f) is_number(nil)
log is_array(1) is_array([]) is_array({}) is_array('') is_array(f) is_array(nil)
log is_object(1) is_object([]) is_object({}) is_object('') is_object(f) is_object(nil)
log is_string(1) is_string([]) is_string({}) is_string('') is_string(f) is_string(nil)
log is_fun(1) is_fun([]) is_fun({}) is_fun('') is_fun(f) is_fun(nil)
log is_nil(1) is_nil([]) is_nil({}) is_nil('') is_nil(f) is_nil(nil)
output
1 0 0 0 0 0
0 1 0 0 0 0
0 0 1 0 0 0
0 0 0 1 0 0
0 0 0 0 1 0
0 0 0 0 0 1

code let-loop-lam
disabled
fun f {
	i := 1
	a := []
	while i <= 5 {
		a += lam { cout i  }
		i += 1
	}
	ret a
}
each f() item()

output
1 2 3 4 5

code shame-escape shame
s := '{"\\"":"\\"", "c":"d"}'
j := honor(s)
log s
log j
log shame(j, { json_mode: true })
log shame(j)
s = '{"a": "\\\\\'", x:1}'
are s
j = honor(s)
log j
//are j.a j.x
log shame(j, { json_mode: true })


output
{"\"":"\"", "c":"d"}
{ "\"": "\"", c: "d" }
{"\"":"\"","c":"d"}
{"\"":"\"",c:"d"}
s {"a": "\\'", x:1}
{ a: "\\'", x: 1 }
//j.a \'  j.x 1
{"a":"\\'","x":1}

code atoms
disabled
log [atom a atom a atom b atom a atom b atom c]

output
[0,0,1,0,1,2]

code log-eol-expr log
log
1+1
log 3
output
3

code fun-equality expr
fun a{} fun b{} c:=a; log a == a b==b a==b a==c c==c
output
1 1 0 1 1

code fun-eq-syntax expr
fun f x = 100 + x
log f(23)
output
123

code and-optimize-out-more-ops expr
fun d {ret 25} log 40>0 and 40<=d()-1
output
0

code free-enums enum
enum free E { a b }

log E[E.c] E[E.a] E[E.b]
log E.c E.a E.b
output
c a b
2 0 1

code free-enum2 enum
enum free E { a b }

log E.c E.a E.b
log E['d']
log E.a E.b E.c E.d
output
2 0 1
3
0 1 2 3

code throw-and-this try this
	a := { d: lam { throw this.num } num: lam = 123 }

	fun f {

		try a.d() catch  log error()
	}

	f()
output
	123

code error-as-value try
	fun f1 { throw { a: 1 b: { c: { d: 555 } }} }

	try {
		f1()
	}
	catch {
		log error.b.c.d
	}

output
	555

code error-outside-catch
	log error

output
.test.i: 1
  log error
      ^E012_0

code each-over-object each loop object
	a := { b: 5 c: 10 d: 20 }
	each a {
		log index item len index
	}
output
	d 20 1
	b 5 1
	c 10 1

code not-nil-obj nil object
	log not nil, not {}

output
	1 0

code loop-leak
	a := nil
	b := nil
	x := 0
	loop {
		log 1001
		a = [1]
		b = [2]
		log 1001
		break
	}
	force_gc()
	log 544
	log a b
	log 'ok'

output
	1001
	1001
	force_gc()
	544
	[1] [2]
	ok

code nil-as-base expr
	nil.a = 12

output
	.test.i: 1
	  nil.a = 12
	  ^ E013_1 nil

code do-ellipsis
	fun _repeat n ... {
		x := 0
		ff := args[1..]

		while x < n {
			each ff item(x)
			inc x
		}
	}

	_repeat(5) do x { log '#' + x }

	_repeat(3)
		do x { log 'tick|' }
		do x { log '    |tock' }

	fun bla x { log x }
	_repeat(3) do = bla('click')

output
	#0
	#1
	#2
	#3
	#4
	tick|
		 |tock
	tick|
		 |tock
	tick|
		 |tock
	click
	click
	click

code declare-item each loop
	item := 1
output
	.test.i: 1
  item := 1
  ^E016_1 item

code item-outside each loop
	log item
output
	.test.i: 1
	  log item
			^E015_1 item

code item-assign-level error each
	a := [1 2 3]
	each a item_for 1 = 1
	log a
output
	.test.i: 2
	  each a item_for 1 = 1
				^E014_2 item 1

code tee assign expr
	fun fork x {
		ret x + 100
	}

	each [0 1 2]
		if tee pid := fork(item)
			are pid

	x := 5
	while (tee x -= 1) > 0 {
		log 'hello'
	}

output
	pid 100 pid 101 pid 102
	hello
	hello
	hello
	hello

code expr-statem-error
	2 > 1

output
	.test.i: 1
	  2 > 1
	  ^E017_0

code sigsegv-trace
	fun x {
		langc {
			*(char*)0 = 0;
		}
	}
	fun y { x() }
	y()

output
	|.#.c:#  program(0, __nil, 0,__nil,__nil,__nil,0);
	|.test.i:7   y()
	|.test.i:6   fun y { x() }
	|.test.i:3  *(char*) 0 = 0;
	 ^ fatal low level crash

code empty-ret-kill
	ret
	if 1 log 'one'
	kill
	if 1 log 'one'
output

code langc-include langc
	langc global {
		#include <stdio.h>
		#include <dirent.h>
	}

	langc {
		DIR *dir = opendir(".");
		printf("%i\n", ((uintptr_t)dir) && 1);
	}
output
	1

code object-as-base expr
	log { 'a': 11 'r': 22 'w': 33 'rw': 44 }['w']
output
	33

code incl-error-sourcing
file lib
	log 'hello'
	a = 2
file main
	fun f1 {}
	fun f2 {}
	fun f3 {}
	fun f4 {}
	fun f5 {}
	fun f6 {}
	fun f7 {}
	incl lib
output
	lib.i: 2
	  a = 2
	  ^scope: undeclared identifier <a>

code expr-as-value-base value
	x := ([1]+2)[1]
	are x
output
	x 2

code div-assign-string
	A := '-1-'
	A /= '1'
	are A
output
	A ['-', '-']

code star-comment
	log 5/*asdas*/55
output
	5 55

code swap-assignment
	a := 100
	b := 200
	a b = [b a]
	log a b
output
	200 100

code c-str-literal
	a := '\0\\"\0'
	log len a a
	log ord a[0] ord a[1] ord a[2] ord a[3]
output
	4
	0 92 34 0

code floats basic
	log 33.3
output
	33.3

code object-merge value object
	log { a: 10 } + { b: 20 }
	log { a: 10 } + { a: 30 b: 20 }
output
	{a:10,b:20}
	{a:30,b:20}

code each-over-number each
	each 5 log 'hello'
output
	hello
	hello
	hello
	hello
	hello

code c_shadow langc
	langc shadow { myname }
	langc global {
		void myname() {
			printf("hello\n");
		}
	}
	fun myname { langc { myname(); } }
	myname()

output
	hello

code loot each
disabled
	a := [1 2 3 4 5 6 7 8 9 10]
	//b := []
	//each 5 b += [a[item:5]]
	b := each 5 loot [a[item:5]]
	are b
output
	b [[1,2,3,4,5],[2,3,4,5,6],[3,4,5,6,7],[4,5,6,7,8],[5,6,7,8,9]]

code union
disabled
	union node { x y }
	node.x = 12
	of node {
		log 'x:' node.x
		log 'y:' node.y
	}
	log of node { node.x * 100  node.y + 200 }

output
	x: 12
	1200

code mod-assign
disabled
	x := 7
	x mod= 5
	log x
output
	2

code mutation-crash
	a := []
	a += 'a'
	a *= 'b'
output

code rec_basic type rec type
	rec a { x y }
	a := { y: 20 x: 15 }
	log a
	log a.x
	inc a.x
	log a.x
output
	rec{x:15,y:20}
	15 16

code rec_partial rec type
	rec a { x y }
	a := { y: 20 }
	log a
output
	rec{ x: nil, y: 20 }

code rec_literal_sub rec type
	rec a { x y }
	b := { a: { x: 1 y: 2 } }
	log b.a.x
	log b.a.y
	log b.a.x
	b.a.x = 5
	log b.a.x
output
	1 2 1 5

code rec_literal_mis rec type
	rec a { x y }
	b := { a: { x: 1 y: 2 z: 4} }
output
	.test.i: 2
	  b := { a: { x: 1 y: 2 z: 4} }
                           ^E018_1 rec-x-y

code exp
	a := exp { x := 2 + 2; x }
	fun f = exp { x := 3 + 3; x }
	log a f()
output
	4 6

code let
	a := 3
	{
		let a := 5
		shadow a := 5 // this must be better?
		log a
	}
	log a
output
	5 3

code each-range each
	each 3-1..7-2 log item
output
	2 3 4 5

code at index
	'a-b-c' / '-' at 2
output
	c

code ret_type type fun
	fun a {
		fun a { ret 5 }
	}
output
	.test.i: 2
	fun a { ret 5 }
		^E020_1 a
	.test.i: 1
	fun a {
		^E019_3 a nil var

finish

