//Example:
//__iterate_array(my, $ a) {
//	vbind[my_index] = my_items[my_index];
//}

#define __iterate_array(uniq, x) \
	__array_compact(x); \
	val *uniq##_items = __leaf(x)->p; \
	__size_t * uniq##_sz = & __leaf(x)->size; \
	for (int uniq##_index = 0; uniq##_index < * uniq##_sz; uniq##_index++)


val __array_allocopy(__size_t size, val *p) {
	leaf *l = leaf_new(leaf_arr);
	l->size = size;
	l->capacity = size * array_grow_factor;
	if (l->capacity < 8) l->capacity = 8;
	l->p = Alloc(l->capacity * sizeof(val));

	val *A = l->p, *B = p;
	if (l->p && p) {
		for (__size_t i = 0; i < size; i++) *A++ = *B++;
	}
	return __mk_val(l);
}

void __array_compact(val a) {
	// not needed for capacity-based design
}

void __array_recap(leaf *A, __size_t size) {
	int old_size = A->capacity;
	A->capacity = size * array_grow_factor + 1;
	A->p = Realloc(A->p, A->capacity * sizeof(val), old_size);
}

void __array_resize(val a, __size_t size) {
	leaf *A = __leaf(a);

	if (size < A->size) {
		A->size = size;
		if ((size << 3) < A->capacity) {
			__array_recap(A, size);
		}
		return;
	}

	if (size > A->capacity) {
		__array_recap(A, size);
	}

	for (__size_t i = A->size; i < size; i++) {
		A->p[i] = __nil;
	}
	A->size = size;
}

val __array_push(val arr, val item) {
	if (!__is_array(arr)) __throw(__str(E012_0));
	leaf *a = __leaf(arr);
	if (a->size == a->capacity) {
		__array_recap(a, a->size);
	}
	__size_t re = a->size++;
	a->p[re] = item;
	return re;
}

val __array_put(val arr, val item) {
	if (!__is_array(arr)) __throw(__str(E013_0));
	leaf *a = __leaf(arr);
	if (a->size == a->capacity) {
		__array_recap(a, a->size);
	}
	__size_t re = a->size;
	for (__size_t i = re; i > 0; i--) a->p[i] = a->p[i - 1];
	a->p[0] = item;
	a->size++;
	return re;
}

__size_t __array_size(val a) {
	return __leaf(a)->size;
}

val __array_get(val a, int64_t index) {
	if (!__is_array(a)) __throw(__str(E002_0));
	leaf *l = __leaf(a);
	if (index < 0) index = l->size + index;
	if (index >= l->size || index < 0) return __nil;
	return __rooted(l->p[index]);
}

val *__array_at(val a, int64_t index) {
	if (!__is_array(a)) __throw(__str(E002_0));
	leaf *l = __leaf(a);
	if (index < 0) {
		index = l->size + index;
	}
	if (index < 0) __throw(__str(E003_0));
	if (index >= l->size) {
		__array_resize(a, index + 1);
	}
	// todo check if must root
	return &l->p[index];
}

void __array_set(val a, __size_t i, val x) {
	*__array_at(a, i) = x;
}

void __array_append(val array_a, val array_b) {
	leaf *A = __leaf(array_a);
	leaf *B = __leaf(array_b);
	__size_t start = A->size;

	__array_resize(array_a, A->size + B->size);

	val
		*d = & A->p[start],
		*s = B->p,
		*e = & B->p[B->size];

	while (s < e) {
		*d++ = *s++;
	}
}

val __array_concat(val a, val b) {
	if (!__is_array(a)) __throw(__str("__array_concat a is not an array"));
	if (!__is_array(b)) __throw(__str("__array_concat b is not an array"));

	val re = __array();
	__array_append(re, a);
	__array_append(re, b);
	return re;
}

val __array_pop(val a) {
	if (!__is_array(a)) __throw(__str(E014_0));

	leaf *A = __leaf(a);

	if (A->size == 0) return __nil;
	A->size--;
	if ((A->size << 3) < A->capacity) {
		__array_recap(A, A->size);
	}
	val re = A->p[A->size];
	return __rooted(re);
}

val __array_pull(val arr) {
	if (!__is_array(arr)) __throw(__str(E015_0));
	leaf *a = __leaf(arr);
	if (a->size == 0) return __nil;
	a->size--;
	val re = a->p[0];
	for (__size_t i = 0; i < a->size; i++) {
		a->p[i] = a->p[i + 1];
	}
	if ((a->size << 3) < a->capacity) {
		__array_recap(a, a->size);
	}
	return __rooted(re);
}

val __array_find_string(val where, val what, __size_t at) {
	__string_compact(what);
	val *p = __leaf(where)->p;
	__size_t e = __leaf(where)->size;
	for (__size_t i = at; i < e; i++) {
		if (__is_string(p[i])) {
			if (__cmp_str(p[i], what) == 0) {
				return __let_i(i);
			}
		}
	}
	return __let_i(-1);
}

val __array_find_val(val where, val what, __size_t at) {
	val *p = __leaf(where)->p;
	__size_t e = __leaf(where)->size;
	for (__size_t i = at; i < e; i++) {
		if (__eq(p[i], what)) {
			return __let_i(i);
		}
	}
	return __let_i(-1);
}

