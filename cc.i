CARG_MAX := 3
CVAR_PREFIX := '_v'
CARG_PREFIX := '_a'
CARG_PTR := '_aptr'
CARG_COUNT := '_acount'
CARG_ENV := '_env'
incl 'macro-gen'

fun opcode _id expr statem {
	ret { _id, expr, statem }
}

fun assert_array ref arg {
	if not is_array(arg)
		throw ref + ' must be array, but received ' + typeof(arg)
}

fun assert_number ref arg {
	if not is_number(arg)
		throw ref + ' must be number, but received ' + typeof(arg)
}

fun assert_string ref arg {
	if not is_string(arg)
		throw ref + ' must be string, but received ' + typeof(arg)
}

fun no_expr { throw this._id + ' is not an expression' }
fun no_statem { throw this._id + ' is not a statement' }

//  op math

fun to_string x = '' + x

fun split arr x {
	re := []
	each arr if index == 0 re += item else re += [x item]
	ret re
}
c_itself := fun x {
	ret opcode('c_itself'
		lam = x
		lam = x
	)
}
fun exprs a = map(a lam x { if is_object(x) ret x.expr(); ret x })
fun statems a = map(a lam x { if is_object(x) ret x.statem(); ret x })

//  op tool

//  op nodes

fun c_nil {
	ret opcode('c_nil'
		lam = '__nil'
		no_statem
	)
}

fun c_const id {
	ret opcode('c_const'
		lam = '__' + id //true false nil
		no_statem
	)
}

fun c_con value { // constant/literal
	if not is_string(value) {
		throw 'c_con() value ' +value+ ' must be stringized'
	}
	ret opcode('c_con'
		lam = ['__let(' value ')']
		no_statem
	)
}
//  variables

fun c_varget ix {
	if not is_number(ix) throw 'c_varget: ix must be a number'
	ret opcode('varget'
		lam = CVAR_PREFIX + ix
		no_statem
	)
}
fun c_varset ix expr {
	if not is_number(ix) throw 'c_varset: id must be a number, got <'+ typeof(ix) +'>'
	ret opcode('varset'
		no_expr
		lam = [CVAR_PREFIX + ix ' = ' expr.expr() ';']
	)
}
fun c_envset ix expr {
	if not is_number(ix) throw 'c_envset: ix must be a number'
	ret opcode('envset'
		no_expr
		lam = [ '__env('+ ix +') = ' expr.expr() ';' ]
	)
}
fun c_envget ix {
	if not is_number(ix) throw 'c_envget: id must be a number'
	ret opcode('envget'
		lam = [ '__env('+ ix +')' ]
		no_statem
	)
}
fun c_envdirect ix {
	if not is_number(ix) throw 'c_envdirect: id must be a number'
	ret opcode('envget'
		lam = [ 'CARG_ENV->p['+ix+']' ]
		no_statem
	)
}

fun c_flynew expr {
	ret opcode('c_flyvar'
		lam = [ '__flyvar(' expr.expr() ')' ]
		no_statem
	)
}

fun c_flyget ix {
	ret opcode('c_flyget'
		lam = [ '__ref(' CVAR_PREFIX + ix ')' ]
		no_statem
	)
}
fun c_flyset ix expr {
	ret opcode('c_flyset'
		no_expr
		lam = [ '__ref(' CVAR_PREFIX + ix ') = ' expr.expr() ';' ]
	)
}
fun c_flyargget ix {
	ret opcode('c_flyargget'
		lam {
			if ix < CARG_MAX ret [ '__ref(' CARG_PREFIX + ix ')' ]
			ret [to_string(ix) '<' CARG_COUNT + '?__ref(' CARG_PTR '[' to_string(ix-CARG_MAX) ']) : __nil']
		}
		no_statem
	)
}
fun c_flyargset ix expr {
	ret opcode('c_flyargset'
		no_expr
		lam {
			x := expr.expr()
			if ix < CARG_MAX ret [ '__ref(' CARG_PREFIX + ix ') = ' x ';' ]
			ret ['if (' to_string(ix) '<' CARG_COUNT + ') __ref(' CARG_PTR '[' (ix - CARG_MAX) ']) = ' x ';']
		}
	)
}
fun c_argget ix {
	ret opcode('c_argget'
		lam {
			if ix < CARG_MAX ret CARG_PREFIX + ix
			ret [to_string(ix) '<' CARG_COUNT + '?' CARG_PTR '[' to_string(ix-CARG_MAX) '] : __nil']
		}
		no_statem
	)
}
fun c_argset ix expr {
	ret opcode('c_argset'
		no_expr
		lam {
			x := expr.expr()
			if ix < CARG_MAX ret [ CARG_PREFIX + ix ' = ' x ';' ]
			ret ['if (' to_string(ix) '<' CARG_COUNT + ') ' CARG_PTR '[' to_string(ix - CARG_MAX) '] = ' x ';']
		}
	)
}
fun c_multi src ix {
	ret opcode('c_multi'
		lam = [ '__multi(' src.expr() ', ' to_string(ix) ')' ]
		no_statem
	)
}

fun c_tee target statem {
	ret opcode('c_tee'
		lam {
			ret [
				'({'
					statem.statem() ';'
					target.expr() ';'
				'})'
			]
		}
		no_statem
	)
}
//  operations

fun c_binary n {
	ret lam a b = opcode(n
		lam = ['__'+n+'(' a.expr() ',' b.expr() ')']
		no_statem
	)
}

fun c_and a b {
	// this is special because 'and' needs to support single operand evaluation
	ret opcode('and'
		lam = ['__let_i(__get_i(' a.expr() ') && __get_i(' b.expr() '))']
		no_statem
	)
}

fun c_assign_to n {
	ret lam a b = opcode(n
		lam = ['__'+n+'(' a.expr() ',' b.expr() ')']
		no_statem
	)
	//ret lam a b = opcode(n
		//no_expr
		//lam = ['__'+n+'(' a.expr() ',' b.expr() ')' ';']
	//)
}

c_add := c_binary('add')
c_sub := c_binary('sub')
c_mul := c_binary('mul')
c_div := c_binary('div')
c_idiv := c_binary('idiv')
c_eq := c_binary('eq')
c_ne := c_binary('ne')
c_lt := c_binary('lt')
c_gt := c_binary('gt')
c_le := c_binary('le')
c_ge := c_binary('ge')
c_or := c_binary('or')
//c_and := c_binary('and')
c_OR := c_binary('OR')
c_AND := c_binary('AND')
c_SHR := c_binary('SHR')
c_SHL := c_binary('SHL')
c_mod := c_binary('mod')
c_add_on := c_assign_to('add_on')// todo replace 'on' with 'to'
c_sub_on := c_assign_to('sub_on')
c_mul_on := c_assign_to('mul_on')
c_div_on := c_assign_to('div_on')

/:
#define __not(a) __let(!__round(a))
#define __inc(a) a = __add(a, __let(1))
#define __dec(a)  a = __sub(a, __let(1))
:/


fun c_negation expr {
	ret opcode('c_negation'
		lam = ['__neg(' expr.expr() ')']
		no_statem
	)
}

fun c_not expr {
	ret opcode('c_not'
		lam = ['__not(' expr.expr() ')']
		no_statem
	)
}

fun c_inc value {
	ret opcode('inc'
		no_expr
		lam = [value.expr() ' = __inc(' value.expr() ')' ';']
	)
}

fun c_dec value {
	ret opcode('dec'
		no_expr
		lam = ['__dec(' value.expr() ')' ';']
	)
}

//fun c_pop expr {
	//ret opcode('c_pop'
		//lam = ['__array_pop(' expr.expr() ')']
		//no_statem
	//)
//}
//
//fun c_push arr value {
	//ret opcode('c_push'
		//no_expr
		//lam = ['__array_push(' arr.expr() ',' value.expr() ')']
	//)
//}
//
fun c_chr expr {
	ret opcode('c_chr'
		lam = ['__chr(' expr.expr() ')']
		no_statem
	)
}

fun c_ord expr {
	ret opcode('c_ord'
		lam = ['__ord(' expr.expr() ')']
		no_statem
	)
}

fun block list {
	ret opcode('block'
		lam {
			if len list == 0 throw 'empty block has no expr'
			ret [ '({' statems(list) '})' ]
		}
		lam {
			if len list == 0 ret '{}'
			ret [ '{' statems(list) '}' ]
		}
	)
}

fun c_ifex a b c {
	ret opcode('ifx'
		lam = ['(__get(' a.expr() ')?' b.expr() ':' c.expr() ')']
		no_statem
	)
}

fun c_if a b e {
	assert_array('c_if: b' b)
	E := []
	if e {
		E = ['else ' block(e).statem() ';']
	}
	ret opcode('if'
		no_expr
		lam = ['if (__get(' a.expr() '))' block(b).statem() ] + E
	)
}
//
//fun c_init x b {
	//assert_array('c_init: b' b)
	//ret opcode('init'
		//no_expr
		//lam = ['if (' x.expr() ' == __nil)' block(b).statem() ]
	//)
//}

//fun c_use x b {
	//assert_array('c_use: b' b)
	//ret opcode('use'
		//no_expr
		//lam = ['if (' x.expr() ' != __nil)' block(b).statem() ]
	//)
//}

fun c_nop {
	ret opcode('nop'
		lam = ''
		lam = ''
	)
}

fun c_rem x {
	ret opcode('rem'
		no_expr
		lam = ['// ' x flatter.eol]
	)
}

fun __optional_return b {
	if len b > 0 and b[-1]._id == 'c_ret' {
		ret ''
	}
	else
		ret statems([c_leave() 'return __nil' ';'])
}

fun c_forward_decl id {
	ret opcode('c_forward_decl ' + id
		no_expr
		lam = [ 'val ' id '(__arg_decl)' ';' ]
	)
}

fun var_decl var_list {
	ret opcode('var_decl'
		no_expr
		lam {
			if len var_list == 0 ret []
			re := []
			each var_list {
				v := CVAR_PREFIX + index
				if item == 0
					re += v + ' = __nil'
				else
					re += v + ' = __flyvar(__nil)'
			}
			ret ['val ' re * ',' '' ';']
		}
	)
}

fun convert_flyargs list {
	if len list == 0 ret []
	re := []
	each list {
		re += c_argset(item c_flynew(c_argget(item))).statem()
	}
	ret re
}

fun c_langc list {
	ret opcode('c_langc'
		lam = statems(list)
		lam = statems(list)
	)
}

fun c_langc_expr x {
	ret opcode('c_langc'
		lam {
			ret ['({' x.expr() +';})']
		}
		no_statem
	)
}

fun c_block list {
	ret opcode('c_block'
		no_expr
		lam {
			ret statems(list)
		}
	)
}

fun c_fun id var_list flyarg_list block {
	assert_array('c_fun: block' block)
	assert_array('c_fun: var_list' var_list) // [0 0 1 1 0] 0-normal 1-flyvar
	assert_array('c_fun: flyarg_list' flyarg_list) // [2 4] indexes of flyargs
	fun line_reset {
		if id == 'program' ret flatter.line_directive_reset
		ret ''
	}

	fun after_main {
		if id == 'program' ret ['__call_after_main()' ';']
		ret ''
	}

	ret opcode('c_fun ' + id
		no_expr
		lam = [
			'val ' id '(__arg_decl)' '{'
				statems([
					c_enter()
					var_decl(var_list)
					convert_flyargs(flyarg_list)
					statems(block)
					after_main()
					__optional_return(block)
				])
				line_reset()
			'}'
		]
	)
}

fun c_call id arg_list {
	assert_array('c_call: arg_list' arg_list)

	fun a_list {
		re := []
		p := []
		each arg_list {
			if index < CARG_MAX {
				re += [item.expr()]
			}
			else {
				n := index - CARG_MAX
				p += [item.expr()]
			}
		}
		while len re < CARG_MAX re += '__nil'
		if len p > 0  re += [['((val[]){' split(p ', ') '})']]
		else  re += '0'
		ret ['0, __nil, ' len arg_list + ',' split(re ',')]
	}

	ret opcode('call_' + id
		lam = [id '(' a_list() ')']
		lam = [id '(' a_list() ')' ';']
	)
}

fun c_vals has_size list {
	ret opcode('c_vals'
		lam {
			re := ''
			if has_size re += (len list)+'' + ', '
			re = [re '((val[]){' exprs(split(list c_itself(','))) '})']
			ret re
		}
		no_statem
	)
}

fun c_closure func_id env_vars {
	assert_array('c_closure: env_vars' env_vars)

	ret opcode('c_closure'
		lam = [ '__closure(' func_id ',' c_vals(true env_vars).expr() ')']
		no_statem
	)
}

fun c_closure_call closure _this arg_list {
	assert_array('c_closure_call: arg_list' arg_list)
	if _this == nil _this = '__nil' else _this =  _this().expr()

	ret opcode('c_closure_call'
		lam = [
			'__closure_call_n('
				closure.expr() ','
				_this ','
				c_vals(true arg_list).expr()
			')'
		]
		lam = [this.expr() ';']
	)
}

fun c_member_call _object _index arg_list {
	assert_array('c_member_call: arg_list' arg_list)

	ret opcode('c_member_call'
		lam = [
			'__member_call_n('_object.expr()', '_index.expr()', 'c_vals(true arg_list).expr()')'
		]
		lam = [this.expr() ';']
	)
}

fun c_array_literal values {
	assert_array('c_array: values' values)

	ret opcode('arr_literal'
		lam = [ '__array_allocopy(' + len values, ', (val[])' '{' exprs(split(values c_itself(','))) '}' ')' ]
		no_statem
	)
}

fun c_index_get obj ix {
	ret opcode('index_get'
		//lam = [ '({ val x = __index_get(' obj.expr() ', ' ix.expr() '); x' '; })' ]
		lam = [ '__index_get(' obj.expr() ', ' ix.expr() ')' ]
		no_statem
	)
}

fun c_index_set obj ix value {
	ret opcode('index_set'
		no_expr
		lam = [ '__index_set(' obj.expr() ', ' ix.expr() ', ' value.expr() ')' ';' ]
	)
}

fun c_bit_get obj ix {
	ret opcode('bit_get'
		lam = [ '__bit_get(' obj.expr() ', ' ix.expr() ')' ]
		no_statem
	)
}

fun c_bit_set obj ix value {
	ret opcode('bit_set'
		no_expr
		lam = [ '__bit_set(' obj.expr() ', ' ix.expr() ', ' value.expr() ')' ';' ]
	)
}

fun c_range obj from to to_is_count {
	ret opcode('c_range'
		lam = [ '__range('
			obj.expr() ', '
			'__get(' from.expr() '), '
			'__get(' to.expr() '), '
			to_string(to_is_count)
		')']
		no_statem
	)
}

fun c_property_get obj prop {
	ret opcode('c_property_get'
		lam = [ '__property_get(' obj.expr() ', ' prop.expr() ')' ]
		no_statem
	)
}

fun c_property_set obj prop value {
	ret opcode('c_property_set'
		no_expr
		lam = [ '__property_set(' obj.expr() ', ' prop.expr() ', ' value.expr() ')' ';' ]
	)
}

fun c_rec_prop_get record ix {
	ret opcode('c_rec_prop_get'
		lam = [ '__rec_get(' record.expr() ', ' ix+'' ')' ]
		no_statem
	)
}

fun c_rec_prop_set obj ix value {
	ret opcode('c_property_set'
		no_expr
		lam = [ '__rec_set(' obj.expr() ', ' ix+'' ', ' value.expr() ')' ';' ]
	)
}

fun c_add_to dest value {
	ret opcode('+='
		no_expr
		lam = [ '__val_add_to(' dest.expr() ', ' value.expr() ')' ';']
	)
}

fun c_enter {
	ret opcode('enter'
		no_expr
		lam = [ '__enter()' ';' ]
	)
}

fun c_leave {
	ret opcode('leave'
		no_expr
		lam = [ '__leave()' ';']
	)
}

//
//val lam1(__env_t *_env, short _acount, val _a0,val _a1,val _a2, val *_aptr) {
	//val *__enter_mark = expr_stack;
	//
	//({
		//val _b = __builtin_choose_expr(__builtin_types_compatible_p(typeof((555)), int32_t), (((val)(555) & 0xFFFFFFFF)), (__new_big(555)));
		//expr_stack = __enter_mark;
		//if ((!(((_b) & 0x0001000000000000ULL) == 0) && !((_b) == 0x0001000000000000ULL))) *(--expr_stack) = _b;;
		//return _b;
	//});
//}


fun c_ret value {
	ret opcode('c_ret'
		no_expr
		lam = [ '__return(' value.expr() ')' ';']
	)
}

fun c_arg obj {
	ret opcode('c_arg'
		lam = [ '__arg(' obj.expr() ')']
		no_statem
	)
}

fun c_this {
	ret opcode('c_this'
		lam = [ '__this']
		no_statem
	)
}

fun c_args {
	ret opcode('c_args'
		lam = [ '__args']
		no_statem
	)
}

fun c_len_args {
	ret opcode('c_len_args'
		lam = [ '__arg_count']
		no_statem
	)
}

fun c_len obj {
	ret opcode('c_len'
		lam = [ '__let_i(__len(' obj.expr() '))']
		no_statem
	)
}

fun c_loop_hold expr {
	ret opcode('c_loop_hold'
		no_expr
		lam = [
			'__hold(&' expr.expr() ')' ';'
			//'{'
				//c_rem('loop holder').statem()
				//'leaf *l = leaf_new(leaf_hold)' ';'
				//'l->hold = &' expr.expr()  ';'
				//'__stack_push(expr, __mk_val(l))' ';'
			//'}'
		]
	)
}

fun c_loop body holds {
	assert_array('c_loop: body' body)

	ret opcode('c_loop'
		no_expr
		lam = [
			statems(holds)
			'while (1)' '{'
				'__loop_mark()' ';'
				statems(body)
				'__loop_release()' ';'
			'}'
		]
	)
}

fun c_while ex body holds  {
	assert_array('c_while: body' body)

	ret opcode('c_while'
		no_expr
		lam = [
			statems(holds)
			'while (' ex.expr() ')' '{'
				'__loop_mark()' ';'
				statems(body)
				'__loop_release()' ';'
			'}'
		]
	)
}

fun c_each iterable block each_id holds {
	assert_array('c_each: body' block)
	p := '__each_iter_' + each_id
	b := statems(block)
	if is_array(iterable) {
		initializer := [
			'__each_range_begin(& ' p ', ' iterable[0].expr() ',' iterable[1].expr() ')' ';'
		]
	}
	else {
		initializer = [
			'__each_begin(& ' p ', ' iterable.expr() ')' ';'
		]
	}

	ret opcode('c_each'
		no_expr
		lam {
			ret [
				'__each_iter ' p ';'
				initializer
				statems(holds)
				'while (__each_next(&' p '))' '{'
					'__loop_mark()' ';'
					b
					'__loop_release()' ';'
				'}'
			]
		}
	)
}

fun c_index each_id {
	ret opcode('c_index'
		lam {
			p := '__each_iter_' + each_id
			ret [ '__each_index(& ' p ')']
		}
		no_statem
	)
}

fun c_item each_id {
	ret opcode('c_item'
		lam {
			p := '__each_iter_' + each_id
			ret [ '__each_item(& ' p ')']
		}
		no_statem
	)
}

fun c_item_set each_id expr {
	ret opcode('c_item_ref'
		no_expr
		lam {
			p := '__each_iter_' + each_id
			ret [ '*__each_item_ref(& ' p ') = ' expr.expr() ';']
		}
	)
}

fun c_enum_reflect enu x {
	ret opcode('c_enum_reflect'
		lam {
			ret [ '__enum_reflect(&' enu.cid ', ' x.expr() ')']
		}
		no_statem
	)
}

fun c_enum_decl enu {
	ret opcode('c_enum_decl'
		no_expr
		lam {
			kv := []
			each keys(enu.items) {
				kv += '{ .key = ' + enu.items[item] +', .value = ' + shame(item) + '}'
			}
			re := [
				'__enum 'enu.cid' = {'
				'.name = "' enu.name '", .size = ' len enu.items+'' ', .kv = {' kv * ', ' +'}'
				'};' flatter.eol
			]
			ret re
		}
	)
}

fun c_enumap enu {
	ret opcode('c_enum_reflect'
		lam {
			q := shame(shame(enu.items))
			ret [ '__honor(__str(' q '))']
		}
		no_statem
	)
}

fun c_break {
	ret opcode('c_break'
		no_expr
		lam = [ '__loop_release()' ';' 'break' ';']
	)
}

fun c_cont {
	ret opcode('c_cont'
		no_expr
		lam = [ '__loop_release()' ';' 'continue' ';' ]
	)
}

/:a
val __str_lit(int id) {
	val re = __str_lits[id];
	return re ? re : __init_str_lit(id);
}

int main() {
	val _v0 = __add(__str_lit(1), __str_lit(1));
	log(1, _v0);
}
a:/

//fun c_str_literals_init list {
	//assert_array('c_str_literals_init: list' list)
	//ret opcode('c_str_literals_init'
		//lam { throw 'c_str_literals_init: cannot be an expr' }
		//lam = [
			//'void __init_str_lit(int id) {'
				//'static val *table = 0' ';'
				//'if (table == 0)' '{'
					//'table = malloc(' +len list+ ' * sizeof(val))' ';'
					//'for (int i = 0; i < '+ len list +'; i++) table[i] = 0' ';'
				//'}'
				//map(list lam x ix = 'table['+ ix +'] = __str_literal('+x+')')
			//'}'
		//]
	//)
//}

//fun c_str_literal id {
	//if not is_number(b) throw 'c_str_literal: string literal id must be a number'
	//ret opcode('str_literal'
		//lam = [ '__str_lit(' id ')' ]
		//lam { throw 'c_str_literal: is not a statement' }
	//)
//}

fun to_octal x {
	s := ''
	while x > 0 {
		d := x AND 7
		d += 48
		s = chr d + s
		x = x SHR 3
	}
	while len s < 3 s = '0' + s
	ret s
}

fun utf_length s {
	re := nil
	begin
		free(ship_pchar($ s));
		$ re = cell_let(last_utf_size);
	end
	ret re
}

fun all_octal s {
	re := ''
	each s re += '\\' + to_octal(item)
	ret re
}

fun c_string s {
	n := 0
	t := ''
	//if s == ';'
		//ret opcode('c_string'
		//lam = [ '__str(";")' ]
		//no_statem
	//)

	each s {
		if item == 0 {
			utfsize := utf_length(s)
			ret opcode('c_string'
				lam = [ '__strn("' all_octal(s) '", ' +utfsize +')' ]
				no_statem
			)
		}
		but item < 32 {
			t += '\\' + to_octal(item)
			// octal escape sequenses are limited to 3 chars long, but \x are weird.
		}
		but item == ord '"' or item == ord '\\' {
			t += '\\' + chr item
		}
		else {
			t += chr item
		}
	}
	ret opcode('c_string'
		lam = [ '__str("' + t +  '")' ]
		no_statem
	)
}

fun c_line line file node {
	ret opcode('c_line'
		lam = [ [flatter.line file line] node.expr() ]
		lam = [ [flatter.line file line] node.statem() ]
	)
}

fun c_object_literal list {
	assert_array('c_object_literal: list' list)

	ret opcode('c_object_literal'
		lam = [ '__object_new(' c_vals(true list).expr() ', 0)' ]
		no_statem
	)
}

fun c_rec_literal rectype list {
	assert_array('c_rec_literal: list' list)

	ret opcode('c_rec_literal'
		lam = [ '__rec_new(' rectype+'' ', ' c_vals(true list).expr() ')' ]
		no_statem
	)
}

fun c_register_rec rec_type_def {
	assert_array('c_register_rec: rec_type_def' rec_type_def)
	each rec_type_def {
		if index > 0 item = '__str("'+item+'")'
	}
	ret opcode('c_register_rec'
		no_expr
		lam = [ '__array_push(__global.rec_type_arr, __array(' rec_type_def * ',' '))' ';']
	)
}

//fun c_rec_literal rec_type values {
	//assert_array('c_rec_literal: values' values)
//
	//ret opcode('rec_literal'
		//lam = [ '__rec_new('
			//rec_type+', '+len values
			//', (val[])' '{' exprs(split(values c_itself(','))) '}'
			//')'
		//]
		//no_statem
	//)
//}

fun c_error {
	ret opcode('c_error' lam = [ '__tc_error' ] no_statem)
}

fun c_throw e {
	ret opcode('c_throw' no_expr lam = [ '__throw(' e.expr() ')' ';' ])
}

fun c_unwind_try_stack depth {
	ret opcode('c_unwind_try_stack'
		no_expr
		lam = [ '__stack(try) += ' + depth ';']
	)
}

fun c_try_catch b_try b_catch {
	assert_array('c_try_catch: b_try' b_try)
	assert_array('c_try_catch: b_catch' b_catch)

	b1 := statems(b_try)
	b2 := statems(b_catch)

	ret opcode('c_try_catch'
		no_expr
		lam = [
			'{'
				'__Try __t = { .e = __nil }' ';'
				'__stack_push(try, __t)' ';'
				'if (!setjmp(__stack(try)->j))' '{'
					b1
					'__stack_pop(try)' ';'

				'}'
				'else' '{'
					'val __tc_error = __stack(try)->e' ';'
					'__stack_pop(try)' ';'
					b2
				'}'
			'}'
		]
	)
}

fun c_enum e {
	ret opcode('c_enum' no_expr lam = '')
}


//  runtime

//fun c_putc c {
	//ret ['putc(' + c + ', stdout)' ';']
//}

fun c_log a {
	assert_array('c_log: a' a)

	ret opcode('log'
		no_expr
		lam {
			if len a == 0 ret [ 'putc(10, stdout)' ';']
			ret ['__log_n(', to_string(len a) ',' exprs(split(a c_itself(','))) ')' ';']
		}
	)
}

fun c_cout a {
	assert_array('c_cout: a' a)

	ret opcode('cout'
		no_expr
		lam = ['__cout_n(', to_string(len a) ',' exprs(split(a c_itself(','))) ')' ';']
	)
}

fun c_color color text { // remove this and make color a builtin
	assert_number('c_color: color' color)
	//assert_string('c_color: text' text)

	ret opcode('cout'
		lam = ['__color(', to_string(color) ', __str("' text '"))']
		no_statem
	)
}

fun c_color_term color text {
	assert_number('c_color_term: color' color)
	//assert_string('c_color_term: text' text)

	ret opcode('cout'
		lam {
			init text ret ['__color_term(', to_string(color) ', __nil)']
			ret ['__color_term(', to_string(color) ', __str("' text '"))']
		}
		no_statem
	)
}

fun c_kill exit_code {
	ret opcode('exit'
		no_expr
		lam = ['__exit(' exit_code.expr() ')' ';']
	)
}

//fun c_runtime_end {
	//ret opcode('runtime_end'
		//no_expr
		//lam = [ '__runtime_end()' ';' ]
	//)
//}

fun c_default_signal_handlers {
	ret opcode('c_default_signal_handlers'
		no_expr
		lam = [ 'install_signal_handlers()' ';']
		//lam = [ 'signal(SIGSEGV, signal_handler_SIGSEGV)' ';' ]
	)
}

fun c_test_mode_setup {
	ret opcode('c_test_mode_setup'
		no_expr
		lam = [
			'switch_to_test_errors()' ';'
			'__global.trace_enabled = 0' ';'
		]
	)
}

fun c_disable_traces {
	ret opcode('c_disable_traces'
		no_expr lam = [ '__global.trace_enabled = 0' ';' ]
	)
}

fun c_show_runtimes {
	ret opcode('c_show_runtimes'
		no_expr lam = [ '__global.trace_runtimes = 1' ';' ]
	)
}

//  main

fun c_program declarea inits body {
	assert_array('c_program: body' body)

	ret opcode('program'
		no_expr
		lam = [
			'#define CARG_MAX ' +CARG_MAX flatter.eol
			'#define CVAR_PREFIX ' CVAR_PREFIX flatter.eol
			'#define CARG_PREFIX ' CARG_PREFIX flatter.eol
			'#define CARG_PTR ' CARG_PTR flatter.eol
			'#define CARG_COUNT ' CARG_COUNT flatter.eol
			'#define CARG_ENV ' CARG_ENV flatter.eol
			macro_gen()
			'#include <ru.c>' flatter.eol
			statems(declarea)

			'int main(int argc, char**argv)' '{'
				'__stack_trace_init(main, &&end)' ';'
				'__runtime_init(argc, argv)' ';'
				'__enter()' ';'
				//'#include "playground.c"' flatter.eol
				statems(inits)
				statems(body)
				'__leave()' ';'
				'__runtime_end()' ';'
				'end:' ';'
				//'printf("DONE\\n")' ';'
				'return 0' ';'
			'}'
		]
	)
}

