/*
	- make json_mode features individually selected
	- make sure parse nil/null both works
	-
*/
//#include "oldprint.c"

val __glob_str(char *c) {
	val re = __str(c);
	__global_roots_add(re);
	return re;
}

#define shame_base_ref "#"
#define shame_ptr_ref "*"
#define shame_base_ref_char '#'
#define shame_ptr_ref_char '*'
#define shame_delim "," // todo make just space

struct {
	val
		new_line, open_line, open, close, aopen_line,
		aopen, aclose, space, empty,
		quote_strings, show_dup_strings,
		condensed, json_mode, indent,
		cb, naked_filter, brace_base_ref;
} __shams;


void __shams_init() {
	static int _init = 0;
	if (_init == 1) return;
	_init = 1;
	__shams.new_line = __glob_str("\n");
	__shams.open_line = __glob_str("{\n");
	__shams.open = __glob_str("{");
	__shams.close = __glob_str("}");
	__shams.aopen_line = __glob_str("[\n");
	__shams.aopen = __glob_str("[");
	__shams.aclose = __glob_str("]");
	__shams.space = __glob_str(" ");
	__shams.empty = __glob_str("");
	__shams.quote_strings = __glob_str("quote_strings"); // ok
	__shams.show_dup_strings = __glob_str("show_dup_strings");
	__shams.condensed = __glob_str("condensed"); // ok
	__shams.json_mode = __glob_str("json_mode"); // ok
	__shams.indent = __glob_str("indent"); // ok
	__shams.cb = __glob_str("filter");
	__shams.naked_filter = __glob_str("naked_filter");
	__shams.brace_base_ref = __glob_str("{" shame_base_ref);
}

struct dup_node {
	int pos;
	leaf *l;
	int id;
	struct dup_node *prev;
};

typedef struct {
	val dest; // serialization array of string
	//val vals; // current object's array of values

	int circular_count; // number of objects that are circular-referenced
	int need_comma;

	char quote_strings; //maybe instead quote strings inside array, and do not otherwise (obj props are already quoted always)
	char show_dup_strings; // if 1, will print all strings even if they are duplicate
	char condensed; // if 1 no spaces between elemets will be printed
	char json_mode; // if 1 nil->null, always quote keys
	char naked_filter; // do not quote values returned by filter callback

	dictionary *dico;

	val cb; // callback similar to  JSON.stringify() second arg
} __shame_ctx ;

__shame_ctx *__shame_ctx_new(val a) {
	__shame_ctx *ctx = malloc(sizeof(__shame_ctx));
	ctx->dico = 0;
	ctx->dest = a;
	ctx->circular_count = 1;
	ctx->condensed = 1;
	ctx->need_comma = false;
	return ctx;
}

void __shame_ctx_free(__shame_ctx *ctx) {
	if (ctx->dico) __dic_delete(ctx->dico);
	free(ctx);
}

int __shame_ctx_add(__shame_ctx *ctx, leaf *l) {
	if (ctx->dico == 0) ctx->dico = __dic_new(10);
	if (__dic_add(ctx->dico, &l, sizeof(leaf*))) {
		if (*ctx->dico->value == 0) {
			*ctx->dico->value = ctx->circular_count++;
		}
		else {
		}
		return 1;
	}
	else {
		// first time, set to zero
		*ctx->dico->value = 0;
		return 0;
	}
}

int __shame_ctx_find(__shame_ctx *ctx, leaf *l) {
	if (ctx->dico == 0) return -1;
	if (__dic_find(ctx->dico, &l, sizeof(leaf*))) {
		return *ctx->dico->value;
	}
	else {
		return -1; // used when filter returns new objects not originally found in the object
		//printf("leaf [%p] type: %s\n", l, leaf_name(l));
		//printf("impossible\n");
		//show_trace();
		//exit(0);
	}
	return -1;
}

void __swap_keys(keynode **a, keynode **b) {
	keynode *c = *a;
	*a = *b;
	*b = c;
}

void __print_key(keynode *c) {
	char *utf = malloc(c->len * 5);
	int end = w2utf(utf, c->len * 5, (uint16_t*)c->key, c->len/2);
	utf[end] = 0;
	printf("'%s'", utf);
	free(utf);
}

int64_t __cmp_keys(keynode *a, keynode *b) {
	int ret;
	if (a->len == b->len) {
		ret = memcmp(a->key, b->key, a->len) > 0;
	}
	else {
		ret = a->len > b->len;
	}
	//printf("                      "); __print_key(a); printf(ret ? " > " : " <= "); __print_key(b); printf("\n");
	return ret;
}

void __dual_quicksort_keys(keynode **left, keynode **right) {
	if (right <= left) return;
	if (__cmp_keys(*left, *right))
		__swap_keys(left, right);
	keynode
		**p = left,
		**q = right,
		**l = left + 1,
		**g = right - 1,
		**k = l;
	while (k <= g) {
		__loop_mark();
		if (__cmp_keys(*p, *k)) {
			__swap_keys(k, l++);
		}
		else if (__cmp_keys(*k, *q)) {
			while (__cmp_keys(*g, *q) && k < g) g--;
			__swap_keys(k, g--);
			if (__cmp_keys(*p, *k)) __swap_keys(k, l++);
		}
		k++;
		__loop_release();
	}
	l--; g++;

	__swap_keys(left, l); __swap_keys(right, g);

	__dual_quicksort_keys(left, l - 1);
	__dual_quicksort_keys(l + 1, g - 1);
	__dual_quicksort_keys(g + 1, right);
}

//int depth = 0;

//void inden() {
	//for (int i = 0; i < depth * 4; i++) printf(" ");
//}

void dic_forEach_sorted(dictionary* dic, __dic_enum_func f, void *user) {
	if (dic->count == 0) return;
	keynode **keys = malloc(dic->count * sizeof(keynode*));
	keynode **p = keys, **keys_end = keys + dic->count;
	for (int i = 0; i < dic->length; i++) {
		if (dic->table[i] != 0) {
			keynode *k = dic->table[i];
			while (k) {
				*p++ = k;
				k = k->next;
			}
		}
	}
	__dual_quicksort_keys(& keys[0], & keys[dic->count - 1]);
	p = keys;
	while (p < keys_end) {
		keynode *c = *p;
		if (!f(c->key, c->len, &c->value, user)) {
			break;
		}
		p++;
	}
	free(keys);
}

void __append_cstr(val dest, char *text) { // only used to add delimiters
	//todo replace with __shames.const
	__array_push(dest, __str(text));
}

val __shame_escape_string_normal(val a) {
	__string_compact(a);
	leaf *al = __leaf(a);
	int size = al->size;
	uchar *S = al->s;

	char simple_case = 1;
	for (int j = 0; j < size; j++) switch (S[j]) {
		case '\n': case '\t': case '\b': case '\r': case '"': case '\\':
			simple_case = 0;
			break;
	}
	if (simple_case) {
		return a;
	}

	uchar *buf = malloc(sizeof(uchar) * size * 2);
	uchar *C = buf;

	for (int j = 0; j < size; j++) {
		uchar c = S[j];
		switch (c) {
			case '\n': { *C++ = '\\'; *C++ = 'n'; break; }
			case '\t': { *C++ = '\\'; *C++ = 't'; break; }
			case '\b': { *C++ = '\\'; *C++ = 'b'; break; }
			case '\r': { *C++ = '\\'; *C++ = 'r'; break; }
			case '\\':
			case '"': { *C++ = '\\'; *C++ = c; break; }
			default: {
				*C++ = c;
			}
		}
	}
	val ret = __string_uchar(buf, C - buf);
	free(buf);
	return ret;
}

int __is_pure_id(val a) {
	leaf *al = __leaf(a);
	int size = al->size;
	uchar *S = al->s;

	for (int j = 0; j < size; j++) {
		uchar c = S[j];
		if (j == 0) {
			if ((c>='a' && c<='z') || (c>='A' && c<='Z') || (c=='_')) continue;
		}
		else {
			if ((c>='a' && c<='z') || (c>='A' && c<='Z') || (c>='0' && c<='9') || (c=='_')) continue;
		}
		return false;
	}
	return true;
}

int __shame_put_property(void *key, int keyn, int *value, __shame_ctx *ctx) {
	__enter();
	val key_str = __nil;
	val v = *(val*)value;

	val repl = __nil;

	void __shame_put(val a, __shame_ctx *ctx);

	key_str = __string_uchar((uchar*)key, keyn/2);
	key_str = __shame_escape_string_normal(key_str);

	int skip = false;

	if (__is_fun(ctx->cb)) {
		repl = __call(__leaf(ctx->cb)->f, key_str, v);

		if (!__is_nil(v) && __is_nil(repl)) {
			skip = true;
		}
		if (__is_string(repl)) {
			__string_compact(repl);
		}
		if (__is_heap(repl)) {
			leaf *l = __leaf(repl);
			if (__shame_ctx_find(ctx, l) == -1) {
				__shame_ctx_add(ctx, l);
			}
		}
	}

	if (!skip) {
		if (ctx->need_comma) {
			__append_cstr(ctx->dest, shame_delim);
			ctx->need_comma = false;
		}
		if (!ctx->condensed) __append_cstr(ctx->dest, " ");

		char unquoted = __is_pure_id(key_str);

		if (ctx->json_mode || !unquoted) __append_cstr(ctx->dest, "\"");
		__array_push(ctx->dest, key_str);
		if (ctx->json_mode || !unquoted) __append_cstr(ctx->dest, "\"");
		if (ctx->condensed) {
			__append_cstr(ctx->dest, ":");
		}
		else {
			__append_cstr(ctx->dest, ": ");
		}
		if (!__is_nil(repl)) {
			if (ctx->naked_filter && __is_string(repl)) {
				__array_push(ctx->dest, repl);
			}
			else {
				__shame_put(repl, ctx);
			}
			ctx->need_comma = true;
		}
		else {
			__shame_put(v, ctx);
			ctx->need_comma = true;
		}
	}
	__leave();
	return 1;
}

void __shame_put(val a, __shame_ctx *ctx) {
	int id = -1;
	leaf *l = 0;
	__enter();
	if (__is_heap(a)) {
		l = __leaf(a);
		int n = __shame_ctx_find(ctx, l);
		if (n < 0) {
			if (ctx->show_dup_strings && __is_string(a));
			else {
				if (!ctx->json_mode) {
					__array_push(ctx->dest, __string_concat(__str(shame_ptr_ref), __int_to_str((-n) - 1)));
					__leave();
					return;
				}
			}
		}
		else {
			id = n;
		}
	}
	if (__is_num(a)) {
		char t[32];
		t[snprintf(t, 32, "%.17G", __get(a))] = 0;
		val num = __str(t);
		__array_push(ctx->dest, num);
	}
	else if (__is_string(a)) {
		__string_compact(a);

		if (ctx->quote_strings == 1) __append_cstr(ctx->dest, "\"");
		val s = __shame_escape_string_normal(a);
		__array_push(ctx->dest, s);
		if (ctx->quote_strings == 1) __append_cstr(ctx->dest, "\"");

		if (id > 0 && !ctx->show_dup_strings && !ctx->json_mode) {
			*ctx->dico->value = -(*ctx->dico->value);
			__array_push(ctx->dest, __string_concat(__str(shame_base_ref), __int_to_str(id-1)));
		}
	}
	else if (__is_array(a)) {
		int size = __len(a);
		if (size == 0) {
			__append_cstr(ctx->dest, "[]");
		}
		else {
			__array_compact(a);
			if (id > 0) {
				*ctx->dico->value = -(*ctx->dico->value);
				__array_push(ctx->dest, __concat(__str("[" shame_base_ref), __int_to_str(id-1), __str(" ")));
			}
			else {
				__append_cstr(ctx->dest, "[");
			}
			val *p = __leaf(a)->p;
			//printf("size: %lu\n", size);
			for (int i = 0; i < size; i++) {
				__loop_mark();
				if (i > 0) __append_cstr(ctx->dest, shame_delim);
				__shame_put(p[i], ctx);
				__loop_release();
			}
			__append_cstr(ctx->dest, "]");
		}
	}
	else if (__is_nil(a)) {
		if (ctx->json_mode) {
			__append_cstr(ctx->dest, "null");
		}
		else {
			__append_cstr(ctx->dest, "nil");
		}
	}
	else if (__is_fly(a)) {
		__throw(__str("internal error: __shame_put->__is_fly is TRUE"));
		//__append_cstr(ctx->dest, "heapvar:");
		//__shame_put(__ref(a), ctx);
	}
	//else if (cell_is_val(*a)) {
		////__append_cstr(ctx->dest, "val:");
		//__shame_put(l->v, ctx);
	//}
	else if (__is_fun(a)) {
		__append_cstr(ctx->dest, "function");
	}
	else if (__is_object(a)) {
		if (id > 0) {
			*ctx->dico->value = -(*ctx->dico->value);
			__array_push(ctx->dest, __string_concat(__shams.brace_base_ref, __int_to_str(id-1)));
			if (ctx->condensed) {
				__array_push(ctx->dest, __shams.space);
			}
		}
		else {
			if (__object_size(a) == 0) {
				__append_cstr(ctx->dest, "{}");
				__leave();
				return ;
			}
			__append_cstr(ctx->dest, "{");
		}
		int old_comma = ctx->need_comma;
		ctx->need_comma = false;
		dic_forEach_sorted(
			l->d,
			(__dic_enum_func) __shame_put_property,
			(void*) ctx
		);

		if (!ctx->condensed) __append_cstr(ctx->dest, " ");

		ctx->need_comma = old_comma;
		__append_cstr(ctx->dest, "}");
	}
	//else if (__is_handle(*a)) {
		//printf("<C_handle>");
	//}
	else if (__is_rec(a)) {
		__append_cstr(ctx->dest, "rec");// TO BE REMOVED
		val b = __rec_to_object(a);
		__shame_ctx_add(ctx, __leaf(b));
		__shame_put(b, ctx);
	}
	else {
		printf("<pv_none>");
	}
	__leave();
}

void __shame_pass1(val a, __shame_ctx *ctx) {
	leaf *l;
	if (__is_heap(a)) {
		l = __leaf(a);
		if (__shame_ctx_add(ctx, l)) return;
	}
	if (__is_array(a)) {
		__iterate_array(my, a) {
			__shame_pass1(my_items[my_index], ctx);
		}
	}
	else if (__is_fly(a)) {
		__throw(__str("internal error: __shame_pass1->__is_fly is TRUE"));
		//__shame_pass1(__ref(a), ctx);
	}
	//else if (__is_val(a)) {
		//__shame_pass1(l->v, ctx);
	//}
	else if (__is_object(a)) {
		__iterate_array(my, __object_values(a)) {
			__shame_pass1(my_items[my_index], ctx);
		}
	}
}

val __indent_str(__size_t count) {
	__enter();
	val s = __str("");
	while (__len(s) < count) {
		s = __string_concat(s, __str(" "));
	}
	__leave();
	__return(s);
}

void __indentify(val a, int indent_step) {
	int indent = 0;
	int size = __len(a);
	for (int i = 0; i < size; i++) {
		val line = __array_get(a, i);
		if (__cmp_str(line, __shams.open) == 0) {
			indent += indent_step;
			__array_set(a, i, __string_concat(__str("{\n"), __indent_str(indent)));
		}
		else if (__cmp_str(line, __shams.close) == 0) {
			indent -= indent_step;
			if (indent < 0) {
				printf("warning: indent negative %i\n", indent);
				indent = 0;
			}
			__array_set(a, i, __string_concat(__string_concat(__str("\n"), __indent_str(indent)), __str("}")));
		}
		else if (__cmp_str(line, __str("[")) == 0) {
			indent += indent_step;
			__array_set(a, i, __string_concat(__str("[\n"), __indent_str(indent)));
		}
		else if (__cmp_str(line, __str("]")) == 0) {
			indent -= indent_step;
			__array_set(a, i, __string_concat(__string_concat(__str("\n"), __indent_str(indent)), __str("]")));
		}
	}
}

typedef struct {
	int quote_strings;
	int show_dup_strings;
	int condensed;
	int json_mode;
	int indent;
	int naked_filter;
	val cb;
} __shame_opt;

void __shame_set_defaults(__shame_opt *opt, int json_mode) {
	opt->quote_strings = 1;
	opt->indent = 0;
	opt->cb = __nil;
	opt->naked_filter = 0;
	opt->condensed = 1;
	if (json_mode) {
		opt->show_dup_strings = 1;
		opt->json_mode = 1;
	}
	else {
		opt->show_dup_strings = 1;
		opt->json_mode = 0;
	}
}

val __shame_serialize(val v, __shame_opt *opt) {
	__enter();
	__shams_init();
	val delim, ret, dest = __array();
	int indent;

	__shame_ctx *ctx = __shame_ctx_new(dest);
	if (opt) {
		ctx->quote_strings = opt->quote_strings;
		ctx->show_dup_strings = opt->show_dup_strings;
		ctx->condensed = opt->condensed;
		ctx->json_mode = opt->json_mode;
		ctx->cb = opt->cb;

		ctx->naked_filter = opt->naked_filter;
		indent = opt->indent;
	}
	else {
		ctx->quote_strings = 1;
		ctx->show_dup_strings = 1;
		ctx->condensed = 0;
		ctx->json_mode = 0;
		ctx->cb = __nil;
		ctx->naked_filter = 0;
		indent = 0;
	}
	__shame_pass1(v, ctx);
	__shame_put(v, ctx);
	__shame_ctx_free(ctx);

	delim = __str_empty();
	__array_compact(dest);
	if (indent > 0) {
		__indentify(dest, indent);
		ret = __array_join(dest, delim);
	}
	else {
		ret = __array_join(dest, delim);
	}
	__string_compact(ret);
	__leave();
	return __rooted(ret);
}

void __shame_read_options(val var_opt, __shame_opt *opt) {
	__enter();
	__shams_init();

	if (!__is_object(var_opt)) {
		__throw(__str("shame options must be object"));
	}

	val indent = __property_get(var_opt, __str("indent"));
	if (indent != __nil) {
		if (__is_num(indent)) {
			opt->indent = __get_i(indent);
		}
		else __throw(__str("shame options.indent must be a number"));
	}
	val condensed = __property_get(var_opt, __str("condensed"));
	if (condensed != __nil) {
		if (__is_num(condensed)) {
			opt->condensed = __get_i(condensed);
		}
		else __throw(__str("shame options.condensed must be boolean"));
	}

	val json_mode = __property_get(var_opt, __str("json_mode"));
	if (json_mode != __nil) {
		if (__is_num(json_mode)) {
			opt->json_mode = __get_i(json_mode);
		}
		else __throw(__str("shame options.json_mode must be boolean"));
	}

	val quote_strings = __property_get(var_opt, __str("quote_strings"));
	if (quote_strings != __nil) {
		if (__is_num(quote_strings)) {
			opt->quote_strings = __get_i(quote_strings);
		}
		else __throw(__str("shame options.quote_strings must be boolean"));
	}

	val show_dup_strings = __property_get(var_opt, __str("show_dup_strings"));
	if (show_dup_strings != __nil) {
		if (__is_num(show_dup_strings)) {
			opt->show_dup_strings = __get_i(show_dup_strings);
		}
		else __throw(__str("shame options.show_dup_strings must be boolean"));
	}

	val naked_filter = __property_get(var_opt, __str("naked_filter"));
	if (naked_filter != __nil) {
		if (__is_num(naked_filter)) {
			opt->naked_filter = __get_i(naked_filter);
		}
		else __throw(__str("shame options.naked_filter must be boolean"));
	}

	val cb = __property_get(var_opt, __str("filter"));
	if (cb != __nil) {
		if (__is_fun(cb)) {
			opt->cb = cb;
		}
		else __throw(__str("shame options.cb must be function"));
	}

	__leave();
}

val __shame(val source, val options) {
	__enter();
	__shame_opt opt;
	__shame_set_defaults(&opt, 0);

	if (__is_object(options)) {
		__shame_read_options(options, &opt);
	}
	val re = __shame_serialize(source, &opt);
	return __rooted(re);
}

val __json(val source, val options) {
	__enter();
	__shame_opt opt;
	__shame_set_defaults(&opt, 1);

	if (__is_object(options)) {
		__shame_read_options(options, &opt);
	}
	val re = __shame_serialize(source, &opt);
	return __rooted(re);
}

uchar *__shame_skip_spaces(uchar *C, uchar *E) {
	for (;;) {
		if (C == E) break; //__throw(__str("parse shame: unexpected end of input"));
		if (*C > ' ') break;
		C++;
	}
	return C;
}

uchar *next_char(uchar *C, uchar *E) {
	C++;
	if (C == E) {
		__throw(__str("parse shame: unexpected end of input"));
	}
	return C;
}

uchar *parse_escape_allocate_string(uchar *C, uchar *E, val *ret) {
	uchar q = C[0], *src = C + 1;
	int backslashes_to_remove = 0;
	for (;;) {
		C = next_char(C, E);
		if (*C == '\\') {
			C = next_char(C, E);
			backslashes_to_remove++;
		}
		else if (*C == q) {
			break;
		}
	}
	int n = C - src - backslashes_to_remove;
	*ret = __string_alloc(n);
	uchar *d = __leaf(*ret)->s;
	while (src < C) {
		if (*src == '\\') {
			src++;
			if (*src == 'r') *d = '\r';
			if (*src == 'n') *d = '\n';
			if (*src == 't') *d = '\t';
			if (*src == 'b') *d = '\b';
			else *d = *src;
		}
		else {
			*d = *src;
		}
		src++;
		d++;
	}
	return C;
}

val __parse_shame(uchar *C, uchar *E, uchar **C_new, val ctx) {
	__enter();
	val ret = __nil, a, b, name, value;
	C = __shame_skip_spaces(C, E);
	while (C < E) {
		__loop_mark();
		uchar c = *C;
		if (c <= ' ') {
			;
		}
		else if (c == shame_ptr_ref_char) {
			C = next_char(C, E);
			int num = 0;
			while (*C >= '0' && *C <= '9') {
				num *= 10;
				num += (*C - '0');
				C = next_char(C, E);
			}
			*C_new = C;
			if (num >= __len(ctx)) __throw(__str("parse shame: object reference index out of bounds"));
			__leave();
			return __array_get(ctx, num);
		}
		else if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '_') {
			// parse nil (or other supported identifiers in the future)
			uchar *id_start = C;
			C++;
			c = *C;
			while ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '_') {
				C++;
				c = *C;
			}
			if (C - id_start == 3) {
				if (id_start[0] == 'n')
				if (id_start[1] == 'i')
				if (id_start[2] == 'l') {
					*C_new = C;
					__leave();
					return ret;
				}
			}
			if (C - id_start == 4) {
				if (id_start[0] == 'n')
				if (id_start[1] == 'u')
				if (id_start[2] == 'l')
				if (id_start[3] == 'l') {
					*C_new = C;
					__leave();
					return __nil;
				}
			}
			if (C - id_start == 4) {
				if (id_start[0] == 't')
				if (id_start[1] == 'r')
				if (id_start[2] == 'u')
				if (id_start[3] == 'e') {
					ret = __let(1);
					*C_new = C;
					__leave();
					return __nil;
				}
			}
			if (C - id_start == 5) {
				if (id_start[0] == 'f')
				if (id_start[1] == 'a')
				if (id_start[2] == 'l')
				if (id_start[3] == 's')
				if (id_start[4] == 'e') {
					ret = __let(0);
					*C_new = C;
					__leave();
					return ret;
				}
			}
			__throw(__str("parse shame: unexpected identifier in shame"));
		}
		else if ((c == '-' && (C[1] >= '0' && C[1] <= '9')) || (c >= '0' && c <= '9')) {
			char t[32], *tt = t;
			uchar *T = C;
			char neg = false;
			if (c == '-') {
				neg = true;
				T++;
				C++;
			}
			for (int i = 0; i < 31; i++) {
				if (T >= E) break;
				*tt++ = *T++;
			}
			*tt = 0;
			uchar *C1;
			char *t1;
			double x = strtold(t, &t1);
			if (neg) x = -x;
			C = C + (t1 - t);
			if (C > E) __throw(__str("__parse_shame: unexpected end of input"));
			*C_new = C;
			__leave();
			return (double)(int)(x) == x
				? __let_i((int)x)
				: __rooted(__let(x));
		}
		else if (c == '"' || c == '\'') {
			C = parse_escape_allocate_string(C, E, &ret);
			C++;
			if (C < E) C = __shame_skip_spaces(C, E);
			if (*C == shame_base_ref_char) {
				C = next_char(C, E);
				int num = 0;
				while (*C >= '0' && *C <= '9') {
					num *= 10;
					num += (*C - '0');
					C = next_char(C, E);
				}
				__array_set(ctx, num, ret);
			}
			*C_new = C;
			__leave();
			return __rooted(ret);
		}
		else if (c == '[') {
			a = __array();
			C = next_char(C, E);
			if (*C == shame_base_ref_char) {
				C = next_char(C, E);
				int num = 0;
				while (*C >= '0' && *C <= '9') {
					num *= 10;
					num += (*C - '0');
					C = next_char(C, E);
				}
				__array_set(ctx, num, a);
			}
			for (;;) {
				C = __shame_skip_spaces(C, E);
				if (*C == ']') {
					C++;
					break;
				}
				else {
					b = __parse_shame(C, E, &C, ctx);
					__array_push(a, b);
					//if (C >= E) __throw(__str("parse shame: unexpected end of input"));
				}
				int commas = 0;
				for (;;) {
					C = __shame_skip_spaces(C, E);
					if (*C == ',') {
						if (commas > 0) {
							__array_push(a, __nil);
						}
						commas++;
						C = next_char(C, E);
					}
					else break;
				}
			}
			*C_new = C;
			__leave();
			return a;
		}
		else if (c == '{') {
			C = next_char(C, E);
			if (C == E) __throw(__str("parse shame: unexpected end of input"));

			val obj = __object();

			if (*C == shame_base_ref_char) {
				C = next_char(C, E);
				int num = 0;
				while (*C >= '0' && *C <= '9') {
					if (C == E) __throw(__str("parse shame: unexpected end of input"));
					num *= 10;
					num += (*C - '0');
					C = next_char(C, E);
				}
				__array_set(ctx, num, obj);
			}

			// enumerate properties
			for (;;) {
				__loop_mark();
				C = __shame_skip_spaces(C, E);
				uchar c = *C, *begin;

				if (*C == '}') {
					C++; // no check
					break;
				}
				else if (c == '"' || c == '\'') {
					C = parse_escape_allocate_string(C, E, &name);
					C = next_char(C, E);
				}
				else {
					begin = C;
					for (;;) {
						uchar c = *C;
						if((c>='a'&&c<='z')||(c>='A'&&c<='Z')||(c>='0'&&c<='9')||c=='_') {
							C = next_char(C, E);
						}
						else {
							break;
						}
					}
					if (C == begin) {
						__throw(__str("parse shame: property name expected"));
					}
					name = __string_uchar(begin, C - begin);
				}
				C = __shame_skip_spaces(C, E);
				if (*C == ':') {
					C = next_char(C, E);
				}
				C = __shame_skip_spaces(C, E);

				value = __parse_shame(C, E, &C, ctx);
				__property_set(obj, name, value);

				C = __shame_skip_spaces(C, E);
				if (*C == ',') {
					C = next_char(C, E);
				}
				C = __shame_skip_spaces(C, E);
				__loop_release();
			}
			*C_new = C;
			__leave();
			return __rooted(obj);
		}
		else {
			int max_context = 32;
			uchar err_str[max_context + 1];
			int i = 0;
			while(i < max_context) {
				err_str[i++] = *C;
				C = next_char(C, E);
				if (C == E) break;
			}
			val err = __string_uchar(err_str, i);
			__throw(__concat(__str("parse shame:\n"), err, __str("\n^ unexpected token")));
		}
		C++;
		__loop_release();
	}
	__leave();
	return ret;
}

val __honor(val s) {
	if (!__is_string(s)) {
		__throw(__str("parse shame: operand is not a string"));
	}
	__enter();
	__string_compact(s);
	leaf *l = __leaf(s);
	uchar *C = l->s;
	uchar *E = C + l->size;
	val ctx = __array();
	val re = __parse_shame(C, E, &C, ctx);
	__leave();
	return __rooted(re);
}

