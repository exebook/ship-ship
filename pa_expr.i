fun make_operators {
	re := []
	re[Token['or']] = 5
	re[Token['and']] = 6
	re[Token['OR']] = 7
	re[Token['XOR']] = 7
	re[Token['AND']] = 9
	re[Token['==']] = 10
	re[Token['!=']] = 10
	re[Token['<']] = 11
	re[Token['>']] = 11
	re[Token['>=']] = 11
	re[Token['<=']] = 11
	re[Token['SHL']] = 12
	re[Token['SHR']] = 12
	re[Token['+']] = 13
	re[Token['-']] = 13
	re[Token['*']] = 14
	re[Token['/']] = 14
	re[Token['mod']] = 14
	re[Token['div']] = 14
	re[Token['mul']] = 14
	re[Token['pow']] = 15
	ret re
}

__precedence := nil

fun precedence a {
	init __precedence = make_operators()
	ret __precedence[a]
}

fun pa_operator {
	ret lam A {
		if A == 1 ret 'pa_operator'
		T := pos_tok(A)
		init T ret
		if precedence(Key(T)) {
			ret [pos_next(A) { type: types.operator op: Key(T) pos: A }]
		}
	}
}

fun pa_the target_pa {
	ret lam A {
		T := pos_tok(A)
		if T and Key(T) == Token['the'] {
			R := target_pa()(pos_next(A))
			if R {
				ret [ ret_pos(R) { pos: A type: types['the'] target: ret_ast(R)} ]
			}
			else {
				throw stop(A '<the> requires argument')
			}
		}
	}
}
__pa_base := nil
fun pa_base {
	init __pa_base = anyof([
		//pa_the(pa_operand)
		each_reference() pa_ident() pa_literal() group()
		pa_args() pa_this() pa_it() pa_that() pa_error()
	])
	ret __pa_base
}

fun check_is_target f {
	ret lam A {
		if A == 1 ret 'check_is_target'
		R := f(A)
		if R {
			B := ret_pos(R)
			S := symbol(Token['='])(B)
			if S and at_new_line(A) {
				ret
				//throw stop(A 'not sure if this is '+f(1)+'ing or a destructuring assignment')
			}
			S = symbol(Token[':='])(B)
			if S and at_new_line(A) {
				ret
				//throw stop(A 'not sure if this is '+f(1)+'ing or a variable declaration')
			}
			ret R
		}
	}
}

fun pa_range {
	ret lam A {
		if A == 1 ret 'range'

		R := expr()(A)
		if R {
			from := ret_ast(R)
			B := ret_pos(R)
		}
		else {
			from = nil
			B = A
		}
		T := pos_tok(B)
		if T {
			K := Key(T)
			if K == Token[':'] or K == Token['..'] {
				delim := K
				S := expr()(pos_next(B))
				if S {
					ret [ret_pos(S), { type: types.range delim, ix: from, range: ret_ast(S) pos: A }]
				}
				else {
					ret [pos_next(B), { type: types.range delim, ix: from, range: nil pos: A }]
				}
			}
			else {
				if from {
					ret [ret_pos(R), { type: types.indexing ix: from pos: A }]
				}
			}
		}
	}
}

fun pa_index {
	ret check_is_target(pa_make('index'
		lam A ast = ast
		embrace(Token['['] pa_range() Token[']'] 'expected index or range expression')
	))
}

fun pa_member {
	ret pa_make('member'
		lam A ast = { type: types.member id: ast[1].id pos: A }
		seq([ symbol(Token['.']) pa_ident() ])
	)
}

fun pa_call {
	ret pa_make('call' //check_is_target
		lam A ast {
			if ast[1] ast[0] += ast[1]
			ret { type: types.call args: ast[0] pos: A }
		}
		seq([
			embrace(Token['('] auto_list(Token[','] expr()) Token[')'] 'argument expected')
			maybe(auto_list(Token[','] pa_do()))
		])
	)
}

fun pa_BIT {
	//ret check_is_target(pa_make('BIT'
	ret (pa_make('BIT'
		lam A ast {
			ret { type: types.BIT expr: ast[1] pos: A }
		}
		seq([symbol(Token['BIT']) expr()])
	))
}

__value_cont := nil
fun value_cont {
	init __value_cont = anyof([pa_BIT() pa_index() pa_call() pa_member()])
	ret __value_cont
}

fun pa_value {
	ret lam A {
		if A == 1 ret 'value'

		//T := pos_tok(A)
		//if T and Key(T) == Token['the'] {
			//log 'OKTHE'
		//}
		R := pa_base()(A)
		if R {
			base := ret_ast(R)
			parent := base
			B := ret_pos(R)
			loop {
				R = value_cont()(B)
				init R break
				B = ret_pos(R)
				next := ret_ast(R)
				next.parent = parent
				parent = next
			}
			if base != parent and base.type == types.id {
				if base.id == 'nil'
				or base.id == 'false'
				or base.id == 'true'
					throw stop(base.pos tem(E013_1 base.id))
			}
			ret [B parent]
			// todo try just: ret next
			//ret [B { type: types.value head: parent, pos: A }]
		}
	}
}

fun group {
	ret pa_make('group' lam A x = x embrace(Token['('] expr() Token[')'] 'expression expected'))
}

fun unary {
	ret lam A {
		if A == 1 ret 'unary'
		T := pos_tok(A)
		if T {
			if Key(T) == Token['-']
			or Key(T) == Token['not']
			or Key(T) == Token['len']
			or Key(T) == Token['chr']
			//or Key(T) == Token['the']
			or Key(T) == Token['ord'] {
				re := pa_operand()(pos_next(A))
				if re {
					re[1] = { type: types.unary op: Key(T) expr: re[1] pos: A }
					ret re
				}
			}
		}
	}
}

fun each_reference {
	ret lam A {
		if A == 1 ret 'each_reference'
		T := pos_tok(A)
		if T {
			typ := nil  _for := 0
			if Key(T) == Token['item'] typ = types.item
			but Key(T) == Token['index'] typ = types.each_index
			but Key(T) == Token['item_for'] { typ = types.item _for = 1 }
			but Key(T) == Token['index_for'] { typ = types.each_index _for = 1 }
			else ret
			B := pos_next(A)
			T = pos_tok(B)
			if _for {
				if Key(T) == Token.Int {
					ret [pos_next(B) { type: typ, pos: A level: T[3] }]
				}
			}
			ret [pos_next(A) { type: typ, pos: A level: 0 }]
			//if Key(T) == Token['ekey'] ret [pos_next(A) { type: types.ekey pos: A }]
		}
	}
}

__pa_args := nil
fun pa_args {
	init __pa_args = pa_make('pa_args'
		lam A ast {
			ret { type: types.args pos: A }
		}
		seq([symbol(Token['args'])])
	)
	ret __pa_args
}

__pa_this := nil
fun pa_this {
	init __pa_this = pa_make('pa_this'
		lam A ast {
			ret { type: types['this'] pos: A }
		}
		symbol(Token['this'])
	)
	ret __pa_this
}

__pa_it := nil
fun pa_it {
	init __pa_it = pa_make('pa_it'
		lam A ast {
			ret { type: types['that'] pos: A }
		}
		symbol(Token['that'])
	)
	ret __pa_it
}

__pa_that := nil
fun pa_that {
	init __pa_that = pa_make('pa_that'
		lam A ast {
			ret { type: types['it'] pos: A }
		}
		symbol(Token['it'])
	)
	ret __pa_that
}

__pa_error := nil
fun pa_error {
	init __pa_error = pa_make('pa_error'
		lam A ast {
			ret { type: types['error'] pos: A }
		}
		symbol(Token['error'])
	)
	ret __pa_error
}

fun charmap_start_pos {
	ret lam A {
		if A == 1 ret 'charmap_start_pos'
		T := pos_tok(A)
		if T {
			if Key(T) == Token['{'] {
				ret [A { pos: A type: types.number num: 0 }]
			}
		}
		R := expr()(A)
		if R ret R
		ret [A nil]
	}
}

fun pa_charmap_enums {
	ret pa_make('pa_charmap_enums'
		lam A b {
			if len b == 0 ret nil
			ret b
		}
		_pa_loop(Token[','] false pa_ident())
	)
}

fun pa_charmap_strings {
	ret pa_make('pa_charmap_strings'
		lam A b {
			strings := []
			each b {
				strings += { tokens: map(item[0] lam x = x.str) result: item[2]}
			}
			ret strings
		}
		_pa_loop(Token[','] false
			seq([
				_pa_loop(Token[','] false pa_string())
				symbol(Token[':'])
				expr()
			])
		)
	)
}

fun pa_charmap = pa_make('charmap'
	lam A b {
		ret {
			pos: A
			type: types.charmap
			iterable: b[1]
			start: b[2]
			tokens: b[3]
		}
	}
	seq([
		symbol(Token['charmap'])
		expr()
		charmap_start_pos()
		embrace(
			Token['{']
			anyof([
				pa_charmap_enums()
				pa_charmap_strings()
			])
			Token['}']
			'keyword or enum expected'
		)
	])
)

fun pa_tee {
	ret lam A {
		T := pos_tok(A)
		if T and Key(T) == Token.tee {
			R := statem()(pos_next(A))
			if R {
				ret [ ret_pos(R) { pos: A type: types.tee set: ret_ast(R)} ]
			}
			else {
				throw stop(pos_next(A).pos 'assignment expected')
			}
		}
	}
}

__operand := nil
fun pa_operand {
	if __operand == nil {
		__operand = anyof([
			//each_reference()
			pa_the(pa_operand)
			unary() pa_value() group() if_ex() pa_lam()
			//pa_arg() pa_args()
			pa_charmap() pa_langc() pa_tee()
		])
	}
	ret __operand
}

__expr := nil
fun expr {
	init __expr = lam A {
		if A == 1 ret 'expression'
		R := pa_operand()(A)
		if R {
			p := ret_ast(R)
			B := ret_pos(R)
			R = __expr_cont(B, [p] A)
			init R = [B p]
			ret R
		}
	}
	ret __expr
}

fun __expr_cont A code A_start {
	if A == 1 ret 'expr_cont'
	stack := []
	B := A
	loop {
		R := pa_operator()(B)
		init R break
		op := ret_ast(R)
		B = ret_pos(R)
		R = pa_operand()(B)
		init R break
		B = ret_pos(R)
		next := ret_ast(R)
		prec := precedence(op.op)
		while len stack > 0 and precedence(stack[-1].op) >= prec {
			code += pop stack
		}
		stack += op
		code += next
	}
	while len stack > 0 {
		code += pop stack
	}
	if len code == 1
		ret [B code[0]]
	else
		ret [B { type: types.expr stack: code pos: A_start }]
}
