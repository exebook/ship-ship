#define __is_heap(a) (!__is_int(a) && !__is_nil(a))
#define __leaf(a) ((leaf*)(a & PTR_MASK))
#define __mk_val(leaf_p) ((val)leaf_p | HEAP_FLAG)

enum { leaf_big, leaf_fun, leaf_str, leaf_arr, leaf_obj, leaf_pchar, leaf_fly, leaf_hold, leaf_rec };

typedef struct {
	// count of env vars is stored in leaf->size
	__size_t bind; // 0: not bound, otherwise number of args + 1, where 1 for this
	val p[]; // example layout { bind: 4, p: [e1 e2 this a1 a2 a3] }
} __env_t;

typedef val (*__lam_t)(__arg_decl);
//typedef val (*__lam_t)(__env_t *_env, short _acount, val _a0,val _a1,val _a2, val *_aptr);

struct leaf_t {
	unsigned int type: 4, mark: 1, pair: 1;//, is_static: 1;//, each_progress: 1, flag_b: 1;
	__size_t size;
	//__gc_debug__(int debug_size;)
	struct leaf_t* next;
	union {
		struct { double big; }; // number
		struct { __lam_t f; __env_t *env; }; // function, size + this + env->bind
		//struct { val a, b; }; // tuple
		struct { val *p; __size_t capacity; }; // array
		struct { dictionary *d; int hidden_type; };
		struct { uchar *s; }; //string
		struct { struct leaf_t *left, *right; }; // string pair
		struct { char *utf; }; // leaf_pchar: utility type for C interop, returned by __to_utf
		struct { val fly; }; // leaf_fly
		struct { val *hold; }; // leaf_hold, for GC to reach some variables in loops
		struct { val *rec; int rectype; }; // record
	};
	val items[]; // rec items
};

typedef struct leaf_t leaf;

char *__type_to_str(char type) {
	char *a[] = {"number", "fun", "string", "array", "object", "__utf", "__fly", "__hold", "rec"};
	if (type < 0 || type >= sizeof(a) / sizeof(char*)) {
		return "out-of-range";
	}
	return a[type];
}

#define __type_to_char(type) type == 0 ? 'n' : type == 1 ? 'f' : type == 2 ? 's' : type == 3 ? 'a' : type == 4 ? 'o' : '?'

char *__type_of_leaf(leaf *l) {
	return __type_to_str(l->type);
}

char *__p_typeof(val a) {
	return __is_nil(a) ? "nil" : __is_heap(a) ? __type_of_leaf(__leaf(a)) : "number";
}

void __mutate(leaf *dest, leaf *src) {
	// this cannot be used with records because they have a varying size
	// do we need to mutate records anyway?
	leaf temp;
	temp = *dest;
	*dest = *src;
	*src = temp;

	// this corrupts memory when __hold refers to dest or src
	//leaf *next = dest->next;
	//*dest = *src;
	//dest->next = next;
}

