incl shamefmt comp argv

TEMP_C_FILE := '.last.c'
TEMP_BIN_FILE := './.last'
incl_search_dirs := ['.' '/v/self/incl']
process_args()

//fun randname dir {
	//r := nil
	//begin
		//$ r = cell_let(random());
	//end
	//ret dir + '/tmp' + r + '.c'
//}

fun process_args {
	A := load_argv(['args a' 'o' 'exec x'], [
		'help h'
		'show-ast-only A'
		'show-tokens-only T'
		'runtime-traces Q'
		'trace-disable'
		'c'
		'test-errors'
		'rand-cache'
	])

	if A.keys.help or len A.list == 0 and A.keys['exec'] == nil {
		show_arg_help('   '
			'-a --args|pass arguments to the program'
			'-h --help'
			'-A --show-ast-only|parse, print AST, exit'
			'-T --show-tokens-only|parse, print tokenizer result, exit'
			'--trace-disable|disable stack traces'
			'|useful when your computer has no GDB'
			'-c|compile only'
			'-o <file>|output'
			'--rand-cache|create random temp file instead of '+TEMP_C_FILE
			'-- everything after -- is passed to C compiler'
			'-x --exec <code>|execute code'
		)
		kill
	}


	options := {
		signals: true
	}

	if A.keys.o TEMP_BIN_FILE = A.keys.o[0]
	if A.keys['rand-cache'] {
		//tmp_name := randname('/v/ship/.ptest')
		//begin
			//char *c = tempnam("/v/ship/.ptest", "i-tmp");
			//printf("TEMP: %s\n", c);
			//$ tmp_name = new_string_from_utf(c);
		//end
		TEMP_C_FILE = '/v/ship/.ptest/tmp' +A.list[0]+ '.c'
		//tmp_name + '.c'
	}
	if A.keys['test-errors'] {
		E00X_setup_for_tests()
		options.signals = true
		options.test_mode = true
		incl_search_dirs += '/v/ship/.ptest/' + A.list[0] / '.i' * '.d'
	}

	options['compile-only'] = A.keys['c'] != nil
	options['show-ast-only'] = A.keys['show-ast-only'] != nil
	options['show-tokens-only'] = A.keys['show-tokens-only'] != nil
	options.show_runtimes = A.keys['runtime-traces'] != nil
	options.trace_disable = A.keys['trace-disable'] != nil

	if the A.keys['exec'] {
		src_string = it * ' '
		src_name = 'exec'
	}
	else {
		src_name := A.list[0]
		src_string := load(A.list[0])
		if src_string[0:2] == '#!' {
			src_string = src_string['\n' find src_string..]
		}
	}

	if options['show-ast-only'] {
		//log 'PLEASE FIX THIS'
		ast := ret_ast(parse_source(lam{}, A.list[0] src_string lam { ret [] }))
		ast = honor(nodestr(ast))

		json := shame(ast, {
			json_mode:true
			filter: lam k v {
				//are k v
				if k == 'pos' ret v[0]
				ret v
			}
		})
		log json
		//load('json')
		////shamefmt(ast)
		//prn_ast(ast 0)
		kill
	}

	if options['show-tokens-only'] {
		log 'PLEASE FIX THIS'
		//each tokens log token_str(item)
		kill
	}

	options.argv = A

	if len src_string > 0 {
		compile_exec(options src_name src_string)
	}
	else {
		log 'no input!'
	}
}

fun get_fsize path {
	re := nil
	begin
		char *n = ship_pchar($ path);
		FILE *f;
		f = fopen(n, "rb");
		free(n);
		if (f) {
			fseek(f, 0, 2);
			int t;
			t = ftell(f);
			fseek(f, 0, 0);
			fclose(f);
			$ re = cell_let(t);
		}
	end
	ret re
}


fun incl_loader pos fname {
	fname += '.i'
	each incl_search_dirs {
		path := item + '/' + fname
		n := get_fsize(path)
		if n != nil {
			src := load(path)
		}
	}
	init src {
		throw stop(pos tem('file <^> not found' fname))
	}
	tokens := tinylex(fname src)
	ret tokens
}

fun compile_exec options fname src {
	try {
		program_code := compile(fname src TEMP_C_FILE options incl_loader)
		save(TEMP_C_FILE program_code)
		cmdl := 'tcc -g -o '
			+ TEMP_BIN_FILE + ' -I. -I/v/ship '
			+ TEMP_C_FILE + ' ' + options.argv.raw * ' '
		r := system_exec(cmdl)
		if r == 0 {
			aa := []
			if len options.argv.list > 1 {
				// script mode, pass all arguments to a callee
				aa += ' '
				each options.argv.keys {
					nm := cle
					if is_array(item) each item aa += '-' + nm + ' "' + item + '"'
					if is_number(item) aa += '-' + nm
				}
				aa += map(options.argv.list[1..] lam a = a)
			}
			if not options['compile-only'] {
				bin_f := TEMP_BIN_FILE
				if (bin_f find '/') < 0 bin_f = './' + bin_f
				system_exec(bin_f + aa*' ')
			}
		}
		else {
			log 'C compilation failed'
		}
	}
	catch {
		try {
			e := error
			init e {
				log 'unidentified error'
				kill
			}
			if e.kind == nil {
				if e.msg log e.msg
				else log e
				ret
			}
			but e.kind == 'stop' {
				log show_stop(e)
			}
			but e.kind == 'dev' {
				log error.msg
			}
		}
		catch {
			log 'internal error in error handler: ' + error.msg
		}
		if not options.test_mode {
			begin exit(1); end
		}
		else {
			begin exit(0); end
		}
	}
}
