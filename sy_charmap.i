fun c_charmap ptr start tokens results {
	ret opcode('charmap'
		lam {
			tree := {}
			max_length := 0
			each tokens {
				if len item > max_length max_length = len item
				t := item
				p := tree
				C := results[index].expr()
				each t {
					c := item
					init p[c] = {}
					if index == len t - 1 {
						p[c].result = C
					}
					p = p[c]
				}
			}
			max_length += ''
			ptr_val := 'p'
			start_val := 'start'
			code := [
				'({' flatter.eol
					'int start = __get_i(' start.expr() ')' ';'
					'val iterable = ' ptr.expr() ';'
					'int ilen = __len(iterable)' ';'
					'uchar *p = __uchar(iterable)' ';'
					'val re = __nil' ';'
					'if (ilen > start)' '{'
						'if (start + ' max_length ' > ilen) ' '{'
							'uchar p1[' max_length ']' ';'
							'int fill = ilen - start' ';'
							'for (int i = 0; i < fill; i++) p1[i] = p[start+i]' ';'
							'for (int i = fill; i < 'max_length'; i++) { p1[i] = 0; }' ';'
							'p = p1' ';'
						'}'
						're = ' dive(tree 0, "__nil") ';'
					'}'
					're' ';'
				'})'
			]

			ret code

			fun dive tree depth result {
				re := []
				each keys(tree) if item != 'result'
					re += [
						ptr_val '[' _add(start_val depth) '] == ' item flatter.eol
						' ? ' dive(tree[item] depth + 1 tree[item].result)
						' : '
					]
				init result = "__nil"
				ret [re result]

				fun _add a b { if b == 0 ret a ret [a '+' b+''] }
			}
		}
		lam = ''
	)

}

fun sy_charmap up node {
	list := node.tokens
	list_str := []
	list_res := []
	iterable := sy_expr(up node.iterable)
	start := sy_expr(up node.start)

	if len list == 0 ret lam m {
		if m == mode.CODE ret c_con("-1")
	}

	if list[0].type == types.id {
		// enums
		each list {
			if item.type != types.id
				throw stop(item.pos tem(E009_0))
		}
		up(uplink.AFTER lam pass {
			if pass == 0 {
				each list {
					e := up(uplink.ENUM_GET item.id)
					init e throw stop(item.pos tem('<^> is not an enum' item.id))
					enu := up(uplink.ENUM_GET item.id)
					source := item.pos
					each enu.items {
						list_str += cle
						list_res += sy_expr(up { pos: source type: types.number num: item })
					}
				}
			}
		})
	}
	else {
		// strings
		each list {
			r := sy_expr(up item.result)
			each item.tokens {
				list_str += item
				list_res += r
			}
		}
	}

	ret lam m {
		if m == mode.CODE {
			each list_res item = item(m)
			ret c_charmap(iterable(m) start(m) list_str list_res)
		}
	}
}
