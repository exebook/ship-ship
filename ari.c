/*
	TinyC does not optimize so have to use macros to be faster,
	GCC/Clang would be happy if everything was a function
*/



#define __match_num (__is_num(a) && __is_num(b))
#define __match_int(a, b) (__is_int((a) | (b))) // trick
#define __slow_op(_op_) (__float(a) _op_ __float(b))

val __slow_add(val a, val b) {
	if (__match_num) return __new_big(__slow_op(+));
	else {
		val A = a;
		val B = b;
		return __val_add(A, B);
	}
}

val __slow_sub(val a, val b) {
	if (__match_num) return __new_big(__slow_op(-));
	else {
		return 0;
	}
}

val __slow_mul(val a, val b) {
	if (__match_num) return __new_big(__slow_op(*));
	else {
		return __val_mul(a, b);
	}
}

val __slow_div(val a, val b) {
	if (__match_int(a, b)) {
		int32_t A = __get_i(a), B = __get_i(b);
		if  (A % B == 0) {
			return __let(A / B);
		}
	}

	if (__match_num)
		return __new_big(__slow_op(/));
	else {
		return __val_div(a, b);
	}
}





val __slow_add_on(val a, val b) {
	if (__match_num) return __new_big(__slow_op(+));
	else {
		return __val_add_on(a, b);
	}
}

val __slow_sub_on(val a, val b) {
	if (__match_num) return __new_big(__slow_op(-));
	else {
		return __val_sub_on(a, b);
	}
}

val __slow_mul_on(val a, val b) {
	if (__match_num) return __new_big(__slow_op(*));
	else {
		return __val_mul_on(a, b);
	}
}

val __slow_div_on(val a, val b) {
	if (__match_int(a, b)) {
		int32_t A = __get_i(a), B = __get_i(b);
		if  (A % B == 0) {
			return __let(A / B);
		}
	}

	if (__match_num)
		return __new_big(__slow_op(/));
	else {
		return __val_div_on(a, b);
	}
}







val __slow_idiv(val a, val b) {
	if (__match_num) {
		return __let((int)(__get(a) / __get(b)));
	}
	else
		return 0;
}

#define __slow_round__(_op_) {\
	return __round(a) _op_ __round(b);\
}

val __slow_mod(val a, val b) {
	if (__match_num) __slow_round__(%)
	else
		return 0;
}
// == 0, < 1, > 2, <= 3, >= 4, != 5
val __slow_eq(val a, val b) {
	if (a == b) return 1;
	if (__match_num) return __let(__slow_op(==));
	else {
		return __cmp(a, b, 0);
	}
}

val __slow_ne(val a, val b) {
	if (a == b) return 0;
	if (__match_num) return __let(__slow_op(!=));
	else {
		return __cmp(a, b, 5);
	}
}

val __slow_le(val a, val b) {
	if (a == b) return 1;
	if (__match_num) return __let(__slow_op(<=));
	else {
		return __cmp(a, b, 3);
	}
}

val __slow_ge(val a, val b) {
	if (a == b) return 1;
	if (__match_num) return __let(__slow_op(>=));
	else {
		return __cmp(a, b, 4);
	}
}
val __slow_lt(val a, val b) {
	if (__match_num) return __let(__slow_op(<));
	else {
		return __cmp(a, b, 1);
	}
}
val __slow_gt(val a, val b) {
	if (__match_num) return __let(__slow_op(>));
	else {
		return __cmp(a, b, 2);
	}
}


val __slow_or(val a, val b) {
	if (__match_num) __slow_round__(||)
	else {
		if (__is_num(a) && __get(a) != 0) return __true;
		if (__is_num(b) && __get(b) != 0) return __true;
		if (!__is_nil(a)) return __true;
		if (!__is_nil(b)) return __true;
		return __false;
	}
}

val __slow_and(val a, val b) {
	if (__match_num) __slow_round__(&&)
	else {
		char A = 0, B = 0;
		if (__is_num(a) && __get(a) != 0) A = 1;
		if (__is_num(b) && __get(b) != 0) B = 1;
		if (!__is_nil(a)) A = 1;
		if (!__is_nil(b)) B = 1;
		return __let_i(A && B);
	}
}

val __slow_OR(val a, val b) {
	if (__match_num) __slow_round__(|)
	else {
		return 0;
	}
}

val __slow_AND(val a, val b) {
	if (__match_num) __slow_round__(&)
	else {
		return 0;
	}
}

val __slow_SHL(val a, val b) {
	if (__match_num) __slow_round__(<<)
	else {
		return 0;
	}
}

val __slow_SHR(val a, val b) {
	if (__match_num) __slow_round__(>>)
	else {
		return 0;
	}
}

#define __fast_overflow(op, a, b) ({ \
	val a__ = a, b__ = b; \
	int32_t c, fast = __match_int(a__, b__)&& !__##op##_overflow(__get_i(a__), __get_i(b__), &c);\
	fast ? __let(c) : __slow_##op(a__, b__);\
})

#define __fast_int(C_OP, _op_, a, b) ({ \
	val __fast_int_a = a, __fast_int_b = b; \
	__match_int(__fast_int_a, __fast_int_b) \
		? __let(__get_i(__fast_int_a) C_OP __get_i(__fast_int_b)) \
		: __slow_##_op_(__fast_int_a, __fast_int_b); \
})

#define __add_on_overflow __add_overflow
#define __sub_on_overflow __sub_overflow
#define __mul_on_overflow __mul_overflow

#define __add(a, b) __fast_overflow(add, a, b)
#define __sub(a, b) __fast_overflow(sub, a, b)
#define __mul(a, b) __fast_overflow(mul, a, b)
#define __div(a, b) __slow_div(a, b)
#define __idiv(a, b) __fast_int(/, idiv, a, b)

//#define __round(a_expr) ({ \
	//int _m_round_i = __round(a_expr); \
	//__let(_m_round_i); \
//})

#define __not(a) __is_int(a) ? (!(a)) : __is_num(a) ? __let_i((!(__round(a)))) : __is_nil(a) ? 1 : 0
//#define __not(a) __let_i(!__round(a))
#define __eq(a, b) __fast_int(==, eq, a, b)
//#define __eq(a, b) a == b ? __let(1) : __fast_int(==, ge, a, b)
#define __mod(a, b) __fast_int(%, mod, a, b)
#define __lt(a, b) __fast_int(<, lt, a, b)
#define __gt(a, b) __fast_int(>, gt, a, b)
#define __le(a, b) __fast_int(<=, le, a, b)
#define __ge(a, b) __fast_int(>=, ge, a, b)
#define __ne(a, b) __fast_int(!=, ne, a, b)
#define __or(a, b) __fast_int(||, or, a, b)
#define __and(a, b) __fast_int(&&, and, a, b)
#define __OR(a, b) __fast_int(|, OR, a, b)
#define __AND(a, b) __fast_int(&, AND, a, b)
#define __SHL(a, b) __fast_int(<<, SHL, a, b)
#define __SHR(a, b) __fast_int(>>, SHR, a, b)

#define __inc(a) a = __add(a, __let(1))
#define __dec(a) a = __sub(a, __let(1))

#define __neg(a) __is_int(a) ? -a & INT_MASK : __sub(__let(0), a)

#define __add_on(a, b) __fast_overflow(add_on, a, b)
#define __sub_on(a, b) __fast_overflow(sub_on, a, b)
#define __mul_on(a, b) __fast_overflow(mul_on, a, b)
#define __div_on(a, b) __slow_div_on(a, b)


//val __and(val a, val b) {
	//return __match_int(a, b)
		//? __let(__get_i(a) && __get_i(b))
		//: __slow_and(a, b);
//}

//val __add_on(val a, val b) {
	//if (__match_int(a, b)) {
		//return __add(a, b);
	//}
	//else {
		//// leaf_big must return new_leaf here, not __leaf(dest)->d += __leaf(increment)->d
		//return __val_add_on(a, b);
	//}
//}
//
//val __sub_on(val a, val b) {
	//if (__match_int(a, b)) {
		//return __sub(a, b);
	//}
	//else {
		//// leaf_big must return new_leaf here, not __leaf(dest)->d += __leaf(increment)->d
		//*dest = __sub(*dest, decrement);
	//}
//}
//
//+= -= /= *=
//a := '1 2 3'
//a /= ' '
//log a
//[1 2 3]


val __ship_round(val x) {
	return __let_i((int)(__get(x) + 0.5));
}

val __ship_floor(val x) {
	return __let_i((int)__get(x));
}

val __ship_ceil(val X) {
	double x = __get(X);
	return (double)(int)x == x ? x : (int)x + 1;
}




