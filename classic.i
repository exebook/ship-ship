// Simplified classic test for ship-ship
b := 100_000
a := 3_000_000
t := time_ms()
while a > 0 {
   a -= 1
   if a mod b == 0 {
      cout a ' '
   }
}
log time_ms() - t
