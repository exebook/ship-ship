#define __error __tc_error

val __last_stack_trace = __nil;

void __throw(val e) {
	if (__stack_count(try) > 0) {
		__last_stack_trace = __trace();
		__stack(try)->e = e;
		longjmp(__stack(try)->j, 1);
	}
	else {
		//printf("panic, unhandled runtime exception:\r\n"); //is this message helpful?
		if (__global.trace_enabled) __show_trace(1);
		__log(e);
		exit(1);
	}
}

void __fatal_error(char *s) {
	printf("%s", s);
	exit(1);
}
