typedef char * pchar;
pchar
	E001_0 = "key is ^",
	E002_0 = "not array",
	E003_0 = "__array_at: out of bounds",
	E004_0 = "array index is not a number",
	E005_1 = "indexing[] of ^",
	E006_0 = "cannot index number",
	E007_0 = "index_set: unknown type",
	E008_1 = "cannot write index of ^",
	E009_2 = "cannot add ^ to ^",
	E010_1 = "unsupported addition left operand of type ^",
	E011_1 = "len is not applicable to ^",
	E012_0 = "push() destination is not array",
	E013_0 = "put() destination is not array",
	E014_0 = "pop() source is not array",
	E015_0 = "pull() source is not array",
	E016_1 = "cannot find ^ in string",
	E017_1 = "cannot find in ^",
	E018_2 = "cannot multiply ^ * ^",
	E019_1 = "cannot join ^",
	E020_1 = "join separator cannot be ^",
	E021_1 = "cannot join array that contains ^",
	E022_0 = "cannot assign to an <item> of a string",
	E023_1 = "^ is out of enum bounds",
	E024_1 = "indexing enum with ^",
	E025_1 = "cannot call, callee <^> is not a function",
	ELAST_0 = "";

void switch_to_test_errors() {
	E001_0 = "E001 ^";
	E002_0 = "E002";
	E003_0 = "E003";
	E004_0 = "E004";
	E005_1 = "E005 ^";
	E006_0 = "E006";
	E007_0 = "E007";
	E008_1 = "E008 ^";
	E009_2 = "E009 ^ ^";
	E010_1 = "E010 ^";
	E011_1 = "E011 ^";
	E012_0 = "E012";
	E013_0 = "E013";
	E014_0 = "E014";
	E015_0 = "E015";
	E016_1 = "E016 ^";
	E017_1 = "E017 ^";
	E018_2 = "E018 ^ ^";
	E019_1 = "E019 ^";
	E020_1 = "E020 ^";
	E021_1 = "E021 ^";
	E022_0 = "E022";
	E023_1 = "E023 ^";
	E024_1 = "E024 ^";
	E025_1 = "E025 ^";
}
