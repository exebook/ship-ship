incl argv comp tinyhi shamefmt

show := false
oks := 0
failed := 0
disabled := 0
output_is_error := false
diff_cmd := 'diff --color -w'
dbfile := 'test-db.i'
start_time := time1000()
cur_test := nil // used by incl loader

A := load_argv([], ['help h' 'file f' 'error e' 'last l' 'failed f' 'display d' 'diff-char C' 'nums n' 'source s' 'list-tags G' 'list-failed F'])

if A.keys.help {
	log 'test    run all tests print summary of errors/successes'
	log 'test a b c    run only tests matching all provided keywords'
	show_arg_help('   ',
		'-e --error | stop on first error and display diff/compiler error output'
		'-l --last | run last test in the list'
		'-f --failed | run last failed test'
		'-C --diff-char | show diff by character'
		'-d --display | only display test source and quit'
		//'-s --source |'
		//'-n --nums |'
		'-f --file | file with database of tests'
		'-G --list-tags | file with database of tests'
		'-F --list-failed | list tests failed last run'
	)
	kill
}

db := load_db()
failed_tests := []
select_tests()
E00X_setup_for_tests()
if A.keys['list-failed'] {
	log load('.test-failed') / ' ' * '\n'
	kill
}
if A.keys['list-tags'] {
	tags := {}
	each db {
		each item.keys[1..] {
			init tags[item] = 0
			tags[item] += 1
		}
	}
	each tags log index + 1, cle +':' item
	kill
}
if A.keys['diff-char'] {
	diff_cmd = 'diff-char'
}
if the A.keys['file'] {
	dbfile = it[0]
}
run_tests()
show_summary(len db oks)

fun select_tests {
	if A.keys.last {
		db = [pop db]
	}
	but A.keys.failed {
		ftest := load('.test-failed') / ' '
		if len ftest < 1 {
			log 'internal error: ftest is corrupt'
			kill
		}
		db = filter(db lam t = ftest[0] == t.name )
	}
	else {
		db = filter(db lam t {
			if len A.list == 0 ret true
			else {
				each A.list if item find t.keys >= 0 ret true
				ret false
			}
		})
	}
}

fun display_progress test diffpos {
	if diffpos < 0
		cout ~green test.name ' ' ~~
	else
		cout ~red test.name ' ' ~~
}

fun wrap_with_caret s pos {
	lineno := 0
	x := 0 xpos := 0
	while x <= pos {
		if s[x] == 10 {
			if x != pos {
				lineno += 1
				xpos = x
			}
		}
		inc x
	}
	xpos = pos - xpos - 1
	s /= '\n'
	re := []
	each s {
		re += item
		if lineno == index {
			t := ''
			while len t < xpos t += ' '
			re += t + color(1 '^--')
		}

	}
	ret re * '\n'
}

fun remove_empty_lines s {
	s = filter(s / '\n' lam x {

		//log '-->' len x x
		ret len x > 0
	})
	ret s * '\n'
}

fun display_test test out diffpos {
	log
	log ~green test.name ~gray test.keys[1..] * ' '
	if the test.files {
		each it {
			log '- - ' cle ' - - - - - - - - - - - - - - - - - - - '
			log ~blue item
		}
	}
	log '- - code - - - - - - - - - - - - - - - - - - - '
	log ~blue remove_empty_lines(test.code) / '\t' * '   '
	log '- - - - - - - - - - - - - - - - - - - - - '
	log ~yellow remove_empty_lines(test.output)
	log '- - - - - - - - - - - - - - - - - - - - - '
	if diffpos < 0
		log ~green remove_empty_lines(out)
	else
		log remove_empty_lines(wrap_with_caret(out diffpos))
	//save('/tmp/expected' Expected + '\n')
	//save('/tmp/actual' Actual + '\n')
	//log '- - - - - - - - - output diff ' test.name ' - - - - - - - - - - - -'
	//system_exec('cd /tmp && '+ diff +' expected actual')
	//log '- - - - - - - - - - - - - - - - - - - - - - - - - '
	//if A.keys.source {
		//log '- - - - - failed test' '"'+item.name+'"' 'source - - - - - - - '
		//nums := ''
		//if A.keys.nums nums = ' -n'
		//log system('cat /tmp/src.i | jsyn' + nums)
	//}
}

fun run_tests {
	each db {
		re := test_one(item)
		if re.out == 'disabled' {
			cout ~gray re.test.name ~~ ' '
			failed = 0
		}
		else {
			try
				diffpos := compare(re.test.output re.out)
			catch {
				diffpos = 0
				if is_object(error) {
					if error.lex_msg re.out = error.lex_msg
					if error.msg re.out = error.msg
				}
				else re.out = error
			}
			if diffpos < 0 {
				oks += 1
			}
			else {
				if failed == 0 {
					failed = 1
				}
				failed_tests += item.name
				if len db == 1 {
					display_test(item re.out diffpos)
					kill
				}


				if A.keys.error {
					log 'stopping due to --error'
					kill
				}
			}
			display_progress(re.test diffpos)
		}
	}
	log
	if len failed_tests > 1 save('.test-failed' failed_tests * ' ')
}

fun show_summary all oks {
	msg_line := []
	if oks > 0 msg_line += color(2 oks) + ' OK'
	if all - oks > 0 msg_line += color(1 all - oks - disabled) + ' FAIL'
	if disabled > 0 msg_line += color(7 disabled) + ' OFF'
	msg_line += (((time1000() - start_time)/1000)+'')[..6] + ' s'
	log msg_line * ', '
}

fun compare Expected Actual {
	x := map(tinylex('x' Expected) lam x = x[3])
	a := map(tinylex('a' Actual) lam x = x[3])
	diffpos := -1
	each x {
		if a[index] != x[index] and x[index] != '#' {
			use tinylex('a' Actual)[index] {
				diffpos = tinylex('a' Actual)[index][1]
				A := a[index]+''
				B := x[index]+''
				each A {
					if item == B[index] inc diffpos
				}
				break
			}
			else {
				ret tinylex('a' Expected)[index][1]
			}
		}
	}
	if diffpos == -1 if len x < len a diffpos = len x
	ret diffpos
}

fun inc_loader pos fname {
	fname += '.i'
	F := cur_test.files
	if F[fname] {
		src := F[fname]
	}
	else {
		throw stop(pos 'file <' + fname + '> was not found')
	}
	tokens := tinylex(fname src)
	//log 'loaded' len tokens 'tokens from' fname
	ret tokens
}

fun test_one test {
	cur_test = test
	options := {
		signals: true
		test_mode: true
	}

	if not is_string(test.code) {
		throw 'test_one: invalid input:' + typeof(test.code)
	}

	if test.code[0:8] == 'disabled' {
		inc disabled
		ret { test, out: 'disabled' }
	}

	c_fname := '.test.c'
	i_fname := '.test.i'
	save(i_fname test.code) // for stack traces
	try program_code := compile(i_fname test.code c_fname options inc_loader)
	catch {
		e := error
		e1 := nil
		if is_string(e) e1 = e
		but is_fun(e) e1 = e()
		but is_object(e) {
			if e.kind == 'stop' {
				e_fname := e.tokens[e.token_pos][0]
				if e_fname == i_fname {
					err_src := test.code
				}
				else err_src = test.files[e_fname]
				e1 = source_error(err_src e.tokens e)
				if e.prev
					e1 = source_error(err_src e.tokens e.prev) +'\n'+ e1
			}
			else e1 = e.msg
		}
		else e1 = 'fatal error, compiler returned error I cannot handle'
		ret { test, out: e1 }
	}
	save(c_fname program_code)
	o := system('tcc -g -I. ' + c_fname + ' -o .test 2>&1') // was test.name
	compile_error := nil
	begin
		$ compile_error = cell_let(op_system_errno);
	end
	if not compile_error {
		o = system('.test 2>&1')
	}
	ret { test, out: o }
}

fun load_db {
	tests := []
	dest := 'none'
	s := load(dbfile) / '\n'

	each s {
		t := item / ' '
		if t[0] == 'code' {
			dest = 'main'
			tests += {
				name: t[1]
				keys: []
				code: []
				output: []
				files: {}
			}
			tests[-1].keys = t[1..] // includes name
			cont
		}
		if t[0] == 'output' {
			if t[1] == 'error' output_is_error = true
			dest = 'output'
			cont
		}
		if t[0] == 'file'  and dest != 'output' {
			if t[1] == 'main' dest = 'main'
			else dest = t[1]+'.i'
			cont
		}
		if t[0] == 'finish' {
			break
		}

		if dest == 'main' tests[-1].code += item
		but dest == 'output' tests[-1].output += trim(item)
		else {
			init tests[-1].files = {}
			init tests[-1].files[dest] = ''
			tests[-1].files[dest] += item + '\n'
		}
	}
	each tests {
		item.code *= '\n'
		item.output *= '\n'
	}
	ret tests
}
