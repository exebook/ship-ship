indent_or_string := anyof([pa_ident() pa_string()])

fun pa_string {
	ret lam A {
		if A == 1 ret 'string'
		T := pos_tok(A)
		if T {
			if Key(T) == Token.Str1 or Key(T) == Token.Str2
				ret [pos_next(A) { type: types.string str: Value(T) pos: A }]
		}
	}
}

fun pa_literal {
	ret lam A {
		if A == 1 ret 'literal'
		T := pos_tok(A)
		if T {
			if Key(T) == Token.Int ret [pos_next(A) { type: types.number num: Value(T) pos: A }]
			if Key(T) == Token.Double ret [pos_next(A) { type: types.number num: Value(T) pos: A }]
			if Key(T) == Token.Str1 or Key(T) == Token.Str2
				ret [pos_next(A) { type: types.string str: Value(T) pos: A }]
			if Key(T) == Token['['] or Key(T) == Token[' ['] ret pa_array_literal()(A)
			if Key(T) == Token['{'] ret pa_object_literal()(A)
			//if Key(T) == Token['rec'] ret pa_rec_literal()(pos_next(A))
		}
	}
}

fun pa_ident {
	ret lam A {
		if A == 1 ret 'ident'
		t := pos_tok(A)
		if t {
			k := Key(t)
			if k == Token.Id {
				//or k > Token['.KeywordsBegin'] and k < Token['.KeywordsEnd'] {

				ret [pos_next(A) { type: types.id id: Value(t) pos: A }]
			}
		}
	}
}

fun pa_integer {
	ret lam A {
		if A == 1 ret 'number'
		t := pos_tok(A)
		if t {
			k := Key(t)
			if k == Token.Int {
				ret [pos_next(A) { type: types.number num: Value(t) pos: A }]
			}
		}
	}
}
/*
function _parse_optional_comma_list(i, arg_parse, break_on_newline) {
	var R = []
	while (true) {
		var nothing = true
		var a = tokens[i]
		if (a) { if (a.name == lex.names.comma) { i++ nothing = false R.push(0) } }
		var n = arg_parse(i)
		if (n) {
			i = n
			nothing = false
			R.push(ret)
			if (break_on_newline && tokens[n - 1].line) {
				break
			}
		}
		else if (nothing) {
			break
		}
	}
	// [] = []
	// [1] = [1]
	// [, 1] = [nil, 1]
	// [1, ] = [1, nil]
	// [1,,2] = [1, nil, 2]
	// [1,2,3] = [1, 2, 3]
	// [1 2 3] = [1, 2, 3]
	// [1,,] = [1, nil, nil]
	// [,,1] = [nil, nil, 1]
	// [,] = [nil, nil]
	ret = []
	if (R.length == 0) { return i }
	if (R[0] != 0) ret.push(R[0]); else ret.push({type: 'nil'})
	for (var x = 1; x < R.length; x++) {
		if (R[x] != 0) { ret.push(R[x]) }
		else { if (R[x - 1] == 0) { ret.push({type: 'nil'}) } }
	}
	if (R[R.length - 1] == 0) ret.push({type: 'nil'})
	return i
}
*/

fun _array_syntax_list A {
	ex := expr()
	comma := Token[',']
	if A == 1 ret '_array_syntax_list'
	L := []
	B := A
	loop {
		r := symbol(comma)(B)
		if r {
			B = ret_pos(r)
			L += ','
			cont
		}
		R := ex(B)
		if R {
			a := ret_ast(R)
			if is_array(a) a = [a]
			L += a
			B = ret_pos(R)
		}
		else break
	}

	_nil := { type: types.id id: 'nil' }
	re := []
	if L[0] == ',' { re += _nil }
	prev := false
	each L {
		if item == ',' { if prev { re += _nil } }
		else re += item
		prev = false
		if item == ',' { prev = true }
	}

	if len L > 0 and L[-1] == ',' { re += _nil }
	ret [B re]
}


fun pa_array_literal {
	ret lam A {
		if A == 1 ret 'array_literal'
		T := pos_tok(A)
		if T {
			K := Key(T)
			if K == Token['['] or K == Token[' ['] A = pos_next(A)
			else ret
			R := _array_syntax_list(A)
			if R {
				B := ret_pos(R)
				if Key(pos_tok(B)) == Token[']'] B = pos_next(B)
				list := ret_ast(R)
				ret [B { type: types.array_literal list, pos: A }]
			}
			else throw 'array_literal ERROR'
		}
	}
}

fun pa_property_literal {
	ret pa_node('property_literal'
		lam A id expr {
			if id.type == types.string id = id.str
			but id.type == types.id id = id.id
			if expr == 42 expr = { type: types.id, id, pos: A } // shorthand assignment
			ret { id, expr, pos:A }
		}
		indent_or_string
		pa_make('property_expr'
			lam A b {
				use b ret b[1]
				ret 42 // shorthand assignment
			}
			maybe(seq([symbol(Token[':']) expr()]))
		)
	)
}

fun pa_object_literal {
	ret lam A {
		if A == 1 ret 'object_literal'
		T := pos_tok(A)
		if T {
			if Key(T) == Token['{'] B := pos_next(A)
			else ret nil
			R := auto_list(Token[','] pa_property_literal())(B)
			if R {
				B = ret_pos(R)
				T2 := pos_tok(B)
				if T2 {
					if Key(T2) == Token['}'] B = pos_next(B)
					list := ret_ast(R)
					ret [B { type: types.object_literal list, pos: A }]
				}
				else throw stop(A 'object not closed until end of file')
			}
			else throw 'object_literal ERROR'
		}
	}
}
//
//fun pa_rec_literal = pa_make('rec'
	//lam A o {
		//log 'REC LIT'
	//ret {
		//type: types.rec_literal list: o.list, pos: A
	//}}
	//pa_object_literal()
//)

fun add_source f {
	ret lam A {
		re := f(A)
		if re {
			tt := A[1][A[0] .. ret_pos(re[0]) - 1]
			x0 := tt[0][1]
			s := ''
			each tt {
				while len s + x0 < item[1] s += ' '
				s += item[3]
			}
			ret_ast(re).source = s
		}
		ret re
	}
}

fun log_empty {
	ret lam A {
		if A == 1 ret 'log_empty'
		R := symbol(Token.log)(A)
		if R {
			if tok_eol(ret_pos(R)) {
				ret [ret_pos(R) { type: types.log args: [] pos: A }]
			}
		}
	}
}

log_colors := {
	black: 0,
	yellow: 3,
	green: 2,
	teal: 6,
	orange: 11,
	blue: 4,
	pink: 13,
	purple: 5,
	cloud: 33,
	brown: 124,
	red: 1,
	white: 15,
	gray: 7,
	back: 15,
	cyan: 45,
	reset: -1,
	rose: 171,
}

fun pa_log_expr {
	ret lam A {
		if A == 1 ret 'pa_log_expr'
		T := pos_tok(A)
		if T {
			if Key(T) == Token['~~'] {
				ret [pos_next(A) { pos: A, type: types.log_color, color: -1 }]
			}
			if Key(T) == Token['~'] {
				B := pos_next(A)
				T = pos_tok(B)
				if Key(T) == Token.Id {
					s := Value(T)
					if log_colors[s] {
						ret [pos_next(B) { pos: A, type: types.log_color, color: log_colors[s] }]
					}
				}
				but Key(T) == Token.Int {
					ret [pos_next(B) { pos: A, type: types.log_color, color: Value(T) }]
				}
				else {
					throw stop(A 'log ~ expects a color id or a color number')
				}
			}
			else {
				ret expr()(A)
			}
		}
	}
}

__pa_log := nil
fun pa_log {
	init __pa_log = anyof([
		log_empty()
		pa_node('log'
			lam A sym list {
				if len list > 0 {
					keyword_line := pos_tok(sym.pos)[5]
					first_item_line := pos_tok(list[0].pos)[5]
					if keyword_line != first_item_line
						ret { type: types.log args: [], pos: list[0].pos }
				}
				ret { type: types.log args: list, pos: A }
			}
			symbol(Token.log)
			eol_list(pa_log_expr())
		)
	])
	ret __pa_log
}

__pa_cout := nil
fun pa_cout {
	init __pa_cout = pa_node('cout'
		lam A sym list = { type: types.cout args: list, pos: A }
		symbol(Token.cout)
		eol_list(pa_log_expr())
	)

	ret __pa_cout
}

__pa_are := nil
fun pa_are {
	init __pa_are = pa_node('log'
		lam A sym list = { type: types.are args: list, pos: A }
		symbol(Token.are)
		eol_list(add_source(expr()))
	)
	ret __pa_are
}

__pa_else := nil
fun pa_else {
	init __pa_else = pa_node('else'
		lam A s d = d
		symbol(Token.else)
		pa_make('else_block'
			lam A b = { type: types.else block: b pos: A }
			pa_block()
		)
	)
	ret __pa_else
}

__pa_but := nil
fun pa_but {
	init __pa_but = pa_node('but'
		lam A s d = d
		symbol(Token.but)
		pa_make('but_node'
			lam A b {
				ret {
					type: types.else pos: A block:
					[{
						type: types.if_st
						expr: b[0]
						block: b[1]
						else: b[2],
						pos: A
					}]
				}
			}
			seq([ expr() pa_block() maybe(pa_if_cont()) ])
		)
	)
	ret __pa_but
}

__if_cont := nil
fun pa_if_cont {
	init __if_cont = lam A {
		R := pa_else()(A)
		if R ret R
		R = pa_but()(A)
		if R ret R
	}
	ret __if_cont
}

fun if_stmt {
	ret pa_node('if_statement'
		lam A s d = d
		symbol(Token.if)
		pa_make('if_decl'
			lam A b {
				ret {
					type: types.if_st
					expr: b[0]
					block: b[1]
					else: b[2],
					pos: A
				}
			}
			seq([ expr() pa_block() maybe(pa_if_cont()) ])
		)
	)
}

fun pa_use {
	ret pa_node('pa_use'
		lam A s d = d
		symbol(Token.use)
		pa_make('pa_use'
			lam A b {
				ret {
					type: types.if_st
					expr:
						{
						type: types.expr
						stack: [
							b[0],
							{ type: types.id id: 'nil' pos: A },
							{ type: types.operator op: Token['!='] pos: A }
						]
					}
					block: b[1]
					else: b[2],
					pos: A
				}
			}
			seq([ expr() pa_block() maybe(pa_if_cont()) ])
		)
	)
}

fun pa_init {
	ret lam A0 {
		if A0 == 1 ret 'init'
		A := A0
		R := pa_target(A)
		if not R throw stop(A 'init expects assignable id or value')
		va := ret_ast(R)
		A = ret_pos(R)
		T := pos_tok(A)
		if T and Key(T) == Token['='] {
			block_pos := A
			A = pos_next(A)
			R = expr()(A)
			if not R throw stop(A 'expression expected')
			ex := ret_ast(R)
			A = ret_pos(R)
			T = pos_tok(A)
			if T and Key(T) == Token['else'] throw stop(A 'else after "init =" is not supported')
			ret [A {
				type: types.if_st
				expr: {
					type: types.expr
					stack: [
						va,
						{ type: types.id id: 'nil' pos: A0 },
						{ type: types.operator op: Token['=='] pos: A0 }
					]
				}
				block: [ { type: types.assign target: [va] thes:[] expr: ex pos: block_pos } ]
				pos: A0
			}]
		}
		else {
			R = seq([pa_block() maybe(pa_if_cont())])(A)
			if not R throw stop(A '= or statement expected')
			b := ret_ast(R)
			A = ret_pos(R)

			ret [A {
				type: types.if_st
				expr: {
					type: types.expr
					stack: [
						va,
						{ type: types.id id: 'nil' pos: A0 },
						{ type: types.operator op: Token['=='] pos: A0 }
					]
				}
				block: b[0]
				else: b[1],
				pos: A0
			}]
		}
	}
}

fun if_ex {
	ret lam A {
		if A == 1 ret 'if_ex'
		T := pos_tok(A)
		if not T ret
		if Key(T) != Token['if'] ret
		B := pos_next(A)

		R := expr()(B)
		if not R throw stop(B 'condition expected')
		B = ret_pos(R)
		X := ret_ast(R)

		R = expr()(B)
		if not R throw stop(B 'conditional expression expected')
		B = ret_pos(R)
		x0 := ret_ast(R)

		T = pos_tok(B)
		if not T throw stop(B 'else expected')
		if Key(T) != Token['else'] throw stop(B 'else expected')
		B = pos_next(B)

		R = expr()(B)
		if not R throw stop(B 'else expression expected')
		B = ret_pos(R)
		x1 := ret_ast(R)

		ret [B { type: types.if_ex expr: X, x0, x1, pos: A }]
	}
	//ret pa_node('if_ex'
		//lam A s d = d
		//symbol(Token.if)
		//pa_make('if expression'
			//lam A b {
				//ret { type: types.if_ex expr: b[0] x0: b[1] x1: b[3] pos: A }
			//}
			//seq([ expr() expr() symbol(Token['else']) expr() ])
		//)
	//)
}

fun statem_until ending {
	ret lam A {
		if A == 1 ret 'statem_until'
		R := _pa_loop(Token[';'] false statem())(A)

		B := ret_pos(R)
		S := ret_ast(R)
		R = ending(B)
		init R {
			throw stop(B 'statement expected')
		}
		ret [ret_pos(R) S]
	}
}

fun pa_fun_body = pa_make('fun_body'
	lam A x {
		if x[0].sym == Token['{'] {
			ret x[1]
		}
		but x[0].sym == Token['='] {
			ret [{ pos: A type: types.ret expr: x[1] }]
		}
	}
	anyof([
		seq([ symbol(Token['{']) statem_until(symbol(Token['}'])) ])
		seq([ symbol(Token['=']) expr() ])
	])
)

__fun := nil
fun pa_fun {
	init __fun = pa_make('fun'
		lam A x {
			ret { type: types.fun id: x[0].id args: x[1] block: x[3], free: x[2] != nil pos: A }
		}
		seq([
			pa_ident()
			_pa_loop(Token[','] false pa_ident())
			maybe(symbol(Token['...']))
			pa_fun_body()
		])
	)
	ret __fun
}

fun pa_each_expr {
	ret lam A {
		R := expr()(A)
		init R ret
		B := symbol(Token['..'])(ret_pos(R))
		init B ret [ret_pos(R) ret_ast(R)]
		C := expr()(ret_pos(B))
		if C ret [
			ret_pos(C)
			{ pos: A type: types.each_range a: ret_ast(R) b: ret_ast(C) }
		]
		throw stop(ret_pos(B) 'expression expected after ..')
	}
}

__pa_each := nil
fun pa_each {
	init __pa_each = pa_make('each'
		lam A x {
			ret { type: types.each iterable: x[0] block: x[1], pos: A }
		}
		seq([
			pa_each_expr()
			pa_block()
		])
	)
	ret __pa_each
}

fun _pa_lam A {
	B := pos_next(A)
	R := _pa_loop(Token[','] false pa_ident())(B)
	if R {
		arg_list := ret_ast(R)
		D := ret_pos(R)
		T := pos_tok(D)
		if not T throw stop(B 'unexpected end of input')
		if Key(T) == Token['{'] {
			D = pos_next(D)
			R = _pa_loop(Token[';'] false statem())(D)
			block := nil
			if R {
				block = ret_ast(R)
				E := ret_pos(R)
			}
			else E = D
			T = pos_tok(E)
			if not T { throw stop(E "undexpected end of input") }
			if Key(T) != Token['}'] { throw stop(E "} expected") }
			ret [pos_next(E) { pos: A type: types.fun args: arg_list block }]
		}
		but Key(T) == Token['='] {
			D = pos_next(D)
			R = expr()(D)
			block = nil
			if R {
				block = [{ pos: D, type: types.ret expr: ret_ast(R) }]
				E = ret_pos(R)
			}
			else stop(E "expression expected")
			ret [E { pos: A type: types.fun args: arg_list block }]
		}
		else throw stop(B 'lam definition expected = or {')
	}
	else throw stop(B 'expected argument list or lambda body')
}

fun pa_lam {
	ret lam A {
		if A == 1 ret 'lam'
		T := pos_tok(A)
		if T and Key(T) == Token['lam'] {
			ret _pa_lam(A)
		}
	}
}
fun pa_do {
	ret lam A {
		if A == 1 ret 'do'
		T := pos_tok(A)
		if T and Key(T) == Token['do'] {
			ret _pa_lam(A)
		}
	}
}

fun decl_tail target thes A {
	each target if item.type != types.id {
		throw stop(item.pos tem(E016_1 pos_tok(item.pos)[3]))
	}
	R := expr()(A)
	if R {
		ret [ ret_pos(R) { type: types.decl target, thes, expr: ret_ast(R) pos: [A[0]-1 A[1]] } ]
	}
}

fun is_assignable node {
	ret node.type == types.id
	or node.type == types.item
	or node.type == types.member
	or node.type == types.indexing
	or node.type == types.range
	or node.type == types.it
	or node.type == types.that
}

fun assign_tail A0 target thes A {
	each target { // validity check
		if is_assignable(item) cont
		if item.type == types.call {
			throw stop(item.pos 'call is not assignable')
		}
		throw stop(item.pos 'expected assignable id or value')
	}
	R := expr()(A)
	if R {
		ret [ ret_pos(R) { type: types.assign target, thes, expr: ret_ast(R) pos: A0 } ]
	}
}

fun op_assign_tail op A0 target thes A {
	each target {
		if item.type == types.call {
			throw stop(item.pos 'call is not assignable')
		}
	}
	R := expr()(A)
	if R {
		ret [ ret_pos(R) { op, type: types.op_assign target, thes, expr: ret_ast(R) pos: A0 } ]
	}
}

fun assignable {
	ret pa_make('assignable'
		lam A x {
			if is_assignable(x) ret x
		}
		pa_value()
	)
}

fun call_statem {
	ret pa_make('call_statem'
		lam A x {
			if x.type == types.call {
				ret { type: types.expr_statem expr: x pos: A }
			}
		}
		pa_value()
	)
}

fun expr_statem {
	ret pa_make('expr_statem'
		lam A x {
			if x.type == types.id throw stop(A E000_0)
			but x.type == types.array_literal {
				have_space :=  Key(pos_tok(A)) == Keys[' [']
				if have_space
					throw stop(A E002_0)
				else
					throw stop(A E001_0)
			}
			ret { type: types.expr_statem expr: x pos: A }
		}
		expr()
	)
}

fun pa_while = pa_make('while'
	lam A b = { type: types.while expr: b[0] block: b[1], pos: pos_prev(A) }
	seq([expr() pa_block()])
)

fun pa_try = pa_make('try'
	lam A b = {
		type: types.try
		b_try: b[0]
		b_catch: b[2]
		pos: pos_prev(A)
	}
	seq([pa_block() symbol(Token['catch']) pa_block()])
)

fun pa_enum_item = pa_make('enum_item'
	lam A b {
		id := nil
		if b[0].type == types.id id = b[0].id
		else id = b[0].str
		num := -1
		use b[1] num = b[1][1].num
		ret { id, num, pos: A }
	}
	seq([
		anyof([pa_ident() pa_string()])
		maybe(seq([ symbol(Token[':']) pa_integer()]))
	])
)

fun pa_enum = pa_make('enum'
	lam A b {
		h := b[2][0]
		if h and h.type == types.symbol and h.sym == Token['='] {
			expr := b[2][1]
		}
		else {
			list := b[2]
		}
		ret {
			pos: A
			type: types.enum
			is_free: b[0] != nil
			id: b[1].id
			list,
			expr,
		}
	}
	seq([
		maybe(symbol(Token['free']))
		pa_ident()
		anyof([
			embrace(
				Token['{']
				_pa_loop(Token[','] false pa_enum_item())
				Token['}']
				'expected <id> or <string>'
			)
			seq([symbol(Token['=']) expr()])
		])
	])
)

__pa_incl := nil
//__pa_incl_anyof := nil
__incl_statems := nil
duplicate_inc := {}

fun pa_incl {
	if __pa_incl == nil {
		__incl_statems = statem_until(pa_eof())
		__pa_incl = lam A {
			if A == 1 ret 'incl'
			R := _pa_loop(Token[','] true indent_or_string)(A)
			if R {
				n := ret_ast(R)
				ids := []
				each n {
					id := ''
					if item.type == types.id id = item.id
					else id = item.str
					ids += id
				}
			}
			else {
				throw stop(A 'string or id expected')
			}

			block := []
			each ids {
				if duplicate_inc[item] != nil {
					//log 'Warning:' tem('^ was already included' item)
					e := stop(A tem('^ was already included' item))
					esrc := e.tokens[A[0]][6]
					log source_error(esrc e.tokens e)

					cont
					//throw stop(A tem('^ was already included' item))
				}
				duplicate_inc[item] = 1
				tokens := loader(A item)
				Q := __incl_statems([0 tokens])
				if Q {
					block += ret_ast(Q)
				}
			}
			ret [ret_pos(R) { type: types.incl block, pos: A }]
		}
	}
	ret __pa_incl
}

	fun fast_forward A count {
		toks := A[1]
		a := A[0]
		e := A[1][A[0]][1] + count
		loop {
			if toks[a] == nil break
			if toks[a][1] >= e break
			inc a
		}
		A[0] = a
		ret A
	}

fun langc_shadow A0 {
	T := pos_tok(A0)
	if not T or Key(T) != Token['{'] {
		throw stop(A0 'shadow expects {')
	}
	A := pos_next(A0)

	R := _pa_loop(Token[','] true indent_or_string)(A)
	if R {
		list := ret_ast(R)
		each list shadow_c_names += item.id
	}
	else throw stop(A 'shadow expects.....')

	B := ret_pos(R)
	T = pos_tok(B)
	if not T or Key(T) != Token['}'] {
		throw stop(B 'shadow expects }')
	}

	ret [pos_next(B) { pos: A0 type: types.langc_shadow }] //does nothing in sy
}

fun pa_langc {
	ret lam A {
		if A == 1 ret 'langc'
		T := pos_tok(A)
		if T and Key(T) != Token['langc'] ret

		A = pos_next(A)
		T = pos_tok(A)
		if T {
			global := false
			if T[3] == 'shadow' {
				ret langc_shadow(pos_next(A))
			}
			if T[3] == 'global' {
				global = true
				A = pos_next(A)
				T = pos_tok(A)
				init T ret
			}
			// fname pos id value next_on_nl line_no
			if Token['{'] == T[2] {
				open := Token['{']
				open1 := Token['{']
				close := Token['}']
				open_char := ord '{'
				close_char := ord '}'
			}
			but Token['['] == T[2] or Token[' ['] == T[2] {
				open = Token['[']
				open1 = Token[' [']
				close = Token[']']
				open_char = ord '['
				close_char = ord ']'
			}
			else throw stop(A 'expected { or [')
			depth := 1
			src := []
			B := pos_next(A)
			A1 := B
			exprs := []
			loop {
				T = pos_tok(B)
				if not T {
					throw stop(B 'langc until EOF')
				}
				if T[2] == open or T[2] == open1 { inc depth src += T[3] }
				but T[2] == close {
					dec depth
					if depth == 0 {
						B = pos_next(B)
						break
					}
					src += T[3]
				}
				but T[2] == Token.Str1 {
					src += "'" + shame(T[3])[1..-2] + "'"
				}
				but T[2] == Token.Str2 {
					src += shame(T[3])
				}
				but T[2] == Token['$'] {
					B = pos_next(B)
					C := pa_value()(B)
					if not C {
						throw stop(B 'langc expects <value> after $')
					}
					exprs += [[len src, ret_ast(C)]]
					src += nil
					B = ret_pos(C)
					T = pos_tok(B)
					cont
				}
				else {
					tok := T[3]
					if Token.Int == T[2] and T[6][T[1]] == ord '0' { // Octals
						tok = '0' + tok
					}

					if len src and src[-1] {
						fun is_alpha C {
							ret C == ord '_'
							or C >= ord '0' and C <= ord '9'
							or C >= ord 'a' and C <= ord 'z'
							or C >= ord 'A' and C <= ord 'Z'
						}
						AT := src[-1]
						a1 := false
						a2 := false
						if is_number(AT) { a1 = true } else { a1 = is_alpha(AT[-1]) }
						if is_number(tok) { a2 = true } else { a2 = is_alpha(tok[0]) }
						if a1 and a2 tok = ' ' + tok
					}
					if T[4] src += flatter.eol
					src += tok+''
				}
				B = pos_next(B)
				if depth == 0 break
			}
			//src += flatter.eol // <- test this
			if depth != 0 throw stop(A 'langc block reached end of file')
			ret [B { pos: A1 type: types.langc src, exprs, global }]
		}
	}
}

fun pa_rec = pa_make('rec'
		lam A b = {
			type: types.rec
			pos: pos_prev(A)
			ids: b[0]
			list: b[1]
		}
		seq([
			_pa_loop(Token[','] false pa_ident())
			embrace(
				Token['{']
				_pa_loop(Token[','] false pa_ident())
				Token['}']
				'expected <id>'
			)
		])
	)

fun pa_simple ret_f = lam A = [ pos_next(A) ret_f(A) ]

fun pa_ret = pa_make('ret' lam A expr = { type: types.ret expr, pos: A } maybe(same_line(expr())))
fun pa_loop = pa_make('loop' lam A b = { type: types.loop block: b, pos: pos_prev(A) } pa_block())
fun pa_break = pa_simple(lam A = { type: types.break pos: A })
fun pa_cont = pa_simple(lam A = { type: types.cont pos: A })
fun pa_inc = pa_make('inc' lam A v = { type: types.inc value: v pos: A } assignable())
fun pa_dec = pa_make('dec' lam A v = { type: types.dec value: v pos: A } assignable())
fun pa_kill = pa_make('kill' lam A expr = { type: types.kill expr, pos: A } maybe(same_line(expr())))
fun pa_throw = pa_make('throw' lam A expr = { type: types.throw expr, pos: A } expr())
pa_target := pa_make('target' lam A x = x anyof([pa_the(pa_value) pa_value()]))

fun pa_targets A {
	R := _pa_loop(Token[','] false pa_target)(A)
	use R {
		targets := []
		thes := []
		L := ret_ast(R)
		each L {
			if item.type == types.the {
				thes += item
				targets += item.target
			}
			else targets += item
		}
		ret [ret_pos(R) targets thes ]
	}
}

// optimization refactor: rename parselets to CamelCase, use local_vars = CamelCase to cache

__pa_statem := nil
fun statem {
	if __pa_statem == nil __pa_statem = lam A {
		if A == 1 ret 'statem'
		T := pos_tok(A)
		//log 'STATEM ZERO' T
		if T {
			K := Key(T)
			if K == Token.fun { ret pa_fun()(pos_next(A)) }
			but K == Token.if { ret if_stmt()(A) }
			but K == Token.log { ret pa_log()(A) }
			but K == Token.cout { ret pa_cout()(A) }
			but K == Token.are { ret pa_are()(A) }
			but K == Token.ret { ret pa_ret()(pos_next(A)) }
			but K == Token.loop { ret pa_loop()(pos_next(A)) }
			but K == Token.break { ret pa_break()(A) }
			but K == Token.cont { ret pa_cont()(A) }
			but K == Token.inc { ret pa_inc()(pos_next(A)) }
			but K == Token.dec { ret pa_dec()(pos_next(A)) }
			but K == Token.each { ret pa_each()(pos_next(A)) }
			but K == Token.kill { ret pa_kill()(pos_next(A)) }
			but K == Token.init { ret pa_init()(pos_next(A)) }
			but K == Token.use { ret pa_use()(A) }
			but K == Token.while { ret pa_while()(pos_next(A)) }
			but K == Token.try { ret pa_try()(pos_next(A)) }
			but K == Token.throw { ret pa_throw()(pos_next(A)) }
			but K == Token.enum { ret pa_enum()(pos_next(A)) }
			but K == Token.incl { ret pa_incl()(pos_next(A)) }
			but K == Token.langc { ret pa_langc()(A) }
			but K == Token.rec { ret pa_rec()(pos_next(A)) }
			else {
			}
		}
		target_tmp := pa_targets(A)
		if target_tmp {
			B := target_tmp[0]
			target := target_tmp[1]
			thes := target_tmp[2]
			if len target > 1 {
				S := call_statem()(A)
				if S { ret S }
			}
			but len target == 0 {
				ret nil
			}
			T = pos_tok(B)
			if T {
				K = Key(T)
				if K == Token[':='] ret decl_tail(target thes pos_next(B))
				but K == Token['='] {
					re := assign_tail(A target thes pos_next(B))
					ret re
				}
				but K == Token['+=']
					or K == Token['-='] or K == Token['/=']
					or K == Token['*='] or K == Token['mod=']
						ret op_assign_tail(K A target thes pos_next(B))
			}
			R1 := expr_statem()(A)
			if R1 { ret R1 }
		}
	}
	//else { log 'repeat' }
	ret __pa_statem
}

fun pa_eof {
	ret lam A {
		if A == 1 ret 'eof'
		if A[0] == len A[1] ret [A {}]
	}
}

__pa_program := nil
fun program {
	if the __pa_program == nil it = lam A {
		if A == 1 ret 'program'
		R := statem_until(pa_eof())(A)
		if R {
			ret [ret_pos(R) { type: types.program block: ret_ast(R) pos: A }]
		}
	}
	else {
		log 'pa_program repeat'
		kill
	}
	ret __pa_program
}
