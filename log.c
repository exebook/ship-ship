typedef struct { int size; val *p; } __vals;

__vals *__vals_new() {
	__vals *a = malloc(sizeof(__vals));
	*a = (__vals){ .size = 0, .p = 0 };
	return a;
}
val *__vals_at(__vals *a, int ix) {
	if (a->size <= ix) {
		a->size = ix + 1;
		int n = a->size * sizeof(val);
		a->p = a->p ? realloc(a->p, n) : malloc(n);
	}
	val *re = &a->p[ix];
	return re;
}

int __vals_find(__vals *a, val b) {
	for (int i = 0; i < a->size; i++) if (*__vals_at(a, i) == b) return i;
	return -1;
}

void __vals_free(__vals *a) {
	if (a) {
		if (a->p) free(a->p);
		free(a);
	}
}


val __fcolor(int n, val text) {
	if (!__is_nil(text)) {
		return __concat(__fcolor(n, __nil), text, __fcolor(-1, __nil));
	}
	val esc = __str("\x1b");
	return n == -1
		? __concat(esc, __str("(B"), esc, __str("[m"))
		: __concat(esc, __str("[38;5;"), n, __str("m"));
}

val __bcolor(int n, val text) {
	if (!__is_nil(text)) {
		return __concat(__bcolor(n, __nil), text, __bcolor(-1, __nil));
	}
	val esc = __str("\x1b");
	return n == -1
		? __concat(esc, __str("[49m"))
		: __concat(esc, __str("[48;5;"), n, __str("m"));
}

val __is_color_term(int n, val text) {
	return isatty(fileno(stdout)) ? __true : __false;
}

val __color_term(int n, val text) {
	return isatty(fileno(stdout)) ? __fcolor(n, text) : text;
}

__vals *recursed = 0;

void __print(val a, int level) {
	__enter();
	if (!recursed) recursed = __vals_new();
	for (int i = 0; i < recursed->size; i++) {
		if (*__vals_at(recursed, i) == a) {
			printf("...");
			__leave();
			return;
		}
	}
	if (__is_int(a)) printf("%'i", __get_i(a));
	else if (__is_nil(a)) {
		printf("nil");
	}
	else if (__is_big(a)) {
		printf("%'G", __float(a));
	}
	else if (__leaf(a)->type == leaf_arr) {
		__size_t size = __array_size(a);
		printf("[");
		if (size > 0) {
			*__vals_at(recursed, recursed->size) = a;
			val *p = __array_at(a, 0);
			for (int i = 0; i < size; i++) {
				if (i == 200) {
					printf("... %i more", size-200);
					break;
				}
				if (i > 0) printf(", ");
				__print(p[i], level + 1);
			}
			recursed->size--;
		}
		printf("]");
	}
	else if (__leaf(a)->type == leaf_str) {
		char *u = __pchar_int(a);
		if (u) {
			if (level > 0) printf("'%s'", u); else printf("%s", u);
			Free(u);
			//printf("Q:%p\n", u);
		}
	}
	else if (__leaf(a)->type == leaf_fun) {
		printf("fun");
		//leaf *l = __leaf(a);
		//printf("fun %i [", (int)l->size);
		//for (int i; i < l->size; i++) {
			//__print(l->env->p[i], level+1);
		//}
		//printf("]");
	}
	else if (__leaf(a)->type == leaf_obj) {
		val ar = __object_serialize(a);
		printf("{ ");
		int i = 0;
		while(i < __len(ar)) {
			__loop_mark();
			if (i > 0) printf(", ");
			char *u = __pchar(__array_get(ar, i++));
			printf("%s", u);
			val v = __array_get(ar, i++);
			printf(": ");
			__print(v, level + 1);
			__loop_release();
		}
		printf(" }");
	}
	else printf("[log unhandled type]");
	__leave();
}

void __cout_n(int count, ...) {
	va_list v;
	va_start(v, count);
	for (int i = 0; i < count; i++) {
		val a = va_arg(v, val);
		__print(a, 0);
	}
	va_end(v);
}

void __log_n(int count, ...) {
	__enter();
	va_list v;
	va_start(v, count);
	char color_cmd = 0;
	for (int i = 0; i < count; i++) {
		val a = va_arg(v, val);
		if (__is_string(a) && __len(a) > 0 && __charcode_at(a, 0) == 27) {
			color_cmd = true; // this is hacky solution, maybe solve this on parser level by attaching color cmds to the following expressions
		}
		else {
			if (color_cmd && i == 1);
			else if (i > 0) printf(" ");
			color_cmd = false;
		}
		if (__is_array(a) || __is_object(a)) {
			printf("%s", __pchar(__shame(a, __nil)));
		}
		else if (__is_rec(a)) {
			printf("%s", __pchar(__shame(a, __nil)));
			//printf("%s", __pchar(__shame(__rec_to_object(a), __nil)));
		}
		else {
			//if (__is_num(a))  a = __fcolor(3, a); // does not live well with fcolor and ~color
			//else if (__is_string(a))  a = __fcolor(1, a);
			__print(a, 0);
		}
	}
	va_end(v);
	printf("\r\n");//\r added because of terminal.i
	__leave();
}

FILE *__log_file = NULL;

void ship_printf(char *c, ...) {
	if (__log_file == NULL) __log_file = stdout;
	va_list args;
	va_start(args, c);
	vfprintf(__log_file, c, args);
	va_end(args);
}

//char* __p_color(int a) {
	//if (a == -1) { return "\x1b(B\x1b[m"; }
	//static char s[30];
	//sprintf(s, "\x1b[38;5;%im", a);
	//return s;
//}


//bg = function bg (a) {
//	if (a == undefined) { return '\x1b[49m' }
//	return '\x1b[48;5;'+a+'m'
//}

